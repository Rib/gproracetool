#-------------------------------------------------
#
# Project created by QtCreator 2016-02-08T11:41:51
#
#-------------------------------------------------

QT       += core gui
QT       += network
QT       += sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GproRaceTool
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++11 -DTHREADSAFE=1

LIBS += -lmlpack
LIBS += -lsqlite3
#INCLUDEPATH += /usr/local/lib/
#LIBS += -L/home/tasogare/Libraries/Torch/install/lib/lua/5.1 -llua5.2
#LIBS += -L/home/tasogare/Libraries/Torch/install/lib/ -lqtlua
#LIBS += -L/home/tasogare/Libraries/Torch/install/lib/ -lluajit
#INCLUDEPATH += /home/tasogare/Libraries/Torch/install/include/qtlua/
#INCLUDEPATH += /home/tasogare/Libraries/Torch/install/include/

SOURCES += main.cpp\
        mainwindow.cpp \
    UI/practicecalculation.cpp \
    mainmenusignalhandler.cpp \
    UI/driverinfo.cpp \
    practicetablemodel.cpp \
    practicetablecell.cpp \
    gproserverinterface.cpp \
    Objects/weather.cpp \
    Handlers/gprocookiemanager.cpp \
    Handlers/databasehandler.cpp \
    UI/databasesettingsdialog.cpp \
    Objects/tyretype.cpp \
    Objects/track.cpp \
    Algorithms/practicealgorithms.cpp \
    Objects/driver.cpp \
    UI/racedatawidget.cpp \
    UI/racecalculator.cpp \
    Objects/car.cpp \
    Algorithms/stintalgorithms.cpp \
    Misc/gproresourcemanager.cpp \
    UI/racedownloader.cpp \
    Objects/race.cpp \
    Objects/practicelap.cpp \
    Algorithms/parsealgorithms.cpp \
    Objects/risk.cpp \
    Objects/lap.cpp \
    Objects/stint.cpp \
    Misc/databasestrings.cpp \
    Algorithms/racealgorithms.cpp \
    UI/gprologindialog.cpp \
    Algorithms/simplecrypt.cpp \
    UI/seasonplanner.cpp \
    Models/seasonlisttablemodel.cpp \
    UI/trackviewwidget.cpp \
    UI/driverviewwidget.cpp \
    UI/carviewwidget.cpp \
    Components/customintlineedit.cpp \
    UI/raceslist.cpp \
    Misc/defaultstyles.cpp \
    Components/customqlabel.cpp \
    Objects/finance.cpp \
    Database/databasetablecreatehelper.cpp \
    constantstrings.cpp \
    Database/databasequeryhelper.cpp \
    Misc/constantcalculations.cpp \
    Utility/transformations.cpp \
    Misc/threadresourcemanager.cpp

HEADERS  += mainwindow.h \
    UI/practicecalculation.h \
    mainmenusignalhandler.h \
    UI/driverinfo.h \
    practicetablemodel.h \
    practicetablecell.h \
    gproserverinterface.h \
    Objects/weather.h \
    Handlers/gprocookiemanager.h \
    Handlers/databasehandler.h \
    settingconstants.h \
    UI/databasesettingsdialog.h \
    Objects/tyretype.h \
    Objects/track.h \
    Algorithms/practicealgorithms.h \
    Objects/driver.h \
    UI/racedatawidget.h \
    UI/racecalculator.h \
    Objects/car.h \
    Algorithms/stintalgorithms.h \
    Misc/gproresourcemanager.h \
    UI/racedownloader.h \
    Objects/race.h \
    Objects/practicelap.h \
    Algorithms/parsealgorithms.h \
    Objects/risk.h \
    Objects/lap.h \
    Objects/stint.h \
    Misc/databasestrings.h \
    Algorithms/racealgorithms.h \
    UI/gprologindialog.h \
    Algorithms/simplecrypt.h \
    UI/seasonplanner.h \
    Models/seasonlisttablemodel.h \
    UI/trackviewwidget.h \
    UI/driverviewwidget.h \
    UI/carviewwidget.h \
    Components/customintlineedit.h \
    UI/raceslist.h \
    Misc/defaultstyles.h \
    Components/customqlabel.h \
    Objects/finance.h \
    Database/databasetablecreatehelper.h \
    constantstrings.h \
    Database/databasequeryhelper.h \
    Misc/constantcalculations.h \
    Utility/transformations.h \
    Misc/threadresourcemanager.h

FORMS    += mainwindow.ui \
    UI/practicecalculation.ui \
    UI/driverinfo.ui \
    practicetablecell.ui \
    UI/databasesettingsdialog.ui \
    UI/racedatawidget.ui \
    UI/racecalculator.ui \
    UI/racedownloader.ui \
    UI/gprologindialog.ui \
    UI/seasonplanner.ui \
    UI/trackviewwidget.ui \
    UI/driverviewwidget.ui \
    UI/carviewwidget.ui \
    UI/raceslist.ui
