#ifndef PRACTICETABLECELL_H
#define PRACTICETABLECELL_H

#include <QWidget>

namespace Ui {
class practiceTableCell;
}

class practiceTableCell : public QWidget
{
    Q_OBJECT

public:
    explicit practiceTableCell(QWidget *parent = 0);
    ~practiceTableCell();

    void setValues(const QVariant &setting, const QVariant &comment, const QVariant &range);

private:
    Ui::practiceTableCell *ui;
};

#endif // PRACTICETABLECELL_H
