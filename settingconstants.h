#ifndef SETTINGCONSTANTS
#define SETTINGCONSTANTS

#include <array>

#include <QString>

namespace Settings {
    const QString WindowHeight = QStringLiteral("window/height");
    const QString WindowWidth = QStringLiteral("window/width");

    const QString DatabaseTypeText = "database/type";
    const QString DatabaseNameText = "database/name";
    const QString DatabaseUserNameText =  "database/username";
    const QString DatabasePasswordText = "database/password";

    const QString DriverNameText = "driver/name";
    const QString DriverOverallText = "driver/overall";
    const QString DriverConcentrationText = "driver/concentration";
    const QString DriverTalentText = "driver/talent";
    const QString DriverAggressivenessText = "driver/aggressiveness";
    const QString DriverExperienceText = "driver/experience";
    const QString DriverTechnicalInsightText = "driver/technical_insight";
    const QString DriverStaminaText = "driver/stamina";
    const QString DriverCharismaText = "driver/charisma";
    const QString DriverMotivationText = "driver/motivation";
    const QString DriverWeightText = "driver/weight";
    const QString DriverAgeText = "driver/age";

    const QString CarPowerText = "car/power";
    const QString CarHandlingText = "car/handling";
    const QString CarAccelerationText = "car/acceleration";

    const QString CarChassisLvlText = "car/lvl/chassis";
    const QString CarChassisWearText = "car/wear/chassis";
    const QString CarEngineLvlText = "car/lvl/engine";
    const QString CarEngineWearText = "car/wear/engine";
    const QString CarFrontWingLvlText = "car/lvl/front_wing";
    const QString CarFrontWingWearText = "car/wear/front_wing";
    const QString CarRearWingLvlText = "car/lvl/rear_wing";
    const QString CarRearWingWearText = "car/wear/rear_wing";
    const QString CarUnderbodyLvlText = "car/lvl/underbody";
    const QString CarUnderbodyWearText = "car/wear/underbody";
    const QString CarSidepodsLvlText = "car/lvl/sidepods";
    const QString CarSidepodsWearText = "car/wear/sidepods";
    const QString CarCoolingLvlText = "car/lvl/cooling";
    const QString CarCoolingWearText = "car/wear/cooling";
    const QString CarGearboxLvlText = "car/lvl/gearbox";
    const QString CarGearboxWearText = "car/wear/gearbox";
    const QString CarBrakesLvlText = "car/lvl/brakes";
    const QString CarBrakesWearText = "car/wear/brakes";
    const QString CarSuspensionLvlText = "car/lvl/suspension";
    const QString CarSuspensionWearText = "car/wear/suspension";
    const QString CarElectronicsLvlText = "car/lvl/electronics";
    const QString CarElectronicsWearText = "car/wear/electronics";

    const QString WeatherQ1Temperature = "weather/q1/temperature";
    const QString WeatherQ1Humidity = "weather/q1/humidity";
    const QString WeatherQ2Temperature = "weather/q2/temperature";
    const QString WeatherQ2Humidity = "weather/q2/humidity";

    const QString WeatherRaceQ1TemperatureLowText = "weather/race/q1/temperature_low";
    const QString WeatherRaceQ1TemperatureHighText = "weather/race/q1/temperature_high";
    const QString WeatherRaceQ1HumidityLowText = "weather/race/q1/humidity_low";
    const QString WeatherRaceQ1HumidityHighText = "weather/race/q1/humidity_high";
    const QString WeatherRaceQ1RProbabilityLowText = "weather/race/q1/rain_probability_low";
    const QString WeatherRaceQ1RProbabilityHighText = "weather/race/q1/rain_probability_high";

    const QString WeatherRaceQ2TemperatureLowText = "weather/race/q2/temperature_low";
    const QString WeatherRaceQ2TemperatureHighText = "weather/race/q2/temperature_high";
    const QString WeatherRaceQ2HumidityLowText = "weather/race/q2/humidity_low";
    const QString WeatherRaceQ2HumidityHighText = "weather/race/q2/humidity_high";
    const QString WeatherRaceQ2RProbabilityLowText = "weather/race/q2/rain_probability_low";
    const QString WeatherRaceQ2RProbabilityHighText = "weather/race/q2/rain_probability_high";

    const QString WeatherRaceQ3TemperatureLowText = "weather/race/q3/temperature_low";
    const QString WeatherRaceQ3TemperatureHighText = "weather/race/q3/temperature_high";
    const QString WeatherRaceQ3HumidityLowText = "weather/race/q3/humidity_low";
    const QString WeatherRaceQ3HumidityHighText = "weather/race/q3/humidity_high";
    const QString WeatherRaceQ3RProbabilityLowText = "weather/race/q3/rain_probability_low";
    const QString WeatherRaceQ3RProbabilityHighText = "weather/race/q3/rain_probability_high";

    const QString WeatherRaceQ4TemperatureLowText = "weather/race/q4/temperature_low";
    const QString WeatherRaceQ4TemperatureHighText = "weather/race/q4/temperature_high";
    const QString WeatherRaceQ4HumidityLowText = "weather/race/q4/humidity_low";
    const QString WeatherRaceQ4HumidityHighText = "weather/race/q4/humidity_high";
    const QString WeatherRaceQ4RProbabilityLowText = "weather/race/q4/rain_probability_low";
    const QString WeatherRaceQ4RProbabilityHighText = "weather/race/q4/rain_probability_high";

    const QString TrackNameText = "track/name";

    const QString SafeLoginUsernameText = "login/safe/username";
    const QString SafeLoginPasswordText = "login/safe/password";
    const QString LoginUsernameText = "login/username";
    const QString LoginPasswordText = "login/password";

    const QString PCSuggestedSettingsText = "practice_calculator/suggested_settings";
    const QString PCTableModel = "practice_calculator/table_model";

    const QString SPTracksText = "season_planner/tracks";
    const QString SPDriversText = "season_planner/drivers";
    const QString SPCarText = "season_planner/cars";
    const QString SPClearTrackText = "season_planner/clear_track";
    const QString SPBudget = "season_planner/budget";
    const QString SPTestTrack = QStringLiteral("season_planner/test_track");

    const QString RDOldRacesData = QStringLiteral("race_downloader/old_races_data");
}

namespace General {
    const QString ProgramName = "GproRaceCalculator";
    const QString CompanyName = "Tasogare Soft";
}

#endif // SETTINGCONSTANTS

