#include "practicetablecell.h"
#include "ui_practicetablecell.h"

practiceTableCell::practiceTableCell(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::practiceTableCell)
{
    ui->setupUi(this);
}

practiceTableCell::~practiceTableCell()
{
    delete ui;
}

void practiceTableCell::setValues(const QVariant &setting, const QVariant &comment,const QVariant &range)
{
    ui->setting_text->setText(setting.toString());
    ui->range_text->setText(range.toString());
    ui->comment_text->setText(comment.toString());
}
