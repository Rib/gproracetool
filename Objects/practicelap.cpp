#include "practicelap.h"

#include <QDebug>

#include "Utility/transformations.h"

QStringList PracticeLap::full_practice_lap_data_labels_ = QStringList();

PracticeLap::PracticeLap():
    lap_time_(0),
    dmistake_(0),
    net_time_(0),
    fwing_(0),
    rwing_(0),
    engine_(0),
    brakes_(0),
    gear_(0),
    suspension_(0),
    tyre_("Extra Soft"),
    wing_comment_(""),
    engine_comment_(""),
    brakes_comment_(""),
    gear_comment_(""),
    suspension_comment_("")
{

}

shared_ptr<PracticeLap> PracticeLap::fromDatabaseStringList(const QStringList &raw_practice_lap)
{
    shared_ptr<PracticeLap> practice_lap = shared_ptr<PracticeLap>(new PracticeLap());

    if(raw_practice_lap.size() != 19) return practice_lap;

    practice_lap->setLapTime(Transformations::transformTimeToDouble(raw_practice_lap.at(1)));
    practice_lap->setDriverMistake(raw_practice_lap.at(2).toDouble());
    practice_lap->setNetTime(Transformations::transformTimeToDouble(raw_practice_lap.at(3)));
    practice_lap->setFrontWing(raw_practice_lap.at(4).toInt());
    practice_lap->setRearWing(raw_practice_lap.at(5).toInt());
    practice_lap->setEngine(raw_practice_lap.at(6).toInt());
    practice_lap->setBrakes(raw_practice_lap.at(7).toInt());
    practice_lap->setGear(raw_practice_lap.at(8).toInt());
    practice_lap->setSuspension(raw_practice_lap.at(9).toInt());
    practice_lap->setTyre(raw_practice_lap.at(10));
    practice_lap->setWingComment(raw_practice_lap.at(11));
    practice_lap->setEngineComment(raw_practice_lap.at(12));
    practice_lap->setBrakesComment(raw_practice_lap.at(13));
    practice_lap->setGearComment(raw_practice_lap.at(14));
    practice_lap->setSuspensionComment(raw_practice_lap.at(15));
    practice_lap->setTrackID(raw_practice_lap.at(16).toInt());
    practice_lap->setSeason(raw_practice_lap.at(17).toInt());
    practice_lap->setID(raw_practice_lap.at(18).toInt());

    return practice_lap;
}
