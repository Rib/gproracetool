#include "track.h"

Track::Track() :
    name_(QStringLiteral("")),
    id_(0),
    distance_(0),
    power_(0),
    handling_(0),
    acceleration_(0),
    downforce_(DOWNFORCE::DMEDIUM),
    overtaking_(OVERTAKING::ONORMAL),
    suspension_(SUSPENSION::SMEDIUM),
    fuel_consumption_(FUEL_CONSUMPTION::FCMEDIUM),
    tyre_wear_(TYRE_WEAR::TWMEDIUM),
    avg_speed_(0),
    lap_length_(0),
    corners_(0),
    grip_(GRIP::GMEDIUM),
    pit_stop_(0),
    laps_(0)
{

}

Track::Track(QStringList fields) :
    Track::Track()
{
    //TrackName, TrackID, TDistance, TPower, THandling, TAcceleration, TDownforce, TOvertaking,
    //TSuspension, TFuelConsumption, TTyreWear, TAvgSpeed, TLapLenght, TCorners, TGrip, TPitStop

    setName(fields.at(0));
    setID(fields.at(1).toInt());
    setDistance(fields.at(2).toDouble());
    setPower(fields.at(3).toInt());
    setHandling(fields.at(4).toInt());
    setAcceleration(fields.at(5).toInt());
    setDownforce(Downforce::toDownforce(fields.at(6)));
    setOvertaking(Overtaking::toOvertaking(fields.at(7)));
    setSuspension(Suspension::toSuspension(fields.at(8)));
    setFuelConsumption(FuelConsumption::toFuelConsumption(fields.at(9)));
    setTyreWear(TyreWear::toTyreWear(fields.at(10)));
    setAvgSpeed(fields.at(11).toDouble());
    setLapLength(fields.at(12).toDouble());
    setCorners(fields.at(13).toInt());
    setGrip(Grip::toGrip(fields.at(14)));
    setPitStop(fields.at(15).toDouble());
}

const QStringList &Track::toStringList()
{
    if(string_list_.size() == 0) {
        string_list_.append(getName());
        string_list_.append(QString::number(getID()));
        string_list_.append(QString::number(getDistance()));
        string_list_.append(QString::number(getPower()));
        string_list_.append(QString::number(getHandling()));
        string_list_.append(QString::number(getAcceleration()));
        string_list_.append(Downforce::toString(getDownforce()));
        string_list_.append(Overtaking::toString(getOvertaking()));
        string_list_.append(Suspension::toString(getSuspension()));
        string_list_.append(FuelConsumption::toString(getFuelConsumption()));
        string_list_.append(TyreWear::toString(getTyreWear()));
        string_list_.append(QString::number(getAvgSpeed()));
        string_list_.append(QString::number(getLapLength()));
        string_list_.append(QString::number(getCorners()));
        string_list_.append(Grip::toString(getGrip()));
        string_list_.append(QString::number(getPitStop()));
    }

    return string_list_;
}

