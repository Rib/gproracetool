#ifndef FINANCE_H
#define FINANCE_H


class Finance
{
private:
    int staff_salary_;
    int facility_maintenance_;
    int driver_salary_;
    int tyre_cost_;
    int sponsor_money_;
    int est_car_cost_;

public:
    Finance();

    int getStaffSalary() { return staff_salary_; }
    int getFacilityMaintenence() { return facility_maintenance_; }
    int getDriverSalary() { return driver_salary_; }
    int getTyreCost() { return tyre_cost_; }
    int getSponsorMoney() { return sponsor_money_; }
    int getEstCarCost() { return est_car_cost_; }

    int getTotalIncome() {
        return - staff_salary_ - facility_maintenance_ - driver_salary_ - tyre_cost_ + sponsor_money_ - est_car_cost_;
    }

    void setStaffSalary(int salary) { staff_salary_ = salary; }
    void setFacilityMaintenence(int maintenance) { facility_maintenance_ = maintenance; }
    void setDriverSalary(int salary) { driver_salary_ = salary; }
    void setTyreCost(int cost) { tyre_cost_ = cost; }
    void setSponsorMoney(int sponsor) { sponsor_money_ = sponsor; }
    void setEstCarCost(int cost) { est_car_cost_ = cost; }
};

#endif // FINANCE_H
