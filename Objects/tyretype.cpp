#include "tyretype.h"
#include <QDebug>


TyreType::TyreType():
    type_(TYPES::TTEXTRA_SOFT)
{

}

TyreType::TyreType(QString tyre_type)
{
    if(tyre_type.contains(QString("Extra Soft"))) type_ = TYPES::TTEXTRA_SOFT;
    else if(tyre_type.contains(QString("Soft"))) type_ = TYPES::TTSOFT;
    else if(tyre_type.contains(QString("Medium"))) type_ = TYPES::TTMEDIUM;
    else if(tyre_type.contains(QString("Hard") )) type_ = TYPES::TTHARD;
    else if(tyre_type.contains(QString("Rain"))) type_ = TYPES::TTRAIN;
    else type_ = TYPES::EMPTY;
}

QString TyreType::toString()
{
    if(type_ == TYPES::TTEXTRA_SOFT) return QString("Extra Soft");
    else if(type_ == TYPES::TTSOFT) return QString("Soft");
    else if(type_ == TYPES::TTMEDIUM) return QString("Medium");
    else if(type_ == TYPES::TTHARD) return QString("Hard");
    else if(type_ == TYPES::TTRAIN) return QString("Rain");
    else return QString("");
}

