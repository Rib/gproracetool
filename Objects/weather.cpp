#include "weather.h"

Weather::Weather():
    q1_status_(Status::Dry),
    q1_temperature_(0),
    q1_humidity_(0),
    q2_status_(Status::Dry),
    q2_temperature_(0),
    q2_humidity_(0),
    track_id_(0),
    season_(0),
    id_(0)
{
    race_weather_.fill(0);
}

double Weather::getEstRaceTemperature()
{
    std::array<double,5> race_temperature = {{ (double)getQ2Temperature(),0,0,0,0}};
    race_temperature.at(1) = (double)(getRaceQ1TemperatureHigh() +
                              getRaceQ1TemperatureLow())/2;
    race_temperature.at(2) = (double)(getRaceQ2TemperatureHigh() +
                                       getRaceQ2TemperatureLow())/2;
    race_temperature.at(3) = (double)(getRaceQ3TemperatureHigh() +
                                      getRaceQ3TemperatureLow())/2;
    race_temperature.at(4) = (double)(getRaceQ4TemperatureHigh() +
                                      getRaceQ4TemperatureLow())/2;

    double race_temperature_final = std::accumulate(race_temperature.begin(), race_temperature.end(), 0.0) /
            race_temperature.size();

    return race_temperature_final;
}

double Weather::getEstRaceHumidity()
{
    std::array<double, 5> race_humidity = {{ (double)getQ2Humidity(),0,0,0,0 }};
    race_humidity.at(1) = (double)(getRaceQ1HumidityHigh() +
                              getRaceQ1HumidityLow())/2;
    race_humidity.at(2) = (double)(getRaceQ2HumidityHigh() +
                                   getRaceQ2HumidityLow())/2;
    race_humidity.at(3) = (double)(getRaceQ3HumidityHigh() +
                                   getRaceQ3HumidityLow())/2;
    race_humidity.at(4) = (double)(getRaceQ4HumidityHigh() +
                                   getRaceQ4HumidityLow())/2;

    double race_humidity_final = std::accumulate(race_humidity.begin(), race_humidity.end(), 0.0) /
            race_humidity.size();

    return race_humidity_final;
}

