#ifndef RACE_H
#define RACE_H

#include <array>

#include <QString>
#include <QList>

#include "Objects/track.h"
#include "Objects/practicelap.h"
#include "Objects/weather.h"
#include "Objects/car.h"
#include "Objects/driver.h"
#include "Objects/risk.h"
#include "Objects/lap.h"
#include "Objects/stint.h"

using std::array;

class Race
{
    int season_;
    int race_;
    Track track_;
    double q1_laptime_;
    double q2_laptime_;
    QList<PracticeLap> practice_laps_;
    Weather weather_;
    Car car_;
    Driver driver_;

    Risk risk_;

    QList<Lap> laps_;

    QList<Stint> stints_;

public:
    explicit Race();

    // Setters
    void setSeason( int season ) { season_ = season; }
    void setRace( int race ) { race_ = race; }
    void setTrack( Track track ) { track_ = track; }
    void setQ1LapTime( double laptime ) { q1_laptime_ = laptime; }
    void setQ2LapTime( double laptime ) { q2_laptime_ = laptime; }
    void addPracticeLap( PracticeLap lap ) { practice_laps_.append(lap); }
    void setWeather( Weather weather ) { weather_ = weather; }
    void setCar( Car car ) { car_ = car; }
    void setDriver( Driver driver ) { driver_ = driver; }
    void setRisk( Risk risk ) { risk_ = risk; }
    void addLap( Lap lap ) { laps_.append(lap); }
    void addStint( Stint stint ) { stints_.append(stint); }

    void clearStints() { stints_.clear(); }

    // Getters
    int getSeason() { return season_; }
    int getRace() { return race_; }
    Track getTrack() { return track_; }
    double getQ1LapTime() { return q1_laptime_; }
    double getQ2LapTime() { return q2_laptime_; }
    QList<PracticeLap> getPracticeLaps() { return practice_laps_; }
    Weather getWeather() { return weather_; }
    Car getCar() { return car_; }
    Driver getDriver() { return driver_; }
    Risk getRisk() { return risk_; }
    QList<Lap> getLaps() { return laps_; }
    QList<Stint> getStints() { return stints_; }

};

#endif // RACE_H
