#ifndef WEATHER_H
#define WEATHER_H

#include <QString>

#include <array>

using std::array;


class Weather
{
public: enum Status {Dry=0,Wet,Mixed};

private:
    Status q1_status_;
    unsigned int q1_temperature_;
    unsigned int q1_humidity_;

    Status q2_status_;
    unsigned int q2_temperature_;
    unsigned int q2_humidity_;

    array<unsigned int, 24> race_weather_;

    int track_id_;
    int season_;
    int id_;

public:
    Weather();

    static double toDouble(Status status) { return static_cast<double>(status); }
    static double toDouble(QString status) {
        if( QString("Wet") == status || QString("Rain") == status) return toDouble(Status::Wet);
        else if( QString("Mixed") == status) return toDouble(Status::Mixed);
        return toDouble(Status::Dry);
    }

    static QString toString(Status status) {
        if(status == Dry) return QString("Dry");
        else if(status == Wet) return QString("Wet");
        else if(status == Mixed) return QString("Mixed");
        return QString("Dry");
    }

    static Status toStatus(QString status) {
        if( QString("Wet") == status || QString("Rain") == status) return Status::Wet;
        else if( QString("Mixed") == status) return Status::Mixed;
        return Status::Dry;
    }

    inline void setQ1Status( Status status ) { q1_status_ = status; }
    inline void setQ1Temperature( unsigned int temperature ) {
        q1_temperature_ = temperature > 50 ? 50 : temperature;
    }
    inline void setQ1Humidity( unsigned int humidity ) {
        q1_humidity_ = humidity > 99 ? 99 : humidity;
    }

    inline void setQ2Status( Status status ) { q2_status_ = status; }
    inline void setQ2Temperature( unsigned int temperature ) {
        q2_temperature_ = temperature > 50 ? 50 : temperature;
    }
    inline void setQ2Humidity( unsigned int humidity ) {
        q2_humidity_ = humidity > 99 ? 99 : humidity;
    }

    inline void setRaceQ1Temperature(unsigned int low, unsigned int high) {
        race_weather_.at(0) = low > 50 ? 50 : low;
        race_weather_.at(1) = high > 50 ? 50 : high;
    }

    inline void setRaceQ2Temperature(unsigned int low, unsigned int high) {
        race_weather_.at(2) = low > 50 ? 50 : low;
        race_weather_.at(3) = high > 50 ? 50 : high;
    }

    inline void setRaceQ3Temperature( unsigned int low, unsigned int high ) {
        race_weather_.at(4) = low > 50 ? 50 : low;
        race_weather_.at(5) = high > 50 ? 50 : high;
    }

    inline void setRaceQ4Temperature( unsigned int low, unsigned int high ) {
        race_weather_.at(6) = low > 50 ? 50 : low;
        race_weather_.at(7) = high > 50 ? 50 : high;
    }

    inline void setRaceQ1Humidity( unsigned int low, unsigned int high ) {
        race_weather_.at(8) = low > 99 ? 99 : low;
        race_weather_.at(9) = high > 99 ? 99 : high;
    }

    inline void setRaceQ2Humidity( unsigned int low, unsigned int high ) {
        race_weather_.at(10) = low > 99 ? 99 : low;
        race_weather_.at(11) = high > 99 ? 99 : high;
    }

    inline void setRaceQ3Humidity( unsigned int low, unsigned int high ) {
        race_weather_.at(12) = low > 99 ? 99 : low;
        race_weather_.at(13) = high > 99 ? 99 : high;
    }

    inline void setRaceQ4Humidity( unsigned int low, unsigned int high ) {
        race_weather_.at(14) = low > 99 ? 99 : low;
        race_weather_.at(15) = high > 99 ? 99 : high;
    }

    inline void setRaceQ1RainProbability( unsigned int low, unsigned int high ) {
        race_weather_.at(16) = low > 100 ? 100 : low;
        race_weather_.at(17) = high > 100 ? 100 : high;
    }

    inline void setRaceQ2RainProbability( unsigned int low, unsigned int high ) {
        race_weather_.at(18) = low > 100 ? 100 : low;
        race_weather_.at(19) = high > 100 ? 100 : high;
    }

    inline void setRaceQ3RainProbability( unsigned int low, unsigned int high ) {
        race_weather_.at(20) = low > 100 ? 100 : low;
        race_weather_.at(21) = high > 100 ? 100 : high;
    }

    inline void setRaceQ4RainProbability( unsigned int low, unsigned int high ) {
        race_weather_.at(22) = low > 100 ? 100 : low;
        race_weather_.at(23) = high > 100 ? 100 : high;
    }

    void setTrackID( int track_id ) { track_id_ = track_id; }
    void setSeason( int season ) { season_ = season; }
    void setID( int id ) { id_ = id; }

    Status getQ1Status() const { return q1_status_; }
    unsigned int getQ1Temperature() const { return q1_temperature_; }
    unsigned int getQ1Humidity() const { return q1_humidity_; }

    Status getQ2Status() const { return q2_status_; }
    unsigned int getQ2Temperature() const { return q2_temperature_; }
    unsigned int getQ2Humidity() const { return q2_humidity_; }

    unsigned int getRaceQ1TemperatureLow() const { return race_weather_.at(0); }
    unsigned int getRaceQ1TemperatureHigh() const { return race_weather_.at(1); }
    unsigned int getRaceQ2TemperatureLow() const { return race_weather_.at(2); }
    unsigned int getRaceQ2TemperatureHigh() const { return race_weather_.at(3); }
    unsigned int getRaceQ3TemperatureLow() const { return race_weather_.at(4); }
    unsigned int getRaceQ3TemperatureHigh() const { return race_weather_.at(5); }
    unsigned int getRaceQ4TemperatureLow() const { return race_weather_.at(6); }
    unsigned int getRaceQ4TemperatureHigh() const { return race_weather_.at(7); }

    unsigned int getRaceQ1HumidityLow() const { return race_weather_.at(8); }
    unsigned int getRaceQ1HumidityHigh() const { return race_weather_.at(9); }
    unsigned int getRaceQ2HumidityLow() const { return race_weather_.at(10); }
    unsigned int getRaceQ2HumidityHigh() const { return race_weather_.at(11); }
    unsigned int getRaceQ3HumidityLow() const { return race_weather_.at(12); }
    unsigned int getRaceQ3HumidityHigh() const { return race_weather_.at(13); }
    unsigned int getRaceQ4HumidityLow() const { return race_weather_.at(14); }
    unsigned int getRaceQ4HumidityHigh() const { return race_weather_.at(15); }

    unsigned int getRaceQ1RainProbabilityLow() const { return race_weather_.at(16); }
    unsigned int getRaceQ1RainProbabilityHigh() const { return race_weather_.at(17); }
    unsigned int getRaceQ2RainProbabilityLow() const { return race_weather_.at(18); }
    unsigned int getRaceQ2RainProbabilityHigh() const { return race_weather_.at(19); }
    unsigned int getRaceQ3RainProbabilityLow() const { return race_weather_.at(20); }
    unsigned int getRaceQ3RainProbabilityHigh() const { return race_weather_.at(21); }
    unsigned int getRaceQ4RainProbabilityLow() const { return race_weather_.at(22); }
    unsigned int getRaceQ4RainProbabilityHigh() const { return race_weather_.at(23); }

    int getTrackID() const { return track_id_; }
    int getSeason() const { return season_; }
    int getID() const { return id_; }

    double getEstRaceTemperature();
    double getEstRaceHumidity();

};

#endif // WEATHER_H
