#ifndef RISK_H
#define RISK_H

#include <array>

#include <QString>

using std::array;

class Risk
{
private:
    QString starting_risk_;
    array<int,5> risks_;

    int track_id_;
    int season_;

public:
    Risk();

    void setStartingRisk( QString starting_risk ) { starting_risk_ = starting_risk; }
    void setRisk( array<int,5> risks ) { risks_ = risks; }
    void setSeason( int season ) { season_ = season; }
    void setTrackID( int track_id ) { track_id_ = track_id; }

    QString getStartingRisk() { return starting_risk_; }
    // order -> clear, wet, defend, malfunction, overtake
    const array<int,5>& getRisks() { return risks_; }
    int getSeason() { return season_; }
    int getTrackID() { return track_id_; }

};

#endif // RISK_H
