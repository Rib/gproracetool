#ifndef PRACTICELAP_H
#define PRACTICELAP_H

#include <memory>

#include <QString>
#include <QSqlDatabase>
#include <QStringList>

#include <mlpack/core.hpp>

#include "Objects/tyretype.h"
#include "Misc/databasestrings.h"
#include "Handlers/databasehandler.h"
#include "Objects/weather.h"
#include "Objects/track.h"

class databasehandler;

using std::shared_ptr;

using namespace arma;

class PracticeLap
{
private:

    double lap_time_;
    double dmistake_;
    double net_time_;
    int fwing_;
    int rwing_;
    int engine_;
    int brakes_;
    int gear_;
    int suspension_;
    TyreType tyre_;

    QString wing_comment_;
    QString engine_comment_;
    QString brakes_comment_;
    QString gear_comment_;
    QString suspension_comment_;

    int season_;
    int track_id_;
    int id_;

    static QStringList full_practice_lap_data_labels_;

    static vec parseFullPracticeLapDataRow(QSqlRecord record, databasehandler *db_handler);

public:
    PracticeLap();

    void setLapTime( double lap_time ) { lap_time_ = lap_time; }
    void setDriverMistake( double dmistake ) { dmistake_ = dmistake; }
    void setNetTime( double net_time ) { net_time_ = net_time; }
    void setFrontWing( int fwing ) { fwing_ = fwing; }
    void setRearWing( int rwing) { rwing_ = rwing; }
    void setEngine( int engine ) { engine_ = engine; }
    void setBrakes( int brakes ) { brakes_ = brakes; }
    void setGear( int gear ) { gear_ = gear; }
    void setSuspension( int suspension ) { suspension_ = suspension; }
    void setTyre( QString tyre ) { tyre_ = TyreType(tyre); }

    void setWingComment( QString comment ) { wing_comment_ = comment; }
    void setEngineComment( QString comment ) { engine_comment_ = comment; }
    void setBrakesComment( QString comment ) { brakes_comment_ = comment; }
    void setGearComment( QString comment ) { gear_comment_ = comment; }
    void setSuspensionComment( QString comment ) { suspension_comment_ = comment; }

    void setSeason(int season) { season_ = season; }
    void setTrackID(int track_id) { track_id_ = track_id; }
    void setID(int id) { id_ = id; }

    double getLapTime() { return lap_time_; }
    double getDriverMistake() { return dmistake_; }
    double getNetTime() { return net_time_; }
    int getFrontWing() { return fwing_; }
    int getRearWing() { return rwing_; }
    int getEngine() { return engine_; }
    int getBrakes() { return brakes_; }
    int getGear() { return gear_; }
    int getSuspension() { return suspension_; }
    TyreType getTyre() { return tyre_; }

    QString getWingComment() { return wing_comment_; }
    QString getEngineComment() { return engine_comment_; }
    QString getBrakesComment() { return brakes_comment_; }
    QString getGearComment() { return gear_comment_; }
    QString getSuspensionComment() { return suspension_comment_; }

    int getSeason() { return season_; }
    int getTrackID() { return track_id_; }
    int getId() { return id_; }

    static shared_ptr<PracticeLap> fromDatabaseStringList(const QStringList &raw_practice_lap);

};

#endif // PRACTICELAP_H
