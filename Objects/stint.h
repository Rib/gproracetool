#ifndef STINT_H
#define STINT_H

#include <QString>

#include "Objects/tyretype.h"

class Stint
{
private:
    int laps_;
    double fuel_used_;
    double fuel_consumption_;
    int final_p_;
    double avg_temperature_;
    double avg_humidity_;
    TyreType tyre_type_;
    QString weather_;
    double km_;
    int start_lap_;
    double pit_stop_;
    int start_fuel_;

    int season_;
    int track_id_;

public:
    Stint();

    void setLaps( int laps ) { laps_ = laps; }
    void setFuelUsed( double fuel_used ) { fuel_used_ = fuel_used; }
    void setFuelConsumption( double fuel_consumption ) { fuel_consumption_ = fuel_consumption; }
    void setFinalP( int final_p ) { final_p_ = final_p; }
    void setAvgTemperature( double avg_temperature ) { avg_temperature_ = avg_temperature; }
    void setAvgHumidity( double avg_humidity ) { avg_humidity_ = avg_humidity; }
    void setTyreType( QString tyre_type ) { tyre_type_ = TyreType(tyre_type); }
    void setWeather( QString weather ) { weather_ = weather; }
    void setKM( double km ) { km_ = km; }
    void setStartLap( int start_lap ) { start_lap_ = start_lap; }
    void setPitStop( double pit_stop ) { pit_stop_ = pit_stop; }
    void setStartFuel( int start_fuel ) { start_fuel_ = start_fuel; }

    void setSeason( int season ) { season_ = season; }
    void setTrackID( int track_id ) { track_id_ = track_id; }

    int getLaps() { return laps_; }
    double getFuelUsed() { return fuel_used_; }
    double getFuelConsumption() { return fuel_consumption_; }
    int getFinalP() { return final_p_; }
    double getAvgTemperature() { return avg_temperature_; }
    double getAvgHumidity() { return avg_humidity_; }
    TyreType getTyreType() { return tyre_type_; }
    QString getWeather() { return weather_; }
    double getKM() { return km_; }
    int getStartLap() { return start_lap_; }
    double getPitStop() { return pit_stop_; }
    int getStartFuel() { return start_fuel_; }

    int getSeason() { return season_; }
    int getTrackID() { return track_id_; }

};

#endif // STINT_H
