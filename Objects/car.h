#ifndef CAR_H
#define CAR_H

#include <array>
#include <QStringList>

using std::array;

class Car
{
private:
    array<int,3> stats_;
    array<int,11> car_lvls_;
    array<int,11> car_wears_;

    int season_;
    int track_id_;

public:
    Car();
    Car(const QStringList &car_sl);

    void setPower(int power) { stats_.at(0) = power; }
    void setHandling(int handling) { stats_.at(1) = handling; }
    void setAcceleration(int acceleration) { stats_.at(2) = acceleration; }

    void setChassis(int lvl, int wear) { car_lvls_.at(0) = lvl; car_wears_.at(0) = wear; }
    void setEngine(int lvl, int wear) { car_lvls_.at(1) = lvl; car_wears_.at(1) = wear; }
    void setFrontWing(int lvl, int wear) { car_lvls_.at(2) = lvl; car_wears_.at(2) = wear; }
    void setRearWing(int lvl, int wear) { car_lvls_.at(3) = lvl; car_wears_.at(3) = wear; }
    void setUnderbody(int lvl, int wear) { car_lvls_.at(4) = lvl; car_wears_.at(4) = wear; }
    void setSidepods(int lvl, int wear) { car_lvls_.at(5) = lvl; car_wears_.at(5) = wear; }
    void setCooling(int lvl, int wear) { car_lvls_.at(6) = lvl; car_wears_.at(6) = wear; }
    void setGearbox(int lvl, int wear) { car_lvls_.at(7) = lvl; car_wears_.at(7) = wear; }
    void setBrakes(int lvl, int wear) { car_lvls_.at(8) = lvl; car_wears_.at(8) = wear; }
    void setSuspension(int lvl, int wear) { car_lvls_.at(9) = lvl; car_wears_.at(9) = wear; }
    void setElectronics(int lvl, int wear) { car_lvls_.at(10) = lvl; car_wears_.at(10) = wear; }

    void setSeason( int season ) { season_ = season; }
    void setTrackID( int track_id ) { track_id_ = track_id; }

    int getPower() const { return stats_.at(0); }
    int getHandling() const { return stats_.at(1); }
    int getAcceleration() const { return stats_.at(2); }

    int getChassisLvl() const { return car_lvls_.at(0); }
    int getChassisWear() const { return car_wears_.at(0); }
    int getEngineLvl() const { return car_lvls_.at(1); }
    int getEngineWear() const { return car_wears_.at(1); }
    int getFrontWingLvl() const { return car_lvls_.at(2); }
    int getFrontWingWear() const { return car_wears_.at(2); }
    int getRearWingLvl() const { return car_lvls_.at(3); }
    int getRearWingWear() const { return car_wears_.at(3); }
    int getUnderbodyLvl() const { return car_lvls_.at(4); }
    int getUnderbodyWear() const { return car_wears_.at(4); }
    int getSidepodsLvl() const { return car_lvls_.at(5); }
    int getSidepodsWear() const { return car_wears_.at(5); }
    int getCoolingLvl() const { return car_lvls_.at(6); }
    int getCoolingWear() const { return car_wears_.at(6); }
    int getGearboxLvl() const { return car_lvls_.at(7); }
    int getGearboxWear() const { return car_wears_.at(7); }
    int getBrakesLvl() const { return car_lvls_.at(8); }
    int getBrakesWear() const { return car_wears_.at(8); }
    int getSuspensionLvl() const { return car_lvls_.at(9); }
    int getSuspensionWear() const { return car_wears_.at(9); }
    int getElectronicsLvl() const { return car_lvls_.at(10); }
    int getElectronicsWear() const { return car_wears_.at(10); }

    int getSeason() const { return season_; }
    int getTrackID() const { return track_id_; }

    QStringList serialize() const;
};

#endif // CAR_H
