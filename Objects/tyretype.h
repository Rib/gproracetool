#ifndef TYRETYPE_H
#define TYRETYPE_H

#include <QString>

class TyreType
{
public: enum TYPES { TTEXTRA_SOFT = 1, TTSOFT, TTMEDIUM, TTHARD, TTRAIN, EMPTY };

private:
    TYPES type_;
public:
    TyreType();
    TyreType(QString tyre_type);

    double toDouble() { return static_cast<double>(type_); }
    QString toString();
};

#endif // TYRETYPE_H
