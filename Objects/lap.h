#ifndef LAP_H
#define LAP_H

#include <QString>

#include "Objects/tyretype.h"

class Lap
{
private:
    int lap_num_;
    QString lap_time_;
    int pos_;
    TyreType tyre_type_;
    QString weather_;
    int temperature_;
    int humidity_;
    QString events_;

    int season_;
    int track_id_;

public:
    Lap();

    void setLapNum(int lap_num) { lap_num_ = lap_num; }
    void setLapTime(QString lap_time ) { lap_time_ = lap_time; }
    void setPos(int pos) { pos_ = pos; }
    void setTyreType(QString tyre_type) { tyre_type_ = TyreType(tyre_type); }
    void setWeather(QString weather) { weather_ = weather; }
    void setTemperature( int temperature ) { temperature_ = temperature; }
    void setHumidity( int humidity ) { humidity_ = humidity; }
    void setEvents( QString events ) { events_ = events; }
    void setSeason( int season ) { season_ = season; }
    void setTrackID( int track_id ) { track_id_ = track_id; }

    int getLapNum() { return lap_num_; }
    QString getLapTime() { return lap_time_; }
    int getPos() { return pos_; }
    TyreType getTyreType() { return tyre_type_; }
    QString getWeather() { return weather_; }
    int getTemperature() { return temperature_; }
    int getHumidity() { return humidity_; }
    QString getEvents() { return events_; }
    int getSeason() { return season_; }
    int getTrackID() { return track_id_; }

};

#endif // LAP_H
