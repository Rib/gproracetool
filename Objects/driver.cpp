#include "driver.h"

#include <QVariant>

Driver::Driver()
{
    attributes_.fill(-1);
}

Driver::Driver(const QStringList &data)
{
    if(data.size() == 13) {
        name_ = data.at(0);

        for(int i = 1; i < data.size(); ++i) {
            attributes_.at(i-1) = data.at(i).toInt();
        }
    }
}

QStringList Driver::serialize()
{
    QStringList data;

    data.append(name_);

    for(int i = 0; i < attributes_.size(); ++i) {
        data.append(QString::number(attributes_.at(i)));
    }

    return data;
}
