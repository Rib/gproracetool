#ifndef DRIVER_H
#define DRIVER_H

#include <array>

#include <QString>

class Driver
{
private:
    std::array<int, 12> attributes_;

    QString name_;

    int track_id_;
    int season_;

public:
    Driver();
    Driver(const QStringList &data);

    void setOverall(int overall) { attributes_.at(0) = overall; }
    void setConcentration(int concentration) { attributes_.at(1) = concentration; }
    void setTalent(int talent) { attributes_.at(2) = talent; }
    void setAggressiveness(int aggressiveness) { attributes_.at(3) = aggressiveness; }
    void setExperience(int experience) { attributes_.at(4) = experience; }
    void setTechnicalInsight(int tech_insight) { attributes_.at(5) = tech_insight; }
    void setStamina(int stamina) { attributes_.at(6) = stamina; }
    void setCharisma(int charisma) { attributes_.at(7) = charisma; }
    void setMotivation(int motivation) { attributes_.at(8) = motivation; }
    void setReputation(int reputation) { attributes_.at(9) = reputation; }
    void setWeight(int weight) { attributes_.at(10) = weight; }
    void setAge(int age) { attributes_.at(11) = age; }
    void setName(QString name) { name_ = name; }

    void setRawAttribute(unsigned int index, int value) { if( index < attributes_.size() ) attributes_.at(index) = value; }

    void setTrackID(int track_id) { track_id_ = track_id; }
    void setSeason( int season ) { season_ = season; }

    int getOverall() const { return attributes_.at(0); }
    int getConcentration() const { return attributes_.at(1); }
    int getTalent() const { return attributes_.at(2); }
    int getAggressiveness() const { return attributes_.at(3); }
    int getExperience() const { return attributes_.at(4); }
    int getTechnicalInsight() const { return attributes_.at(5); }
    int getStamina() const { return attributes_.at(6); }
    int getCharisma() const { return attributes_.at(7); }
    int getMotivation() const { return attributes_.at(8); }
    int getReputation() const { return attributes_.at(9); }
    int getWeight() const { return attributes_.at(10); }
    int getAge() const { return attributes_.at(11); }
    QString getName() const { return name_; }

    int getTrackID() const { return track_id_; }
    int getSeason() const { return season_; }

    QStringList serialize();
};

#endif // DRIVER_H
