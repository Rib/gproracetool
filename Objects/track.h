#ifndef TRACK_H
#define TRACK_H

#include <QString>
#include <QStringList>


class Track
{

public:
    enum DOWNFORCE { DLOW, DMEDIUM, DHIGH };
    enum OVERTAKING { OVERY_EASY, OEASY, ONORMAL, OHARD, OVERY_HARD };
    enum SUSPENSION { SSOFT, SMEDIUM, SHARD };
    enum FUEL_CONSUMPTION { FCVERY_LOW, FCLOW, FCMEDIUM, FCHIGH, FCVERY_HIGH };
    enum TYRE_WEAR { TWVERY_LOW, TWLOW, TWMEDIUM, TWHIGH, TWVERY_HIGH };
    enum GRIP { GVERY_LOW, GLOW, GMEDIUM, GHIGH, GVERY_HIGH };

    Track();
    Track(QStringList fields);

    void setName(QString name) { name_ = name; }
    void setID(int id) { id_ = id; }
    void setDistance( double distance ) { distance_ = distance; }
    void setPower( unsigned int power ) { power_ = power; }
    void setHandling( unsigned int handling ) { handling_ = handling; }
    void setAcceleration( unsigned int acceleration ) { acceleration_ = acceleration; }
    void setDownforce( DOWNFORCE downforce ) { downforce_ = downforce; }
    void setOvertaking( OVERTAKING overtaking ) { overtaking_ = overtaking; }
    void setSuspension( SUSPENSION suspension ) { suspension_ = suspension; }
    void setFuelConsumption( FUEL_CONSUMPTION fuel_consumption ) { fuel_consumption_ = fuel_consumption; }
    void setTyreWear( TYRE_WEAR tyre_wear ) { tyre_wear_ = tyre_wear; }
    void setAvgSpeed( double avg_speed ) { avg_speed_ = avg_speed; }
    void setLapLength( double lap_length ) { lap_length_ = lap_length; }
    void setCorners( unsigned int corners ) { corners_ = corners; }
    void setGrip( GRIP grip ) { grip_ = grip; }
    void setPitStop( double pit_stop ) { pit_stop_ = pit_stop; }
    void setLaps( int laps) { laps_ = laps; }

    QString getName() const { return name_; }
    int getID() const { return id_; }
    double getDistance() const { return distance_; }
    unsigned int getPower() const { return power_; }
    unsigned int getHandling() const { return handling_; }
    unsigned int getAcceleration() const { return acceleration_; }
    DOWNFORCE getDownforce() const { return downforce_; }
    OVERTAKING getOvertaking() const { return overtaking_; }
    SUSPENSION getSuspension() const { return suspension_; }
    FUEL_CONSUMPTION getFuelConsumption() const { return fuel_consumption_; }
    TYRE_WEAR getTyreWear() const { return tyre_wear_; }
    double getAvgSpeed() const { return avg_speed_; }
    double getLapLength() const { return lap_length_; }
    unsigned int getCorners() const { return corners_; }
    GRIP getGrip() const { return grip_; }
    double getPitStop() const { return pit_stop_; }
    int getLaps() const { return laps_; }

    const QStringList &toStringList();

    class Downforce {
    public:
        Downforce();
        static DOWNFORCE toDownforce(QString downforce) {
            if(downforce == QString("Low")) return DOWNFORCE::DLOW;
            else if( downforce == QString("Medium")) return DOWNFORCE::DMEDIUM;
            else if( downforce == QString("High")) return DOWNFORCE::DHIGH;

            return DOWNFORCE::DLOW;
        }

        static double toDouble(DOWNFORCE downforce) { return static_cast<double>(downforce); }
        static double toDouble(QString downforce) {
            if(downforce == QString("Low")) return toDouble(DOWNFORCE::DLOW);
            else if( downforce == QString("Medium")) return toDouble(DOWNFORCE::DMEDIUM);
            else if( downforce == QString("High")) return toDouble(DOWNFORCE::DHIGH);

            return toDouble(DOWNFORCE::DLOW);
        }

        static QString toString(DOWNFORCE downforce) {
            if(downforce == DOWNFORCE::DLOW) return QString("Low");
            if(downforce == DOWNFORCE::DMEDIUM) return QString("Medium");
            if(downforce == DOWNFORCE::DHIGH) return QString("High");

            return QString("Medium");
        }
    };

    class Overtaking {
    public:
        Overtaking();

        static OVERTAKING toOvertaking( QString overtaking ) {
            if(overtaking == QString("Very easy")) return OVERTAKING::OVERY_EASY;
            else if( overtaking == QString("Easy")) return OVERTAKING::OEASY;
            else if( overtaking == QString("Normal")) return OVERTAKING::ONORMAL;
            else if( overtaking == QString("Hard")) return OVERTAKING::OHARD;
            else if( overtaking == QString("Very hard")) return OVERTAKING::OVERY_HARD;

            return OVERTAKING::OVERY_EASY;
        }

        static double toDouble(OVERTAKING overtaking) { return static_cast<double>(overtaking); }
        static double toDouble(QString overtaking) {
            if(overtaking == QString("Very easy")) return toDouble(OVERTAKING::OVERY_EASY);
            else if( overtaking == QString("Easy")) return toDouble(OVERTAKING::OEASY);
            else if( overtaking == QString("Normal")) return toDouble(OVERTAKING::ONORMAL);
            else if( overtaking == QString("Hard")) return toDouble(OVERTAKING::OHARD);
            else if( overtaking == QString("Very hard")) return toDouble(OVERTAKING::OVERY_HARD);

            return toDouble(OVERTAKING::OVERY_EASY);
        }

        static QString toString(OVERTAKING overtaking) {
            if(overtaking == OVERTAKING::OVERY_EASY) return QString("Very easy");
            if(overtaking == OVERTAKING::OEASY) return QString("Easy");
            if(overtaking == OVERTAKING::ONORMAL) return QString("Normal");
            if(overtaking == OVERTAKING::OHARD) return QString("Hard");
            if(overtaking == OVERTAKING::OVERY_HARD) return QString("Very hard");

            return QString("Normal");
        }
    };

    class Suspension {
    public:
        Suspension();

        static SUSPENSION toSuspension(QString suspension) {
            if( suspension == QString("Soft")) return SUSPENSION::SSOFT;
            else if( suspension == QString("Medium")) return SUSPENSION::SMEDIUM;
            else if( suspension == QString("Hard")) return SUSPENSION::SHARD;

            return SUSPENSION::SSOFT;
        }

        static double toDouble(SUSPENSION suspension) { return static_cast<double>(suspension); }
        static double toDouble(QString suspension) {
            if( suspension == QString("Soft")) return toDouble(SUSPENSION::SSOFT);
            else if( suspension == QString("Medium")) return toDouble(SUSPENSION::SMEDIUM);
            else if( suspension == QString("Hard")) return toDouble(SUSPENSION::SHARD);

            return toDouble(SUSPENSION::SSOFT);
        }

        static QString toString(SUSPENSION suspension) {
            if(suspension == SUSPENSION::SSOFT) return QString("Soft");
            else if(suspension == SUSPENSION::SMEDIUM) return QString("Medium");
            else if(suspension == SUSPENSION::SHARD) return QString("Hard");

            return QString("Medium");
        }
    };

    class FuelConsumption {
    public:
        FuelConsumption();

        static FUEL_CONSUMPTION toFuelConsumption( QString fuel_consumption ) {
            if( fuel_consumption == QString("Very low") ) return FUEL_CONSUMPTION::FCVERY_LOW;
            else if( fuel_consumption == QString("Low") ) return FUEL_CONSUMPTION::FCLOW;
            else if( fuel_consumption == QString("Medium") ) return FUEL_CONSUMPTION::FCMEDIUM;
            else if( fuel_consumption == QString("High") ) return FUEL_CONSUMPTION::FCHIGH;
            else if( fuel_consumption == QString("Very high") ) return FUEL_CONSUMPTION::FCVERY_HIGH;


            return FUEL_CONSUMPTION::FCVERY_LOW;
        }

        static double toDouble(FUEL_CONSUMPTION fuel_consumption) { return static_cast<double>(fuel_consumption); }
        static double toDouble(QString fuel_consumption) {
            if( fuel_consumption == QString("Very low") ) return toDouble(FUEL_CONSUMPTION::FCVERY_LOW);
            else if( fuel_consumption == QString("Low") ) return toDouble(FUEL_CONSUMPTION::FCLOW);
            else if( fuel_consumption == QString("Medium") ) return toDouble(FUEL_CONSUMPTION::FCMEDIUM);
            else if( fuel_consumption == QString("High") ) return toDouble(FUEL_CONSUMPTION::FCHIGH);
            else if( fuel_consumption == QString("Very high") ) return toDouble(FUEL_CONSUMPTION::FCVERY_HIGH);


            return toDouble(FUEL_CONSUMPTION::FCVERY_LOW);
        }

        static QString toString(FUEL_CONSUMPTION fuel_consumption) {
            if(fuel_consumption == FUEL_CONSUMPTION::FCVERY_LOW) return QString("Very low");
            if(fuel_consumption == FUEL_CONSUMPTION::FCLOW) return QString("Low");
            if(fuel_consumption == FUEL_CONSUMPTION::FCMEDIUM) return QString("Medium");
            if(fuel_consumption == FUEL_CONSUMPTION::FCHIGH) return QString("High");
            if(fuel_consumption == FUEL_CONSUMPTION::FCVERY_HIGH) return QString("Very high");

            return QString("Medium");
        }
    };

    class TyreWear {
    public:
        TyreWear();

        static TYRE_WEAR toTyreWear( QString tyre_wear ) {
            if( tyre_wear == QString("Very low") ) return TYRE_WEAR::TWVERY_LOW;
            else if( tyre_wear == QString("Low") ) return TYRE_WEAR::TWLOW;
            else if( tyre_wear == QString("Medium") ) return TYRE_WEAR::TWMEDIUM;
            else if( tyre_wear == QString("High") ) return TYRE_WEAR::TWHIGH;
            else if( tyre_wear == QString("Very high") ) return TYRE_WEAR::TWVERY_HIGH;


            return TYRE_WEAR::TWVERY_LOW;
        }

        static double toDouble(TYRE_WEAR tyre_wear) { return static_cast<double>(tyre_wear); }
        static double toDouble(QString tyre_wear) {
            if( tyre_wear == QString("Very low") ) return toDouble(TYRE_WEAR::TWVERY_LOW);
            else if( tyre_wear == QString("Low") ) return toDouble(TYRE_WEAR::TWLOW);
            else if( tyre_wear == QString("Medium") ) return toDouble(TYRE_WEAR::TWMEDIUM);
            else if( tyre_wear == QString("High") ) return toDouble(TYRE_WEAR::TWHIGH);
            else if( tyre_wear == QString("Very high") ) return toDouble(TYRE_WEAR::TWVERY_HIGH);


            return toDouble(TYRE_WEAR::TWVERY_LOW);
        }

        static QString toString(TYRE_WEAR tyre_wear) {
            if(tyre_wear == TYRE_WEAR::TWVERY_LOW) return QString("Very low");
            if(tyre_wear == TYRE_WEAR::TWLOW) return QString("Low");
            if(tyre_wear == TYRE_WEAR::TWMEDIUM) return QString("Medium");
            if(tyre_wear == TYRE_WEAR::TWHIGH) return QString("High");
            if(tyre_wear == TYRE_WEAR::TWVERY_HIGH) return QString("Very high");

            return QString("Medium");
        }
    };

    class Grip {
    public:
        Grip();

        static GRIP toGrip( QString grip ) {
            if( grip == QString("Very low") ) return GRIP::GVERY_LOW;
            else if( grip == QString("Low") ) return GRIP::GLOW;
            else if( grip == QString("Normal") ) return GRIP::GMEDIUM;
            else if( grip == QString("High") ) return GRIP::GHIGH;
            else if( grip == QString("Very high") ) return GRIP::GVERY_HIGH;


            return GRIP::GVERY_LOW;
        }

        static double toDouble(GRIP grip) { return static_cast<double>(grip); }
        static double toDouble(QString grip) {
            if( grip == QString("Very low") ) return toDouble(GRIP::GVERY_LOW);
            else if( grip == QString("Low") ) return toDouble(GRIP::GLOW);
            else if( grip == QString("Medium") ) return toDouble(GRIP::GMEDIUM);
            else if( grip == QString("High") ) return toDouble(GRIP::GHIGH);
            else if( grip == QString("Very high") ) return toDouble(GRIP::GVERY_HIGH);


            return toDouble(GRIP::GVERY_LOW);
        }

        static QString toString(GRIP grip) {
            if(grip == GRIP::GVERY_LOW) return QString("Very low");
            if(grip == GRIP::GLOW) return QString("Low");
            if(grip == GRIP::GMEDIUM) return QString("Medium");
            if(grip == GRIP::GHIGH) return QString("High");
            if(grip == GRIP::GVERY_HIGH) return QString("Very high");

            return QString("Medium");
        }
    };

private:
    QString name_;
    int id_;
    double distance_;
    unsigned int power_;
    unsigned int handling_;
    unsigned int acceleration_;
    DOWNFORCE downforce_;
    OVERTAKING overtaking_;
    SUSPENSION suspension_;
    FUEL_CONSUMPTION fuel_consumption_;
    TYRE_WEAR tyre_wear_;
    double avg_speed_;
    double lap_length_;
    unsigned int corners_;
    GRIP grip_;
    double pit_stop_;
    int laps_;

    QStringList string_list_;
};

#endif // TRACK_H
