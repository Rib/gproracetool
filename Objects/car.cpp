#include "car.h"

Car::Car() :
    season_(0),
    track_id_(0)
{
    stats_.fill(0);
    car_lvls_.fill(0);
    car_wears_.fill(0);
}

Car::Car(const QStringList &car_sl)
{
    if(car_sl.size() > 2) {
        for(int i = 0; i < stats_.size(); ++i) {
            stats_.at(i) = car_sl.at(i).toInt();
        }
    }

    if(car_sl.size() > 13) {
        for(int i = 0; i < car_lvls_.size(); ++i) {
            car_lvls_.at(i) = car_sl.at(i + 3).toInt();
        }
    }

    if(car_sl.size() == 25) {
        for(int i = 0; i < car_wears_.size(); ++i) {
            car_wears_.at(i) = car_sl.at(i + 14).toInt();
        }
    }
}

QStringList Car::serialize() const
{
    QStringList sl_car;

    for(int i = 0; i < stats_.size(); ++i) {
        sl_car.append(QString::number(stats_.at(i)));
    }

    for(int i = 0; i < car_lvls_.size(); ++i) {
        sl_car.append(QString::number(car_lvls_.at(i)));
    }

    for(int i = 0; i < car_wears_.size(); ++i) {
        sl_car.append(QString::number(car_wears_.at(i)));
    }

    return sl_car;
}
