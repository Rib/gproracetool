#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <memory>

#include <QMainWindow>
#include "UI/driverinfo.h"
#include "UI/practicecalculation.h"

#include "UI/racedatawidget.h"
#include "UI/racecalculator.h"
#include "UI/seasonplanner.h"
#include "Misc/gproresourcemanager.h"

using std::shared_ptr;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void resizeEvent(QResizeEvent * event);

    void resizeToSettings();

private slots:
    void on_actionExit_triggered();

    void on_actionPractice_Calculation_triggered();

    void on_actionDriver_triggered();

    void on_actionDatabase_triggered();

    void on_actionRace_Data_triggered();

    void on_actionRace_Calculator_triggered();

    void on_actionRace_Downloader_triggered();

    void on_actionUser_triggered();

    void on_actionSeason_Planner_triggered();

    void on_actionUpdate_Available_Data_triggered();

private:
    Ui::MainWindow *ui;

    RaceDataWidget *race_data_widget;
    RaceCalculator *race_calculator_widget;
    SeasonPlanner *season_planner_widget;

    GproResourceManager *resource_manager_;
};

#endif // MAINWINDOW_H
