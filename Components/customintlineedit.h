#ifndef CUSTOMINTLINEEDIT_H
#define CUSTOMINTLINEEDIT_H

#include <QWidget>
#include <QLineEdit>

class CustomIntLineEdit : public QLineEdit
{
    Q_OBJECT
public:
    CustomIntLineEdit(QWidget *parent = 0);

    void textChanged(const QString & text);

    inline void setLimits(int low, int high) { lower_limit_ = low; high_limit_ = high; }

private:
    int lower_limit_;
    int high_limit_;

    int EditFieldInt(const QString &str);

};

#endif // CUSTOMINTLINEEDIT_H
