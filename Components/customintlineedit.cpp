#include "customintlineedit.h"

CustomIntLineEdit::CustomIntLineEdit(QWidget *parent) :
    QLineEdit(parent),
    lower_limit_(0),
    high_limit_(999)
{
    connect(this, &QLineEdit::textChanged, this, &CustomIntLineEdit::textChanged);
}

void CustomIntLineEdit::textChanged(const QString &text)
{
   setText(QString::number(EditFieldInt(text)));
}

int CustomIntLineEdit::EditFieldInt(const QString &str)
{
    bool ok = false;
    int int_val = str.toInt(&ok);

    if(ok && int_val >= lower_limit_ && int_val <= high_limit_) return int_val;
    else if(ok) {
        QString tstring = str.right(1);
        int_val = tstring.toInt(&ok);
        if(ok && int_val >= lower_limit_ && int_val <= high_limit_) return int_val;
        return lower_limit_;
    }

    QString tstring = str;
    tstring.chop(1);
    int_val = tstring.toInt(&ok);
    if(ok && int_val >= lower_limit_ && int_val <= high_limit_) return int_val;

    return lower_limit_;
}
