#ifndef CUSTOMQLABEL_H
#define CUSTOMQLABEL_H

#include <QLabel>

class CustomQLabel : public QLabel
{
    Q_OBJECT
public:
    explicit CustomQLabel(QWidget *parent = 0);

    void setText(const QString &text) {
        QLabel::setText(text);
        emit textChanged(text);
    }

signals:
    void textChanged(const QString &text);

public slots:
};

#endif // CUSTOMQLABEL_H
