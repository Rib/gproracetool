#ifndef CONSTANTSTRINGS_H
#define CONSTANTSTRINGS_H

#include <QString>

class ConstantStrings
{
public:
    ConstantStrings();

    const static QString DefaultDatabaseName;

    const static QString SynchronizedText;
    const static QString NotSynchronizedText;
    const static QString DownloadingText;
};

#endif // CONSTANTSTRINGS_H
