#include "practicetablemodel.h"
#include <QDebug>
#include <cmath>

practiceTableModel::practiceTableModel(QObject *parent) :
    QAbstractTableModel(parent),
    cells_(),
    data_(),
    comments_(),
    default_range_(135)
{
    init_headers();
}

practiceTableModel::practiceTableModel(QStringList data, QObject *parent) :
    practiceTableModel(parent)
{
    vector< array<double, 6> > tdata;
    vector< array<int, 6> > tcomments;

    int elements = (data.size()-1) / 2;

    for(int i = 0; i < (elements / 6); ++i) {
        array<double,6> row;
        for(int j = 0; j < 6; ++j) {
            row.at(j) = data.at(6*i+j).toDouble();
        }
        tdata.push_back(row);
    }

    for(int i = (elements / 6); i < (elements / 3); ++i) {
        array<int,6> row;
        for(int j = 0; j < 6; ++j) {
            row.at(j) = data.at(6*i+j).toInt();
        }
        tcomments.push_back(row);
    }

    if(data.size() > 0)
        default_range_ = data.at(data.size()-1).toDouble();

    for(int i = 0; i < (elements/6); ++i) {
        addRow(tdata.at(i), tcomments.at(i), calculateRange(tdata.at(i), tcomments.at(i)));
    }

}

bool practiceTableModel::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent, row, row+count-1);

    for(int i = 0; i < count; ++i) {
        array<practiceTableCell*,6> cell_row;
        for(int i = 0; i < 6; ++i) {
            cell_row.at(i) = new practiceTableCell();
            cell_row.at(i)->setValues(0,0,0);
        }

        cells_.push_back(cell_row);
        data_.push_back(array<double,6>());
        comments_.push_back(array<int,6>());
    }

    endInsertRows();

    return true;
}

vector<array<double, 6> > practiceTableModel::calculateRanges() const
{
    vector< array<double,6> > temp_ranges;

    if (data_.size() == comments_.size()) {
        for(unsigned int i = 0; i < data_.size(); ++i) {
            if (temp_ranges.size() == 0) {
                array<double,6> dranges = {{ default_range_, default_range_,
                                           default_range_, default_range_,
                                           default_range_, default_range_}};

                for(unsigned int crange = 0; crange < 6; ++crange) {
                    if (std::abs(comments_.at(i).at(crange)) < 3) {
                        dranges.at(crange) = dranges.at(crange) / 2;
                    }
                }

                temp_ranges.push_back(dranges);
            } else {
                array<double,6> dranges = temp_ranges.at(temp_ranges.size()-1);

                for(unsigned int crange = 0; crange < 6; ++crange) {
                    if (std::abs(comments_.at(i).at(crange)) < 3) {
                        dranges.at(crange) = dranges.at(crange) / 2;
                    }
                }

                temp_ranges.push_back(dranges);
            }
        }
    } else return temp_ranges;

    return temp_ranges;
}

array<double, 6> practiceTableModel::calculateRange(const array<double, 6>& data, const array<int, 6>& comment) const
{
    vector< array<double,6> > temp_ranges;

    vector< array<double,6> > temp_data = data_;
    vector< array<int,6> > temp_comments = comments_;

    temp_data.push_back(data);
    temp_comments.push_back(comment);

    if (temp_data.size() == temp_comments.size()) {
        for(unsigned int i = 0; i < temp_data.size(); ++i) {
            if (temp_ranges.size() == 0) {
                array<double,6> dranges = {{ default_range_, default_range_,
                                           default_range_, default_range_,
                                           default_range_, default_range_}};

                for(unsigned int crange = 0; crange < 6; ++crange) {
                    if (std::abs(temp_comments.at(i).at(crange)) < 3) {
                        dranges.at(crange) = dranges.at(crange) / 2;
                    }
                }

                temp_ranges.push_back(dranges);
            } else {
                array<double,6> dranges = temp_ranges.at(temp_ranges.size()-1);

                for(unsigned int crange = 0; crange < 6; ++crange) {
                    if (std::abs(temp_comments.at(i).at(crange)) < 3) {
                        dranges.at(crange) = dranges.at(crange) / 2;
                    }
                }

                temp_ranges.push_back(dranges);
            }
        }
    } else return array<double,6>();

    return temp_ranges.at(temp_ranges.size()-1);
}

QStringList practiceTableModel::serialize()
{
    QStringList data;

    for(unsigned int i = 0; i < data_.size(); ++i) {
        for(unsigned int j = 0; j < data_.at(i).size(); ++j)
            data.append(QString::number(data_.at(i).at(j)));
    }

    for(unsigned int i = 0; i < comments_.size(); ++i) {
        for(unsigned int j = 0; j < comments_.at(i).size(); ++j)
            data.append(QString::number(comments_.at(i).at(j)));
    }

    data.append(QString::number(default_range_));

    return data;
}
