#ifndef PARSEALGORITHMS_H
#define PARSEALGORITHMS_H

#include <QString>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QList>

#include "Objects/weather.h"
#include "Objects/track.h"
#include "Objects/race.h"
#include "Objects/practicelap.h"
#include "Objects/stint.h"
#include "Misc/gproresourcemanager.h"

class ParseAlgorithms
{
public:
    ParseAlgorithms();

    Weather parseWeather(QString text);

    Weather parseTestWeather(QString text);

    QString parseTestTrackName(QString text);

    Weather parseWeatherfromRace(QString text);

    Car parseCarfromRace(QString text);

    Driver parseDriverfromRace(QString text);

    QString parseStartingRiskfromRace(QString text);

    array<int,5> parseRisksfromRace(QString text);

    Race parseTrackfromRace(QString text);

    QList<PracticeLap> parsePracticeLaps(QString text, int track_id, int season);

    QList<Lap> parseLapsfromRace(QString text);

    QList<Stint> parseStintsfromRace(QString text, Track track);
};

#endif // PARSEALGORITHMS_H
