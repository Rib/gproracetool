#include "racealgorithms.h"

#include <QDebug>

uvec RaceAlgorithms::getTrackVector(int track_id, shared_ptr<mat> full_data)
{
    if(full_data->n_elem == 0) return uvec();

    QMap<int,int> tracks;

    int counter = 0;

    uword tid_index = getIndex(QStringLiteral("TrackID"));

    for(uword i = 0; i < full_data->n_rows; ++i) {
        if(!tracks.contains(full_data->at(i,tid_index))) {
            tracks.insert(full_data->at(i,tid_index),counter);
            ++counter;
        }
    }

    int rows = tracks.size();
    uvec ret_vec(rows);

    for(int i = 0; i < rows; ++i) {
        ret_vec(i) = 0;
    }

    ret_vec(tracks.find(track_id).value()) = 1;

    return ret_vec;
}

mat RaceAlgorithms::generateWearTrainingSet(shared_ptr<mat> full_data, QString part)
{
    if(full_data->n_elem == 0) return mat();

    uvec rand_track_vec = getTrackVector(full_data->at(0,getIndex(QString("TrackID"))), full_data);
    mat return_training_set(full_data->n_rows, 5 + rand_track_vec.n_elem);

    for(uword i = 0; i < full_data->n_rows; ++i) {
        uvec track_vec = getTrackVector(full_data->at(i,getIndex(QString("TrackID"))), full_data);

        /*return_training_set(i,0) = full_data->at(i,getIndex(QString("TCorners")));
        return_training_set(i,1) = (uword)(full_data->at(i,getIndex(QString("TDownforce"))) == Track::DOWNFORCE::DLOW);
        return_training_set(i,2) = (uword)(full_data->at(i,getIndex(QString("TDownforce"))) == Track::DOWNFORCE::DMEDIUM);
        return_training_set(i,3) = (uword)(full_data->at(i,getIndex(QString("TDownforce"))) == Track::DOWNFORCE::DHIGH);
        return_training_set(i,4) = (uword)(full_data->at(i,getIndex(QString("TSuspension"))) == Track::SUSPENSION::SSOFT);
        return_training_set(i,5) = (uword)(full_data->at(i,getIndex(QString("TSuspension"))) == Track::SUSPENSION::SMEDIUM);
        return_training_set(i,6) = (uword)(full_data->at(i,getIndex(QString("TSuspension"))) == Track::SUSPENSION::SHARD);
        return_training_set(i,7) = full_data->at(i,getIndex(QString("TPower")));
        return_training_set(i,8) = full_data->at(i,getIndex(QString("THandling")));
        return_training_set(i,9) = full_data->at(i,getIndex(QString("TAcceleration")));*/

        if(full_data->at(i,getIndex(QString("SWeather"))) == Weather::Mixed) return_training_set(i,0) = -1;
        else {
            return_training_set(i,0) = (full_data->at(i,getIndex(QString("SWeather"))) == Weather::Dry ? full_data->at(i,getIndex(QString("RClear"))) :
                                                                                                    full_data->at(i,getIndex(QString("RWet"))));
        }

        return_training_set(i,1) = full_data->at(i,getIndex(QString("DConcentration")));
        return_training_set(i,2) = full_data->at(i,getIndex(QString("DTalent")));
        return_training_set(i,3) = full_data->at(i,getIndex(QString("DExperience")));

        return_training_set(i,4) = full_data->at(i,getIndex(part));
        return_training_set(i,5) = full_data->at(i,getIndex(part)) * return_training_set(i,0);

        for(int j = 6; j < return_training_set.n_cols; ++j) {
            return_training_set(i,j) = track_vec(j-6);
        }

    }

    return return_training_set;
}

vec RaceAlgorithms::generateWearResultSet(shared_ptr<mat> full_data, QString part)
{
    vec result_set(full_data->n_rows);

    for(uword i = 0; i < full_data->n_rows; ++i) {
        result_set(i) = full_data->at(i,getIndex(part)) > 0 ? full_data->at(i,getIndex(part)) : -1;
    }

    return result_set;
}

uvec RaceAlgorithms::extractWearRows(const mat &training_set, const vec &result_set)
{
    uword full_row_amount = training_set.n_rows;
    uword full_col_amount = training_set.n_cols;

    QList<uword> row_numbers;

    int row_amount = 0;

    for(uword i = 0; i < full_row_amount; ++i) {
        bool include_cond = true;

        for(uword j = 0; j < full_col_amount; ++j) {
            include_cond &= (training_set(i,j) >= 0);
        }

        include_cond &= result_set(i) >= 0;

        if(include_cond) {
            row_amount++;
            row_numbers.append(i);
        }
    }

    uvec rows(row_amount);

    for(uword i = 0; i < row_amount; ++i) {
        rows.at(i) = row_numbers.at(i);
    }

    return rows;
}

mat RaceAlgorithms::generateOverallTrainingSet(shared_ptr<mat> full_data)
{
    mat return_training_set(full_data->n_rows, 11);

    uword concentration_index = getIndex(QStringLiteral("DConcentration"));
    uword talent_index = getIndex(QStringLiteral("DTalent"));
    uword aggressiveness_index = getIndex(QStringLiteral("DAggressiveness"));
    uword exp_index = getIndex(QStringLiteral("DExperience"));
    uword ti_index = getIndex(QStringLiteral("DTechInsight"));
    uword stamina_index = getIndex(QStringLiteral("DStamina"));
    uword charisma_index = getIndex(QStringLiteral("DCharisma"));
    uword moti_index = getIndex(QStringLiteral("DMotivation"));
    uword weight_index = getIndex(QStringLiteral("DWeight"));
    uword age_index = getIndex(QStringLiteral("DAge"));
    uword rep_index = getIndex(QStringLiteral("DReputation"));

    for(uword i = 0; i < full_data->n_rows; ++i) {
        return_training_set(i,0) = full_data->at(i,concentration_index);
        return_training_set(i,1) = full_data->at(i,talent_index);
        return_training_set(i,2) = full_data->at(i,aggressiveness_index);
        return_training_set(i,3) = full_data->at(i,exp_index);
        return_training_set(i,4) = full_data->at(i,ti_index);
        return_training_set(i,5) = full_data->at(i,stamina_index);
        return_training_set(i,6) = full_data->at(i,charisma_index);
        return_training_set(i,7) = full_data->at(i,moti_index);
        return_training_set(i,8) = full_data->at(i,weight_index);
        return_training_set(i,9) = full_data->at(i,age_index) < 0 ? 0 : full_data->at(i,age_index);
        return_training_set(i,10) = full_data->at(i,rep_index);
    }

    return return_training_set;
}

vec RaceAlgorithms::generateOverallResultSet(shared_ptr<mat> full_data)
{
    vec result_set(full_data->n_rows);

    for(uword i = 0; i < full_data->n_rows; ++i) {
        result_set(i) = full_data->at(i,getIndex(QString("DOverall")));
    }

    return result_set;
}

void RaceAlgorithms::WChassisCalc()
{
    mat training_set = generateWearTrainingSet(race_data_, QString("CChassis"));
    vec result_set = generateWearResultSet(race_data_, QString("CWChassis"));

    uvec row_numbers = extractWearRows(training_set, result_set);

    mat training_set_2 = training_set.rows(row_numbers);
    vec result_set_2 = result_set.rows(row_numbers);

    chassis_wear_training_set_ = shared_ptr<mat> (new mat(training_set_2));
    chassis_wear_result_set_ = shared_ptr<vec> (new vec(result_set_2));

    LinearRegression lr(chassis_wear_training_set_->t(), *chassis_wear_result_set_, 1);

    chassis_wear_error_ = lr.ComputeError(chassis_wear_training_set_->t(), *chassis_wear_result_set_);

    chassis_wear_parameters_ = shared_ptr<vec> (new vec(lr.Parameters()));

    qDebug() << "Chassis error: " << chassis_wear_error_;
}

void RaceAlgorithms::WEngineCalc()
{
    mat training_set = generateWearTrainingSet(race_data_, QString("CEngine"));
    vec result_set = generateWearResultSet(race_data_, QString("CWEngine"));

    uvec row_numbers = extractWearRows(training_set, result_set);

    mat training_set_2 = training_set.rows(row_numbers);
    vec result_set_2 = result_set.rows(row_numbers);

    engine_wear_training_set_ = shared_ptr<mat> (new mat(training_set_2));
    engine_wear_result_set_ = shared_ptr<vec> (new vec(result_set_2));

    LinearRegression lr(engine_wear_training_set_->t(), *engine_wear_result_set_, 0);

    engine_wear_error_ = lr.ComputeError(engine_wear_training_set_->t(), *engine_wear_result_set_);

    engine_wear_parameters_ = shared_ptr<vec> (new vec(lr.Parameters()));

    qDebug() << "Engine error: " << engine_wear_error_;
}

void RaceAlgorithms::WFrontWingCalc()
{
    mat training_set = generateWearTrainingSet(race_data_, QString("CFrontWing"));
    vec result_set = generateWearResultSet(race_data_, QString("CWFrontWing"));

    uvec row_numbers = extractWearRows(training_set, result_set);

    mat training_set_2 = training_set.rows(row_numbers);
    vec result_set_2 = result_set.rows(row_numbers);

    front_wing_wear_training_set_ = shared_ptr<mat> (new mat(training_set_2));
    front_wing_wear_result_set_ = shared_ptr<vec> (new vec(result_set_2));

    LinearRegression lr(front_wing_wear_training_set_->t(), *front_wing_wear_result_set_, 0);

    front_wing_wear_error_ = lr.ComputeError(front_wing_wear_training_set_->t(), *front_wing_wear_result_set_);

    front_wing_wear_parameters_ = shared_ptr<vec> (new vec(lr.Parameters()));

    qDebug() << "Front Wing error: " << front_wing_wear_error_;
}

void RaceAlgorithms::WRearWingCalc()
{
    mat training_set = generateWearTrainingSet(race_data_, QString("CRearWing"));
    vec result_set = generateWearResultSet(race_data_, QString("CWRearWing"));

    uvec row_numbers = extractWearRows(training_set, result_set);

    mat training_set_2 = training_set.rows(row_numbers);
    vec result_set_2 = result_set.rows(row_numbers);

    rear_wing_wear_training_set_ = shared_ptr<mat>(new mat(training_set_2));
    rear_wing_wear_result_set_ = shared_ptr<vec>(new vec(result_set_2));

    LinearRegression lr(rear_wing_wear_training_set_->t(), *rear_wing_wear_result_set_, 0);

    rear_wing_wear_error_ = lr.ComputeError(rear_wing_wear_training_set_->t(), *rear_wing_wear_result_set_);

    rear_wing_wear_parameters_ = shared_ptr<vec>(new vec(lr.Parameters()));

    qDebug() << "Rear Wing error: " << rear_wing_wear_error_;
}

void RaceAlgorithms::WUnderbodyCalc()
{
    mat training_set = generateWearTrainingSet(race_data_, QString("CUnderbody"));
    vec result_set = generateWearResultSet(race_data_, QString("CWUnderbody"));

    uvec row_numbers = extractWearRows(training_set, result_set);

    mat training_set_2 = training_set.rows(row_numbers);
    vec result_set_2 = result_set.rows(row_numbers);

    underbody_wear_training_set_ = shared_ptr<mat> (new mat(training_set_2));
    underbody_wear_result_set_ = shared_ptr<vec> (new vec(result_set_2));

    LinearRegression lr(underbody_wear_training_set_->t(), *underbody_wear_result_set_, 0);

    underbody_wear_error_ = lr.ComputeError(underbody_wear_training_set_->t(), *underbody_wear_result_set_);

    underbody_wear_parameters_ = shared_ptr<vec> (new vec(lr.Parameters()));

    qDebug() << "Underbody error: " << underbody_wear_error_;
}

void RaceAlgorithms::WSidepodsCalc()
{
    mat training_set = generateWearTrainingSet(race_data_, QString("CSidepods"));
    vec result_set = generateWearResultSet(race_data_, QString("CWSidepods"));

    uvec row_numbers = extractWearRows(training_set, result_set);

    mat training_set_2 = training_set.rows(row_numbers);
    vec result_set_2 = result_set.rows(row_numbers);

    sidepods_wear_training_set_ = shared_ptr<mat> (new mat(training_set_2));
    sidepods_wear_result_set_ = shared_ptr<vec> (new vec(result_set_2));

    LinearRegression lr(sidepods_wear_training_set_->t(), *sidepods_wear_result_set_, 0);

    sidepods_wear_error_ = lr.ComputeError(sidepods_wear_training_set_->t(), *sidepods_wear_result_set_);

    sidepods_wear_parameters_ = shared_ptr<vec> (new vec(lr.Parameters()));

    qDebug() << "Sidepods error: " << sidepods_wear_error_;
}

void RaceAlgorithms::WCoolingCalc()
{
    mat training_set = generateWearTrainingSet(race_data_, QString("CCooling"));
    vec result_set = generateWearResultSet(race_data_, QString("CWCooling"));

    uvec row_numbers = extractWearRows(training_set, result_set);

    mat training_set_2 = training_set.rows(row_numbers);
    vec result_set_2 = result_set.rows(row_numbers);

    cooling_wear_training_set_ = shared_ptr<mat> (new mat(training_set_2));
    cooling_wear_result_set_ = shared_ptr<vec> (new vec(result_set_2));

    LinearRegression lr(cooling_wear_training_set_->t(), *cooling_wear_result_set_, 0);

    cooling_wear_error_ = lr.ComputeError(cooling_wear_training_set_->t(), *cooling_wear_result_set_);

    cooling_wear_parameters_ = shared_ptr<vec> (new vec(lr.Parameters()));

    qDebug() << "Cooling error: " << cooling_wear_error_;
}

void RaceAlgorithms::WGearboxCalc()
{
    mat training_set = generateWearTrainingSet(race_data_, QString("CGearbox"));
    vec result_set = generateWearResultSet(race_data_, QString("CWGearbox"));

    uvec row_numbers = extractWearRows(training_set, result_set);

    mat training_set_2 = training_set.rows(row_numbers);
    vec result_set_2 = result_set.rows(row_numbers);

    gearbox_wear_training_set_ = shared_ptr<mat> (new mat(training_set_2));
    gearbox_wear_result_set_ = shared_ptr<vec> (new vec(result_set_2));

    LinearRegression lr(gearbox_wear_training_set_->t(), *gearbox_wear_result_set_, 0);

    gearbox_wear_error_ = lr.ComputeError(gearbox_wear_training_set_->t(), *gearbox_wear_result_set_);

    gearbox_wear_parameters_ = shared_ptr<vec> (new vec(lr.Parameters()));

    qDebug() << "Gearbox error: " << gearbox_wear_error_;
}

void RaceAlgorithms::WBrakesCalc()
{
    mat training_set = generateWearTrainingSet(race_data_, QString("CBrakes"));
    vec result_set = generateWearResultSet(race_data_, QString("CWBrakes"));

    uvec row_numbers = extractWearRows(training_set, result_set);

    mat training_set_2 = training_set.rows(row_numbers);
    vec result_set_2 = result_set.rows(row_numbers);

    brakes_wear_training_set_ = shared_ptr<mat> (new mat(training_set_2));
    brakes_wear_result_set_ = shared_ptr<vec> (new vec(result_set_2));

    LinearRegression lr(brakes_wear_training_set_->t(), *brakes_wear_result_set_, 0);

    brakes_wear_error_ = lr.ComputeError(brakes_wear_training_set_->t(), *brakes_wear_result_set_);

    brakes_wear_parameters_ = shared_ptr<vec> (new vec(lr.Parameters()));

    qDebug() << "Brakes error: " << brakes_wear_error_;
}

void RaceAlgorithms::WSuspensionCalc()
{
    mat training_set = generateWearTrainingSet(race_data_, QString("CSuspension"));
    vec result_set = generateWearResultSet(race_data_, QString("CWSuspension"));

    uvec row_numbers = extractWearRows(training_set, result_set);

    mat training_set_2 = training_set.rows(row_numbers);
    vec result_set_2 = result_set.rows(row_numbers);

    suspension_wear_training_set_ = shared_ptr<mat> (new mat(training_set_2));
    suspension_wear_result_set_ = shared_ptr<vec> (new vec(result_set_2));

    LinearRegression lr(suspension_wear_training_set_->t(), *suspension_wear_result_set_, 0);

    suspension_wear_error_ = lr.ComputeError(suspension_wear_training_set_->t(), *suspension_wear_result_set_);

    suspension_wear_parameters_ = shared_ptr<vec> (new vec(lr.Parameters()));

    qDebug() << "Suspension error: " << suspension_wear_error_;
}

void RaceAlgorithms::WElectronicsCalc()
{
    mat training_set = generateWearTrainingSet(race_data_, QString("CElectronics"));
    vec result_set = generateWearResultSet(race_data_, QString("CWElectronics"));

    uvec row_numbers = extractWearRows(training_set, result_set);

    mat training_set_2 = training_set.rows(row_numbers);
    vec result_set_2 = result_set.rows(row_numbers);

    electronics_wear_training_set_ = shared_ptr<mat> (new mat(training_set_2));
    electronics_wear_result_set_ = shared_ptr<vec> (new vec(result_set_2));

    LinearRegression lr(electronics_wear_training_set_->t(), *electronics_wear_result_set_, 0);

    electronics_wear_error_ = lr.ComputeError(electronics_wear_training_set_->t(), *electronics_wear_result_set_);

    electronics_wear_parameters_ = shared_ptr<vec> (new vec(lr.Parameters()));

    qDebug() << "Electronics error: " << electronics_wear_error_;
}

RaceAlgorithms::RaceAlgorithms():
    race_data_(0),
    race_data_labels_(QStringList()),
    chassis_wear_training_set_(0),
    chassis_wear_result_set_(0),
    chassis_wear_parameters_(0),
    chassis_wear_error_(0),
    engine_wear_training_set_(0),
    engine_wear_result_set_(0),
    engine_wear_parameters_(0),
    engine_wear_error_(0),
    front_wing_wear_training_set_(0),
    front_wing_wear_result_set_(0),
    front_wing_wear_parameters_(0),
    front_wing_wear_error_(0),
    rear_wing_wear_training_set_(0),
    rear_wing_wear_result_set_(0),
    rear_wing_wear_parameters_(0),
    rear_wing_wear_error_(0),
    underbody_wear_training_set_(0),
    underbody_wear_result_set_(0),
    underbody_wear_parameters_(0),
    underbody_wear_error_(0),
    sidepods_wear_training_set_(0),
    sidepods_wear_result_set_(0),
    sidepods_wear_parameters_(0),
    sidepods_wear_error_(0),
    cooling_wear_training_set_(0),
    cooling_wear_result_set_(0),
    cooling_wear_parameters_(0),
    cooling_wear_error_(0),
    gearbox_wear_training_set_(0),
    gearbox_wear_result_set_(0),
    gearbox_wear_parameters_(0),
    gearbox_wear_error_(0),
    brakes_wear_training_set_(0),
    brakes_wear_result_set_(0),
    brakes_wear_parameters_(0),
    brakes_wear_error_(0),
    suspension_wear_training_set_(0),
    suspension_wear_result_set_(0),
    suspension_wear_parameters_(0),
    suspension_wear_error_(0),
    electronics_wear_training_set_(0),
    electronics_wear_result_set_(0),
    electronics_wear_parameters_(0),
    electronics_wear_error_(0),
    doverall_training_set_(0),
    doverall_result_set_(0),
    doverall_parameters_(0),
    doverall_error_(0)
{

}

double RaceAlgorithms::getEstimatedOverall(int concentration, int talent, int aggressiveness, int experience, int techinsight,
                                                 int stamina, int charisma, int motivation, int weight, int age, int reputation)
{
    if(doverall_training_set_ == 0 || doverall_result_set_ == 0 || doverall_parameters_ == 0 ||
            doverall_training_set_->n_rows == 0 || doverall_result_set_->n_rows == 0) {
        mat training_set = generateOverallTrainingSet(race_data_);
        vec result_set = generateOverallResultSet(race_data_);

        uvec row_numbers = extractWearRows(training_set, result_set);

        mat training_set_2 = training_set.rows(row_numbers);
        vec result_set_2 = result_set.rows(row_numbers);

        doverall_training_set_ = shared_ptr<mat> (new mat(training_set_2));
        doverall_result_set_ = shared_ptr<vec> (new vec(result_set_2));

        LinearRegression lr(doverall_training_set_->t(), *doverall_result_set_, 0);

        doverall_error_ = lr.ComputeError(doverall_training_set_->t(), *doverall_result_set_);

        doverall_parameters_ = shared_ptr<vec> (new vec(lr.Parameters()));

        qDebug() << "Driver Overall error: " << doverall_error_;
    }

    double overall = doverall_parameters_->at(0);
    overall += doverall_parameters_->at(1) * concentration;
    overall += doverall_parameters_->at(2) * talent;
    overall += doverall_parameters_->at(3) * aggressiveness;
    overall += doverall_parameters_->at(4) * experience;
    overall += doverall_parameters_->at(5) * techinsight;
    overall += doverall_parameters_->at(6) * stamina;
    overall += doverall_parameters_->at(7) * charisma;
    overall += doverall_parameters_->at(8) * motivation;
    overall += doverall_parameters_->at(9) * weight;
    overall += doverall_parameters_->at(10) * age;
    overall += doverall_parameters_->at(11) * reputation;

    return overall;
}

QList<double> RaceAlgorithms::getEstimatedWears( int track_id, int clear_track, int dconcentration, int dtalent, int dexp, int cchassis, int cengine,
                                                 int cfront_wing, int crear_wing, int cunderbody, int csidepods, int ccooling, int cgearbox,
                                                 int cbrakes, int csuspension, int celectronics)
{
    QMutexLocker mutex_locker(&mutex_);

    QList<double> result_set;
    uvec track_vec = getTrackVector(track_id, race_data_);

    QFuture< void > chassis_calc_future;

    QFuture< void > engine_calc_future;
    QFuture< void > front_wing_calc_future;
    QFuture< void > rear_wing_calc_future;
    QFuture< void > underbody_calc_future;
    QFuture< void > sidepods_calc_future;

    QFuture< void > cooling_calc_future;
    QFuture< void > gearbox_calc_future;
    QFuture< void > brakes_calc_future;
    QFuture< void > suspension_calc_future;
    QFuture< void > electronics_calc_future;

    //Chassis
    if (chassis_wear_training_set_ == 0 || chassis_wear_result_set_ == 0 || chassis_wear_parameters_ == 0) {
        chassis_calc_future = QtConcurrent::run(this, &RaceAlgorithms::WChassisCalc);
    }

    //Engine

    if (engine_wear_training_set_ == 0 || engine_wear_result_set_ == 0 || engine_wear_parameters_ == 0) {
        engine_calc_future = QtConcurrent::run(this, &RaceAlgorithms::WEngineCalc);
    }

    //Front wing

    if (front_wing_wear_training_set_ == 0 || front_wing_wear_result_set_ == 0 || front_wing_wear_parameters_ == 0) {
        front_wing_calc_future = QtConcurrent::run(this, &RaceAlgorithms::WFrontWingCalc);
    }

    //Rear Wing

    if (rear_wing_wear_training_set_ == 0 || rear_wing_wear_result_set_ == 0 || rear_wing_wear_parameters_ == 0) {
        rear_wing_calc_future = QtConcurrent::run(this, &RaceAlgorithms::WRearWingCalc);
    }

    //Underbody

    if (underbody_wear_training_set_ == 0 || underbody_wear_result_set_ == 0 || underbody_wear_parameters_ == 0) {
        underbody_calc_future = QtConcurrent::run(this, &RaceAlgorithms::WUnderbodyCalc);
    }

    //Sidepods

    if (sidepods_wear_training_set_ == 0 || sidepods_wear_result_set_ == 0 || sidepods_wear_parameters_ == 0) {
        sidepods_calc_future = QtConcurrent::run(this, &RaceAlgorithms::WSidepodsCalc);
    }

    //Cooling

    if (cooling_wear_training_set_ == 0 || cooling_wear_result_set_ == 0 || cooling_wear_parameters_ == 0) {
        cooling_calc_future = QtConcurrent::run(this, &RaceAlgorithms::WCoolingCalc);
    }

    //Gearbox

    if (gearbox_wear_training_set_ == 0 || gearbox_wear_result_set_ == 0 || gearbox_wear_parameters_ == 0) {
        gearbox_calc_future = QtConcurrent::run(this, &RaceAlgorithms::WGearboxCalc);
    }

    //Brakes

    if (brakes_wear_training_set_ == 0 || brakes_wear_result_set_ == 0 || brakes_wear_parameters_ == 0) {
        brakes_calc_future = QtConcurrent::run(this, &RaceAlgorithms::WBrakesCalc);
    }

    //Suspension

    if (suspension_wear_training_set_ == 0 || suspension_wear_result_set_ == 0 || suspension_wear_parameters_ == 0) {
        suspension_calc_future = QtConcurrent::run(this, &RaceAlgorithms::WSuspensionCalc);
    }

    //Electronics

    if (electronics_wear_training_set_ == 0 || electronics_wear_result_set_ == 0 || electronics_wear_parameters_ == 0) {
        electronics_calc_future = QtConcurrent::run(this, &RaceAlgorithms::WElectronicsCalc);
    }

    if(chassis_calc_future.isRunning())
        chassis_calc_future.waitForFinished();

    if(engine_calc_future.isRunning())
        engine_calc_future.waitForFinished();

    if(front_wing_calc_future.isRunning())
        front_wing_calc_future.waitForFinished();

    if(rear_wing_calc_future.isRunning())
        rear_wing_calc_future.waitForFinished();

    if(underbody_calc_future.isRunning())
        underbody_calc_future.waitForFinished();

    if(sidepods_calc_future.isRunning())
        sidepods_calc_future.waitForFinished();

    if(cooling_calc_future.isRunning())
        cooling_calc_future.waitForFinished();

    if(gearbox_calc_future.isRunning())
        gearbox_calc_future.waitForFinished();

    if(brakes_calc_future.isRunning())
        brakes_calc_future.waitForFinished();

    if(suspension_calc_future.isRunning())
        suspension_calc_future.waitForFinished();

    if(electronics_calc_future.isRunning())
        electronics_calc_future.waitForFinished();

    double chassis_wear = chassis_wear_parameters_->at(0);
    chassis_wear += chassis_wear_parameters_->at(1) * clear_track;
    chassis_wear += chassis_wear_parameters_->at(2) * dconcentration;
    chassis_wear += chassis_wear_parameters_->at(3) * dtalent;
    chassis_wear += chassis_wear_parameters_->at(4) * dexp;
    chassis_wear += chassis_wear_parameters_->at(5) * cchassis;
    chassis_wear += chassis_wear_parameters_->at(6) * cchassis * clear_track;
    for(uword i = 0; i < track_vec.n_elem; ++i) {
        chassis_wear += chassis_wear_parameters_->at(7+i) * track_vec(i);
    }

    result_set.append(chassis_wear);

    double engine_wear = engine_wear_parameters_->at(0);
    engine_wear += engine_wear_parameters_->at(1) * clear_track;
    engine_wear += engine_wear_parameters_->at(2) * dconcentration;
    engine_wear += engine_wear_parameters_->at(3) * dtalent;
    engine_wear += engine_wear_parameters_->at(4) * dexp;
    engine_wear += engine_wear_parameters_->at(5) * cengine;
    engine_wear += engine_wear_parameters_->at(6) * cengine * clear_track;

    double min = 0;
    double max = 0;
    for(int i = 0; i < track_vec.n_elem; ++i) {
        min = (min > engine_wear_parameters_->at(7+i) && engine_wear_parameters_->at(7+i) > -100) ? engine_wear_parameters_->at(7+i) : min;
        max = (max < engine_wear_parameters_->at(7+i) && engine_wear_parameters_->at(7+i) < 100) ? engine_wear_parameters_->at(7+i) : max;

        if(engine_wear_parameters_->at(7+i) < -100) engine_wear += min * track_vec(i);
        else if(engine_wear_parameters_->at(7+i) > 100) engine_wear += max * track_vec(i);
        else engine_wear += engine_wear_parameters_->at(7+i) * track_vec(i);
    }

    result_set.append(engine_wear);

    double front_wing_wear = front_wing_wear_parameters_->at(0);
    front_wing_wear += front_wing_wear_parameters_->at(1) * clear_track;
    front_wing_wear += front_wing_wear_parameters_->at(2) * dconcentration;
    front_wing_wear += front_wing_wear_parameters_->at(3) * dtalent;
    front_wing_wear += front_wing_wear_parameters_->at(4) * dexp;
    front_wing_wear += front_wing_wear_parameters_->at(5) * cfront_wing;
    front_wing_wear += front_wing_wear_parameters_->at(6) * cfront_wing * clear_track;
    for(int i = 0; i < track_vec.n_elem; ++i) {
        front_wing_wear += front_wing_wear_parameters_->at(7+i) * track_vec(i);
    }

    result_set.append(front_wing_wear);

    double rear_wing_wear = rear_wing_wear_parameters_->at(0);
    rear_wing_wear += rear_wing_wear_parameters_->at(1) * clear_track;
    rear_wing_wear += rear_wing_wear_parameters_->at(2) * dconcentration;
    rear_wing_wear += rear_wing_wear_parameters_->at(3) * dtalent;
    rear_wing_wear += rear_wing_wear_parameters_->at(4) * dexp;
    rear_wing_wear += rear_wing_wear_parameters_->at(5) * crear_wing;
    rear_wing_wear += rear_wing_wear_parameters_->at(6) * crear_wing * clear_track;
    for(int i = 0; i < track_vec.n_elem; ++i) {
        rear_wing_wear += rear_wing_wear_parameters_->at(7+i) * track_vec(i);
    }

    result_set.append(rear_wing_wear);

    double underbody_wear = underbody_wear_parameters_->at(0);
    underbody_wear += underbody_wear_parameters_->at(1) * clear_track;
    underbody_wear += underbody_wear_parameters_->at(2) * dconcentration;
    underbody_wear += underbody_wear_parameters_->at(3) * dtalent;
    underbody_wear += underbody_wear_parameters_->at(4) * dexp;
    underbody_wear += underbody_wear_parameters_->at(5) * cunderbody;
    underbody_wear += underbody_wear_parameters_->at(6) * cunderbody * clear_track;
    for(int i = 0; i < track_vec.n_elem; ++i) {
        underbody_wear += underbody_wear_parameters_->at(7+i) * track_vec(i);
    }

    result_set.append(underbody_wear);

    double sidepods_wear = sidepods_wear_parameters_->at(0);
    sidepods_wear += sidepods_wear_parameters_->at(1) * clear_track;
    sidepods_wear += sidepods_wear_parameters_->at(2) * dconcentration;
    sidepods_wear += sidepods_wear_parameters_->at(3) * dtalent;
    sidepods_wear += sidepods_wear_parameters_->at(4) * dexp;
    sidepods_wear += sidepods_wear_parameters_->at(5) * csidepods;
    sidepods_wear += sidepods_wear_parameters_->at(6) * csidepods * clear_track;
    for(int i = 0; i < track_vec.n_elem; ++i) {
        sidepods_wear += sidepods_wear_parameters_->at(7+i) * track_vec(i);
    }

    result_set.append(sidepods_wear);

    double cooling_wear = cooling_wear_parameters_->at(0);
    cooling_wear += cooling_wear_parameters_->at(1) * clear_track;
    cooling_wear += cooling_wear_parameters_->at(2) * dconcentration;
    cooling_wear += cooling_wear_parameters_->at(3) * dtalent;
    cooling_wear += cooling_wear_parameters_->at(4) * dexp;
    cooling_wear += cooling_wear_parameters_->at(5) * ccooling;
    cooling_wear += cooling_wear_parameters_->at(6) * ccooling * clear_track;
    for(int i = 0; i < track_vec.n_elem; ++i) {
        cooling_wear += cooling_wear_parameters_->at(7+i) * track_vec(i);
    }

    result_set.append(cooling_wear);



    double gearbox_wear = gearbox_wear_parameters_->at(0);
    gearbox_wear += gearbox_wear_parameters_->at(1) * clear_track;
    gearbox_wear += gearbox_wear_parameters_->at(2) * dconcentration;
    gearbox_wear += gearbox_wear_parameters_->at(3) * dtalent;
    gearbox_wear += gearbox_wear_parameters_->at(4) * dexp;
    gearbox_wear += gearbox_wear_parameters_->at(5) * cgearbox;
    gearbox_wear += gearbox_wear_parameters_->at(6) * cgearbox * clear_track;
    for(int i = 0; i < track_vec.n_elem; ++i) {
        gearbox_wear += gearbox_wear_parameters_->at(7+i) * track_vec(i);
    }

    result_set.append(gearbox_wear);

    double brakes_wear = brakes_wear_parameters_->at(0);
    brakes_wear += brakes_wear_parameters_->at(1) * clear_track;
    brakes_wear += brakes_wear_parameters_->at(2) * dconcentration;
    brakes_wear += brakes_wear_parameters_->at(3) * dtalent;
    brakes_wear += brakes_wear_parameters_->at(4) * dexp;
    brakes_wear += brakes_wear_parameters_->at(5) * cbrakes;
    brakes_wear += brakes_wear_parameters_->at(6) * cbrakes * clear_track;
    for(int i = 0; i < track_vec.n_elem; ++i) {
        brakes_wear += brakes_wear_parameters_->at(7+i) * track_vec(i);
    }

    result_set.append(brakes_wear);

    double suspension_wear = suspension_wear_parameters_->at(0);
    suspension_wear += suspension_wear_parameters_->at(1) * clear_track;
    suspension_wear += suspension_wear_parameters_->at(2) * dconcentration;
    suspension_wear += suspension_wear_parameters_->at(3) * dtalent;
    suspension_wear += suspension_wear_parameters_->at(4) * dexp;
    suspension_wear += suspension_wear_parameters_->at(5) * csuspension;
    suspension_wear += suspension_wear_parameters_->at(6) * csuspension * clear_track;
    for(int i = 0; i < track_vec.n_elem; ++i) {
        suspension_wear += suspension_wear_parameters_->at(7+i) * track_vec(i);
    }

    result_set.append(suspension_wear);

    double electronics_wear = electronics_wear_parameters_->at(0);
    electronics_wear += electronics_wear_parameters_->at(1) * clear_track;
    electronics_wear += electronics_wear_parameters_->at(2) * dconcentration;
    electronics_wear += electronics_wear_parameters_->at(3) * dtalent;
    electronics_wear += electronics_wear_parameters_->at(4) * dexp;
    electronics_wear += electronics_wear_parameters_->at(5) * celectronics;
    electronics_wear += electronics_wear_parameters_->at(6) * celectronics * clear_track;
    for(int i = 0; i < track_vec.n_elem; ++i) {
        electronics_wear += electronics_wear_parameters_->at(7+i) * track_vec(i);
    }

    result_set.append(electronics_wear);

    return result_set;
}
