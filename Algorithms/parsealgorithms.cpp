#include "parsealgorithms.h"

#include <QDebug>

ParseAlgorithms::ParseAlgorithms()
{

}

Weather ParseAlgorithms::parseWeather(QString text)
{
    Weather weather;

    QString reply_str = text;

    // <th>Stamina<\/th>[\s]*<td>(\d{1,3})<\/td>
    // <th>Stamina</th>[\\s]*<td>(\\d{1,3})(\\S\\s)*</td>
    //qDebug() << reply_str;
    QRegularExpression regexp( QString("<img[\\s]+src=[\"].+[\"][\\s]+alt=[\"][\\sa-zA-Z]+[\"]") +
                               QString("[\\s]+title=[\"]([\\sa-zA-Z]+)[\"][\\s]+name=[\"]WeatherQ[\"]") +
                               QString("[\\s]*><br>[\\s]+Temp:[\\s](\\d{1,2})") +
                               QString("&deg;C<br>[\\s]+Humidity:[\\s](\\d{1,2})") +
                               QString("[\\s\\S]+<img[\\s]+src=[\"].*[\"][\\s]+alt=[\"][\\sa-zA-Z]*[\"]") +
                               QString("[\\s]+title=[\"]([\\sa-zA-Z]*)[\"][\\s]+name=[\"]WeatherR[\"]") +
                               QString("[\\s]*><br>[\\s]+Temp:[\\s](\\d{1,2})") +
                               QString("&deg;C<br>[\\s]+Humidity:[\\s](\\d{1,2})"));

    QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        //qDebug() << "Weather alternate test";
        QRegularExpressionMatch match = iter.next();
        //qDebug() << match.captured(0);
        weather.setQ1Status(Weather::Status::Dry);
        if(match.captured(1).contains("Rain"))
            weather.setQ1Status(Weather::Status::Wet);
        weather.setQ1Temperature(match.captured(2).toUInt());
        weather.setQ1Humidity(match.captured(3).toUInt());
        weather.setQ2Status(Weather::Status::Dry);
        if(match.captured(4).contains("Rain"))
            weather.setQ2Status(Weather::Status::Wet);
        weather.setQ2Temperature(match.captured(5).toUInt());
        weather.setQ2Humidity(match.captured(6).toUInt());
    }

    QString pattern = QString("<td[\\s]+class=[\"]center[\"][\\s]+nowrap>[\\s]*") +
            QString("Temp:[\\s]+(\\d{1,2})&deg;-(\\d{1,2})&deg;<br>[\\s]*") +
            QString("Humidity:[\\s]+(\\d{1,2})%-(\\d{1,2})%<br>[\\s]*") +
            QString("Rain[\\s]+probability:[\\s]*(\\d{1,2})%(-(\\d{1,2})%)?[\\s]*</td>[\\s]*") +
            QString("<td[\\s]+class=[\"]center[\"][\\s]+nowrap>[\\s]*") +
            QString("Temp:[\\s]+(\\d{1,2})&deg;-(\\d{1,2})&deg;<br>[\\s]*") +
            QString("Humidity:[\\s]+(\\d{1,2})%-(\\d{1,2})%<br>[\\s]*") +
            QString("Rain[\\s]+probability:[\\s]*(\\d{1,2})%(-(\\d{1,2})%)?[\\s]*</td>") +
            QString("[\\s\\S]*<td[\\s]+class=[\"]center[\"][\\s]+nowrap>[\\s]*") +
            QString("Temp:[\\s]+(\\d{1,2})&deg;-(\\d{1,2})&deg;<br>[\\s]*") +
            QString("Humidity:[\\s]+(\\d{1,2})%-(\\d{1,2})%<br>[\\s]*") +
            QString("Rain[\\s]+probability:[\\s]*(\\d{1,2})%(-(\\d{1,2})%)?[\\s]*</td>[\\s]*") +
            QString("<td[\\s]+class=[\"]center[\"][\\s]+nowrap>[\\s]*") +
            QString("Temp:[\\s]+(\\d{1,2})&deg;-(\\d{1,2})&deg;<br>[\\s]*") +
            QString("Humidity:[\\s]+(\\d{1,2})%-(\\d{1,2})%<br>[\\s]*") +
            QString("Rain[\\s]+probability:[\\s]*(\\d{1,2})%(-(\\d{1,2})%)?[\\s]*</td>");
    regexp.setPattern( pattern );
    iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();

        // Race q1
        weather.setRaceQ1Temperature( match.captured(1).toUInt(), match.captured(2).toUInt() );
        weather.setRaceQ1Humidity( match.captured(3).toUInt(), match.captured(4).toUInt());
        int temp = match.captured(7).length() > 0 ? match.captured(7).toUInt() : 0;
        weather.setRaceQ1RainProbability( match.captured(5).toUInt(), temp);

        // Race q2
        weather.setRaceQ2Temperature( match.captured(8).toUInt(), match.captured(9).toUInt() );
        weather.setRaceQ2Humidity( match.captured(10).toUInt(), match.captured(11).toUInt());
        temp = match.captured(14).length() > 0 ? match.captured(14).toUInt() : 0;
        weather.setRaceQ2RainProbability( match.captured(12).toUInt(), temp);

        // Race q3
        weather.setRaceQ3Temperature( match.captured(15).toUInt(), match.captured(16).toUInt() );
        weather.setRaceQ3Humidity( match.captured(17).toUInt(), match.captured(18).toUInt());
        temp = match.captured(21).length() > 0 ? match.captured(21).toUInt() : 0;
        weather.setRaceQ3RainProbability( match.captured(19).toUInt(), temp);

        // Race q4
        weather.setRaceQ4Temperature( match.captured(22).toUInt(), match.captured(23).toUInt() );
        weather.setRaceQ4Humidity( match.captured(24).toUInt(), match.captured(25).toUInt());
        temp = match.captured(28).length() > 0 ? match.captured(28).toUInt() : 0;
        weather.setRaceQ4RainProbability( match.captured(26).toUInt(), temp);
    }

    return weather;
}

Weather ParseAlgorithms::parseTestWeather(QString text)
{
    Weather weather;

    QString reply_str = text;

    // <th>Stamina<\/th>[\s]*<td>(\d{1,3})<\/td>
    // <th>Stamina</th>[\\s]*<td>(\\d{1,3})(\\S\\s)*</td>
    // qDebug() << reply_str;
    QRegularExpression regexp( QString("<img[\"\\a-z= A-Z./0-9]+title[=\\\"]+([A-Za-z ]+)[\"\\a-z= A-Z./0-9]+><br>[\r\n\t]+"
                                       "Temp[: ]+([\\d]{1,2})[°C]+<br>[\r\n\t]+Humidity[: ]+([\\d]{1,3})[%]"));

    QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        //qDebug() << "Weather alternate test";
        QRegularExpressionMatch match = iter.next();
        //qDebug() << match.captured(0);
        weather.setQ1Status(Weather::Status::Dry);
        if(match.captured(1).contains("Rain"))
            weather.setQ1Status(Weather::Status::Wet);
        weather.setQ1Temperature(match.captured(2).toUInt());
        weather.setQ1Humidity(match.captured(3).toUInt());
    }

    return weather;
}

QString ParseAlgorithms::parseTestTrackName(QString text)
{
    QString track_name;

    QString reply_str = text;

    // <th>Stamina<\/th>[\s]*<td>(\d{1,3})<\/td>
    // <th>Stamina</th>[\\s]*<td>(\\d{1,3})(\\S\\s)*</td>
    // qDebug() << reply_str;
    QRegularExpression regexp( QString("Testing[:][ ]([a-zA-Z0-9 -]+) [(]"));

    QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        //qDebug() << "Weather alternate test";
        QRegularExpressionMatch match = iter.next();
        qDebug() << "Test track name" << match.captured(1);
        track_name = match.captured(1);
    }

    return track_name;
}

Weather ParseAlgorithms::parseWeatherfromRace(QString text)
{
    Weather weather;

    QString reply_str = text;

    // <th>Stamina<\/th>[\s]*<td>(\d{1,3})<\/td>
    // <th>Stamina</th>[\\s]*<td>(\\d{1,3})(\\S\\s)*</td>
    //qDebug() << reply_str;
    QRegularExpression regexp( QString("<img[\\sa-zA-Z=\\\"0-9:/.]+title[=\\\"]+([a-zA-Z\\s]+)") +
                               QString("[\\sa-zA-Z=\\\"0-9:/.]+WeatherQ[\\\"]+><br>[\r\n\t]+Temp[:\\s]+([\\d]{1,2})") +
                               QString("[&a-zA-Z#0-9;]+<br>[\r\n\t]+Humidity[:\\s]+([\\d]{1,2})[\r\n\t%]+</td>[\r\n\t]+") +
                               QString("<td[\\sa-zA-Z=\\\"0-9:/.]+>[\r\n\t]+<img[\\sa-zA-Z=\\\"0-9:/.]+title[=\\\"]+") +
                               QString("([a-zA-Z\\s]+)[\\sa-zA-Z=\\\"0-9:/.]+WeatherR[\\\"]+><br>[\r\n\t]+Temp[:\\s]+") +
                               QString("([\\d]{1,2})[&a-zA-Z#0-9;]+<br>[\r\n\t]+Humidity[:\\s]+([\\d]{1,2})[\r\n\t%]+</td>") );


    QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();

        weather.setQ1Status(Weather::Status::Dry);
        if(match.captured(1).contains("Rain"))
            weather.setQ1Status(Weather::Status::Wet);
        weather.setQ1Temperature(match.captured(2).toUInt());
        weather.setQ1Humidity(match.captured(3).toUInt());
        weather.setQ2Status(Weather::Status::Dry);
        if(match.captured(4).contains("Rain"))
            weather.setQ2Status(Weather::Status::Wet);
        weather.setQ2Temperature(match.captured(5).toUInt());
        weather.setQ2Humidity(match.captured(6).toUInt());
    }

    QString pattern = QString("Temp:[\\s]+(\\d{1,2})&deg;[\\s]*-[\\s]*([\\d]{1,2})&deg;"
                              "<br>[\r\n\t]+Humidity[:\\s]+([\\d]{1,2})[%\\s-]+([\\d]{1,2})%"
                              "<br>[\r\n\t]+Rain[\\s]probability[:\r\n\t]+([\\d]{1,2})%([\\s-]+"
                              "([\\d]{1,2})%)?[\r\n\t]+</td>" );
    regexp.setPattern( pattern );
    iter = regexp.globalMatch(reply_str);

    int counter = 0;

    while(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();

        if(counter == 0) {
            // Race q1
            weather.setRaceQ1Temperature( match.captured(1).toUInt(), match.captured(2).toUInt() );
            weather.setRaceQ1Humidity( match.captured(3).toUInt(), match.captured(4).toUInt());
            int temp = match.captured(7).length() > 0 ? match.captured(7).toUInt() : 0;
            weather.setRaceQ1RainProbability( match.captured(5).toUInt(), temp);
        }

        else if (counter == 1) {
            // Race q2
            weather.setRaceQ2Temperature( match.captured(1).toUInt(), match.captured(2).toUInt() );
            weather.setRaceQ2Humidity( match.captured(3).toUInt(), match.captured(4).toUInt());
            int temp = match.captured(7).length() > 0 ? match.captured(7).toUInt() : 0;
            weather.setRaceQ2RainProbability( match.captured(5).toUInt(), temp);
        }

        else if(counter == 2) {
            // Race q3
            weather.setRaceQ3Temperature( match.captured(1).toUInt(), match.captured(2).toUInt() );
            weather.setRaceQ3Humidity( match.captured(3).toUInt(), match.captured(4).toUInt());
            int temp = match.captured(7).length() > 0 ? match.captured(7).toUInt() : 0;
            weather.setRaceQ3RainProbability( match.captured(5).toUInt(), temp);
        }

        else if(counter == 3) {
            // Race q4
            weather.setRaceQ4Temperature( match.captured(1).toUInt(), match.captured(2).toUInt() );
            weather.setRaceQ4Humidity( match.captured(3).toUInt(), match.captured(4).toUInt());
            int temp = match.captured(7).length() > 0 ? match.captured(7).toUInt() : 0;
            weather.setRaceQ4RainProbability( match.captured(5).toUInt(), temp);
        }

        counter++;
    }

    return weather;
}

Car ParseAlgorithms::parseCarfromRace(QString text)
{
    Car car;

    QString reply_str = text;

    QRegularExpression regexp( QString("Elec</b></td>[\r\t\n]+</tr>[\r\t\n]+<tr[\\sa-zA-Z\\\".='#0-9]+>[\r\t\n]+"
                                       "<td[\\s]+align=[\\\"]+center[\\\"]+>([\\d])</td>"
                                       "<td[\\s]+align=[\\\"]+center[\\\"]+>([\\d])</td>"
                                       "<td[\\s]+align=[\\\"]+center[\\\"]+>([\\d])</td>"
                                       "<td[\\s]+align=[\\\"]+center[\\\"]+>([\\d])</td>"
                                       "<td[\\s]+align=[\\\"]+center[\\\"]+>([\\d])</td>"
                                       "<td[\\s]+align=[\\\"]+center[\\\"]+>([\\d])</td>"
                                       "<td[\\s]+align=[\\\"]+center[\\\"]+>([\\d])</td>"
                                       "<td[\\s]+align=[\\\"]+center[\\\"]+>([\\d])</td>"
                                       "<td[\\s]+align=[\\\"]+center[\\\"]+>([\\d])</td>"
                                       "<td[\\s]+align=[\\\"]+center[\\\"]+>([\\d])</td>"
                                       "<td[\\s]+align=[\\\"]+center[\\\"]+>([\\d])</td>") );

    QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();

        car.setChassis(match.captured(1).toInt(), 0);
        car.setEngine(match.captured(2).toInt(), 0);
        car.setFrontWing(match.captured(3).toInt(), 0);
        car.setRearWing(match.captured(4).toInt(), 0);
        car.setUnderbody(match.captured(5).toInt(), 0);
        car.setSidepods(match.captured(6).toInt(), 0);
        car.setCooling(match.captured(7).toInt(), 0);
        car.setGearbox(match.captured(8).toInt(), 0);
        car.setBrakes(match.captured(9).toInt(), 0);
        car.setSuspension(match.captured(10).toInt(), 0);
        car.setElectronics(match.captured(11).toInt(), 0);
    }

    regexp.setPattern( QString("Car[\\s]+parts[\\s]+wear[\\s(a-z]+[)]</th></tr>[\r\n\t]+"
                               "<tr[\\sa-z=\\\".'#A-Z0-9]+>[\r\n\t]+(<td[\\sa-z\\\"=]+>)([\\d]{1,3})[%]</td>"
                               "\\1([\\d]{1,3})[%]</td>\\1([\\d]{1,3})[%]</td>\\1([\\d]{1,3})[%]</td>"
                               "\\1([\\d]{1,3})[%]</td>\\1([\\d]{1,3})[%]</td>\\1([\\d]{1,3})[%]</td>"
                               "\\1([\\d]{1,3})[%]</td>\\1([\\d]{1,3})[%]</td>\\1([\\d]{1,3})[%]</td>"
                               "\\1([\\d]{1,3})[%]</td>") );

    iter = regexp.globalMatch(reply_str);

    array<int, 11> wears;

    int counter = 0;

    while(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();

        if(counter == 0) {
            for(unsigned int i = 0; i < wears.size(); ++i) {
                wears.at(i) = -match.captured(i+2).toInt();
            }
        } else {
            for(unsigned int i = 0; i < wears.size(); ++i) {
                int temp_wear = match.captured(i+2).toInt();
                wears.at(i) += temp_wear;
                if(temp_wear >= 99) wears.at(i) = -1;
            }
        }

        counter++;
    }

    car.setChassis(car.getChassisLvl(), wears.at(0));
    car.setEngine(car.getEngineLvl(), wears.at(1));
    car.setFrontWing(car.getFrontWingLvl(), wears.at(2));
    car.setRearWing(car.getRearWingLvl(), wears.at(3));
    car.setUnderbody(car.getUnderbodyLvl(), wears.at(4));
    car.setSidepods(car.getSidepodsLvl(), wears.at(5));
    car.setCooling(car.getCoolingLvl(), wears.at(6));
    car.setGearbox(car.getGearboxLvl(), wears.at(7));
    car.setBrakes(car.getBrakesLvl(), wears.at(8));
    car.setSuspension(car.getSuspensionLvl(), wears.at(9));
    car.setElectronics(car.getElectronicsLvl(), wears.at(10));

    regexp.setPattern( QString("<th([\\sa-zA-Z0-9\\\":%!;=]+)>Power</th>[\r\n\t]+"
                               "<th\\1>Handling</th>[\r\n\t]+"
                               "<th>Acceleration</th>[\r\n\t]+</tr>[\r\n\t]+"
                               "<tr>[\r\n\t]+<td([\\sa-z\\\"=]+)>([\\d]{1,3})</td>"
                               "<td\\2>([\\d]{1,3})</td><td\\2>([\\d]{1,3})</td>") );

    iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();

        car.setPower(match.captured(3).toInt());
        car.setHandling(match.captured(4).toInt());
        car.setAcceleration(match.captured(5).toInt());
    }

    return car;
}

Driver ParseAlgorithms::parseDriverfromRace(QString text)
{
    Driver  driver;

    QString reply_str = text;

    QRegularExpression regexp( QStringLiteral("<a[ a-z=\\\"]+DriverProfile[.a-zA-Z0-9=?\\\"]+>([ A-Za-z-]+)</a>[\r\n\t]+</td>[\r\n\t]+"
                                              "<td[ a-z=\\\"]+>([\\d]{1,3})</td>[\r\n\t]+"
                                              "<td[ a-z=\\\"]+>([\\d]{1,3})</td>[\r\n\t]+"
                                              "<td[ a-z=\\\"]+>([\\d]{1,3})</td>[\r\n\t]+"
                                              "<td[ a-z=\\\"]+>([\\d]{1,3})</td>[\r\n\t]+"
                                              "<td[ a-z=\\\"]+>([\\d]{1,3})</td>[\r\n\t]+"
                                              "<td[ a-z=\\\"]+>([\\d]{1,3})</td>[\r\n\t]+"
                                              "<td[ a-z=\\\"]+>([\\d]{1,3})</td>[\r\n\t]+"
                                              "<td[ a-z=\\\"]+>([\\d]{1,3})</td>[\r\n\t]+"
                                              "<td[ a-z=\\\"]+>([\\d]{1,3})</td>[\r\n\t]+"
                                              "<td[ a-z=\\\"]+>([\\d]{1,3})</td>[\r\n\t]+"
                                              "<td[ a-z=\\\"]+>([\\d]{1,3})</td>[\r\n\t]+") );

    QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();

        driver.setName( match.captured(1) );

        for(int i = 0; i < 11; ++i) {
            driver.setRawAttribute(i, match.captured(i+2).toInt());
        }
    }

    return driver;

}

QString ParseAlgorithms::parseStartingRiskfromRace(QString text)
{
    QString start_risk;

    QString reply_str = text;

    QRegularExpression regexp( QString("Starting[\\s]risk</th></tr>[\r\n\t]+<tr>[\r\n\t]+"
                                       "<td[\\sa-z\\\"0-9=]+>[\r\n\t]+([a-zA-Z ]+)") );

    QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();

        start_risk = match.captured(1);
    }

    return start_risk;
}

array<int,5> ParseAlgorithms::parseRisksfromRace(QString text)
{
    array<int,5> risks;

    QString reply_str = text;

    QRegularExpression regexp( QString("Malfunct</td>[\r\n\t]+</tr>[\r\n\t]+<tr>[\r\n\t]+"
                                       "<td>([\\d]{1,3})</td><td[\\sa-zA-Z=\\\"]+>([\\d]{1,3})</td>"
                                       "<td[\\sa-zA-Z=\\\"]+>([\\d]{1,3})</td>"
                                       "<td[\\sa-zA-Z=\\\"]+>([\\d]{1,3}|[-])</td><td[\\sa-zA-Z=\\\"]+>([\\d]{1,3})</td>") );

    QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();

        risks.at(0) = match.captured(1).toInt();
        risks.at(1) = match.captured(2).toInt();
        risks.at(2) = match.captured(3).toInt();
        risks.at(3) = match.captured(4) == QString("-") ? match.captured(3).toInt() : match.captured(4).toInt();
        risks.at(4) = match.captured(5).toInt();
    }

    return risks;
}

Race ParseAlgorithms::parseTrackfromRace(QString text)
{
    Race race;
    Track track;

    QString reply_str = text;

    QString str_pattern = QString("Race[\\s]+analysis[\\s]+[-][\\s]+Season[\\s]+([\\d]{1,2})"
                                  "[\\s]+[-][\\s]+Race[\\s]+([\\d]{1,2})[\\s]+[-][\\s]+"
                                  "([A-Za-z-0-9\\s()]+)[\\s]+[-][\\s]+Grand[\\s]+Prix[\\s]+Racing[\\s]+Online");

     QRegularExpression regexp(str_pattern);
     QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);
     if(iter.hasNext()) {
         QRegularExpressionMatch match = iter.next();
         race.setSeason(match.captured(1).toInt());
         race.setRace(match.captured(2).toInt());

         QList< QStringList > tracks = GproResourceManager::getInstance().getDatabaseHandler()->getTracks();

         foreach( QStringList str_list, tracks) {
             if(match.captured(3).contains(str_list.at(0))) {
                 track.setName(str_list.at(0));
                 track.setID( str_list.at(1).toInt() );
                 track.setDistance( str_list.at(2).toDouble() );
                 track.setPower( str_list.at(3).toInt() );
                 track.setHandling( str_list.at(4).toInt() );
                 track.setAcceleration( str_list.at(5).toInt() );
                 track.setDownforce( Track::Downforce::toDownforce( str_list.at(6) ) );
                 track.setOvertaking( Track::Overtaking::toOvertaking( str_list.at(7) ) );
                 track.setSuspension( Track::Suspension::toSuspension( str_list.at(8) ) );
                 track.setFuelConsumption( Track::FuelConsumption::toFuelConsumption( str_list.at(9) ) );
                 track.setTyreWear( Track::TyreWear::toTyreWear( str_list.at(10) ) );
                 track.setAvgSpeed( str_list.at(11).toDouble() );
                 track.setLapLength( str_list.at(12).toDouble() );
                 track.setCorners( str_list.at(13).toUInt() );
                 track.setGrip( Track::Grip::toGrip( str_list.at(14) ) );
                 track.setPitStop( str_list.at(15).toDouble() );
                 break;
             }
         }

         race.setTrack(track);
     }

     qDebug() << "Track name: " << race.getTrack().getName();

     return race;
}

QList<PracticeLap> ParseAlgorithms::parsePracticeLaps(QString text, int track_id, int season)
{
    QList<PracticeLap> plaps;

    QString reply_str = text;

    QString str_pattern;
    QRegularExpression regexp;
    QRegularExpressionMatchIterator iter;

    str_pattern = QString("<b>([\\d]?):?([\\d]{2}).([\\d]{3})s</b>[\r\n\t]+<?[/a-zA-Z]*>?[\r\n\t]+</td>[\r\n\t]+"
                          "<td[\\s]+align=\\\"center\\\"[\\s]+[A-Za-z\\\"=\\s'.()0-9;]+>"
                          "[\r\n\t]+(<font[\\s]+color=[\\\"sA-Za-z]*)?>?[\r\n\t]+"
                          "<b>([\\d]{1}.[\\d]{3})s</b>[\r\n\t]+<?[/a-zA-Z]*>?[\r\n\t]+</td>[\r\n\t]+<td[A-Za-z\\\"=\\s'.()0-9;]+>"
                          "[\r\n\t]+(<font[\\s]+color=[\\\"sA-Za-z]*)?>?[\r\n\t]+"
                          "<b>([\\d]?):?([\\d]{2}).([\\d]{3})s</b>[\r\n\t]+<?[/a-zA-Z]*>?[\r\n\t]+</td>[\r\n\t]+"
                          "<td[A-Za-z\\\"=\\s'.()0-9;]+>[\r\n\t]+([\\d]{1,3})[\r\n\t]+</td>[\r\n\t]+"
                          "<td[A-Za-z\\\"=\\s'.()0-9;]+>[\r\n\t]+([\\d]{1,3})[\r\n\t]+</td>[\r\n\t]+"
                          "<td[A-Za-z\\\"=\\s'.()0-9;]+>[\r\n\t]+([\\d]{1,3})[\r\n\t]+</td>[\r\n\t]+"
                          "<td[A-Za-z\\\"=\\s'.()0-9;]+>[\r\n\t]+([\\d]{1,3})[\r\n\t]+</td>[\r\n\t]+"
                          "<td[A-Za-z\\\"=\\s'.()0-9;]+>[\r\n\t]+([\\d]{1,3})[\r\n\t]+</td>[\r\n\t]+"
                          "<td[A-Za-z\\\"=\\s'.()0-9;]+>[\r\n\t]+([\\d]{1,3})[\r\n\t]+</td>[\r\n\t]+"
                          "<td[A-Za-z\\\"=\\s'.()0-9;]+>[\r\n\t]+([A-Za-z\\s]+)[\r\n\t]+</td>[\r\n\t]+"
                          "<td[A-Za-z\\\"=\\s'.()0-9;:]+><input[A-Za-z\\\"=\\s'.()0-9;:]+"
                          "(<br><b>Wings</b>:[\\s]+([A-Za-z\\s.]+))?"
                          "(<br><b>Engine</b>:[\\s]+([A-Za-z\\s.]+))?"
                          "(<br><b>Brakes</b>:[\\s]+([A-Za-z\\s.]+))?"
                          "(<br><b>Gear</b>:[\\s]+([A-Za-z\\s.]+))?"
                          "(<br><b>Suspension</b>:[\\s]+([A-Za-z\\s.]+))?");

    regexp.setPattern(str_pattern);
    iter = regexp.globalMatch(reply_str);

    PracticeLap practice_lap;

    while(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();
        practice_lap.setLapTime( match.captured(1).toDouble() * 60 + match.captured(2).toDouble() + match.captured(3).toDouble() / 1000);
        practice_lap.setDriverMistake( match.captured(5).toDouble() + match.captured(6).toDouble() / 1000);
        practice_lap.setNetTime( match.captured(7).toDouble() * 60 + match.captured(8).toDouble() + match.captured(9).toDouble() / 1000);
        practice_lap.setFrontWing( match.captured(10).toInt() );
        practice_lap.setRearWing( match.captured(11).toInt() );
        practice_lap.setEngine( match.captured(12).toInt() );
        practice_lap.setBrakes( match.captured(13).toInt() );
        practice_lap.setGear( match.captured(14).toInt() );
        practice_lap.setSuspension( match.captured(15).toInt() );
        practice_lap.setTyre( match.captured(16) );

        practice_lap.setWingComment( match.captured(18) );
        practice_lap.setEngineComment( match.captured(20) );
        practice_lap.setBrakesComment( match.captured(22) );
        practice_lap.setGearComment( match.captured(24) );
        practice_lap.setSuspensionComment( match.captured(26) );

        practice_lap.setID(track_id);
        practice_lap.setSeason(season);

        plaps.append(practice_lap);
    }

    return plaps;
}

QList<Lap> ParseAlgorithms::parseLapsfromRace(QString text)
{
    QList<Lap> laps;

    QString reply_str = text;

    QString str_pattern;
    QRegularExpression regexp;
    QRegularExpressionMatchIterator iter;
    //

    str_pattern = QString("<a[\\sa-z=\\\"A-Z.?0-9&\\-;:]+>([\\d]{1,2})</a></td>"
                          "[\r\n\t]+<td[\\sa-z=\\\"]+>(((<font[\\sa-z\\\"=]+>)?([\\-0-9.:]*(</font>)?))|(<font[\\sa-z\\\"=]+><b>([a-zA-Z\\s]+)</b></font>))</td>"
                          "[\r\n\t]*<td[\\sa-zA-Z\\\"=]+><font[\\sa-zA-Z0-9\\\"#=]+><b>([\\d]{1,2})</b></font></td>"
                          "[\r\n\t]*<td[\\sa-zA-Z\\\"=]+><font[\\sa-zA-Z0-9\\\"#=]+><b>([a-zA-Z\\s]+)</b></font></td>"
                          "[\r\n\t]*<td[\\sa-zA-Z\\\"=]+>[\r\n\t<>b]*([a-zA-Z ]+)[<>b/\r\n\t]*</td>"
                          "[\r\n\t]*<td[\\sa-zA-Z\\\"=]+>([\\d]{1,2})[0-9#;&]+</td>[\r\n\t]*<td[\\sa-zA-Z\\\"=]+>([\\d]{1,2})%</td>"
                          "[\r\n\t]*<td[\\sa-zA-Z\\\"=]+>(<font[a-zA-Z\\\"=\\s]+>)?([a-zA-Z\\-\\s]+)");

    regexp.setPattern(str_pattern);
    iter = regexp.globalMatch(reply_str);



    while(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();

        Lap lap;

        lap.setLapNum(match.captured(1).toInt());
        lap.setLapTime(match.captured(5));
        lap.setPos(match.captured(9).toInt());
        lap.setTyreType(match.captured(10));
        lap.setWeather(match.captured(11));
        lap.setTemperature(match.captured(12).toInt());
        lap.setHumidity(match.captured(13).toInt());
        lap.setEvents(match.captured(15));

        laps.append(lap);
    }

    return laps;
}

QList<Stint> ParseAlgorithms::parseStintsfromRace(QString text, Track track)
{
    QList<Stint> stints;

    QList<Lap> race_laps = parseLapsfromRace(text);

    QString reply_str = text;

    QString str_pattern;
    QRegularExpression regexp;
    QRegularExpressionMatchIterator iter;

    str_pattern = QString("Stop[&a-z;]+([\\d]{1,2})<br[ ]+/>[(Lap&nbsp;]+([\\d]{1,2})[)]+</td>[\r\n\t]+"
                          "<td[ a-z\\\"=]+>([ a-zA-Z]+)</td>[\r\n\t]+<td[ a-z\\\"=]+><font[ a-z=\\\"#0-9A-Z]+>"
                          "<b>([\\d]{1,2})%</b></font></td>[\r\n\t]+<td[ a-z\\\"=]+><b>([\\d]{1,2})%</b></td>[\r\n\t]+<td[ a-z\\\"=]+>"
                          "<b>([\\d]{1,3})[ a-z]+</b></font></td>[\r\n\t]+<td[ a-z\\\"=]+><b>([\\d.]+)s</b></font></td>[\r\n\t]+</tr>[\r\n\t]*");

    regexp.setPattern(str_pattern);
    iter = regexp.globalMatch(reply_str);

    QString start_fuel_pattern = QString("Start[ ]+fuel:[ ]+<b>([\\d]{1,3})");
    QRegularExpression start_fuel_regexp(start_fuel_pattern);
    QRegularExpressionMatchIterator start_fuel_iter = start_fuel_regexp.globalMatch(reply_str);

    double start_fuel = 0;

    if(start_fuel_iter.hasNext()) {
        QRegularExpressionMatch match = start_fuel_iter.next();
        start_fuel = match.captured(1).toDouble();
    }

    int start_lap = 1;

    if(!iter.hasNext()) return stints;

    while(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();

        Stint stint;

        int laps = match.captured(2).toInt()-start_lap + 1;
        double fuel_used = start_fuel - 180 * (match.captured(5).toDouble() / 100);
        double fuel_consumption = (track.getLapLength() * laps) / fuel_used;
        double avg_temperature = 0;
        double avg_humidity = 0;
        QString tyre_type;
        QString weather;
        double km = laps * track.getLapLength();
        double pit_stop = match.captured(7).toDouble();

        for(int i = start_lap; i < match.captured(2).toInt()+1 && i < race_laps.size(); ++i) {
            Lap temp_lap = race_laps.at(i);

            if(i == start_lap) tyre_type = temp_lap.getTyreType().toString();

            avg_temperature += temp_lap.getTemperature();
            avg_humidity += temp_lap.getHumidity();

            if(temp_lap.getWeather() != QString("Rain") && weather.size() == 0) weather = QString("Dry");
            else if(temp_lap.getWeather() == QString("Rain") && weather.size() == 0) weather = QString("Wet");
            else if(temp_lap.getWeather() != QString("Rain") && weather == QString("Wet")) weather = QString("Mixed");
            else if(temp_lap.getWeather() == QString("Rain") && weather == QString("Dry")) weather = QString("Mixed");
        }

        avg_temperature /= laps;
        avg_humidity /= laps;

        stint.setLaps(laps);
        stint.setFuelUsed(fuel_used);
        stint.setFuelConsumption(fuel_consumption);
        stint.setFinalP(match.captured(4).toInt());
        stint.setAvgTemperature(avg_temperature);
        stint.setAvgHumidity(avg_humidity);
        stint.setTyreType(tyre_type);
        stint.setWeather(weather);
        stint.setKM(km);
        stint.setStartLap(start_lap);
        stint.setPitStop(pit_stop);
        stint.setStartFuel(start_fuel);

        /*qDebug() << match.captured(1) << match.captured(2) << match.captured(3) << match.captured(4) << match.captured(5) <<
                    match.captured(6) << match.captured(7);
        qDebug() << stint.getLaps() << stint.getFuelUsed() << stint.getFuelConsumption() << stint.getFinalP() <<
                    stint.getAvgTemperature() << stint.getAvgHumidity() << stint.getTyreType().toString() <<
                    stint.getWeather() << stint.getKM() << stint.getPitStop() << stint.getStartFuel();*/

        start_lap = match.captured(2).toInt() + 1;
        start_fuel = match.captured(6).toDouble();

        stints.append(stint);
    }

    QString end_str_pattern = QString("Tyres[ ]+condition[ ]+after[ ]finish:[ ]+<b><font[a-z =\\\"#A-Z0-9]+>"
                                      "([\\d]{1,3})%</font></b></p>[\r\n\t]+<p>[A-Za-z: ]+<b>([\\d]{1,3})[ ]+liters</b></p>[\r\n\t]+");
    QRegularExpression end_regexp(end_str_pattern);
    QRegularExpressionMatchIterator end_iter = end_regexp.globalMatch(reply_str);

    if(end_iter.hasNext()) {
        QRegularExpressionMatch match = end_iter.next();

        Stint stint;

        int laps = race_laps.size() - start_lap;
        double fuel_used = start_fuel - match.captured(2).toInt();
        double fuel_consumption = (track.getLapLength() * laps) / fuel_used;
        double avg_temperature = 0;
        double avg_humidity = 0;
        QString tyre_type;
        QString weather;
        double km = laps * track.getLapLength();
        double pit_stop = -1;

        for(int i = start_lap; i < race_laps.size(); ++i) {
            Lap temp_lap = race_laps.at(i);

            if(i == start_lap) tyre_type = temp_lap.getTyreType().toString();

            avg_temperature += temp_lap.getTemperature();
            avg_humidity += temp_lap.getHumidity();

            if(temp_lap.getWeather() != QString("Rain") && weather.size() == 0) weather = QString("Dry");
            else if(temp_lap.getWeather() == QString("Rain") && weather.size() == 0) weather = QString("Wet");
            else if(temp_lap.getWeather() != QString("Rain") && weather == QString("Wet")) weather = QString("Mixed");
            else if(temp_lap.getWeather() == QString("Rain") && weather == QString("Dry")) weather = QString("Mixed");

        }

        avg_temperature /= laps;
        avg_humidity /= laps;

        stint.setLaps(laps);
        stint.setFuelUsed(fuel_used);
        stint.setFuelConsumption(fuel_consumption);
        stint.setFinalP(match.captured(1).toInt());
        stint.setAvgTemperature(avg_temperature);
        stint.setAvgHumidity(avg_humidity);
        stint.setTyreType(tyre_type);
        stint.setWeather(weather);
        stint.setKM(km);
        stint.setStartLap(start_lap);
        stint.setPitStop(pit_stop);
        stint.setStartFuel(start_fuel);

        /*qDebug() << stint.getLaps() << stint.getFuelUsed() << stint.getFuelConsumption() << stint.getFinalP() <<
                    stint.getAvgTemperature() << stint.getAvgHumidity() << stint.getTyreType().toString() <<
                    stint.getWeather() << stint.getKM() << stint.getStartLap() << stint.getPitStop() << stint.getStartFuel();*/

        stints.append(stint);
    }

    Stint full_stint;

    int laps = race_laps.size() - 1;
    double fuel_used = 0;
    foreach(Stint loc_stint, stints) {
        fuel_used += loc_stint.getFuelUsed();
    }

    double fuel_consumption = (track.getLapLength() * laps) / fuel_used;
    double avg_temperature = 0;
    double avg_humidity = 0;
    QString tyre_type;
    QString weather;
    double km = laps * track.getLapLength();

    for(int i = 1; i < race_laps.size(); ++i) {
        Lap temp_lap = race_laps.at(i);

        avg_temperature += temp_lap.getTemperature();
        avg_humidity += temp_lap.getHumidity();

        if(temp_lap.getWeather() != QString("Rain") && weather.size() == 0) weather = QString("Dry");
        else if(temp_lap.getWeather() == QString("Rain") && weather.size() == 0) weather = QString("Wet");
        else if(temp_lap.getWeather() != QString("Rain") && weather == QString("Wet")) weather = QString("Mixed");
        else if(temp_lap.getWeather() == QString("Rain") && weather == QString("Dry")) weather = QString("Mixed");
    }



    avg_temperature /= laps;
    avg_humidity /= laps;

    full_stint.setLaps(laps);
    full_stint.setFuelUsed(fuel_used);
    full_stint.setFuelConsumption(fuel_consumption);
    full_stint.setFinalP(-1);
    full_stint.setAvgTemperature(avg_temperature);
    full_stint.setAvgHumidity(avg_humidity);
    full_stint.setTyreType(tyre_type);
    full_stint.setWeather(weather);
    full_stint.setKM(km);
    full_stint.setStartLap(1);
    full_stint.setPitStop(-1);
    full_stint.setStartFuel(-1);

    stints.append(full_stint);

    /*qDebug() << full_stint.getLaps() << full_stint.getFuelUsed() << full_stint.getFuelConsumption() << full_stint.getFinalP() <<
                full_stint.getAvgTemperature() << full_stint.getAvgHumidity() << full_stint.getTyreType().toString() <<
                full_stint.getWeather() << full_stint.getKM() << full_stint.getStartLap() << full_stint.getPitStop() << full_stint.getStartFuel();*/


    return stints;
}
