#include "practicealgorithms.h"

#include <mlpack/methods/linear_regression/linear_regression.hpp>

#include <QSettings>
#include <QDebug>

#include "settingconstants.h"

using namespace mlpack::regression;

uvec PracticeAlgorithms::getVector(double value, shared_ptr<mat> full_data, QString type)
{
    if(full_data->n_elem == 0) return uvec();

    QMap<int,int> data;

    int counter = 0;

    for(uword i = 0; i < full_data->n_rows; ++i) {
        if(!data.contains(full_data->at(i,getIndex(type)))) {
            data.insert(full_data->at(i,getIndex(type)),counter);
            ++counter;
        }
    }

    int rows = data.size();
    uvec ret_vec(rows);

    for(int i = 0; i < rows; ++i) {
        ret_vec(i) = 0;
    }

    ret_vec(data.value(value)) = 1;

    return ret_vec;
}

mat PracticeAlgorithms::generateTrainingSet(shared_ptr<mat> full_data, QString comment, QString part, QString part_2)
{
    if(full_data->n_elem == 0) return mat();

    int p2_size = part_2.size() > 0 ? 1 : 0;
    uvec rand_track_vec = getVector(full_data->at(0,getIndex(QString("TrackID"))), full_data, QString("TrackID"));
    uvec rand_comment_vec = getVector(full_data->at(0,getIndex(comment)), full_data, comment);
    mat return_training_set(full_data->n_rows, 6 + rand_track_vec.n_elem + rand_comment_vec.n_elem + p2_size);

    for(uword i = 0; i < full_data->n_rows; ++i) {
        uvec track_vec = getVector(full_data->at(i,getIndex(QString("TrackID"))), full_data, QString("TrackID"));
        uvec comment_vec = getVector(full_data->at(i,getIndex(comment)), full_data, comment);

        if(full_data->at(i,getIndex(QString("Q1Status"))) == Weather::Mixed) return_training_set(i,0) = -1;
        else if(full_data->at(i,getIndex(QString("Q1Status"))) == Weather::Wet) return_training_set(i,0) = 1;
        else return_training_set(i,0) = 0;

        return_training_set(i,1) = full_data->at(i,getIndex(QString("Q1Temperature")));
        return_training_set(i,2) = full_data->at(i,getIndex(QString("Q1Humidity")));

        return_training_set(i,3) = full_data->at(i,getIndex(QString("DExperience")));
        return_training_set(i,4) = full_data->at(i,getIndex(QString("DTechInsight")));
        return_training_set(i,5) = full_data->at(i,getIndex(QString("DExperience"))) * full_data->at(i,getIndex(QString("DTechInsight")));

        return_training_set(i,6) = full_data->at(i,getIndex(part));
        if(p2_size > 0) return_training_set(i,7) = full_data->at(i,getIndex(part_2));

        for(unsigned int j = 7+p2_size; j < 7 + p2_size + track_vec.n_elem; ++j) {
            return_training_set(i,j) = track_vec(j-7-p2_size);
        }

        for(unsigned int j = 7 + p2_size + track_vec.n_elem; j < return_training_set.n_cols; ++j) {
            return_training_set(i,j) = comment_vec(j-7 - p2_size-track_vec.n_elem);
        }

    }

    return return_training_set;
}

vec PracticeAlgorithms::generateResultSet(shared_ptr<mat> full_data, QString part)
{
    if(full_data->n_elem == 0) return vec();

    vec result_set(full_data->n_rows);

    for(uword i = 0; i < full_data->n_rows; ++i) {
        result_set(i) = full_data->at(i,getIndex(part)) > 0 ? full_data->at(i,getIndex(part)) : -1;
    }

    return result_set;
}

uvec PracticeAlgorithms::extractSettingRows(mat training_set, vec result_set)
{
    uword full_row_amount = training_set.n_rows;
    uword full_col_amount = training_set.n_cols;

    QList<uword> row_numbers;

    unsigned int row_amount = 0;

    for(uword i = 0; i < full_row_amount; ++i) {
        bool include_cond = true;

        for(uword j = 0; j < full_col_amount; ++j) {
            include_cond &= (training_set(i,j) >= 0);
        }

        include_cond &= result_set(i) >= 0;

        if(include_cond) {
            row_amount++;
            row_numbers.append(i);
        }
    }

    uvec rows(row_amount);

    for(uword i = 0; i < row_amount; ++i) {
        rows.at(i) = row_numbers.at(i);
    }

    return rows;
}

mat PracticeAlgorithms::generateTyreDiffTrainingSet(shared_ptr<mat> full_data)
{
    if(full_data->n_elem == 0) return mat();

    mat return_training_set(full_data->n_rows, 3);

    uword temperature_index = getTDIndex(QString("Q1Temperature"));
    uword humidity_index = getTDIndex(QString("Q1Humidity"));
    uword corners_index = getTDIndex(QString("TCorners"));
    uword lap_length_index = getTDIndex(QString("TLapLength"));

    for(uword i = 0; i < full_data->n_rows; ++i) {
        return_training_set(i,0) = full_data->at(i, temperature_index);
        return_training_set(i,1) = full_data->at(i, humidity_index);
        return_training_set(i,2) = full_data->at(i, corners_index) / full_data->at(i, lap_length_index);
    }

    return return_training_set;
}

vec PracticeAlgorithms::generateTyreDiffResultSet(shared_ptr<mat> full_data)
{
    if(full_data->n_elem == 0) return vec();

    vec result_set(full_data->n_rows);

    uword soft_time_index = getTDIndex(QStringLiteral("SoftTime"));
    uword es_time_index = getTDIndex(QStringLiteral("ESTime"));
    uword lap_length_index = getTDIndex(QStringLiteral("TLapLength"));

    result_set = (full_data->col(soft_time_index) - full_data->col(es_time_index)) /
            full_data->col(lap_length_index);

    return result_set;
}

uvec PracticeAlgorithms::extractTyreDiffRows(const mat &training_set, const vec &result_set)
{
    uword full_row_amount = training_set.n_rows;
    uword full_col_amount = training_set.n_cols;

    QList<uword> row_numbers;

    unsigned int row_amount = 0;

    for(uword i = 0; i < full_row_amount; ++i) {
        bool include_cond = true;

        for(uword j = 0; j < full_col_amount; ++j) {
            include_cond &= (training_set(i,j) >= 0);
        }

        include_cond &= result_set(i) >= 0;

        if(include_cond) {
            row_amount++;
            row_numbers.append(i);
        }
    }

    uvec rows(row_amount);

    for(uword i = 0; i < row_amount; ++i) {
        rows.at(i) = row_numbers.at(i);
    }

    return rows;
}

void PracticeAlgorithms::SSWingCalc()
{
    mat wing_ts = generateTrainingSet(practice_lap_data_, QString("WingComment"), QString("CFrontWing"), QString("CRearWing"));
    vec wing_cmb = (generateResultSet(practice_lap_data_, QString("FrontWing"))+generateResultSet(practice_lap_data_, QString("RearWing"))) / 2;

    uvec wing_rows = extractSettingRows(wing_ts, wing_cmb);

    wing_ts = wing_ts.rows(wing_rows);
    wing_cmb = wing_cmb.rows(wing_rows);

    wing_training_set_ = shared_ptr<mat>(new mat(wing_ts));
    wing_result_set_ = shared_ptr<vec>(new vec(wing_cmb));

    LinearRegression lr(wing_training_set_->t(), *wing_result_set_, 1);

    wing_error_ = lr.ComputeError(wing_training_set_->t(), *wing_result_set_);

    wing_parameters_ = shared_ptr<vec>(new vec(lr.Parameters()));

    qDebug() << "Wing error: " << wing_error_;
}

void PracticeAlgorithms::SSEngineCalc()
{
    mat engine_ts = generateTrainingSet(practice_lap_data_, QString("EngineComment"), QString("CEngine"));
    vec engine_cmb = generateResultSet(practice_lap_data_, QString("Engine"));

    uvec engine_rows = extractSettingRows(engine_ts, engine_cmb);

    engine_ts = engine_ts.rows(engine_rows);
    engine_cmb = engine_cmb.rows(engine_rows);

    engine_training_set_ = shared_ptr<mat>(new mat(engine_ts));
    engine_result_set_ = shared_ptr<vec>(new vec(engine_cmb));

    LinearRegression lr(engine_training_set_->t(), *engine_result_set_, 1);

    engine_error_ = lr.ComputeError(engine_training_set_->t(), *engine_result_set_);

    engine_parameters_ = shared_ptr<vec>(new vec(lr.Parameters()));

    qDebug() << "Engine error: " << engine_error_;
}

void PracticeAlgorithms::SSBrakesCalc()
{
    mat brakes_ts = generateTrainingSet(practice_lap_data_, QString("BrakesComment"), QString("CBrakes"));
    vec brakes_cmb = generateResultSet(practice_lap_data_, QString("Brakes"));

    uvec brakes_rows = extractSettingRows(brakes_ts, brakes_cmb);

    brakes_ts = brakes_ts.rows(brakes_rows);
    brakes_cmb = brakes_cmb.rows(brakes_rows);

    brakes_training_set_ = shared_ptr<mat>(new mat(brakes_ts));
    brakes_result_set_ = shared_ptr<vec>(new vec(brakes_cmb));

    LinearRegression lr(brakes_training_set_->t(), *brakes_result_set_, 1);

    brakes_error_ = lr.ComputeError(brakes_training_set_->t(), *brakes_result_set_);

    brakes_parameters_ = shared_ptr<vec>(new vec(lr.Parameters()));

    qDebug() << "Brakes error: " << brakes_error_;
}

void PracticeAlgorithms::SSGearCalc()
{
    mat gear_ts = generateTrainingSet(practice_lap_data_, QString("GearComment"), QString("CGearbox"));
    vec gear_cmb = generateResultSet(practice_lap_data_, QString("Gear"));

    uvec gear_rows = extractSettingRows(gear_ts, gear_cmb);

    gear_ts = gear_ts.rows(gear_rows);
    gear_cmb = gear_cmb.rows(gear_rows);

    gear_training_set_ = shared_ptr<mat>(new mat(gear_ts));
    gear_result_set_ = shared_ptr<vec>(new vec(gear_cmb));

    LinearRegression lr(gear_training_set_->t(), *gear_result_set_, 1);

    gear_error_ = lr.ComputeError(gear_training_set_->t(), *gear_result_set_);

    gear_parameters_ = shared_ptr<vec>(new vec(lr.Parameters()));

    qDebug() << "Gear error: " << gear_error_;
}

void PracticeAlgorithms::SSSuspensionCalc()
{
    mat suspension_ts = generateTrainingSet(practice_lap_data_, QString("SuspensionComment"), QString("CSuspension"));
    vec suspension_cmb = generateResultSet(practice_lap_data_, QString("Suspension"));

    uvec suspension_rows = extractSettingRows(suspension_ts, suspension_cmb);

    suspension_ts = suspension_ts.rows(suspension_rows);
    suspension_cmb = suspension_cmb.rows(suspension_rows);

    suspension_training_set_ = shared_ptr<mat>(new mat(suspension_ts));
    suspension_result_set_ = shared_ptr<vec>(new vec(suspension_cmb));

    LinearRegression lr(suspension_training_set_->t(), *suspension_result_set_, 1);

    suspension_error_ = lr.ComputeError(suspension_training_set_->t(), *suspension_result_set_);

    suspension_parameters_ = shared_ptr<vec>(new vec(lr.Parameters()));

    qDebug() << "Suspension error: " << suspension_error_;
}

PracticeAlgorithms::PracticeAlgorithms():
    practice_data_(0),
    practice_lap_data_(0),
    tyre_diff_data_(0),
    range_params_(3),
    wing_training_set_(0),
    wing_result_set_(0),
    wing_parameters_(0),
    wing_error_(0),
    engine_training_set_(0),
    engine_result_set_(0),
    engine_parameters_(0),
    engine_error_(0),
    brakes_training_set_(0),
    brakes_result_set_(0),
    brakes_parameters_(0),
    brakes_error_(0),
    gear_training_set_(0),
    gear_result_set_(0),
    gear_parameters_(0),
    gear_error_(0),
    suspension_training_set_(0),
    suspension_result_set_(0),
    suspension_parameters_(0),
    suspension_error_(0),
    tyre_diff_training_set_(0),
    tyre_diff_result_set_(0),
    tyre_diff_parameters_(0),
    tyre_diff_error_(0)
{
    vec cmb = {{ 70,120,220,270 }};

    mat data = {{250, 0, 250, 0},{250, 250, 0, 0}};

    LinearRegression lr(data, cmb, 0.5);

    range_params_ = lr.Parameters();
}

void PracticeAlgorithms::setPracticeLapData(shared_ptr<mat> practice_laps, QStringList labels)
{
    practice_lap_data_ = practice_laps;
    practice_lap_labels_ = labels;
    practice_lap_labels_indexes_.clear();

    int counter = 0;
    foreach (QString str, practice_lap_labels_) {
        practice_lap_labels_indexes_.insert(str,counter);
        counter++;
    }
}

arma::vec PracticeAlgorithms::getSettingsDiff(double temperature, double humidity)
{
    if(practice_data_ == 0) return arma::vec();

    //qDebug() << practice_data_labels_;
    //int counter = 0;
    //foreach (QString str, practice_data_labels_) {
    //    qDebug() << str << " index: " << counter;
    //    counter++;
    //}

    /*arma::Col<arma::uword> wing_data_cols = {{0,8,9,10,14,15,16,17,19,24,25,32,33}};
    arma::Col<arma::uword> engine_data_cols = {{0,8,9,10,14,15,16,17,19,24,25,31}};
    arma::Col<arma::uword> brakes_data_cols = {{0,8,9,10,14,15,16,17,19,24,25,38}};
    arma::Col<arma::uword> gear_data_cols = {{0,8,9,10,14,15,16,17,19,24,25,37}};
    arma::Col<arma::uword> suspension_data_cols = {{0,8,9,10,14,15,16,17,19,24,25,39}};

    arma::vec wing_cmb = (practice_data_->col(1)+practice_data_->col(2)) / 2;
    arma::vec engine_cmb = practice_data_->col(3);
    arma::vec brakes_cmb = practice_data_->col(4);
    arma::vec gear_cmb = practice_data_->col(5);
    arma::vec suspension_cmb = practice_data_->col(6);

    LinearRegression lr_wing(practice_data_->cols( wing_data_cols ).t(), wing_cmb, 0.5);
    LinearRegression lr_engine(practice_data_->cols( engine_data_cols ).t(), engine_cmb, 0.5);
    LinearRegression lr_brakes(practice_data_->cols( brakes_data_cols ).t(), brakes_cmb, 0.5);
    LinearRegression lr_gear(practice_data_->cols( gear_data_cols ).t(), gear_cmb, 0.5);
    LinearRegression lr_suspension(practice_data_->cols( suspension_data_cols ).t(), suspension_cmb, 0.5);

    arma::vec co_eff_wing = lr_wing.Parameters();
    arma::vec co_eff_engine = lr_engine.Parameters();
    arma::vec co_eff_brakes = lr_brakes.Parameters();
    arma::vec co_eff_gear = lr_gear.Parameters();
    arma::vec co_eff_suspension = lr_suspension.Parameters();

    arma::vec results = {{temperature*co_eff_wing(3)+humidity*co_eff_wing(4),
                         temperature*co_eff_engine(3)+humidity*co_eff_engine(4),
                         temperature*co_eff_brakes(3)+humidity*co_eff_brakes(4),
                         temperature*co_eff_gear(3)+humidity*co_eff_gear(4),
                         temperature*co_eff_suspension(3)+humidity*co_eff_suspension(4)}};*/

    if( wing_parameters_ == 0 || engine_parameters_ == 0 ||  brakes_parameters_ == 0 ||
          gear_parameters_ == 0 ||  suspension_parameters_ == 0 ) {
        getSuggestedSettings(0,0,0,0,0,0,0,temperature, humidity, 0,0,0,0);
    }

    arma::vec results = {{ wing_parameters_->at(2) * temperature + wing_parameters_->at(3) * humidity,
                         engine_parameters_->at(2) * temperature + engine_parameters_->at(3) * humidity,
                           brakes_parameters_->at(2) * temperature + brakes_parameters_->at(3) * humidity,
                         gear_parameters_->at(2) * temperature + gear_parameters_->at(3) * humidity,
                        suspension_parameters_->at(2) * temperature + suspension_parameters_->at(3) * humidity }};

    return results;
}

double PracticeAlgorithms::getCommentRange()
{
    QSettings settings(General::ProgramName, General::CompanyName);
    int exp = settings.value(Settings::DriverExperienceText,"0").toInt();
    int tech_in = settings.value(Settings::DriverTechnicalInsightText,"0").toInt();
    return (range_params_.at(0) + exp * range_params_.at(1) + tech_in * range_params_.at(2)) / 2;
}

array<int,6> PracticeAlgorithms::getSuggestedSettings(int cfwing, int crwing, int cengine, int cbrakes, int cgear, int csuspension,
                                                      uword weather, int temperature, int humidity, int dexp, int dtech, int track_id, uword comment)
{    
    QMutexLocker mutex_locker(&mutex_);

    array<int,6> suggested_settings;

    if(practice_lap_data_ == 0) return suggested_settings;

    uvec track_vec = getVector(track_id, practice_lap_data_, QString("TrackID"));

    QFuture< void > wing_calc_future;
    QFuture< void > engine_calc_future;
    QFuture< void > brakes_calc_future;
    QFuture< void > gear_calc_future;
    QFuture< void > suspension_calc_future;

    if(wing_training_set_ == 0 || wing_result_set_ == 0 || wing_parameters_ == 0) {
        wing_calc_future = QtConcurrent::run(this, &PracticeAlgorithms::SSWingCalc);
    }

    if(engine_training_set_ == 0 || engine_result_set_ == 0 || engine_parameters_ == 0) {
        engine_calc_future = QtConcurrent::run(this, &PracticeAlgorithms::SSEngineCalc);
    }

    if( brakes_training_set_ == 0 || brakes_result_set_ == 0 || brakes_parameters_ == 0 ) {
        brakes_calc_future = QtConcurrent::run(this, &PracticeAlgorithms::SSBrakesCalc);
    }

    if( gear_training_set_ == 0 || gear_result_set_ == 0 || gear_parameters_ == 0 ) {
        gear_calc_future = QtConcurrent::run(this, &PracticeAlgorithms::SSGearCalc);
    }

    if( suspension_training_set_ == 0 || suspension_result_set_ == 0 || suspension_parameters_ == 0 ) {
        suspension_calc_future = QtConcurrent::run(this, &PracticeAlgorithms::SSSuspensionCalc);
    }

    if(wing_calc_future.isRunning())
        wing_calc_future.waitForFinished();

    if(engine_calc_future.isRunning())
        engine_calc_future.waitForFinished();

    if(brakes_calc_future.isRunning())
        brakes_calc_future.waitForFinished();

    if(gear_calc_future.isRunning())
        gear_calc_future.waitForFinished();

    if(suspension_calc_future.isRunning())
        suspension_calc_future.waitForFinished();

    double wing_setting = wing_parameters_->at(0);
    wing_setting += weather == Weather::Wet ? wing_parameters_->at(1) : 0;
    wing_setting += wing_parameters_->at(2) * temperature;
    wing_setting += wing_parameters_->at(3) * humidity;
    wing_setting += wing_parameters_->at(4) * dexp;
    wing_setting += wing_parameters_->at(5) * dtech;
    wing_setting += wing_parameters_->at(6) * dexp * dtech;
    wing_setting += wing_parameters_->at(7) * cfwing;
    wing_setting += wing_parameters_->at(8) * crwing;
    for(uword i = 0; i < track_vec.n_elem; ++i) {
        wing_setting +=  wing_parameters_->at(9+i) * track_vec(i);
    }
    uvec wing_comment_vec = getVector(comment, practice_lap_data_, QString("WingComment"));
    for(uword i = 0; i < wing_comment_vec.n_elem; ++i) {
        wing_setting +=  wing_parameters_->at(9+track_vec.n_elem+i) * wing_comment_vec(i);
    }

    double engine_setting = engine_parameters_->at(0);
    engine_setting += weather == Weather::Wet ? engine_parameters_->at(1) : 0;
    engine_setting += engine_parameters_->at(2) * temperature;
    engine_setting += engine_parameters_->at(3) * humidity;
    engine_setting += engine_parameters_->at(4) * dexp;
    engine_setting += engine_parameters_->at(5) * dtech;
    engine_setting += engine_parameters_->at(6) * dexp * dtech;
    engine_setting += engine_parameters_->at(7) * cengine;
    for(uword i = 0; i < track_vec.n_elem; ++i) {
        engine_setting +=  engine_parameters_->at(8+i) * track_vec(i);
    }
    uvec engine_comment_vec = getVector(comment, practice_lap_data_, QString("EngineComment"));
    for(uword i = 0; i < engine_comment_vec.n_elem; ++i) {
        engine_setting +=  engine_parameters_->at(8+track_vec.n_elem+i) * engine_comment_vec(i);
    }

    double brakes_setting = brakes_parameters_->at(0);
    brakes_setting += weather == Weather::Wet ? brakes_parameters_->at(1) : 0;
    brakes_setting += brakes_parameters_->at(2) * temperature;
    brakes_setting += brakes_parameters_->at(3) * humidity;
    brakes_setting += brakes_parameters_->at(4) * dexp;
    brakes_setting += brakes_parameters_->at(5) * dtech;
    brakes_setting += brakes_parameters_->at(6) * dexp * dtech;
    brakes_setting += brakes_parameters_->at(7) * cbrakes;
    for(uword i = 0; i < track_vec.n_elem; ++i) {
        brakes_setting +=  brakes_parameters_->at(8+i) * track_vec(i);
    }
    uvec brakes_comment_vec = getVector(comment, practice_lap_data_, QString("BrakesComment"));
    for(uword i = 0; i < brakes_comment_vec.n_elem; ++i) {
        brakes_setting +=  brakes_parameters_->at(8+track_vec.n_elem+i) * brakes_comment_vec(i);
    }

    double gear_setting = gear_parameters_->at(0);
    gear_setting += weather == Weather::Wet ? gear_parameters_->at(1) : 0;
    gear_setting += gear_parameters_->at(2) * temperature;
    gear_setting += gear_parameters_->at(3) * humidity;
    gear_setting += gear_parameters_->at(4) * dexp;
    gear_setting += gear_parameters_->at(5) * dtech;
    gear_setting += gear_parameters_->at(6) * dexp * dtech;
    gear_setting += gear_parameters_->at(7) * cgear;
    for(uword i = 0; i < track_vec.n_elem; ++i) {
        gear_setting +=  gear_parameters_->at(8+i) * track_vec(i);
    }
    uvec gear_comment_vec = getVector(comment, practice_lap_data_, QString("GearComment"));
    for(uword i = 0; i < gear_comment_vec.n_elem; ++i) {
        gear_setting +=  gear_parameters_->at(8+track_vec.n_elem+i) * gear_comment_vec(i);
    }

    double suspension_setting = suspension_parameters_->at(0);
    suspension_setting += weather == Weather::Wet ? suspension_parameters_->at(1) : 0;
    suspension_setting += suspension_parameters_->at(2) * temperature;
    suspension_setting += suspension_parameters_->at(3) * humidity;
    suspension_setting += suspension_parameters_->at(4) * dexp;
    suspension_setting += suspension_parameters_->at(5) * dtech;
    suspension_setting += suspension_parameters_->at(6) * dexp * dtech;
    suspension_setting += suspension_parameters_->at(7) * csuspension;
    for(uword i = 0; i < track_vec.n_elem; ++i) {
        suspension_setting +=  suspension_parameters_->at(8+i) * track_vec(i);
    }
    uvec suspension_comment_vec = getVector(comment, practice_lap_data_, QString("SuspensionComment"));
    for(uword i = 0; i < suspension_comment_vec.n_elem; ++i) {
        suspension_setting +=  suspension_parameters_->at(8+track_vec.n_elem+i) * suspension_comment_vec(i);
    }

    suggested_settings.at(0) = wing_setting;
    suggested_settings.at(1) = wing_setting;
    suggested_settings.at(2) = engine_setting;
    suggested_settings.at(3) = brakes_setting;
    suggested_settings.at(4) = gear_setting;
    suggested_settings.at(5) = suspension_setting;

    return suggested_settings;
}

array<int, 6> PracticeAlgorithms::getWeatherDiff(bool to_rain)
{
    if (wing_training_set_ == 0 || wing_result_set_ == 0 || wing_parameters_ == 0 ||
            engine_training_set_ == 0 || engine_result_set_ == 0 || engine_parameters_ == 0 ||
            brakes_training_set_ == 0 || brakes_result_set_ == 0 || brakes_parameters_ == 0 ||
            gear_training_set_ == 0 || gear_result_set_ == 0 || gear_parameters_ == 0 ||
            suspension_training_set_ == 0 || suspension_result_set_ == 0 || suspension_parameters_ == 0 ) {
        // loading missing stuff
        getSuggestedSettings(0,0,0,0,0,0,0,0,0,0,0,0,0);
    }

    array<int,6> result = {{ (int)std::round(wing_parameters_->at(1)), (int)std::round(wing_parameters_->at(1)), (int)std::round(engine_parameters_->at(1)),
                             (int)std::round(brakes_parameters_->at(1)), (int)std::round(gear_parameters_->at(1)), (int)std::round(suspension_parameters_->at(1)) }};

    if(!to_rain) {
        unsigned int size = result.size();
        for(unsigned int i = 0; i < size; ++i) {
            result.at(i) *= -1;
        }
    }

    return result;
}

double PracticeAlgorithms::getTyreDiff(double temperature, double humidity, int corners, double lap_length)
{
    if(tyre_diff_data_ == 0) return 0;

    if(tyre_diff_training_set_ == 0 || tyre_diff_result_set_ == 0 || tyre_diff_parameters_ == 0) {

        mat tyre_diff_ts = generateTyreDiffTrainingSet(tyre_diff_data_);
        vec tyre_diff_rs = generateTyreDiffResultSet(tyre_diff_data_);

        uvec row_numbers = extractTyreDiffRows(tyre_diff_ts, tyre_diff_rs);

        mat training_set_2 = tyre_diff_ts.rows(row_numbers);
        vec result_set_2 = tyre_diff_rs.rows(row_numbers);

        tyre_diff_training_set_ = shared_ptr<mat>(new mat(training_set_2));
        tyre_diff_result_set_ = shared_ptr<vec>(new vec(result_set_2));

        LinearRegression lr(tyre_diff_training_set_->t(), *tyre_diff_result_set_, 1);

        tyre_diff_error_ = lr.ComputeError(tyre_diff_training_set_->t(), *tyre_diff_result_set_);

        tyre_diff_parameters_ = shared_ptr<vec>(new vec(lr.Parameters()));

        qDebug() << "TyreDiff error: " << tyre_diff_error_;
    }

    double tyre_diff = tyre_diff_parameters_->at(0);
    tyre_diff += temperature * tyre_diff_parameters_->at(1);
    tyre_diff += humidity * tyre_diff_parameters_->at(2);
    tyre_diff += (double)corners / lap_length * tyre_diff_parameters_->at(3);

    return tyre_diff;
}
