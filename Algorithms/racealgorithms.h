#ifndef RACEALGORITHMS_H
#define RACEALGORITHMS_H

#include <memory>

#include <QString>
#include <QStringList>
#include <QMap>
#include <QList>
#include <QMutex>
#include <QMutexLocker>
#include <QtConcurrent/QtConcurrentRun>
#include <QFuture>

#include <mlpack/core.hpp>
#include <mlpack/methods/linear_regression/linear_regression.hpp>

#include "Objects/track.h"
#include "Objects/weather.h"

using std::shared_ptr;

using namespace arma;
using namespace mlpack::regression;

class RaceAlgorithms
{
private:
    QMutex mutex_;

    shared_ptr<mat> race_data_;
    QStringList race_data_labels_;
    QMap<QString,uword> race_data_labels_indexes_;

    //chassis
    shared_ptr<mat> chassis_wear_training_set_;
    shared_ptr<vec> chassis_wear_result_set_;
    shared_ptr<vec> chassis_wear_parameters_;
    double chassis_wear_error_;

    //engine
    shared_ptr<mat> engine_wear_training_set_;
    shared_ptr<vec> engine_wear_result_set_;
    shared_ptr<vec> engine_wear_parameters_;
    double engine_wear_error_;

    //front wing
    shared_ptr<mat> front_wing_wear_training_set_;
    shared_ptr<vec> front_wing_wear_result_set_;
    shared_ptr<vec> front_wing_wear_parameters_;
    double front_wing_wear_error_;

    // rear wing
    shared_ptr<mat> rear_wing_wear_training_set_;
    shared_ptr<vec> rear_wing_wear_result_set_;
    shared_ptr<vec> rear_wing_wear_parameters_;
    double rear_wing_wear_error_;

    //underbody
    shared_ptr<mat> underbody_wear_training_set_;
    shared_ptr<vec> underbody_wear_result_set_;
    shared_ptr<vec> underbody_wear_parameters_;
    double underbody_wear_error_;

    //sidepods
    shared_ptr<mat> sidepods_wear_training_set_;
    shared_ptr<vec> sidepods_wear_result_set_;
    shared_ptr<vec> sidepods_wear_parameters_;
    double sidepods_wear_error_;

    //cooling
    shared_ptr<mat> cooling_wear_training_set_;
    shared_ptr<vec> cooling_wear_result_set_;
    shared_ptr<vec> cooling_wear_parameters_;
    double cooling_wear_error_;

    //gear
    shared_ptr<mat> gearbox_wear_training_set_;
    shared_ptr<vec> gearbox_wear_result_set_;
    shared_ptr<vec> gearbox_wear_parameters_;
    double gearbox_wear_error_;

    //brakes
    shared_ptr<mat> brakes_wear_training_set_;
    shared_ptr<vec> brakes_wear_result_set_;
    shared_ptr<vec> brakes_wear_parameters_;
    double brakes_wear_error_;

    //suspension
    shared_ptr<mat> suspension_wear_training_set_;
    shared_ptr<vec> suspension_wear_result_set_;
    shared_ptr<vec> suspension_wear_parameters_;
    double suspension_wear_error_;

    //electronics
    shared_ptr<mat> electronics_wear_training_set_;
    shared_ptr<vec> electronics_wear_result_set_;
    shared_ptr<vec> electronics_wear_parameters_;
    double electronics_wear_error_;

    //driver overall
    shared_ptr<mat> doverall_training_set_;
    shared_ptr<vec> doverall_result_set_;
    shared_ptr<vec>  doverall_parameters_;
    double doverall_error_;

    int getIndex(QString str) { return race_data_labels_indexes_.value(str); }

    uvec getTrackVector(int track_id, shared_ptr<mat> full_data);

    mat generateWearTrainingSet(shared_ptr<mat> full_data, QString part);
    vec generateWearResultSet(shared_ptr<mat> full_data, QString part);
    uvec extractWearRows(const mat &training_set, const vec &result_set);

    mat generateOverallTrainingSet(shared_ptr<mat> full_data);
    vec generateOverallResultSet(shared_ptr<mat> full_data);

    void WChassisCalc();
    void WEngineCalc();
    void WFrontWingCalc();
    void WRearWingCalc();
    void WUnderbodyCalc();
    void WSidepodsCalc();
    void WCoolingCalc();
    void WGearboxCalc();
    void WBrakesCalc();
    void WSuspensionCalc();
    void WElectronicsCalc();

public:
    RaceAlgorithms();

    void setRaceData( shared_ptr<mat> race_data ) {
        race_data_ = race_data;
    }

    inline void setRaceDataLabelsIndexes( const QMap<QString,uword> &labels_indexes ) { race_data_labels_indexes_ = labels_indexes;  }

    double getEstimatedOverall(int concentration, int talent, int aggressiveness, int experience, int techinsight, int stamina, int charisma, int motivation, int weight, int age, int reputation);

    QList<double> getEstimatedWears(int track_id, int clear_track, int dconcentration, int dtalent, int dexp,
                                    int cchassis, int cengine, int cfront_wing, int crear_wing, int cunderbody, int csidepods, int ccooling, int cgearbox, int cbrakes, int csuspension, int celectronics);


};

#endif // RACEALGORITHMS_H
