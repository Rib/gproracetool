#include "stintalgorithms.h"
#include "Objects/tyretype.h"
#include "Objects/weather.h"
#include "Objects/track.h"

#include <QDebug>

mat StintAlgorithms::generateTyreWearTrainingSet(shared_ptr<mat> full_data)
{
    if(full_data == 0) return mat();

    mat return_training_set(full_data->n_rows, 21);

    for(uword i = 0; i < full_data->n_rows; ++i) {
        return_training_set(i,0) = (uword)(full_data->at(i,stint_data_label_indexes_.value(QString("TTyreWear"))) == Track::TWVERY_LOW);
        return_training_set(i,1) = (uword)(full_data->at(i,stint_data_label_indexes_.value(QString("TTyreWear"))) == Track::TWLOW);
        return_training_set(i,2) = (uword)(full_data->at(i,stint_data_label_indexes_.value(QString("TTyreWear"))) == Track::TWMEDIUM);
        return_training_set(i,3) = (uword)(full_data->at(i,stint_data_label_indexes_.value(QString("TTyreWear"))) == Track::TWHIGH);
        return_training_set(i,4) = (uword)(full_data->at(i,stint_data_label_indexes_.value(QString("TTyreWear"))) == Track::TWVERY_HIGH);
        return_training_set(i,5) = (uword)(full_data->at(i,stint_data_label_indexes_.value(QString("SWeather"))) == Weather::Wet);
        return_training_set(i,6) = full_data->at(i,stint_data_label_indexes_.value(QString("STemperature")));
        return_training_set(i,7) = full_data->at(i,stint_data_label_indexes_.value(QString("SHumidity")));
        return_training_set(i,8) = (uword)(full_data->at(i,stint_data_label_indexes_.value(QString("STyreType"))) == TyreType::TTEXTRA_SOFT);
        return_training_set(i,9) = (uword)(full_data->at(i,stint_data_label_indexes_.value(QString("STyreType"))) == TyreType::TTSOFT);
        return_training_set(i,10) = (uword)(full_data->at(i,stint_data_label_indexes_.value(QString("STyreType"))) == TyreType::TTMEDIUM);
        return_training_set(i,11) = (uword)(full_data->at(i,stint_data_label_indexes_.value(QString("STyreType"))) == TyreType::TTHARD);
        return_training_set(i,12) = (uword)(full_data->at(i,stint_data_label_indexes_.value(QString("STyreType"))) == TyreType::TTRAIN);

        if(full_data->at(i,stint_data_label_indexes_.value(QString("SWeather"))) == Weather::Wet) {
             return_training_set(i,13) = full_data->at(i,stint_data_label_indexes_.value(QString("RWet")));
        } else {
             return_training_set(i,13) = full_data->at(i,stint_data_label_indexes_.value(QString("RClear")));
        }

        return_training_set(i,14) = full_data->at(i,stint_data_label_indexes_.value(QString("DExperience")));
        return_training_set(i,15) = full_data->at(i,stint_data_label_indexes_.value(QString("DAggressiveness")));
        return_training_set(i,16) = full_data->at(i,stint_data_label_indexes_.value(QString("DWeight")));
        return_training_set(i,17) = full_data->at(i,stint_data_label_indexes_.value(QString("CSuspension")));

        return_training_set(i,18) = (uword)(full_data->at(i,stint_data_label_indexes_.value(QString("TSuspension"))) == Track::SSOFT);
        return_training_set(i,19) = (uword)(full_data->at(i,stint_data_label_indexes_.value(QString("TSuspension"))) == Track::SMEDIUM);
        return_training_set(i,20) = (uword)(full_data->at(i,stint_data_label_indexes_.value(QString("TSuspension"))) == Track::SHARD);

    }

    return return_training_set;
}

vec StintAlgorithms::generateTyreWearResultSet(shared_ptr<mat> full_data)
{
    if(full_data == 0) return vec();

    if(full_data->n_elem == 0 || !stint_data_label_indexes_.contains(QStringLiteral("SFinalP")) ||
            !stint_data_label_indexes_.contains(QStringLiteral("SKm")) ) return vec();

    Col<uword> list = {{ stint_data_label_indexes_.value(QString("SFinalP")),
                                   stint_data_label_indexes_.value(QString("SKm"))}};

    vec final_p = full_data->col(list.at(0));

    vec km = full_data->col(list.at(1));

    vec result_set = (100 - final_p) / km;

    return result_set;
}

Col<uword> StintAlgorithms::getTyreWearCols()
{
    Col<uword> list = {{ stint_data_label_indexes_.value(QString("SWeather")),
                                   stint_data_label_indexes_.value(QString("TTyreWear")),
                                   stint_data_label_indexes_.value(QString("STemperature")),
                                   stint_data_label_indexes_.value(QString("SHumidity")),
                                   stint_data_label_indexes_.value(QString("STyreType")),
                                   stint_data_label_indexes_.value(QString("RClear")),
                                   stint_data_label_indexes_.value(QString("RWet")),
                                   stint_data_label_indexes_.value(QString("DExperience")),
                                   stint_data_label_indexes_.value(QString("DAggressiveness")),
                                   stint_data_label_indexes_.value(QString("DWeight")),
                                   stint_data_label_indexes_.value(QString("CSuspension")),
                                   stint_data_label_indexes_.value(QString("TSuspension"))}};

    return list;
}

uvec StintAlgorithms::extractTyreWearRows(shared_ptr<mat> full_data)
{
    if(full_data == 0 || full_data->n_elem == 0) return uvec();

    uword weather_col_index = stint_data_label_indexes_.value(QString("SWeather"));
    uword tyre_wear_index = stint_data_label_indexes_.value(QString("TTyreWear"));
    uword temperature_index = stint_data_label_indexes_.value(QString("STemperature"));
    uword humidity_index = stint_data_label_indexes_.value(QString("SHumidity"));
    uword tyre_type_index = stint_data_label_indexes_.value(QString("STyreType"));
    uword rclear_index = stint_data_label_indexes_.value(QString("RClear"));
    uword experience_index = stint_data_label_indexes_.value(QString("DExperience"));
    uword aggro_index = stint_data_label_indexes_.value(QString("DAggressiveness"));
    uword weight_index = stint_data_label_indexes_.value(QString("DWeight"));
    uword car_suspension_index = stint_data_label_indexes_.value(QString("CSuspension"));
    uword track_suspension_index = stint_data_label_indexes_.value(QString("TSuspension"));

    vec weather_col = full_data->col(weather_col_index);
    uword full_row_amount = full_data->n_rows;
    uword full_col_amount = full_data->n_cols;

    QList<uword> row_numbers;

    int row_amount = 0;

    for(uword i = 0; i < full_row_amount; ++i) {
        bool include_cond = true;

        for(uword j = 0; j < full_col_amount; ++j) {

            if(j == weather_col_index)
                include_cond &= (full_data->at(i,j) < Weather::Mixed && full_data->at(i,j) >= 0);

            if(j == tyre_wear_index || j == temperature_index || j == humidity_index ||
                    j == tyre_type_index || j == rclear_index || j == experience_index ||
                    j == aggro_index || j == weight_index || j == car_suspension_index ||
                    j == track_suspension_index)
                include_cond &= (full_data->at(i,j) >= 0);


        }

        if(include_cond) {
            row_amount++;
            row_numbers.append(i);
        }
    }

    uvec rows(row_amount);

    for(uword i = 0; i < row_amount; ++i) {
        rows.at(i) = row_numbers.at(i);
    }

    return rows;
}

mat StintAlgorithms::generateFuelConsumptionTrainingSet(shared_ptr<mat> full_data)
{
    if (full_data != 0) {
        mat return_training_set(full_data->n_rows, 16);

        for(uword i = 0; i < full_data->n_rows; ++i) {
            return_training_set(i,0) = (uword)(full_data->at(i,stint_data_label_indexes_.value(QString("SWeather"))) == Weather::Wet);
            return_training_set(i,1) = full_data->at(i,stint_data_label_indexes_.value(QString("CEngine")));
            return_training_set(i,2) = full_data->at(i,stint_data_label_indexes_.value(QString("CElectronics")));
            return_training_set(i,3) = (uword)(full_data->at(i,stint_data_label_indexes_.value(QString("TFuelConsumption"))) == Track::FCVERY_LOW);
            return_training_set(i,4) = (uword)(full_data->at(i,stint_data_label_indexes_.value(QString("TFuelConsumption"))) == Track::FCLOW);
            return_training_set(i,5) = (uword)(full_data->at(i,stint_data_label_indexes_.value(QString("TFuelConsumption"))) == Track::FCMEDIUM);
            return_training_set(i,6) = (uword)(full_data->at(i,stint_data_label_indexes_.value(QString("TFuelConsumption"))) == Track::FCHIGH);
            return_training_set(i,7) = (uword)(full_data->at(i,stint_data_label_indexes_.value(QString("TFuelConsumption"))) == Track::FCVERY_HIGH);
            return_training_set(i,8) = (uword)(full_data->at(i,stint_data_label_indexes_.value(QString("TGrip"))) == Track::GVERY_LOW);
            return_training_set(i,9) = (uword)(full_data->at(i,stint_data_label_indexes_.value(QString("TGrip"))) == Track::GLOW);
            return_training_set(i,10) = (uword)(full_data->at(i,stint_data_label_indexes_.value(QString("TGrip"))) == Track::GMEDIUM);
            return_training_set(i,11) = (uword)(full_data->at(i,stint_data_label_indexes_.value(QString("TGrip"))) == Track::GHIGH);
            return_training_set(i,12) = (uword)(full_data->at(i,stint_data_label_indexes_.value(QString("TGrip"))) == Track::GVERY_HIGH);
            return_training_set(i,13) = (uword)(full_data->at(i,stint_data_label_indexes_.value(QString("TDownforce"))) == Track::DLOW);
            return_training_set(i,14) = (uword)(full_data->at(i,stint_data_label_indexes_.value(QString("TDownforce"))) == Track::DMEDIUM);
            return_training_set(i,15) = (uword)(full_data->at(i,stint_data_label_indexes_.value(QString("TDownforce"))) == Track::DHIGH);

        }

        return return_training_set;
    }

    return mat();
}

vec StintAlgorithms::generateFuelConsumptionResultSet(shared_ptr<mat> full_data)
{
    if(full_data->n_elem == 0) return vec();

    uword index = stint_data_label_indexes_.value(QString("SConsumption"));

    return full_data->col(index);
}

Col<uword> StintAlgorithms::getFuelConsumptionCols()
{
    Col<uword> list = {{ stint_data_label_indexes_.value(QString("SWeather")),
                                   stint_data_label_indexes_.value(QString("CEngine")),
                                   stint_data_label_indexes_.value(QString("CElectronics")),
                                   stint_data_label_indexes_.value(QString("TFuelConsumption")),
                                   stint_data_label_indexes_.value(QString("TGrip")),
                                   stint_data_label_indexes_.value(QString("TDownforce")) }};

    return list;
}

uvec StintAlgorithms::extractFuelConsumptionRows(shared_ptr<mat> full_data)
{
    if(full_data->n_elem == 0) return uvec();

    uword consumption_index = stint_data_label_indexes_.value(QString("SConsumption"));
    uword weather_col_index = stint_data_label_indexes_.value(QString("SWeather"));
    uword car_engine_index = stint_data_label_indexes_.value(QString("CEngine"));
    uword car_electronics_index = stint_data_label_indexes_.value(QString("STemperature"));
    uword track_fuel_consumption_index = stint_data_label_indexes_.value(QString("TFuelConsumption"));
    uword track_grip_index = stint_data_label_indexes_.value(QString("TGrip"));
    uword track_downforce_index = stint_data_label_indexes_.value(QString("TDownforce"));

    uword full_row_amount = full_data->n_rows;
    uword full_col_amount = full_data->n_cols;

    QList<uword> row_numbers;

    int row_amount = 0;

    for(uword i = 0; i < full_row_amount; ++i) {
        bool include_cond = true;

        for(uword j = 0; j < full_col_amount; ++j) {

            if(j == weather_col_index)
                include_cond &= (full_data->at(i,j) < Weather::Mixed && full_data->at(i,j) >= 0);

            if(j == consumption_index || j == car_engine_index || j == car_electronics_index ||
                    j == track_fuel_consumption_index || j == track_grip_index || j == track_downforce_index)
                include_cond &= (full_data->at(i,j) >= 0);
        }

        if(include_cond) {
            row_amount++;
            row_numbers.append(i);
        }
    }

    uvec rows(row_amount);

    for(uword i = 0; i < row_amount; ++i) {
        rows.at(i) = row_numbers.at(i);
    }

    return rows;
}

mat StintAlgorithms::generateCTTimeTrainingSet(shared_ptr<mat> full_data)
{
    if(full_data->n_elem == 0) return mat();

    uvec rand_track_vec = getVector(full_data->at(0,ct_time_data_label_indexes_.value(QString("track_id"))), full_data, QString("track_id"));
    mat return_training_set(full_data->n_rows, 17+rand_track_vec.n_elem);

    uword weather_index = ct_time_data_label_indexes_.value(QString("SWeather"));
    uword temperature_index = ct_time_data_label_indexes_.value(QString("STemperature"));
    uword humidity_index = ct_time_data_label_indexes_.value(QString("SHumidity"));
    uword stamina_index = ct_time_data_label_indexes_.value(QString("DStamina"));
    uword ct_index = ct_time_data_label_indexes_.value(QString("ClearTrack"));
    uword start_fuel_index = ct_time_data_label_indexes_.value(QString("SStartFuel"));
    uword stint_fuel_index = ct_time_data_label_indexes_.value(QString("SFuel"));
    uword car_power_index = ct_time_data_label_indexes_.value(QString("CPower"));
    uword car_handling_index = ct_time_data_label_indexes_.value(QString("CHandling"));
    uword car_acceleration_index = ct_time_data_label_indexes_.value(QString("CAcceleration"));
    //uword track_power_index = ct_time_data_label_indexes_.value(QString("TPower"));
    //uword track_handling_index = ct_time_data_label_indexes_.value(QString("THandling"));
    //uword track_acceleration_index = ct_time_data_label_indexes_.value(QString("TAcceleration"));
    uword driver_concentration_index = ct_time_data_label_indexes_.value(QString("DConcentration"));
    uword driver_talent_index = ct_time_data_label_indexes_.value(QString("DTalent"));
    uword driver_aggressiveness_index = ct_time_data_label_indexes_.value(QString("DAggressiveness"));
    uword driver_experience_index = ct_time_data_label_indexes_.value(QString("DExperience"));
    uword driver_motivation_index = ct_time_data_label_indexes_.value(QString("DMotivation"));
    uword driver_weight_index = ct_time_data_label_indexes_.value(QString("DWeight"));
    uword track_id_index = ct_time_data_label_indexes_.value(QString("track_id"));

    for(uword i = 0; i < full_data->n_rows; ++i) {
        uvec track_vec = getVector(full_data->at(i,track_id_index), full_data, QString("track_id"));

        return_training_set(i,0) = (uword)(full_data->at(i,weather_index) == Weather::Wet);
        return_training_set(i,1) = full_data->at(i,temperature_index);
        return_training_set(i,2) = full_data->at(i, humidity_index);
        return_training_set(i,3) = full_data->at(i, stamina_index);
        return_training_set(i,4) = full_data->at(i, ct_index);

        return_training_set(i,5) = full_data->at(i,stamina_index) * full_data->at(i,ct_index);
        return_training_set(i,6) = full_data->at(i,start_fuel_index);
        return_training_set(i,7) = full_data->at(i,stint_fuel_index);
        return_training_set(i,8) = full_data->at(i,car_power_index);
        return_training_set(i,9) = full_data->at(i, car_handling_index);

        return_training_set(i,10) = full_data->at(i,car_acceleration_index);
        //return_training_set(i,11) = full_data->at(i, track_power_index);
        //return_training_set(i,12) = full_data->at(i,track_handling_index);
        //return_training_set(i,13) = full_data->at(i, track_acceleration_index);
        return_training_set(i,11) = full_data->at(i,driver_concentration_index);

        return_training_set(i,12) = full_data->at(i, driver_talent_index);
        return_training_set(i,13) = full_data->at(i, driver_aggressiveness_index);
        return_training_set(i,14) = full_data->at(i,driver_experience_index);
        return_training_set(i,15) = full_data->at(i,driver_motivation_index);
        return_training_set(i,16) = full_data->at(i, driver_weight_index);

        for(int j = 17; j < 17 + track_vec.n_elem; ++j) {
            return_training_set(i,j) = track_vec(j-17);
        }

    }

    return return_training_set;
}

vec StintAlgorithms::generateCTTimeResultSet(shared_ptr<mat> full_data)
{
    if(full_data->n_elem == 0) return vec();

    vec result_set(full_data->n_rows);

    uword time_index = ct_time_data_label_indexes_.value(QString("TRLapTime"));
    uword lap_lenght_index = ct_time_data_label_indexes_.value(QString("TLapLength"));

    result_set = full_data->col(time_index) / full_data->col(lap_lenght_index);

    return result_set;
}

Col<uword> StintAlgorithms::getCTTimeCols()
{
}

uvec StintAlgorithms::extractCTTimeRows(const mat &training_set, const vec &result_set)
{
    uword full_row_amount = training_set.n_rows;
    uword full_col_amount = training_set.n_cols;

    QList<uword> row_numbers;

    int row_amount = 0;

    for(uword i = 0; i < full_row_amount; ++i) {
        bool include_cond = true;

        for(uword j = 0; j < full_col_amount; ++j) {
            include_cond &= (training_set(i,j) >= 0);
        }

        include_cond &= result_set(i) >= 0;

        if(include_cond) {
            row_amount++;
            row_numbers.append(i);
        }
    }

    uvec rows(row_amount);

    for(uword i = 0; i < row_amount; ++i) {
        rows.at(i) = row_numbers.at(i);
    }

    return rows;
}

mat StintAlgorithms::generateFuelTimeTrainingSet(shared_ptr<mat> full_data)
{
    if(full_data == 0) return mat();

    mat return_training_set(full_data->n_rows, 9);

    uword q1_status_index = fuel_time_data_label_indexes_.value(QString("Q1Status"));
    uword q2_status_index = fuel_time_data_label_indexes_.value(QString("Q2Status"));
    uword q1_temperature_index = fuel_time_data_label_indexes_.value(QString("Q1Temperature"));
    uword q2_temperature_index = fuel_time_data_label_indexes_.value(QString("Q2Temperature"));
    uword q1_humidity_index = fuel_time_data_label_indexes_.value(QString("Q1Humidity"));
    uword q2_humidity_index = fuel_time_data_label_indexes_.value(QString("Q2Humidity"));
    uword dstamina_index = fuel_time_data_label_indexes_.value(QString("DStamina"));
    uword start_fuel_index = fuel_time_data_label_indexes_.value(QString("SStartFuel"));

    for(uword i = 0; i < full_data->n_rows; ++i) {

        return_training_set(i,0) = (uword)(full_data->at(i,q1_status_index) == Weather::Wet);
        return_training_set(i,1) = (uword)(full_data->at(i,q2_status_index) == Weather::Wet);
        return_training_set(i,2) = full_data->at(i, q1_temperature_index);
        return_training_set(i,3) = full_data->at(i, q2_temperature_index);
        return_training_set(i,4) = full_data->at(i, q1_humidity_index);
        return_training_set(i,5) = full_data->at(i,q2_humidity_index);

        return_training_set(i,6) = full_data->at(i,dstamina_index);
        return_training_set(i,7) = full_data->at(i,start_fuel_index);
        return_training_set(i,8) = full_data->at(i,dstamina_index) * full_data->at(i,start_fuel_index);
    }

    return return_training_set;
}

vec StintAlgorithms::generateFuelTimeResultSet(shared_ptr<mat> full_data)
{
    if(full_data->n_elem == 0) return vec();

    vec result_set(full_data->n_rows);

    uword q1_time_index = fuel_time_data_label_indexes_.value(QString("Q1Time"));
    uword q2_time_index = fuel_time_data_label_indexes_.value(QString("Q2Time"));
    uword lap_length_index = fuel_time_data_label_indexes_.value(QString("TLapLength"));

    result_set = (full_data->col(q2_time_index)-full_data->col(q1_time_index)) / full_data->col(lap_length_index);

    return result_set;
}

Col<uword> StintAlgorithms::getFuelTimeCols()
{

}

uvec StintAlgorithms::extractFuelTimeRows(const mat &training_set, const vec &result_set)
{
    uword full_row_amount = training_set.n_rows;
    uword full_col_amount = training_set.n_cols;

    QList<uword> row_numbers;

    int row_amount = 0;

    for(uword i = 0; i < full_row_amount; ++i) {
        bool include_cond = true;

        for(uword j = 0; j < full_col_amount; ++j) {
            include_cond &= (training_set(i,j) >= 0);
        }

        include_cond &= result_set(i) >= 0;

        if(include_cond) {
            row_amount++;
            row_numbers.append(i);
        }
    }

    uvec rows(row_amount);

    for(uword i = 0; i < row_amount; ++i) {
        rows.at(i) = row_numbers.at(i);
    }

    return rows;
}

uvec StintAlgorithms::getVector(double value, shared_ptr<mat> full_data, QString type)
{
    QMap<int,int> data;

    int counter = 0;

    for(uword i = 0; i < full_data->n_rows; ++i) {
        if(!data.contains(full_data->at(i,ct_time_data_label_indexes_.value(type)))) {
            data.insert(full_data->at(i,ct_time_data_label_indexes_.value(type)),counter);
            ++counter;
        }
    }

    int rows = data.size();
    uvec ret_vec(rows);

    for(int i = 0; i < rows; ++i) {
        ret_vec(i) = 0;
    }

    ret_vec(data.value(value)) = 1;

    return ret_vec;
}

StintAlgorithms::StintAlgorithms():
    stint_data_(0),
    stint_data_labels_(),
    stint_data_label_indexes_(),
    tyre_wear_training_set_(0),
    tyre_wear_result_set_(0),
    tyre_wear_parameters_(0),
    tyre_wear_error_(0),
    fuel_consumption_training_set_(0),
    fuel_consumption_result_set_(0),
    fuel_consumption_parameters_(0),
    fuel_consumption_error_(0),
    ct_time_data_(0),
    ct_time_data_labels_(),
    ct_time_data_label_indexes_(),
    ct_time_training_set_(0),
    ct_time_result_set_(0),
    ct_time_parameters_(0),
    ct_time_error_(0),
    fuel_time_data_(0),
    fuel_time_data_label_indexes_(),
    fuel_time_training_set_(0),
    fuel_time_result_set_(0),
    fuel_time_parameters_(0),
    fuel_time_error_(0)
{

}

void StintAlgorithms::setStintDataLabels(QList<QString> labels, bool override)
{
    if (stint_data_labels_.size() == 0 || override) {
        stint_data_labels_ = labels;

        int counter = 0;
        foreach (QString str, stint_data_labels_) {
            stint_data_label_indexes_.insert(str,counter);
            counter++;
        }
    }
}

double StintAlgorithms::getTyreWear(uword track_tyre_wear, uword weather, double temperature, double humidity,
                                    uword tyre_type, double clear_track, double driver_experience, double driver_aggressiveness, double driver_weight,
                                    double car_suspension, uword track_suspension)
{
    if(stint_data_ == 0 || stint_data_->n_elem == 0) return 0;

    if (tyre_wear_training_set_ == 0 || tyre_wear_result_set_ == 0 || tyre_wear_parameters_ == 0) {
        mat training_set = generateTyreWearTrainingSet(stint_data_);
        vec result_set = generateTyreWearResultSet(stint_data_);

        uvec row_numbers = extractTyreWearRows(stint_data_);

        mat training_set_2 = training_set.rows(row_numbers);
        vec result_set_2 = result_set.rows(row_numbers);

        tyre_wear_training_set_ = shared_ptr<mat>(new mat(training_set_2));
        tyre_wear_result_set_ = shared_ptr<vec>(new vec(result_set_2));

        LinearRegression lr(tyre_wear_training_set_->t(), *tyre_wear_result_set_, 0.5);

        tyre_wear_error_ = lr.ComputeError(tyre_wear_training_set_->t(), *tyre_wear_result_set_);

        tyre_wear_parameters_ = shared_ptr<vec>(new vec(lr.Parameters()));
    }

    double tyre_wear = tyre_wear_parameters_->at(0);
    tyre_wear += track_tyre_wear == Track::TWVERY_LOW ? tyre_wear_parameters_->at(1) : 0;
    tyre_wear += track_tyre_wear == Track::TWLOW ? tyre_wear_parameters_->at(2) : 0;
    tyre_wear += track_tyre_wear == Track::TWMEDIUM ? tyre_wear_parameters_->at(3) : 0;
    tyre_wear += track_tyre_wear == Track::TWHIGH ? tyre_wear_parameters_->at(4) : 0;
    tyre_wear += track_tyre_wear == Track::TWVERY_HIGH ? tyre_wear_parameters_->at(5) : 0;
    tyre_wear += weather == Weather::Wet ? tyre_wear_parameters_->at(6) : 0;
    tyre_wear += temperature * tyre_wear_parameters_->at(7);
    tyre_wear += humidity * tyre_wear_parameters_->at(8);
    tyre_wear += tyre_type == TyreType::TTEXTRA_SOFT ? tyre_wear_parameters_->at(9) : 0;
    tyre_wear += tyre_type == TyreType::TTSOFT ? tyre_wear_parameters_->at(10) : 0;
    tyre_wear += tyre_type == TyreType::TTMEDIUM ? tyre_wear_parameters_->at(11) : 0;
    tyre_wear += tyre_type == TyreType::TTHARD ? tyre_wear_parameters_->at(12) : 0;
    tyre_wear += tyre_type == TyreType::TTRAIN ? tyre_wear_parameters_->at(13) : 0;
    tyre_wear += clear_track * tyre_wear_parameters_->at(14);
    tyre_wear += driver_experience * tyre_wear_parameters_->at(15);
    tyre_wear += driver_aggressiveness * tyre_wear_parameters_->at(16);
    tyre_wear += driver_weight * tyre_wear_parameters_->at(17);
    tyre_wear += car_suspension * tyre_wear_parameters_->at(18);
    tyre_wear += track_suspension == Track::SSOFT ? tyre_wear_parameters_->at(19) : 0;
    tyre_wear += track_suspension == Track::SMEDIUM ? tyre_wear_parameters_->at(20) : 0;
    tyre_wear += track_suspension == Track::SHARD ? tyre_wear_parameters_->at(21) : 0;

    return tyre_wear;
}

double StintAlgorithms::getFuelConsumption(uword weather, double car_engine, double car_electronics, uword track_fuel_consumption,
                                           uword track_grip, uword track_downforce)
{
    if(stint_data_ == 0) return 0;

    if (fuel_consumption_training_set_ == 0 || fuel_consumption_result_set_ == 0 || fuel_consumption_parameters_ == 0) {
        mat training_set = generateFuelConsumptionTrainingSet(stint_data_);
        vec result_set = generateFuelConsumptionResultSet(stint_data_);

        uvec row_numbers = extractFuelConsumptionRows(stint_data_);

        mat training_set_2 = training_set.rows(row_numbers);
        vec result_set_2 = result_set.rows(row_numbers);

        fuel_consumption_training_set_ = shared_ptr<mat>(new mat(training_set_2));
        fuel_consumption_result_set_ = shared_ptr<vec>(new vec(result_set_2));

        LinearRegression lr(fuel_consumption_training_set_->t(), *fuel_consumption_result_set_, 0.5);

        fuel_consumption_error_ = lr.ComputeError(fuel_consumption_training_set_->t(), *fuel_consumption_result_set_);

        fuel_consumption_parameters_ = shared_ptr<vec>(new vec(lr.Parameters()));
    }

    double fuel_consumption = fuel_consumption_parameters_->at(0);
        fuel_consumption += weather == Weather::Wet ? fuel_consumption_parameters_->at(1) : 0;
        fuel_consumption += car_engine * fuel_consumption_parameters_->at(2);
        fuel_consumption += car_electronics * fuel_consumption_parameters_->at(3);
        fuel_consumption += track_fuel_consumption == Track::FCVERY_LOW ? fuel_consumption_parameters_->at(4) : 0;
        fuel_consumption += track_fuel_consumption == Track::FCLOW ? fuel_consumption_parameters_->at(5) : 0;
        fuel_consumption += track_fuel_consumption == Track::FCMEDIUM ? fuel_consumption_parameters_->at(6) : 0;
        fuel_consumption += track_fuel_consumption == Track::FCHIGH ? fuel_consumption_parameters_->at(7) : 0;
        fuel_consumption += track_fuel_consumption == Track::FCVERY_HIGH ? fuel_consumption_parameters_->at(8) : 0;
        fuel_consumption += track_grip == Track::GVERY_LOW ? fuel_consumption_parameters_->at(9) : 0;
        fuel_consumption += track_grip == Track::GLOW ? fuel_consumption_parameters_->at(10) : 0;
        fuel_consumption += track_grip == Track::GMEDIUM ? fuel_consumption_parameters_->at(11) : 0;
        fuel_consumption += track_grip == Track::GHIGH ? fuel_consumption_parameters_->at(12) : 0;
        fuel_consumption += track_grip == Track::GVERY_HIGH ? fuel_consumption_parameters_->at(13) : 0;
        fuel_consumption += track_downforce == Track::DLOW ? fuel_consumption_parameters_->at(14) : 0;
        fuel_consumption += track_downforce == Track::DMEDIUM ? fuel_consumption_parameters_->at(15) : 0;
        fuel_consumption += track_downforce == Track::DHIGH ? fuel_consumption_parameters_->at(16) : 0;

        return fuel_consumption;
}

double StintAlgorithms::getCTTime(int clear_track, int dstamina)
{
    if(ct_time_data_ == 0) return 0;

    if(ct_time_training_set_ == 0 || ct_time_result_set_ == 0 || ct_time_parameters_ == 0) {
        mat ct_time_ts = generateCTTimeTrainingSet(ct_time_data_);
        vec ct_time_rs = generateCTTimeResultSet(ct_time_data_);

        uvec row_numbers = extractCTTimeRows(ct_time_ts, ct_time_rs);

        mat training_set_2 = ct_time_ts.rows(row_numbers);
        vec result_set_2 = ct_time_rs.rows(row_numbers);

        ct_time_training_set_ = shared_ptr<mat>(new mat(training_set_2));
        ct_time_result_set_ = shared_ptr<vec>(new vec(result_set_2));

        LinearRegression lr(ct_time_training_set_->t(), *ct_time_result_set_, 1);

        ct_time_error_ = lr.ComputeError(ct_time_training_set_->t(), *ct_time_result_set_);

        ct_time_parameters_ = shared_ptr<vec>(new vec(lr.Parameters()));

        qDebug() << "CTTime error: " << ct_time_error_;
    }

    double ct_time = clear_track * ct_time_parameters_->at(5);
    ct_time += clear_track * dstamina * ct_time_parameters_->at(6);

    return ct_time;
}

double StintAlgorithms::getDriverTime(int stamina, int concentration,  int talent, int aggressiveness, int experience,
                                      int motivation, int weight)
{
    if(ct_time_training_set_ == 0 || ct_time_result_set_ == 0 || ct_time_parameters_ == 0) {
        getCTTime(0,0);
    }

    double dtime = stamina * ct_time_parameters_->at(4);
    //qDebug() << "Stamina" << stamina * ct_time_parameters_->at(4) << stamina;
    dtime += concentration * ct_time_parameters_->at(12);
    //qDebug() << "Concentration" << concentration * ct_time_parameters_->at(12) << concentration;
    dtime += talent * ct_time_parameters_->at(13);
    //qDebug() << "Talent" << talent * ct_time_parameters_->at(13) << talent;
    dtime += aggressiveness * ct_time_parameters_->at(14);
    //qDebug() << "Aggressiveness" << aggressiveness * ct_time_parameters_->at(14) << aggressiveness;
    dtime += experience * ct_time_parameters_->at(15);
    //qDebug() << "Experience" << experience * ct_time_parameters_->at(15) << experience;
    dtime += motivation * ct_time_parameters_->at(16);
    //qDebug() << "Motivation" << motivation * ct_time_parameters_->at(16) << motivation;
    dtime += weight * ct_time_parameters_->at(17);
    //qDebug() << "Weight" << weight * ct_time_parameters_->at(17) << weight;

    return dtime;
}

double StintAlgorithms::getFuelTime(double fuel, int dstamina)
{
    if(fuel_time_data_ == 0) return 0;

    if(fuel_time_training_set_ == 0 || fuel_time_result_set_ == 0 || fuel_time_parameters_ == 0) {
        mat fuel_time_ts = generateFuelTimeTrainingSet(fuel_time_data_);
        vec fuel_time_rs = generateFuelTimeResultSet(fuel_time_data_);

        uvec row_numbers = extractFuelTimeRows(fuel_time_ts, fuel_time_rs);

        mat training_set_2 = fuel_time_ts.rows(row_numbers);
        vec result_set_2 = fuel_time_rs.rows(row_numbers);

        fuel_time_training_set_ = shared_ptr<mat>(new mat(training_set_2));
        fuel_time_result_set_ = shared_ptr<vec>(new vec(result_set_2));

        LinearRegression lr(fuel_time_training_set_->t(), *fuel_time_result_set_, 1);

        fuel_time_error_ = lr.ComputeError(fuel_time_training_set_->t(), *fuel_time_result_set_);

        fuel_time_parameters_ = shared_ptr<vec>(new vec(lr.Parameters()));

        qDebug() << "FuelTime error: " << fuel_time_error_;
    }

    double fuel_time = fuel * fuel_time_parameters_->at(8);
    fuel_time += fuel * dstamina * fuel_time_parameters_->at(9);

    return fuel_time;
}

