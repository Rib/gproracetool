#ifndef STINTALGORITHMS_H
#define STINTALGORITHMS_H

#include <memory>

#include <QString>
#include <QList>
#include <QMap>

#include <mlpack/core.hpp>
#include <mlpack/methods/linear_regression/linear_regression.hpp>

using std::shared_ptr;

using namespace arma;
using namespace mlpack::regression;

class StintAlgorithms
{
private:
    shared_ptr<mat> stint_data_;
    QList<QString> stint_data_labels_;
    QMap<QString,uword> stint_data_label_indexes_;

    shared_ptr<mat> tyre_wear_training_set_;
    shared_ptr<vec> tyre_wear_result_set_;
    shared_ptr<mat> tyre_wear_parameters_;
    double tyre_wear_error_;

    shared_ptr<mat> fuel_consumption_training_set_;
    shared_ptr<mat> fuel_consumption_result_set_;
    shared_ptr<mat> fuel_consumption_parameters_;
    double fuel_consumption_error_;

    shared_ptr<mat> ct_time_data_;
    QStringList ct_time_data_labels_;
    QMap<QString,uword> ct_time_data_label_indexes_;

    shared_ptr<mat> ct_time_training_set_;
    shared_ptr<mat> ct_time_result_set_;
    shared_ptr<mat> ct_time_parameters_;
    double ct_time_error_;

    shared_ptr<mat> fuel_time_data_;
    QMap<QString,uword> fuel_time_data_label_indexes_;

    shared_ptr<mat> fuel_time_training_set_;
    shared_ptr<mat> fuel_time_result_set_;
    shared_ptr<mat> fuel_time_parameters_;
    double fuel_time_error_;

    mat generateTyreWearTrainingSet(shared_ptr<mat> full_data);
    vec generateTyreWearResultSet(shared_ptr<mat> full_data);
    Col<uword> getTyreWearCols();
    uvec extractTyreWearRows(shared_ptr<mat> full_data);

    mat generateFuelConsumptionTrainingSet(shared_ptr<mat> full_data);
    vec generateFuelConsumptionResultSet(shared_ptr<mat> full_data);
    Col<uword> getFuelConsumptionCols();
    uvec extractFuelConsumptionRows(shared_ptr<mat> full_data);

    mat generateCTTimeTrainingSet(shared_ptr<mat> full_data);
    vec generateCTTimeResultSet(shared_ptr<mat> full_data);
    Col<uword> getCTTimeCols();
    uvec extractCTTimeRows(const mat &training_set, const vec &result_set);

    mat generateFuelTimeTrainingSet(shared_ptr<mat> full_data);
    vec generateFuelTimeResultSet(shared_ptr<mat> full_data);
    Col<uword> getFuelTimeCols();
    uvec extractFuelTimeRows(const mat &training_set, const vec &result_set);

    uvec getVector(double value, shared_ptr<mat> full_data, QString type);

public:
    StintAlgorithms();

    void setStintData(shared_ptr<mat> stint_data, bool override = false) { if(stint_data_ == 0 || override) stint_data_ = stint_data; }
    void setStintDataLabels(QList<QString> labels, bool override = false);

    double getTyreWear(uword track_tyre_wear, uword weather, double temperature, double humidity, uword tyre_type, double clear_track,
                       double driver_experience, double driver_aggressiveness, double driver_weight, double car_suspension,
                       uword track_suspension);

    double getFuelConsumption(uword weather, double car_engine, double car_electronics, uword track_fuel_consumption, uword track_grip, uword track_downforce);

    void setCTTimeData(shared_ptr<mat> ct_time_data, bool override = false) { if(ct_time_data_ == 0 || override) ct_time_data_ = ct_time_data; }
    void setCTTimeDataLabelIndexes(QMap<QString,uword> label_indexes) { ct_time_data_label_indexes_ = label_indexes; }

    double getCTTime(int clear_track, int dstamina);

    //uses ct parameters
    double getDriverTime(int stamina, int concentration, int talent, int aggressiveness, int experience, int motivation, int weight);


    void setFuelTimeData(shared_ptr<mat> fuel_time_data, bool override = false) { if(fuel_time_data_ == 0 || override) fuel_time_data_ = fuel_time_data; }
    void setFuelTimeDataLabelIndexes(QMap<QString,uword> label_indexes) { fuel_time_data_label_indexes_ = label_indexes; }

    double getFuelTime(double fuel, int dstamina);
};

#endif // STINTALGORITHMS_H
