#ifndef PRACTICEALGORITHMS_H
#define PRACTICEALGORITHMS_H

#include <array>
#include <memory>

#include <QList>
#include <QString>
#include <QMap>
#include <QStringList>
#include <QMutex>
#include <QMutexLocker>
#include <QtConcurrent/QtConcurrentRun>
#include <QFuture>

#include <mlpack/core.hpp>

#include "Objects/driver.h"
#include "Objects/practicelap.h"

using std::array;
using std::shared_ptr;

using namespace arma;

class PracticeAlgorithms
{
private:
    QMutex mutex_;

    shared_ptr<mat> practice_data_;
    QMap<QString,uword> practice_data_labels_indexes_;

    shared_ptr<mat> practice_lap_data_;
    QStringList practice_lap_labels_;
    QMap<QString,int> practice_lap_labels_indexes_;

    shared_ptr<mat> tyre_diff_data_;
    QMap<QString,uword> tyre_diff_data_labels_indexes_;

    arma::vec range_params_;

    // wing
    shared_ptr<mat> wing_training_set_;
    shared_ptr<vec> wing_result_set_;
    shared_ptr<vec> wing_parameters_;
    double wing_error_;

    // engine
    shared_ptr<mat> engine_training_set_;
    shared_ptr<vec> engine_result_set_;
    shared_ptr<vec> engine_parameters_;
    double engine_error_;

    // brakes
    shared_ptr<mat> brakes_training_set_;
    shared_ptr<vec> brakes_result_set_;
    shared_ptr<vec> brakes_parameters_;
    double brakes_error_;

    // gear
    shared_ptr<mat> gear_training_set_;
    shared_ptr<vec> gear_result_set_;
    shared_ptr<vec> gear_parameters_;
    double gear_error_;

    // suspension
    shared_ptr<mat> suspension_training_set_;
    shared_ptr<vec> suspension_result_set_;
    shared_ptr<vec> suspension_parameters_;
    double suspension_error_;

    shared_ptr<mat> tyre_diff_training_set_;
    shared_ptr<mat> tyre_diff_result_set_;
    shared_ptr<mat> tyre_diff_parameters_;
    double tyre_diff_error_;

    int getIndex(QString str) { return practice_lap_labels_indexes_.value(str); }

    uvec getVector(double value, shared_ptr<mat> full_data, QString type);

    mat generateTrainingSet(shared_ptr<mat> full_data, QString comment, QString part, QString part_2 = "");
    vec generateResultSet(shared_ptr<mat> full_data, QString part);
    uvec extractSettingRows(mat training_set, vec result_set);

    mat generateTyreDiffTrainingSet(shared_ptr<mat> full_data);
    vec generateTyreDiffResultSet(shared_ptr<mat> full_data);
    uvec extractTyreDiffRows(const mat &training_set, const vec &result_set);

    void SSWingCalc();
    void SSEngineCalc();
    void SSBrakesCalc();
    void SSGearCalc();
    void SSSuspensionCalc();

public:
    PracticeAlgorithms();

    void setPracticeData( shared_ptr<mat> practice_data) { practice_data_ = practice_data; }

    void setPracticeDataLabelsIndexes(QMap<QString,uword> labels_indexes) { practice_data_labels_indexes_ = labels_indexes; }

    void setPracticeLapData(shared_ptr<mat> practice_laps, QStringList labels);

    void setTyreDiffData(shared_ptr<mat> tyre_diff_data, bool override = false) { if(tyre_diff_data_ == 0 || override) tyre_diff_data_ = tyre_diff_data; }
    void setTyreDiffDataLabelIndexes(QMap<QString,uword> label_indexes) { tyre_diff_data_labels_indexes_ = label_indexes; }
    int getTDIndex(QString label) { return tyre_diff_data_labels_indexes_.value(label); }

    arma::vec getSettingsDiff( double temperature, double humidity );

    double getCommentRange();

    array<int, 6> getSuggestedSettings(int cfwing = 0, int crwing = 0, int cengine = 0, int cbrakes = 0, int cgear = 0, int csuspension = 0,
                                        uword weather = 0, int temperature = 0, int humidity = 0, int dexp = 0, int dtech = 0, int track_id = 0, uword comment = 0);

    array<int, 6> getWeatherDiff(bool to_rain = true);

    double getTyreDiff(double temperature, double humidity, int corners, double lap_length);



};

#endif // PRACTICEALGORITHMS_H
