#include "gproserverinterface.h"

#include <QByteArray>
#include <QtNetwork/QNetworkAccessManager>
#include <QDebug>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QUrlQuery>
#include <QUrl>

#include <QList>

#include "Algorithms/parsealgorithms.h"

QStringList gproServerInterface::parseReply(QNetworkReply *rep)
{
    //QList<QNetworkCookie> nw_cookies;
    //cookie_jar_->setCookiesFromUrl(cookie_jar_->cookiesForUrl(rep->url()), rep->url());
    QByteArray bts = rep->readAll();
    QString str(bts);
    //qDebug() << str;
    QRegularExpression regexp("((<(td|th)(.*)>)([\\s|\\S]*?)(</(\\3)>))|(<(img)(.*)>)", QRegularExpression::PatternOptions( QRegularExpression::CaseInsensitiveOption|QRegularExpression::ExtendedPatternSyntaxOption ));

    QRegularExpressionMatchIterator i = regexp.globalMatch(str);

    QStringList words;
    QList<QRegularExpressionMatch*> matches;

    while( i.hasNext() ) {
        QRegularExpressionMatch match = i.next();
        QString word = match.captured(0);
        words << word;
        matches.append(new QRegularExpressionMatch(match));
    }

    if ( words.length() == 0 ) {
        qDebug("Can't find a match.");
        return QStringList();
    }


    matches_ = matches;
    current_list_ = words;
    qDebug() << "Gpro login fetch list size: " << current_list_.length();

    return current_list_;
}

gproServerInterface::gproServerInterface(QObject *parent):
       QObject(parent),
       cookie_manager_(new gproCookieManager(this))
{

}

bool gproServerInterface::gproLogin(QString login_name, QString password)
{
    QUrl url("http://www.gpro.net/gb/Login.asp?langCode=gb");
    QString token = "44f859fcd81dd818894aad49fd429504";
    QString logon = "Login";
    QString logon_fake = "Sign+in";

    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    request.setAttribute(QNetworkRequest::RedirectionTargetAttribute,true);

    QUrlQuery params;
    params.addQueryItem("textLogin", login_name);
    params.addQueryItem("textPassword", password);
    params.addQueryItem("token", token);
    params.addQueryItem("Logon", logon);
    params.addQueryItem("LogonFake", logon_fake);

    url.setQuery(params);

    cookie_manager_->sendLoginRequest(url, url.toEncoded());

    return true;
}

Weather gproServerInterface::getWeather()
{
    //QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QUrl url = QUrl("http://gpro.net/gb/Qualify.asp");

    QNetworkReply *reply = cookie_manager_->sendGetRequest(url);

    QByteArray bts = reply->readAll();
    QString reply_str(bts);

    ParseAlgorithms pa;

    Weather weather = pa.parseWeather(reply_str);

    return weather;
}

Weather gproServerInterface::getTestWeather()
{
    //QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QUrl url = QUrl("http://gpro.net/gb/Testing.asp");

    QNetworkReply *reply = cookie_manager_->sendGetRequest(url);

    QByteArray bts = reply->readAll();
    QString reply_str(bts);

    ParseAlgorithms pa;

    Weather weather = pa.parseTestWeather(reply_str);

    return weather;
}

QString gproServerInterface::getTestTrackName()
{
    //QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QUrl url = QUrl("http://gpro.net/gb/Testing.asp");

    QNetworkReply *reply = cookie_manager_->sendGetRequest(url);

    QByteArray bts = reply->readAll();
    QString reply_str(bts);

    ParseAlgorithms pa;

    QString track = pa.parseTestTrackName(reply_str);

    return track;
}

Driver gproServerInterface::getDriver()
{
    //QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QUrl url = QUrl("http://gpro.net/gb/TrainingSession.asp");

    QNetworkReply *reply = cookie_manager_->sendGetRequest(url);

    QByteArray bts = reply->readAll();
    QString reply_str(bts);

    Driver driver;

    // <th>Stamina<\/th>[\s]*<td>(\d{1,3})<\/td>
    // <th>Stamina</th>[\\s]*<td>(\\d{1,3})(\\S\\s)*</td>
    QRegularExpression regexp("<th>Overall:</th>[.\\s]*<td>(\\d{1,3})[\\S]*</td>");
    QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);
    if(iter.hasNext()) {
        //qDebug() << reply_str;
        driver.setOverall( iter.next().captured(1).toInt() );
    }

    regexp.setPattern("<th>Concentration:</b></td>[\\s]*<td>(\\d{1,3})[\\s\\S]*</td>");
    iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        driver.setConcentration( iter.next().captured(1).toInt() );
    }

    regexp.setPattern("<th>Talent:</th>[.\\s]*<td>(\\d{1,3})[\\S]*</td>");
    iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        driver.setTalent( iter.next().captured(1).toInt() );
    }

    regexp.setPattern("<th>Aggressiveness:</th>[.\\s]*<td>(\\d{1,3})[\\S]*</td>");
    iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        driver.setAggressiveness( iter.next().captured(1).toInt() );
    }

    regexp.setPattern("<th>Experience:</th>[.\\s]*<td>(\\d{1,3})[\\S]*</td>");
    iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        driver.setExperience( iter.next().captured(1).toInt() );
    }

    regexp.setPattern("<th>Technical insight:</th>[.\\s]*<td>(\\d{1,3})[\\S]*</td>");
    iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        driver.setTechnicalInsight( iter.next().captured(1).toInt() );
    }

    regexp.setPattern("<th>Stamina:</th>[.\\s]*<td>(\\d{1,3})[\\S]*</td>");
    iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        driver.setStamina( iter.next().captured(1).toInt() );
    }

    regexp.setPattern("<th>Charisma:</th>[.\\s]*<td>(\\d{1,3})[\\S]*</td>");
    iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        driver.setCharisma( iter.next().captured(1).toInt() );
    }

    regexp.setPattern("<th>Motivation:</th>[.\\s]*<td>(\\d{1,3})[\\S]*</td>");
    iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        driver.setMotivation( iter.next().captured(1).toInt() );
    }

    regexp.setPattern("<th>Reputation:</th>[.\\s]*<td>(\\d{1,3})[\\S]*</td>");
    iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        driver.setReputation( iter.next().captured(1).toInt() );
    }

    regexp.setPattern("<th>Weight[\\S]*:</th>[\\s]*<td>(\\d{1,3})[\\S]*</td>");
    iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        driver.setWeight( iter.next().captured(1).toInt() );
    }

    regexp.setPattern("<th>Age:</th>[.\\s]*<td>(\\d{1,3})[\\S]*</td>");
    iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        driver.setAge( iter.next().captured(1).toInt() );
    }

    regexp.setPattern("<th>Name[:]</th>[\r\n\t]+<td>([A-Za-z ]+)</td>");
    iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        driver.setName( iter.next().captured(1) );
    }

    return driver;
}

Car gproServerInterface::getCar()
{
    QUrl url = QUrl("http://gpro.net/gb/UpdateCar.asp");

    QNetworkReply *reply = cookie_manager_->sendGetRequest(url);

    QByteArray bts = reply->readAll();
    QString reply_str(bts);

    qDebug() << reply_str;

    Car car;

    QString str_pattern = QString("<tr>[\\s]*<td[\\s]+align=[\"]center[\"][\\s]+width=[\"]33%[\"]>") +
            QString("Power</td>[\\s]*<td[\\s]+align=[\"]center[\"][\\s]+width=[\"]33%[\"]>Handling</td>") +
            QString("[\\s]*<td[\\s]+align=[\"]center[\"][\\s]+width=[\"]34%[\"]>Acceleration</td>") +
            QString("[\\s]*</tr>[\\s]*<tr>[\\s]*<td align=[\"]center[\"]>([\\d]{1,3})</td>") +
            QString("[\\s]*<td[\\s]+align=[\"]center[\"]>([\\d]{1,3})</td>") +
            QString("[\\s]*<td[\\s]+align=[\"]center[\"]>([\\d]{1,3})</td>[\\s]*</tr>");

    QRegularExpression regexp(str_pattern);
    QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);
    if(iter.hasNext()) {
        //qDebug() << reply_str;
        QRegularExpressionMatch match = iter.next();
        qDebug() << "Power" << match.captured(1);
        qDebug() << "Handling" << match.captured(2);
        qDebug() << "Acceleration" << match.captured(3);

        car.setPower(match.captured(1).toInt());
        car.setHandling(match.captured(2).toInt());
        car.setAcceleration(match.captured(3).toInt());
    }

    str_pattern = QString("<td>[ ]*<b>Chassis:?</b>[ ]*:?</td>[\r\n\t]+<td[ a-z=\\\"]+>([\\d]{1,3})</td>"
                          "[\r\n\t]+<td><select[ a-z=\\\":0-9%!;A-Z()-]+>[\r\n\t]+"
                          "<option[ a-z=\\\"0-9]+newwear[=\\\"]+([\\d]{1,3})[\\\"]+>");

    regexp.setPattern(str_pattern);
    iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();
        qDebug() << "Chassis lvl: " << match.captured(1);
        qDebug() << "Chassis wear: " << match.captured(2);

        car.setChassis(match.captured(1).toInt(), match.captured(2).toInt());
    }

    str_pattern = QString("<td>[ ]*<b>Engine:?</b>[ ]*:?</td>[\r\n\t]+<td[ a-z=\\\"]+>([\\d]{1,3})</td>"
                          "[\r\n\t]+<td><select[ a-z=\\\":0-9%!;A-Z()-]+>[\r\n\t]+"
                          "<option[ a-z=\\\"0-9]+newwear[=\\\"]+([\\d]{1,3})[\\\"]+>");

    regexp.setPattern(str_pattern);
    iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();
        qDebug() << "Engine lvl: " << match.captured(1);
        qDebug() << "Engine wear: " << match.captured(2);

        car.setEngine(match.captured(1).toInt(), match.captured(2).toInt());
    }

    str_pattern = QString("<td>[ ]*<b>Front[ ]wing:?</b>[ ]*:?</td>[\r\n\t]+<td[ a-z=\\\"]+>([\\d]{1,3})</td>"
                          "[\r\n\t]+<td><select[ a-z=\\\":0-9%!;A-Z()-]+>[\r\n\t]+"
                          "<option[ a-z=\\\"0-9]+newwear[=\\\"]+([\\d]{1,3})[\\\"]+>");

    regexp.setPattern(str_pattern);
    iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();
        qDebug() << "Front Wing lvl: " << match.captured(1);
        qDebug() << "Front Wing wear: " << match.captured(2);

        car.setFrontWing(match.captured(1).toInt(), match.captured(2).toInt());
    }

    str_pattern = QString("<td>[ ]*<b>Rear[ ]wing:?</b>[ ]*:?</td>[\r\n\t]+<td[ a-z=\\\"]+>([\\d]{1,3})</td>"
                          "[\r\n\t]+<td><select[ a-z=\\\":0-9%!;A-Z()-]+>[\r\n\t]+"
                          "<option[ a-z=\\\"0-9]+newwear[=\\\"]+([\\d]{1,3})[\\\"]+>");

    regexp.setPattern(str_pattern);
    iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();
        qDebug() << "Rear Wing lvl: " << match.captured(1);
        qDebug() << "Rear Wing wear: " << match.captured(2);

        car.setRearWing(match.captured(1).toInt(), match.captured(2).toInt());
    }

    str_pattern = QString("<td>[ ]*<b>Underbody:?</b>[ ]*:?</td>[\r\n\t]+<td[ a-z=\\\"]+>([\\d]{1,3})</td>"
                          "[\r\n\t]+<td><select[ a-z=\\\":0-9%!;A-Z()-]+>[\r\n\t]+"
                          "<option[ a-z=\\\"0-9]+newwear[=\\\"]+([\\d]{1,3})[\\\"]+>");

    regexp.setPattern(str_pattern);
    iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();
        qDebug() << "Underbody lvl: " << match.captured(1);
        qDebug() << "Underbody wear: " << match.captured(2);

        car.setUnderbody(match.captured(1).toInt(), match.captured(2).toInt());
    }

    str_pattern = QString("<td>[ ]*<b>Sidepods:?</b>[ ]*:?</td>[\r\n\t]+<td[ a-z=\\\"]+>([\\d]{1,3})</td>"
                          "[\r\n\t]+<td><select[ a-z=\\\":0-9%!;A-Z()-]+>[\r\n\t]+"
                          "<option[ a-z=\\\"0-9]+newwear[=\\\"]+([\\d]{1,3})[\\\"]+>");

    regexp.setPattern(str_pattern);
    iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();
        qDebug() << "Sidepods lvl: " << match.captured(1);
        qDebug() << "Sidepods wear: " << match.captured(2);

        car.setSidepods(match.captured(1).toInt(), match.captured(2).toInt());
    }

    str_pattern = QString("<td>[ ]*<b>Cooling:?</b>[ ]*:?</td>[\r\n\t]+<td[ a-z=\\\"]+>([\\d]{1,3})</td>"
                          "[\r\n\t]+<td><select[ a-z=\\\":0-9%!;A-Z()-]+>[\r\n\t]+"
                          "<option[ a-z=\\\"0-9]+newwear[=\\\"]+([\\d]{1,3})[\\\"]+>");

    regexp.setPattern(str_pattern);
    iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();
        qDebug() << "Cooling lvl: " << match.captured(1);
        qDebug() << "Cooling wear: " << match.captured(2);

        car.setCooling(match.captured(1).toInt(), match.captured(2).toInt());
    }

    str_pattern = QString("<td>[ ]*<b>Gearbox:?</b>[ ]*:?</td>[\r\n\t]+<td[ a-z=\\\"]+>([\\d]{1,3})</td>"
                          "[\r\n\t]+<td><select[ a-z=\\\":0-9%!;A-Z()-]+>[\r\n\t]+"
                          "<option[ a-z=\\\"0-9]+newwear[=\\\"]+([\\d]{1,3})[\\\"]+>");

    regexp.setPattern(str_pattern);
    iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();
        qDebug() << "Gearbox lvl: " << match.captured(1);
        qDebug() << "Gearbox wear: " << match.captured(2);

        car.setGearbox(match.captured(1).toInt(), match.captured(2).toInt());
    }

    str_pattern = QString("<td>[ ]*<b>Brakes:?</b>[ ]*:?</td>[\r\n\t]+<td[ a-z=\\\"]+>([\\d]{1,3})</td>"
                          "[\r\n\t]+<td><select[ a-z=\\\":0-9%!;A-Z()-]+>[\r\n\t]+"
                          "<option[ a-z=\\\"0-9]+newwear[=\\\"]+([\\d]{1,3})[\\\"]+>");

    regexp.setPattern(str_pattern);
    iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();
        qDebug() << "Brakes lvl: " << match.captured(1);
        qDebug() << "Brakes wear: " << match.captured(2);

        car.setBrakes(match.captured(1).toInt(), match.captured(2).toInt());
    }

    str_pattern = QString("<td>[ ]*<b>Suspension:?</b>[ ]*:?</td>[\r\n\t]+<td[ a-z=\\\"]+>([\\d]{1,3})</td>"
                          "[\r\n\t]+<td><select[ a-z=\\\":0-9%!;A-Z()-]+>[\r\n\t]+"
                          "<option[ a-z=\\\"0-9]+newwear[=\\\"]+([\\d]{1,3})[\\\"]+>");

    regexp.setPattern(str_pattern);
    iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();
        qDebug() << "Suspension lvl: " << match.captured(1);
        qDebug() << "Suspension wear: " << match.captured(2);

        car.setSuspension(match.captured(1).toInt(), match.captured(2).toInt());
    }

    str_pattern = QString("<td>[ ]*<b>Electronics:?</b>[ ]*:?</td>[\r\n\t]+<td[ a-z=\\\"]+>([\\d]{1,3})</td>"
                          "[\r\n\t]+<td><select[ a-z=\\\":0-9%!;A-Z()-]+>[\r\n\t]+"
                          "<option[ a-z=\\\"0-9]+newwear[=\\\"]+([\\d]{1,3})[\\\"]+>");

    regexp.setPattern(str_pattern);
    iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();
        qDebug() << "Electronics lvl: " << match.captured(1);
        qDebug() << "Electronics wear: " << match.captured(2);

        car.setElectronics(match.captured(1).toInt(), match.captured(2).toInt());
    }

    return car;
}

Track gproServerInterface::getTrackName()
{
    QUrl url = QUrl("http://gpro.net/gb/Qualify.asp");

    QNetworkReply *reply = cookie_manager_->sendGetRequest(url);

    QByteArray bts = reply->readAll();
    QString reply_str(bts);

    Track track;

    // <h2>[\\\\rnt]+Next[\\s]+race:[\\\\rnt]+([A-Za-z\\\- ]+)[\\s]+GP
    QString str_pattern = QString("<h2>[\r\n\t]+Next[\\s]race:[\r\n\t]+([A-Za-z- ]+)[\\s]+GP");

    QRegularExpression regexp(str_pattern);
    QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);
    if(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();
        //qDebug() << "Race: " << match.captured(1);
        track.setName(match.captured(1));
    }

    return track;
}

Race gproServerInterface::getRace(QString header)
{
    ParseAlgorithms pa;

    QUrl url = QUrl(QString("http://gpro.net/gb/RaceAnalysis.asp") + QString("?SR=") + QString(header));

    QNetworkReply *reply = cookie_manager_->sendGetRequest(url);

    QByteArray bts = reply->readAll();
    QString reply_str(bts);

    //qDebug() << reply_str;

    Race race = pa.parseTrackfromRace(reply_str);

    QString str_pattern;
    QRegularExpression regexp;
    QRegularExpressionMatchIterator iter;


    str_pattern = QString("<th[\\s]+width=\\\"50%\\\">Qualification[\\s]+1</th>"
                          "[\r\n\t]+<th[\\s]+width=\\\"50%\\\">Qualification[\\s]+2</th>"
                          "[\r\n\t]+</tr>[\r\n\t]+<tr>[\r\n\t]+<td[\\s]align=\\\"center\\\">([\\d]?):?([\\d]{2}).([\\d]{3})s</td>"
                          "[\r\n\t]+<td[\\s]align=\\\"center\\\">([\\d]?):?([\\d]{2}).([\\d]{3})s</td>[\r\n\t]+</tr>");

    regexp.setPattern(str_pattern);
    iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();
        race.setQ1LapTime(match.captured(1).toDouble()*60 + match.captured(2).toDouble() + match.captured(3).toDouble()/1000);
        race.setQ2LapTime(match.captured(4).toDouble()*60 + match.captured(5).toDouble() + match.captured(6).toDouble()/1000);
    }

    qDebug() << "Q1 time: " << race.getQ1LapTime();
    qDebug() << "Q2 time: " << race.getQ2LapTime();

    QList<PracticeLap> plaps = pa.parsePracticeLaps(reply_str, race.getTrack().getID(), race.getSeason());

    foreach(PracticeLap plap, plaps) {
        race.addPracticeLap(plap);
    }

    Weather weather = pa.parseWeatherfromRace(reply_str);

    race.setWeather(weather);

    Car car = pa.parseCarfromRace(reply_str);

    race.setCar(car);

    Driver  driver = pa.parseDriverfromRace(reply_str);

    race.setDriver(driver);

    Risk risk;
    risk.setRisk(pa.parseRisksfromRace(reply_str));
    risk.setStartingRisk(pa.parseStartingRiskfromRace(reply_str));

    race.setRisk(risk);

    QList<Lap> laps = pa.parseLapsfromRace(reply_str);

    foreach(Lap lap, laps) {
        race.addLap(lap);
    }

    QList<Stint> stints = pa.parseStintsfromRace(reply_str, race.getTrack());

    foreach(Stint stint, stints) {
        race.addStint(stint);
    }

    return race;
}

QStringList gproServerInterface::getCurrentList()
{
    return current_list_;
}

QStringList gproServerInterface::getOldRacesHeaders()
{
    QUrl url = QUrl("http://gpro.net/gb/RaceAnalysis.asp");

    QNetworkReply *reply = cookie_manager_->sendGetRequest(url);

    QByteArray bts = reply->readAll();
    QString reply_str(bts);

    QStringList old_race_headers;

    //qDebug() << reply_str;

    QString str_pattern = QString("option[\\s]+value=\\\"([\\d]{2},[\\d]{1,2})\\\"");

    QRegularExpression regexp(str_pattern);
    QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);
    while(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();
        old_race_headers.append(match.captured(1));
    }

    return old_race_headers;
}

QStringList gproServerInterface::getSeasonTracks()
{
    QUrl url = QUrl("http://gpro.net/gb/Calendar.asp");

    QNetworkReply *reply = cookie_manager_->sendGetRequest(url);

    QByteArray bts = reply->readAll();
    QString reply_str(bts);

    QStringList tracks;

    //qDebug() << reply_str;

    QString str_pattern = QStringLiteral("[>]([ a-zA-Z0-9-]+)[ ]GP[ ]");

    QRegularExpression regexp(str_pattern);
    QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);

    while(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();

        tracks.append(match.captured(1));
    }

    return tracks;
}

Finance gproServerInterface::getNonRaceFinance()
{
    Finance finance;

    // staff salary and facility maintenance
    QUrl url = QUrl("http://gpro.net/gb/StaffAndFacilities.asp");

    QNetworkReply *reply = cookie_manager_->sendGetRequest(url);

    QByteArray bts = reply->readAll();
    QString reply_str(bts);

    QString str_pattern = QString("Staff[ ]+salary[: $]+([\\d.]+)[&a-z;]+Facilities[ ]+maintenance[: $]+([\\d.]+)");

    QRegularExpression regexp(str_pattern);
    QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();
        finance.setStaffSalary(match.captured(1).replace(".", "").toInt());
        finance.setFacilityMaintenence(match.captured(2).replace(".", "").toInt());
    }

    qDebug() << "Staff salary" << finance.getStaffSalary();
    qDebug() << "Facility Maintenance" << finance.getFacilityMaintenence();

    // driver salary
    url = QUrl("http://gpro.net/gb/TrainingSession.asp");

    reply = cookie_manager_->sendGetRequest(url);

    bts = reply->readAll();

    reply_str = QString(bts);

    str_pattern = QString("Salary[:&a-z;]+</th>[\r\n\t]+<td>[$]([\\d.]+)</td>");

    regexp.setPattern(str_pattern);
    iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();
        finance.setDriverSalary(match.captured(1).replace(".","").toInt());
    }

    qDebug() << "Driver salary" << finance.getDriverSalary();

    // tyre cost
    url = QUrl("http://gpro.net/gb/Suppliers.asp");

    reply = cookie_manager_->sendGetRequest(url);

    bts = reply->readAll();

    reply_str = QString(bts);

    str_pattern = QString("<p>Cost[ ]+per[ ]+race[: ]+<strong>[$]([\\d.]+)</strong></p>"
                          "[\r\n\t]+<form[ a-z=\\\"A-Z.:;0-9]+>[\r\n\t]+<font[ a-z=\\\"]+><b>Contract[ ]+active</b>");

    regexp.setPattern(str_pattern);
    iter = regexp.globalMatch(reply_str);

    if(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();
        finance.setTyreCost(match.captured(1).replace(".","").toInt());
    }

    qDebug() << "Tyre Cost" << finance.getTyreCost();

    // sponsors
    url = QUrl("http://gpro.net/gb/NegotiationsOverview.asp");

    reply = cookie_manager_->sendGetRequest(url);

    bts = reply->readAll();

    reply_str = QString(bts);

    str_pattern = QString("<td[ a-z=\\\"]+>[A-Za-z ]+</td>[\r\n\t]+<td[ a-z=\\\"]+>(([-])|([$][\\d.]+))</td>");

    regexp.setPattern(str_pattern);
    iter = regexp.globalMatch(reply_str);

    int sponsor_total = 0;

    while(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();
        sponsor_total += match.captured(3).replace(QRegularExpression("[$.]"), "").toInt();
    }

    finance.setSponsorMoney(sponsor_total);

    qDebug() << "Sponsor Moneyz" << finance.getSponsorMoney();

    return finance;
}

Track gproServerInterface::getTrack(int gpro_track_id, bool debug)
{
    Track track;

    QUrl url = QUrl("http://gpro.net/gb/TrackDetails.asp?id=" + QString::number(gpro_track_id));

    QNetworkReply *reply = cookie_manager_->sendGetRequest(url);

    QByteArray bts = reply->readAll();
    QString reply_str(bts);

    // getting track name
    {
        QString str_pattern = QStringLiteral("<title>[ A-Za-z]+-[ ]([ A-Za-z0-9-]+)[ ]-[ A-Za-z]+</title>");

        QRegularExpression regexp(str_pattern);
        QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);

        if(iter.hasNext()) {
            QRegularExpressionMatch match = iter.next();
            track.setName(match.captured(1));
            if(debug) qDebug() << "Track name" << track.getName();
        }
    }

    // getting power
    {
        QString str_pattern = QStringLiteral("<TD[ a-z=\\\"0-9]+>Power[:]</TD>[\r\n\t]+<TD[ a-z=\\\"0-9]+title=[\\\"]+([\\d]+)[\\\"]+>");

        QRegularExpression regexp(str_pattern);
        QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);

        if(iter.hasNext()) {
            QRegularExpressionMatch match = iter.next();
            track.setPower(match.captured(1).toInt());
            if(debug) qDebug() << "Track Power" << track.getPower();
        }
    }

    // getting handling
    {
        QString str_pattern = QStringLiteral("<TD[ a-z=\\\"0-9]+>Handling[:]</TD>[\r\n\t]+<TD[ a-z=\\\"0-9]+title=[\\\"]+([\\d]+)[\\\"]+>");

        QRegularExpression regexp(str_pattern);
        QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);

        if(iter.hasNext()) {
            QRegularExpressionMatch match = iter.next();
            track.setHandling(match.captured(1).toInt());
            if(debug) qDebug() << "Track Handling" << track.getHandling();
        }
    }

    // getting acceleration
    {
        QString str_pattern = QStringLiteral("<TD[ a-z=\\\"0-9]+>Acceleration[:]</TD>[\r\n\t]+<TD[ a-z=\\\"0-9]+title=[\\\"]+([\\d]+)[\\\"]+>");

        QRegularExpression regexp(str_pattern);
        QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);

        if(iter.hasNext()) {
            QRegularExpressionMatch match = iter.next();
            track.setAcceleration(match.captured(1).toInt());
            if(debug) qDebug() << "Track Acceleration" << track.getAcceleration();
        }
    }

    // getting race distance
    {
        QString str_pattern = QStringLiteral("<TD[ a-z=\\\"0-9]+>Race distance[:]</TD>[\r\n\t]+<TD[ a-z=0-9]+>([\\d.]+)km</TD>");

        QRegularExpression regexp(str_pattern);
        QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);

        if(iter.hasNext()) {
            QRegularExpressionMatch match = iter.next();
            track.setDistance(match.captured(1).toDouble());
            if(debug) qDebug() << "Track Race Distance" << track.getDistance();
        }
    }

    // getting laps
    {
        QString str_pattern = QStringLiteral("<TD[ a-z=\\\"0-9]+>Laps[:]</TD>[\r\n\t]+<TD>([\\d]+)</TD>");

        QRegularExpression regexp(str_pattern);
        QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);

        if(iter.hasNext()) {
            QRegularExpressionMatch match = iter.next();
            track.setLaps(match.captured(1).toInt());
            if(debug) qDebug() << "Track Race Laps" << track.getLaps();
        }
    }

    // getting downforce
    {
        QString str_pattern = QStringLiteral("<TD[ a-z=\\\"0-9]+>Downforce[:]</TD>[\r\n\t]+<TD>([A-Za-z ]+)</TD>");

        QRegularExpression regexp(str_pattern);
        QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);

        if(iter.hasNext()) {
            QRegularExpressionMatch match = iter.next();
            track.setDownforce(Track::Downforce::toDownforce(match.captured(1)));
            if(debug) qDebug() << "Track Race Downforce" << Track::Downforce::toString(track.getDownforce());
        }
    }

    // getting lap length
    {
        QString str_pattern = QStringLiteral("<TD[ a-z=\\\"0-9]+>Lap distance[:]</TD>[\r\n\t]+<TD>([\\d.]+)[a-z;&]+</TD>");

        QRegularExpression regexp(str_pattern);
        QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);

        if(iter.hasNext()) {
            QRegularExpressionMatch match = iter.next();
            track.setLapLength(match.captured(1).toDouble());
            if(debug) qDebug() << "Track Lap Length" << track.getLapLength();
        }
    }

    // getting overtaking
    {
        QString str_pattern = QStringLiteral("<TD[ a-z=\\\"0-9]+>Overtaking[:]</TD>[\r\n\t]+<TD>([a-zA-Z ]+)</TD>");

        QRegularExpression regexp(str_pattern);
        QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);

        if(iter.hasNext()) {
            QRegularExpressionMatch match = iter.next();
            track.setOvertaking(Track::Overtaking::toOvertaking(match.captured(1)));
            if(debug) qDebug() << "Track Race Overtaking" << Track::Overtaking::toString(track.getOvertaking());
        }
    }

    // getting average speed
    {
        QString str_pattern = QStringLiteral("<TD[ a-z=\\\"0-9]+>Average speed[:]</TD>[\r\n\t]+<TD>([\\d.]+)[a-z;&/]+</TD>");

        QRegularExpression regexp(str_pattern);
        QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);

        if(iter.hasNext()) {
            QRegularExpressionMatch match = iter.next();
            track.setAvgSpeed(match.captured(1).toDouble());
            if(debug) qDebug() << "Track Avg Speed" << track.getAvgSpeed();
        }
    }

    // getting suspension rigidity
    {
        QString str_pattern = QStringLiteral("<TD[ a-z=\\\"0-9]+>Suspension rigidity[:]</TD>[\r\n\t]+<TD>([A-Za-z ]+)</TD>");

        QRegularExpression regexp(str_pattern);
        QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);

        if(iter.hasNext()) {
            QRegularExpressionMatch match = iter.next();
            track.setSuspension(Track::Suspension::toSuspension(match.captured(1)));
            if(debug) qDebug() << "Track Suspension" << Track::Suspension::toString(track.getSuspension());
        }
    }

    // getting fuel consumption
    {
        QString str_pattern = QStringLiteral("<TD[ a-z=\\\"0-9]+>Fuel consumption[:]</TD>[\r\n\t]+<TD>([A-Za-z ]+)</TD>");

        QRegularExpression regexp(str_pattern);
        QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);

        if(iter.hasNext()) {
            QRegularExpressionMatch match = iter.next();
            track.setFuelConsumption(Track::FuelConsumption::toFuelConsumption(match.captured(1)));
            if(debug) qDebug() << "Track Fuel Consumption" << Track::FuelConsumption::toString(track.getFuelConsumption());
        }
    }

    // getting corners
    {
        QString str_pattern = QStringLiteral("<TD[ a-z=\\\"0-9]+>Number of corners[:]</TD>[\r\n\t]+<TD>([\\d]+)</TD>");

        QRegularExpression regexp(str_pattern);
        QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);

        if(iter.hasNext()) {
            QRegularExpressionMatch match = iter.next();
            track.setCorners(match.captured(1).toInt());
            if(debug) qDebug() << "Track Corners" << track.getCorners();
        }
    }

    // getting tyre wear
    {
        QString str_pattern = QStringLiteral("<TD[ a-z=\\\"0-9]+>Tyre wear[:]</TD>[\r\n\t]+<TD[ a-z=\\\"0-9]*>([A-Za-z ]+)</TD>");

        QRegularExpression regexp(str_pattern);
        QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);

        if(iter.hasNext()) {
            QRegularExpressionMatch match = iter.next();
            track.setTyreWear(Track::TyreWear::toTyreWear(match.captured(1)));
            if(debug) qDebug() << "Track Tyre Wear" << Track::TyreWear::toString(track.getTyreWear());
        }
    }

    // getting pit stop
    {
        QString str_pattern = QStringLiteral("<TD[ a-z=\\\"0-9]+>Time in/out of pits[:]</TD>[\r\n\t]+<TD>([\\d.]+)s</TD>");

        QRegularExpression regexp(str_pattern);
        QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);

        if(iter.hasNext()) {
            QRegularExpressionMatch match = iter.next();
            track.setPitStop(match.captured(1).toDouble());
            if(debug) qDebug() << "Track Pit Stop" << track.getPitStop();
        }
    }

    // getting grip
    {
        QString str_pattern = QStringLiteral("<TD[ a-z=\\\"0-9]+>Grip level[:]</TD>[\r\n\t]+<TD[ a-z=\\\"0-9]*>([A-Za-z ]+)</TD>");

        QRegularExpression regexp(str_pattern);
        QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);

        if(iter.hasNext()) {
            QRegularExpressionMatch match = iter.next();
            track.setGrip(Track::Grip::toGrip(match.captured(1)));
            if(debug) qDebug() << "Track Grip" << Track::Grip::toString(track.getGrip());
        }
    }

    return track;
}

QList<Track> gproServerInterface::getAllTracks()
{
    QList<Track> tracks;

    QUrl url = QUrl("http://gpro.net/gb/ViewTracks.asp");

    QNetworkReply *reply = cookie_manager_->sendGetRequest(url);

    QByteArray bts = reply->readAll();
    QString reply_str(bts);

    QString str_pattern = QString("<a[ ?a-zA-Z.\\\"=]+id=([\\d]+)[\\\"]+>([ A-Za-z0-9-]+)</a>");

    QRegularExpression regexp(str_pattern);
    QRegularExpressionMatchIterator iter = regexp.globalMatch(reply_str);

    while(iter.hasNext()) {
        QRegularExpressionMatch match = iter.next();
        Track track = getTrack(match.captured(1).toInt());
        track.setID(match.captured(1).toInt());
        tracks.append(track);
    }

    return tracks;
}

void gproServerInterface::replyFinished(QNetworkReply *rep)
{
    //qDebug() << rep->readAll();
}

void gproServerInterface::weatherOnFetch(QNetworkReply *rep)
{
    /*QByteArray bts = rep->readAll();
    QString str(bts);
    qDebug() << "WFetch Start";
    qDebug() << str;
    qDebug() << "WFetch End";*/
}

