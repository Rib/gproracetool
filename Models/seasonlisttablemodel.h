#ifndef SEASONLISTTABLEMODEL_H
#define SEASONLISTTABLEMODEL_H

#include <memory>

#include <QAbstractTableModel>
#include <QStringList>
#include <QLabel>
#include <QTableView>
#include <QMap>

#include "Objects/track.h"
#include "Objects/driver.h"
#include "Objects/car.h"

using std::shared_ptr;

class SeasonListTableModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit SeasonListTableModel(QObject *parent = 0);

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role = Qt::EditRole) override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    bool setTrackData(const QModelIndex &index, const Track &value,
                 int role = Qt::EditRole);

    bool setDriverData(const QModelIndex &index, shared_ptr<Driver> value,
                       int role = Qt::EditRole);

    bool setCarData(const QModelIndex &index, shared_ptr<Car> value,
                    int role = Qt::EditRole);

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    // Add data:
    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
    bool insertColumns(int column, int count, const QModelIndex &parent = QModelIndex()) override;

    // Remove data:
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
    bool removeColumns(int column, int count, const QModelIndex &parent = QModelIndex()) override;

    Track getTrack(const QModelIndex &index);
    const QList< Track > &getTracks() { return tracks_; }

    const QList< shared_ptr<Driver> > &getDrivers() { return drivers_; }

    const QList< shared_ptr<Car> > &getCars() { return cars_; }

    void showFullData(const QModelIndex &index);

    const QList<QModelIndex> &getActiveWidgets() { return widgets_active_; }
    void clearActiveWidgets() { widgets_active_.clear(); }
    void addActiveWidget(QModelIndex index) { widgets_active_.append(index); }

private:
    QStringList headers_;

    QList< QStringList > data_;

    QList< Track > tracks_;

    QList< shared_ptr<Driver> > drivers_;

    QList< shared_ptr<Car> > cars_;

    QTableView *parent_;

    QList<QModelIndex> widgets_active_;

    int show_full_data_;
};

#endif // SEASONLISTTABLEMODEL_H
