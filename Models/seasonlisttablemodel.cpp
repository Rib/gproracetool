#include "seasonlisttablemodel.h"

#include <QDebug>

SeasonListTableModel::SeasonListTableModel(QObject *parent)
    : QAbstractTableModel(parent),
      parent_((QTableView*)parent),
      show_full_data_(-1)
{
    setHeaderData(0, Qt::Orientation::Horizontal, QString("Track"), Qt::DisplayRole);
    setHeaderData(1, Qt::Orientation::Horizontal, QString("Driver"), Qt::DisplayRole);
    setHeaderData(2, Qt::Orientation::Horizontal, QString("Car"), Qt::DisplayRole);
}

QVariant SeasonListTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if( !(orientation == Qt::Orientation::Horizontal && section < headers_.size() && section >= 0) )
        return QVariant();

    if( role == Qt::DisplayRole ) {
        return headers_.at(section);
    } else if( role == Qt::TextAlignmentRole ) {
        return Qt::AlignCenter;
    } else if ( role == Qt::SizeHintRole ) {
        QSize size = parent_->size();
        int allow_size = size.width()-40;
        if(allow_size < 800) {
            if(headers_.at(section) == "Car") size.setWidth(250);
            else size.setWidth((allow_size - 250)/2);
        } else {
            size.setWidth(allow_size - ( 300 + 250 ));
            size.setHeight(25);

            if(headers_.at(section) == "Track") size.setWidth(300);

            if(headers_.at(section) == "Driver") size.setWidth(250);
        }

        return size;
    }

    return QVariant();
}

bool SeasonListTableModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role)
{
    if (value != headerData(section, orientation, role)) {

        if(role == Qt::DisplayRole && orientation == Qt::Orientation::Horizontal && section < headers_.size() && section > 0) {
            headers_[section] = value.toString();
        } else if (role == Qt::DisplayRole && orientation == Qt::Orientation::Horizontal && section == headers_.size() && section >= 0) {
            headers_.append(value.toString());
        }

        emit headerDataChanged(orientation, section, section);
        return true;
    }
    return false;
}


int SeasonListTableModel::rowCount(const QModelIndex &parent) const
{
    if (!parent.isValid() && false)
        return 0;

    return data_.size();
}

int SeasonListTableModel::columnCount(const QModelIndex &parent) const
{
    if (!parent.isValid() && false)
        return 0;

    return headers_.size();
}

QVariant SeasonListTableModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid() || !(index.column() < headers_.size() && index.column() >= 0 &&
          index.row() < data_.size() && index.row() >= 0) )
        return QVariant();

    bool widget_view = widgets_active_.size() > 0 && widgets_active_.at(0).row() == index.row();

    if( role == Qt::DisplayRole && !widget_view) {
        return data_.at(index.row()).at(index.column());
    }

    return QVariant();
}

bool SeasonListTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(!(index.column() < headers_.size() && index.column() >= 0) )
        return false;

    if (data(index, role) != value) {

        if( role == Qt::DisplayRole && index.row() < data_.size()) {
            data_[index.row()][index.column()] = value.toString();
        }

        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

bool SeasonListTableModel::setTrackData(const QModelIndex &index, const Track &value, int role)
{
    if(!(index.column() < headers_.size() && index.column() >= 0) )
        return false;

    if (data(index, role) != value.getName()) {

        if( role == Qt::DisplayRole && index.row() < data_.size()) {
            data_[index.row()][index.column()] = value.getName();
            tracks_[index.row()] = value;
        }

        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

bool SeasonListTableModel::setDriverData(const QModelIndex &index, shared_ptr<Driver> value, int role)
{
    if(!(index.column() < headers_.size() && index.column() >= 0) )
        return false;

    if( role == Qt::DisplayRole && index.row() < data_.size()) {
        data_[index.row()][index.column()] = value->getName();
        drivers_[index.row()] = value;

        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }


    return false;
}

bool SeasonListTableModel::setCarData(const QModelIndex &index, shared_ptr<Car> value, int role)
{
    if(!(index.column() < headers_.size() && index.column() >= 0) )
        return false;

    if( role == Qt::DisplayRole && index.row() < data_.size()) {
        data_[index.row()][index.column()] = "";
        cars_[index.row()] = value;

        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }


    return false;
}

Qt::ItemFlags SeasonListTableModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    if(headers_.at(index.column()) == QString("Track")) return Qt::ItemIsEnabled;

    return Qt::ItemIsEnabled | Qt::ItemIsEditable; // FIXME: Implement me!
}

bool SeasonListTableModel::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent, row, row + count - 1);

    QStringList sl;

    foreach(QString str, headers_) sl.append("");

    for(int i = 0; i < count; ++i) {
        data_.append(sl);
        tracks_.append(Track());
        drivers_.append(0);
        cars_.append(0);
    }

    endInsertRows();
}

bool SeasonListTableModel::insertColumns(int column, int count, const QModelIndex &parent)
{
    beginInsertColumns(parent, column, column + count - 1);
    // FIXME: Implement me!
    endInsertColumns();
}

bool SeasonListTableModel::removeRows(int row, int count, const QModelIndex &parent)
{
    QList<QStringList> data;
    QList< Track > tracks;
    QList< shared_ptr<Driver> > drivers;
    QList< shared_ptr<Car> > cars;

    beginRemoveRows(parent, row, row + count - 1);

    for(int i = 0; i < data.size(); ++i) {
        if(i < row || i >= row + count) {
            data.append(data_.at(i));
            tracks.append(tracks_.at(i));
            drivers.append(drivers_.at(i));
            cars.append(cars_.at(i));
        }
    }

    data_ = data;
    tracks_ = tracks;
    drivers_ = drivers;
    cars_ = cars;

    endRemoveRows();
}

bool SeasonListTableModel::removeColumns(int column, int count, const QModelIndex &parent)
{
    beginRemoveColumns(parent, column, column + count - 1);
    // FIXME: Implement me!
    endRemoveColumns();
}

Track SeasonListTableModel::getTrack(const QModelIndex &index)
{
    if(!index.isValid()) {
        return Track();
    }

    return tracks_.at(index.row());
}

void SeasonListTableModel::showFullData(const QModelIndex &index)
{
    show_full_data_ = index.row();
}
