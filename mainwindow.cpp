#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QLayout>
#include <QDesktopWidget>

#include "UI/databasesettingsdialog.h"
#include "UI/racedownloader.h"
#include "UI/gprologindialog.h"
#include "settingconstants.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    race_data_widget(new RaceDataWidget),
    race_calculator_widget(new RaceCalculator),
    season_planner_widget(new SeasonPlanner)
{
    ui->setupUi(this);

    race_data_widget->setParent(ui->centralWidget);
    race_calculator_widget->setParent(ui->centralWidget);
    season_planner_widget->setParent(ui->centralWidget);

    ui->DriverInfoWidget->setVisible(false);
    ui->PracticeCalculationWidget->setVisible(true);
    race_data_widget->setVisible(false);
    race_calculator_widget->setVisible(false);
    season_planner_widget->setVisible(false);

    resizeToSettings();
}

MainWindow::~MainWindow()
{
    QSettings settings(General::ProgramName, General::CompanyName);

    QSize size = this->size();

    settings.setValue(Settings::WindowWidth, size.width());
    settings.setValue(Settings::WindowHeight, size.height());

    delete ui;
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    ui->PracticeCalculationWidget->setMinimumHeight(event->size().height()-50);
    ui->PracticeCalculationWidget->setMaximumHeight(event->size().height()-50);
    ui->PracticeCalculationWidget->setMinimumWidth(event->size().width());
    ui->PracticeCalculationWidget->setMaximumWidth(event->size().width());

    ui->DriverInfoWidget->setMinimumHeight(event->size().height()-50);
    ui->DriverInfoWidget->setMaximumHeight(event->size().height()-50);
    ui->DriverInfoWidget->setMinimumWidth(event->size().width());
    ui->DriverInfoWidget->setMaximumWidth(event->size().width());

    race_data_widget->setMinimumHeight(event->size().height()-50);
    race_data_widget->setMaximumHeight(event->size().height()-50);
    race_data_widget->setMinimumWidth(event->size().width());
    race_data_widget->setMaximumWidth(event->size().width());

    race_calculator_widget->setMinimumHeight(event->size().height()-50);
    race_calculator_widget->setMaximumHeight(event->size().height()-50);
    race_calculator_widget->setMinimumWidth(event->size().width());
    race_calculator_widget->setMaximumWidth(event->size().width());

    season_planner_widget->setMinimumHeight(event->size().height()-50);
    season_planner_widget->setMaximumHeight(event->size().height()-50);
    season_planner_widget->setMinimumWidth(event->size().width());
    season_planner_widget->setMaximumWidth(event->size().width());

    return;
}

void MainWindow::resizeToSettings()
{
    QSettings settings(General::ProgramName, General::CompanyName);

    int width = settings.value(Settings::WindowWidth, "800").toInt();
    int height = settings.value(Settings::WindowHeight, "600").toInt();

    QSize newSize( width , height );

    setGeometry(
        QStyle::alignedRect(
            Qt::LeftToRight,
            Qt::AlignCenter,
            newSize,
            qApp->desktop()->availableGeometry()
        )
    );
}

void MainWindow::on_actionExit_triggered()
{
    this->close();
}

void MainWindow::on_actionPractice_Calculation_triggered()
{
    ui->DriverInfoWidget->setVisible(false);
    ui->PracticeCalculationWidget->setVisible(true);
    race_data_widget->setVisible(false);
    race_calculator_widget->setVisible(false);
    season_planner_widget->setVisible(false);
}

void MainWindow::on_actionDriver_triggered()
{
    ui->DriverInfoWidget->setVisible(true);
    ui->PracticeCalculationWidget->setVisible(false);
    race_data_widget->setVisible(false);
    race_calculator_widget->setVisible(false);
    season_planner_widget->setVisible(false);
}

void MainWindow::on_actionDatabase_triggered()
{
    DatabaseSettingsDialog *dsb = new DatabaseSettingsDialog(this);
    dsb->loadSettings();
    dsb->show();
}

void MainWindow::on_actionRace_Data_triggered()
{
    ui->DriverInfoWidget->setVisible(false);
    ui->PracticeCalculationWidget->setVisible(false);
    race_data_widget->setVisible(true);
    race_calculator_widget->setVisible(false);
    season_planner_widget->setVisible(false);
}

void MainWindow::on_actionRace_Calculator_triggered()
{
    ui->DriverInfoWidget->setVisible(false);
    ui->PracticeCalculationWidget->setVisible(false);
    race_data_widget->setVisible(false);
    race_calculator_widget->setVisible(true);
    season_planner_widget->setVisible(false);
}

void MainWindow::on_actionRace_Downloader_triggered()
{
    RaceDownloader *rdl = new RaceDownloader(this);
    rdl->loadSettings();
    rdl->show();
}

void MainWindow::on_actionUser_triggered()
{
    GproLoginDialog *gld = new GproLoginDialog(this);
    gld->show();
}

void MainWindow::on_actionSeason_Planner_triggered()
{
    ui->DriverInfoWidget->setVisible(false);
    ui->PracticeCalculationWidget->setVisible(false);
    race_data_widget->setVisible(false);
    race_calculator_widget->setVisible(false);
    season_planner_widget->setVisible(true);
}

void MainWindow::on_actionUpdate_Available_Data_triggered()
{
    // TODO make full race data update here (for current data)
}
