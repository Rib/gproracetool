#include "transformations.h"

#include <QRegularExpression>
#include <QRegularExpressionMatchIterator>
#include <QRegularExpressionMatch>
#include <cmath>

Transformations::Transformations()
{

}

double Transformations::transformTimeToDouble(const QString &time)
{
    static QRegularExpression regexp(QStringLiteral("([\\d]?)[:]?([\\d]{2})[.]([\\d]{3})"));

    QRegularExpressionMatchIterator iter = regexp.globalMatch(time);

    if(iter.hasNext()) {
       QRegularExpressionMatch match = iter.next();
       return (match.captured(1).toDouble() * 60 +
               match.captured(2).toDouble() +
               match.captured(3).toDouble() / 1000);

    } else return 0;
}

QString Transformations::transformTimeFromDouble(const double &time)
{
    return QString::number((int)(time/60)) + QStringLiteral(":") +
                        (((int)time % 60) < 10 ? "0" : "") + QString::number(((int)time) % 60) + QStringLiteral(".") +
                        (std::round(((int)(time*1000)) % 1000) < 100 ? "0" : "") +
            (std::round((int)(time) % 1000) < 10 ? QStringLiteral("0") : QStringLiteral("")) +
                        QString::number(std::round(((int)(time*1000)) % 1000));
}
