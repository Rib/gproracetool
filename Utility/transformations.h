#ifndef TRANSFORMATIONS_H
#define TRANSFORMATIONS_H

#include <QString>

class Transformations
{
public:
    Transformations();

    static double transformTimeToDouble(const QString &time);

    static QString transformTimeFromDouble(const double &time);
};

#endif // TRANSFORMATIONS_H
