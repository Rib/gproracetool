#include "databasehandler.h"

#include <QString>
#include <QSettings>
#include <QtSql/QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QDebug>
#include <QSqlError>
#include <QTime>
#include <QSharedPointer>

#include "settingconstants.h"
#include "Objects/tyretype.h"
#include "Objects/weather.h"
#include "Objects/track.h"
#include "Database/databasequeryhelper.h"
#include "Utility/transformations.h"
#include "Misc/threadresourcemanager.h"

QString getCarLvlFields() {
    QString car_string = QString(" Car.Power AS CPower, ") +
            QString("Car.Handling AS CHandling, ") +
            QString("Car.Acceleration AS CAcceleration, ") +
            QString("Car.Chassis AS CChassis, ") +
            QString("Car.Engine AS CEngine, ") +
            QString("Car.FrontWing AS CFrontWing, ") +
            QString("Car.RearWing AS CRearWing, ") +
            QString("Car.Underbody AS CUnderbody, ") +
            QString("Car.Sidepods AS CSidepods, ") +
            QString("Car.Cooling AS CCooling, ") +
            QString("Car.Gearbox AS CGearbox, ") +
            QString("Car.Brakes AS CBrakes, ") +
            QString("Car.Suspension AS CSuspension, ") +
            QString("Car.Electronics AS CElectronics ");
    return car_string;
}

QString getCarWearFields() {
    QString car_wear_string = QString("CarWear.ChassisWear AS CWChassis, ") +
            QString("CarWear.EngineWear AS CWEngine, ") +
            QString("CarWear.FrontWingWear AS CWFrontWing, ") +
            QString("CarWear.RearWingWear AS CWRearWing, ") +
            QString("CarWear.UnderbodyWear AS CWUnderbody, ") +
            QString("CarWear.SidepodsWear AS CWSidepods, ") +
            QString("CarWear.CoolingWear AS CWCooling, ") +
            QString("CarWear.GearboxWear AS CWGearbox, ") +
            QString("CarWear.BrakesWear AS CWBrakes, ") +
            QString("CarWear.SuspensionWear AS CWSuspension, ") +
            QString("CarWear.ElectronicsWear AS CWElectronics ");
    return car_wear_string;
}

QString getDriverFields() {
    QString driver_string = QString(" Driver.Overall AS DOverall, ") +
            QString(" Driver.Concentration AS DConcentration, ") +
            QString(" Driver.Talent AS DTalent, ") +
            QString(" Driver.Aggresiveness AS DAggressiveness, ") +
            QString(" Driver.Experience AS DExperience, ") +
            QString(" Driver.TechInsight AS DTechInsight, ") +
            QString(" Driver.Stamina AS DStamina, ") +
            QString(" Driver.Charisma AS DCharisma, ") +
            QString(" Driver.Motivation AS DMotivation, ") +
            QString(" Driver.Weight AS DWeight, ") +
            QString(" Driver.Reputation AS DReputation, ") +
            QString(" Driver.Age AS DAge ");
    return driver_string;
}

QString getRiskFields() {
    QString risk_string = QString("Risk.Clear AS RClear, ") +
            QString("Risk.Wet AS RWet, ") +
            QString("Risk.Defend AS RDefend, ") +
            QString("Risk.Malfunction AS RMalfunction, ") +
            QString("Risk.Overtake AS ROvertake, ") +
            QString("Risk.StartRisk AS RStartRisk ");

    return risk_string;
}

QString getPracticeLapFields() {
    QString practice_lap_string = QString("PracticeLap.LapNumber, ") +
            QString( "PracticeLap.LapTime, ") +
            QString("PracticeLap.DriverMistake, ") +
            QString("PracticeLap.NetTime, ") +
            QString("PracticeLap.FrontWing,") +
            QString("PracticeLap.RearWing, ") +
            QString("PracticeLap.Engine, ") +
            QString("PracticeLap.Brakes, ") +
            QString("PracticeLap.Gear,") +
            QString("PracticeLap.Suspension,") +
            QString("PracticeLap.Tyres,") +
            QString("PracticeLap.WingComment, ") +
            QString("PracticeLap.EngineComment,") +
            QString("PracticeLap.BrakesComment, ") +
            QString("PracticeLap.GearComment, ") +
            QString("PracticeLap.SuspensionComment, ") +
            QString("PracticeLap.track_id, PracticeLap.Season, ") +
            QString("PracticeLap._id ");

    return practice_lap_string;
}

QString getLapFields() {
    QString lap_string = QString( "Lap.Season, Lap.Lap, "
                                 "Lap.LapTime, Lap.Pos, "
                                 "Lap.Tyres, Lap.Weather, "
                                 "Lap.Temp, Lap.Hum, "
                                 "Lap.Events, Lap.track_id" );

    return lap_string;
}

QString getPracticeLapQuery() {
    QString select = "SELECT ";
    QString practice_laps_fields = getPracticeLapFields();
    QString from = QString("FROM ") +
            QString("PracticeLap");

    return select + practice_laps_fields + from;
}

QString getRaceQuery() {
    QString select = "SELECT ";
    QString race_fields = QString("Season, track_id ");
    QString from = QString("FROM ") +
            QString("Race");

    return select + race_fields + from;
}

QString getWeatherQuery() {
    QString select = QString("SELECT * ");
    QString from = QString("FROM ") +
            QString("Weather");

    return select + from;
}

QString getCarDataQuery() {
    QString select = "SELECT ";
    QString car_lvl_fields = getCarLvlFields() + QString(", ");
    QString car_wear_field = getCarWearFields() + QString(", ");
    QString race_fields = QString("Race.Season, Race.track_id ");
    QString from = QString("FROM ") +
            QString("Race JOIN Car USING(Season,track_id) "
                    "JOIN CarWear USING(Season,track_id);");

    return select + car_lvl_fields + car_wear_field + race_fields +
            from;
}

QString getDriverDataQuery() {
    QString select = "SELECT ";
    QString driver_fields = getDriverFields() + QString(", ");
    QString race_fields = QString("Driver.Season, Driver.track_id ");
    QString from = QString("FROM ") +
            QString("Driver ");

    return select + driver_fields + race_fields + from;
}

QString getRiskDataQuery() {
    QString select = "SELECT ";
    QString risk_fields = getRiskFields() + QString(", ");
    QString race_fields = QString("Risk.Season, Risk.track_id ");
    QString from = QString("FROM ") +
            QString("Risk ");

    return select + risk_fields + race_fields + from;
}

QString getLapQuery(int track_id = 0, int season = 0) {
    QString select = "SELECT ";
    QString lap_fields = getLapFields() + QString(" ");
    QString from = QString("FROM ") +
            QString("Lap ");

    if (track_id != 0 || season != 0) {
        QString where = "WHERE ";
        where += track_id == 0 ? QString("") : QString("Lap.track_id = ") + QString::number(track_id);
        if(track_id != 0) where += QString(" AND ");
        where += season == 0 ? QString("") : QString(" Lap.Season = ") + QString::number(season);

        from += where;
    }

    return select + lap_fields + from;
}

QList<PracticeLap> *databasehandler::readPracticeLapData()
{
    QMutexLocker locker(&ThreadResourceManager::getInstance().getDBMutex());

    if (!beginConnection()) {
        qDebug() << "Begin connection failed.";
        return new QList<PracticeLap>();
    }

    QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());

    QSqlQuery query = db.exec(getPracticeLapQuery());

    query.next();

    qDebug() << "DBHandler practice_lap_data: " <<"Rows: " << query.size() << "Cols: " << query.record().count();
    QList<PracticeLap> *return_data = new QList<PracticeLap>();

    for(int i = 0; i < query.record().count(); ++i) {
        practice_lap_labels_.append(query.record().fieldName(i));
    }

    while ( query.isActive() ) {
        PracticeLap temp_practice = parsePracticeLapDataRow(query.record());

        return_data->append(temp_practice);

        if(!query.next()) query.finish();
    }

    endConnection();

    return return_data;
}

PracticeLap databasehandler::parsePracticeLapDataRow(QSqlRecord record)
{
    PracticeLap return_lap;
    for(int i = 0; i < record.count(); ++i) {
        if(practice_lap_labels_.at(i) == QString("LapTime")) {
            QString temp_time = record.value(i).toString();
            QRegularExpression reg_exp("(\\d?)[:]?(\\d{2})[.](\\d{3})");
            QRegularExpressionMatchIterator iter = reg_exp.globalMatch(temp_time);
            double time = 0;

            if( iter.hasNext() ) {
                QRegularExpressionMatch match = iter.next();

                if( match.capturedLength() >= 4 ) {
                    time += 60 * match.captured(1).toDouble();
                    time += match.captured(2).toDouble();
                    time += match.captured(3).toDouble() / 1000;
                }
            }

            return_lap.setLapTime(time);
        } else if(practice_lap_labels_.at(i) == QString("NetTime")) {
            QString temp_time = record.value(i).toString();
            QRegularExpression reg_exp("(\\d?)[:]?(\\d{2})[.](\\d{3})");
            QRegularExpressionMatchIterator iter = reg_exp.globalMatch(temp_time);
            double time = 0;

            if( iter.hasNext() ) {
                QRegularExpressionMatch match = iter.next();

                if( match.capturedLength() >= 4 ) {
                    time += 60 * match.captured(1).toDouble();
                    time += match.captured(2).toDouble();
                    time += match.captured(3).toDouble() / 1000;
                }
            }

            return_lap.setNetTime(time);
        } else if(practice_lap_labels_.at(i) == QString("DriverMistake")) {
            return_lap.setDriverMistake(record.value(i).toDouble());
        } else if(practice_lap_labels_.at(i) == QString("FrontWing")) {
            return_lap.setFrontWing(record.value(i).toInt());
        } else if(practice_lap_labels_.at(i) == QString("RearWing")) {
            return_lap.setRearWing(record.value(i).toInt());
        } else if(practice_lap_labels_.at(i) == QString("Engine")) {
            return_lap.setEngine(record.value(i).toInt());
        } else if(practice_lap_labels_.at(i) == QString("Brakes")) {
            return_lap.setBrakes(record.value(i).toInt());
        } else if(practice_lap_labels_.at(i) == QString("Gear")) {
            return_lap.setGear(record.value(i).toInt());
        } else if(practice_lap_labels_.at(i) == QString("Suspension")) {
            return_lap.setSuspension(record.value(i).toInt());
        } else if(practice_lap_labels_.at(i) == QString("Tyres")) {
            return_lap.setTyre(record.value(i).toString());
        } else if(practice_lap_labels_.at(i) == QString("WingComment")) {
            return_lap.setWingComment(record.value(i).toString());
        } else if(practice_lap_labels_.at(i) == QString("EngineComment")) {
            return_lap.setEngineComment(record.value(i).toString());
        } else if(practice_lap_labels_.at(i) == QString("BrakesComment")) {
            return_lap.setBrakesComment(record.value(i).toString());
        } else if(practice_lap_labels_.at(i) == QString("GearComment")) {
            return_lap.setGearComment(record.value(i).toString());
        } else if(practice_lap_labels_.at(i) == QString("SuspensionComment")) {
            return_lap.setSuspensionComment(record.value(i).toString());
        } else if(practice_lap_labels_.at(i) == QString("track_id")) {
            return_lap.setTrackID(record.value(i).toInt());
        } else if(practice_lap_labels_.at(i) == QString("Season")) {
            return_lap.setSeason(record.value(i).toInt());
        } else if(practice_lap_labels_.at(i) == QString("practice_lap_id")) {
            return_lap.setID(record.value(i).toInt());
        }
    }

    return return_lap;
}

QList<array<int, 2> > *databasehandler::readRaces()
{
    QMutexLocker locker(&ThreadResourceManager::getInstance().getDBMutex());

    if (!beginConnection()) {
        qDebug() << "Begin connection failed.";
        return 0;
    }

    QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());

    QSqlQuery query = db.exec(getRaceQuery());

    query.next();

    qDebug() << "DBHandler races: " <<"Rows: " << query.size() << "Cols: " << query.record().count();
    QList< array<int,2> > *return_data = new QList< array<int,2> >();

    while ( query.isActive() ) {
        array<int,2> race = parseRaceRow(query.record());

        return_data->append(race);

        if(!query.next()) query.finish();
    }

    endConnection();

    return return_data;
}

array<int,2> databasehandler::parseRaceRow(QSqlRecord record)
{
    array<int,2> race;
    race.at(0) = record.value(1).toInt();
    race.at(1) = record.value(0).toInt();

    return race;
}

QList<Weather> *databasehandler::readWeatherData()
{
    QMutexLocker locker(&ThreadResourceManager::getInstance().getDBMutex());

    if (!beginConnection()) {
        qDebug() << "Begin connection failed.";
        return 0;
    }

    QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());

    QSqlQuery query = db.exec(getWeatherQuery());

    query.next();

    qDebug() << "DBHandler weather: " <<"Rows: " << query.size() << "Cols: " << query.record().count();
    QList<Weather> *return_data = new QList<Weather>();

    while ( query.isActive() ) {
        Weather weather = parseWeatherRow(query.record());

        return_data->append(weather);

        if(!query.next()) query.finish();
    }

    endConnection();

    return return_data;
}

Weather databasehandler::parseWeatherRow(QSqlRecord record)
{
    Weather weather;

    weather.setQ1Status(Weather::toStatus(record.value(0).toString()));
    weather.setQ1Temperature(record.value(1).toInt());
    weather.setQ1Humidity(record.value(2).toInt());

    weather.setQ2Status(Weather::toStatus(record.value(3).toString()));
    weather.setQ2Temperature(record.value(4).toInt());
    weather.setQ2Humidity(record.value(5).toInt());

    weather.setRaceQ1Temperature(record.value(6).toInt(), record.value(7).toInt());
    weather.setRaceQ1Humidity(record.value(8).toInt(), record.value(9).toInt());
    weather.setRaceQ1RainProbability(record.value(10).toInt(), record.value(11).toInt());

    weather.setRaceQ2Temperature(record.value(12).toInt(), record.value(13).toInt());
    weather.setRaceQ2Humidity(record.value(14).toInt(), record.value(15).toInt());
    weather.setRaceQ2RainProbability(record.value(16).toInt(), record.value(17).toInt());

    weather.setRaceQ3Temperature(record.value(18).toInt(), record.value(19).toInt());
    weather.setRaceQ3Humidity(record.value(20).toInt(), record.value(21).toInt());
    weather.setRaceQ3RainProbability(record.value(22).toInt(), record.value(23).toInt());

    weather.setRaceQ4Temperature(record.value(24).toInt(), record.value(25).toInt());
    weather.setRaceQ4Humidity(record.value(26).toInt(), record.value(27).toInt());
    weather.setRaceQ4RainProbability(record.value(28).toInt(), record.value(29).toInt());

    weather.setTrackID(record.value(30).toInt());
    weather.setSeason(record.value(31).toInt());
    weather.setID(record.value(32).toInt());

    return weather;
}

QList<Car> *databasehandler::readCars()
{
    QMutexLocker locker(&ThreadResourceManager::getInstance().getDBMutex());

    if (!beginConnection()) {
        qDebug() << "Begin connection failed.";
        return 0;
    }

    QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());

    QSqlQuery query = db.exec(getCarDataQuery());

    qDebug() << "DBHandler cars: " <<"Rows: " << query.size() << "Cols: " << query.record().count();

    query.next();

    QList<Car> *return_data = new QList<Car>();

    while ( query.isActive() ) {
        Car car = parseCarRow(query.record());

        return_data->append(car);

        if(!query.next()) query.finish();
    }

    endConnection();

    return return_data;
}

Car databasehandler::parseCarRow(QSqlRecord record)
{
    Car row;

    row.setPower(record.value(0).toInt());
    row.setHandling(record.value(1).toInt());
    row.setAcceleration(record.value(2).toInt());

    row.setChassis(record.value(3).toInt(), record.value(14).toInt());

    row.setEngine(record.value(4).toInt(), record.value(15).toInt());
    row.setFrontWing(record.value(5).toInt(), record.value(16).toInt());
    row.setRearWing(record.value(6).toInt(), record.value(17).toInt());
    row.setUnderbody(record.value(7).toInt(), record.value(18).toInt());
    row.setSidepods(record.value(8).toInt(), record.value(19).toInt());

    row.setCooling(record.value(9).toInt(), record.value(20).toInt());
    row.setGearbox(record.value(10).toInt(), record.value(21).toInt());
    row.setBrakes(record.value(11).toInt(), record.value(22).toInt());
    row.setSuspension(record.value(12).toInt(), record.value(23).toInt());
    row.setElectronics(record.value(13).toInt(), record.value(24).toInt());

    row.setSeason(record.value(25).toInt());
    row.setTrackID(record.value(26).toInt());

    return row;
}

QList<Driver> *databasehandler::readDrivers()
{
    QMutexLocker locker(&ThreadResourceManager::getInstance().getDBMutex());

    if (!beginConnection()) {
        qDebug() << "Begin connection failed.";
        return 0;
    }

    QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());

    QSqlQuery query = db.exec(getDriverDataQuery());

    query.next();

    qDebug() << "DBHandler drivers: " <<"Rows: " << query.size() << "Cols: " << query.record().count();
    QList<Driver> *return_data = new QList<Driver>();

    while ( query.isActive() ) {
        Driver driver = parseDriverRow(query.record());

        return_data->append(driver);

        if(!query.next()) query.finish();
    }

    endConnection();

    return return_data;
}

Driver databasehandler::parseDriverRow(QSqlRecord record)
{
    Driver row;

    row.setOverall(record.value(0).toInt());
    row.setConcentration(record.value(1).toInt());
    row.setTalent(record.value(2).toInt());
    row.setAggressiveness(record.value(3).toInt());
    row.setExperience(record.value(4).toInt());
    row.setTechnicalInsight(record.value(5).toInt());
    row.setStamina(record.value(6).toInt());
    row.setCharisma(record.value(7).toInt());
    row.setMotivation(record.value(8).toInt());
    row.setWeight(record.value(9).toInt());
    row.setReputation(record.value(10).toInt());
    row.setAge(record.value(11).toInt());

    row.setSeason(record.value(12).toInt());
    row.setTrackID(record.value(13).toInt());

    return row;
}

QList<Risk> *databasehandler::readRisks()
{
    QMutexLocker locker(&ThreadResourceManager::getInstance().getDBMutex());

    if (!beginConnection()) {
        qDebug() << "Begin connection failed.";
        return 0;
    }

    QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());

    QSqlQuery query = db.exec(getRiskDataQuery());

    query.next();

    qDebug() << "DBHandler risks: " <<"Rows: " << query.size() << "Cols: " << query.record().count();
    QList<Risk> *return_data = new QList<Risk>();

    while ( query.isActive() ) {
        Risk risk = parseRiskRow(query.record());

        return_data->append(risk);

        if(!query.next()) query.finish();
    }

    endConnection();

    return return_data;
}

Risk databasehandler::parseRiskRow(QSqlRecord record)
{
    Risk row;

    array<int,5> risks;

    risks.at(0) = record.value(4).toInt();
    risks.at(1) = record.value(2).toInt();
    risks.at(2) = record.value(0).toInt();
    risks.at(3) = record.value(1).toInt();
    risks.at(4) = record.value(3).toInt();

    QString srisk = record.value(5).toString();

    int season = record.value(6).toInt();
    int track_id = record.value(7).toInt();

    row.setRisk(risks);
    row.setStartingRisk(srisk);
    row.setSeason(season);
    row.setTrackID(track_id);

    return row;
}

QList<Lap> *databasehandler::readLapData(int track_id, int season)
{
    QMutexLocker locker(&ThreadResourceManager::getInstance().getDBMutex());

    if (!beginConnection()) {
        qDebug() << "Begin connection failed.";
        return 0;
    }

    QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());

    QSqlQuery query = db.exec(getLapQuery(track_id, season));

    query.next();

    qDebug() << "DBHandler lap_data: " <<"Rows: " << query.size() << "Cols: " << query.record().count();
    QList<Lap> *return_data = new QList<Lap>();

    while ( query.isActive() ) {
        Lap temp_lap = parseLapDataRow(query.record());

        return_data->append(temp_lap);

        if(!query.next()) query.finish();
    }

    endConnection();

    return return_data;
}

Lap databasehandler::parseLapDataRow(QSqlRecord record)
{

    Lap row;

    row.setSeason(record.value(0).toInt());
    row.setLapNum(record.value(1).toInt());
    row.setLapTime(record.value(2).toString());
    row.setPos(record.value(3).toInt());
    row.setTyreType(record.value(4).toString());
    row.setWeather(record.value(5).toString());
    row.setTemperature(record.value(6).toInt());
    row.setHumidity(record.value(7).toInt());
    row.setEvents(record.value(8).toString());
    row.setTrackID(record.value(9).toInt());

    return row;
}

QList<Stint> *databasehandler::readStintData(int track_id, int season)
{
    QMutexLocker locker(&ThreadResourceManager::getInstance().getDBMutex());

    if (!beginConnection()) {
        qDebug() << "Begin connection failed.";
        return 0;
    }

    QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());

    QSqlQuery query = db.exec(DatabaseStrings::getStintQuery(track_id, season));

    bool has_first = query.first();

    qDebug() << "DBHandler stints: " <<"Rows: " << query.size() << "Cols: " << query.record().count();
    QList<Stint> *return_data = new QList<Stint>();

    while ( has_first && query.isActive() ) {
        Stint temp_stint = parseStintRow(query.record());

        return_data->append(temp_stint);

        if(!query.next()) query.finish();
    }

    endConnection();

    return return_data;
}

Stint databasehandler::parseStintRow(QSqlRecord record)
{
    Stint row;

    row.setLaps(record.value(0).toInt());
    row.setFuelUsed(record.value(1).toDouble());
    row.setFuelConsumption(record.value(2).toDouble());
    row.setFinalP(record.value(3).toInt());
    row.setAvgHumidity(record.value(4).toDouble());
    row.setAvgTemperature(record.value(5).toDouble());
    row.setTyreType(record.value(6).toString());
    row.setWeather(record.value(7).toString());
    row.setKM(record.value(8).toDouble());
    row.setStartLap(record.value(9).toInt());
    row.setPitStop(record.value(10).toDouble());
    row.setStartFuel(record.value(11).toInt());
    row.setSeason(record.value(12).toInt());
    row.setTrackID(record.value(13).toInt());

    return row;
}

shared_ptr<mat> databasehandler::parseFullPracticeLapData(QList<QStringList> *raw_data, bool debug)
{
    int rows = 0;
    int cols = 0;

    if(raw_data == 0) return shared_ptr<mat>(new mat(rows,cols));

    if(raw_data->size() > 0) cols = raw_data->at(0).size();
    if(raw_data->size() > 1) rows = raw_data->size()-1;

    if(debug) qDebug() << "parseFullPracticeLapData:" << "Rows" << rows << "Columns" << cols;

    shared_ptr<mat> pld = shared_ptr<mat>(new mat(rows,cols));

    if(debug && raw_data->size() > 0) qDebug() << "Column names:" << raw_data->at(0);

    for(int i = 0; i < rows; ++i) {

        for(int j = 0; j < cols; ++j) {

            if ( arma_practice_lap_labels_.at(j).contains(QString("Comment")) ) {
                        pld->at(i,j) = getPracticeCommentValue((raw_data->at(i+1).at(j)));
            } else if ((raw_data->at(i+1).at(j)).size() == 0) {
                pld->at(i,j) = -1;
            } else if ( arma_practice_lap_labels_.at(j) == QString("Q1Status")) {
                pld->at(i,j) = Weather::toDouble((raw_data->at(i+1).at(j)));
            } else if ( arma_practice_lap_labels_.at(j) == QString("TDownforce")) {
                pld->at(i,j) = Track::Downforce::toDouble((raw_data->at(i+1).at(j)));
            } else if ( arma_practice_lap_labels_.at(j) == QString("TOvertaking") ) {
                pld->at(i,j) = Track::Overtaking::toDouble((raw_data->at(i+1).at(j)));
            } else if ( arma_practice_lap_labels_.at(j) == QString("TSuspension") ) {
                pld->at(i,j) = Track::Suspension::toDouble((raw_data->at(i+1).at(j)));
            } else if ( arma_practice_lap_labels_.at(j) == QString("TFuelConsumption") ) {
                pld->at(i,j) = Track::FuelConsumption::toDouble((raw_data->at(i+1).at(j)));
            } else if ( arma_practice_lap_labels_.at(j) == QString("TTyreWear") ) {
                pld->at(i,j) = Track::TyreWear::toDouble((raw_data->at(i+1).at(j)));
            } else if ( arma_practice_lap_labels_.at(j) == QString("TGrip") ) {
                pld->at(i,j) = Track::Grip::toDouble((raw_data->at(i+1).at(j)));
            } else if ((raw_data->at(i+1).at(j)).toDouble() == 0.0) {
                //qDebug() << stint_data_labels_.at(i) << record.value(i).toString();
                pld->at(i,j) = (raw_data->at(i+1).at(j)).toDouble();
            } else {
                pld->at(i,j) = (raw_data->at(i+1).at(j)).toDouble();
            }
        }

    }

    return pld;
}

shared_ptr<mat> databasehandler::parseStintData(QList<QStringList> *raw_data, bool debug)
{
    int rows = 0;
    int cols = 0;

    if(raw_data == 0) return shared_ptr<mat>(new mat(rows,cols));

    if(raw_data->size() > 0) cols = raw_data->at(0).size();
    if(raw_data->size() > 1) rows = raw_data->size()-1;

    if(debug) qDebug() << "parseStintData:" << "Rows" << rows << "Columns" << cols;

    shared_ptr<mat> st_data = shared_ptr<mat>(new mat(rows,cols));

    if(debug && raw_data->size() > 0) qDebug() << "Column names:" << raw_data->at(0);

    for(int i = 0; i < rows; ++i) {

        for(int j = 0; j < cols; ++j) {

            if (raw_data->at(i+1).at(j).size() == 0) {
                st_data->at(i,j) = -1;
            } else if ( stint_data_labels_.at(j) == QString("STyreType") ) {
                st_data->at(i,j) = TyreType(raw_data->at(i+1).at(j)).toDouble();
            } else if ( stint_data_labels_.at(j) == QString("SWeather")) {
                st_data->at(i,j) = Weather::toDouble(raw_data->at(i+1).at(j));
            } else if ( stint_data_labels_.at(j) == QString("TDownforce")) {
                st_data->at(i,j) = Track::Downforce::toDouble(raw_data->at(i+1).at(j));
            } else if ( stint_data_labels_.at(j) == QString("TOvertaking") ) {
                st_data->at(i,j) = Track::Overtaking::toDouble(raw_data->at(i+1).at(j));
            } else if ( stint_data_labels_.at(j) == QString("TSuspension") ) {
                st_data->at(i,j) = Track::Suspension::toDouble(raw_data->at(i+1).at(j));
            } else if ( stint_data_labels_.at(j) == QString("TFuelConsumption") ) {
                st_data->at(i,j) = Track::FuelConsumption::toDouble(raw_data->at(i+1).at(j));
            } else if ( stint_data_labels_.at(j) == QString("TTyreWear") ) {
                st_data->at(i,j) = Track::TyreWear::toDouble(raw_data->at(i+1).at(j));
            } else if ( stint_data_labels_.at(j) == QString("TGrip") ) {
                st_data->at(i,j) = Track::Grip::toDouble(raw_data->at(i+1).at(j));
            } else if (raw_data->at(i+1).at(j).toDouble() == 0.0) {
                //qDebug() << stint_data_labels_.at(i) << raw_data->at(i+1).at(j);
                st_data->at(i,j) = raw_data->at(i+1).at(j).toDouble();
            } else {
                st_data->at(i,j) = raw_data->at(i+1).at(j).toDouble();
            }
        }

    }

    return st_data;
}

shared_ptr<mat> databasehandler::parseCTTimeData(QList<QStringList> *raw_data, bool debug)
{
    QList<QStringList> *raw_data2 = new QList<QStringList>();

    if(raw_data != 0 && raw_data->size() > 0) {
        ct_time_data_label_indexes_.clear();

        for(int i = 0; i < raw_data->at(0).size(); ++i) {
            ct_time_data_label_indexes_.insert(raw_data->at(0).at(i), i);
        }

        raw_data->removeAt(0);

        int lap_time_index = ct_time_data_label_indexes_.value(QStringLiteral("LapTime"));
        int stint_id_index = ct_time_data_label_indexes_.value(QStringLiteral("STID"));

        int stint_id = -1;

        double lap_time = 0;
        int counter = 0;

        QRegularExpression regexp(QStringLiteral("([\\d]?)[:]?([\\d]{2})[.]([\\d]{3})"));

        foreach(QStringList row, *raw_data) {

            if(QString::number(stint_id) != row.at(stint_id_index)) {

                if(stint_id != -1) {
                    QStringList temp = row;
                    temp[lap_time_index] = QString::number(lap_time / counter);
                    raw_data2->append(temp);

                    lap_time = 0;
                    counter = 0;

                    stint_id = row.at(stint_id_index).toInt();
                }

            }

            QRegularExpressionMatchIterator iter = regexp.globalMatch(row.at(lap_time_index));

            if(iter.hasNext()) {
                QRegularExpressionMatch match = iter.next();
                double temp = match.captured(1).toDouble() * 60 + match.captured(2).toDouble() + match.captured(3).toDouble() / 1000;
                lap_time += temp;
            }

            stint_id = row.at(stint_id_index).toInt();
            ++counter;
        }

        if(stint_id != -1) {
            QStringList temp = raw_data->at(raw_data->size()-1);
            temp[ct_time_data_label_indexes_.value(QStringLiteral("LapTime"))] = QString::number(lap_time / counter);
            raw_data2->append(temp);
        }

    }

    int rows = 0;
    int cols = 0;

    if(raw_data == 0 || raw_data2 == 0) return shared_ptr<mat>(new mat(rows,cols));

    if(raw_data2->size() > 0) cols = raw_data2->at(0).size();
    if(raw_data2->size() > 0) rows = raw_data2->size();

    if(debug) qDebug() << "parseStintData:" << "Rows" << rows << "Columns" << cols;

    shared_ptr<mat> ct_time_data = shared_ptr<mat>(new mat(rows,cols));

    if(debug && raw_data2->size() > 0) qDebug() << "Column names:" << ct_time_data_label_indexes_;

    uword weather_index = ct_time_data_label_indexes_.value(QString("SWeather"));
    uword event_index = ct_time_data_label_indexes_.value(QString("Events"));

    for(uword i = 0; i < rows; ++i) {

        for(uword j = 0; j < cols; ++j) {
            if(raw_data2->at(i).at(j).size() == 0 && i != event_index) {
                ct_time_data->at(i,j) = -1;
            } else if ( i == weather_index ) {
                ct_time_data->at(i,j) = Weather::toDouble(raw_data2->at(i).at(j));
            } else if (raw_data2->at(i).at(j).toDouble() == 0.0) {
                //qDebug() << stint_data_labels_.at(i) << record.value(i).toString();
                ct_time_data->at(i,j) = raw_data2->at(i).at(j).toDouble();
            } else {
                ct_time_data->at(i,j) = raw_data2->at(i).at(j).toDouble();
            }
        }
    }

    return ct_time_data;

}

shared_ptr<mat> databasehandler::parseFuelTimeData(QList<QStringList> *raw_data, bool debug)
{
    int rows = 0;
    int cols = 0;

    if(raw_data == 0) return shared_ptr<mat>(new mat(rows,cols));

    fuel_time_data_label_indexes_.clear();

    for(int i = 0; i < raw_data->at(0).size(); ++i) {
        fuel_time_data_label_indexes_.insert(raw_data->at(0).at(i), i);
    }

    raw_data->removeAt(0);

    if(raw_data->size() > 0) cols = raw_data->at(0).size();
    if(raw_data->size() > 0) rows = raw_data->size();

    if(debug) qDebug() << "parseFuelTimeData:" << "Rows" << rows << "Columns" << cols;

    shared_ptr<mat> fuel_time_data = shared_ptr<mat>(new mat(rows,cols));

    uword q1_status_index = fuel_time_data_label_indexes_.value(QString("Q1Status"));
    uword q2_status_index = fuel_time_data_label_indexes_.value(QString("Q2Status"));

    for(uword i = 0; i < rows; ++i) {

        for(uword j = 0; j < cols; ++j) {
            if ( (i == q1_status_index) || (i == q2_status_index) ) {
                fuel_time_data->at(i,j) = Weather::toDouble(raw_data->at(i).at(j));
            } else {
                fuel_time_data->at(i,j) = raw_data->at(i).at(j).toDouble();
            }
        }

    }

    return fuel_time_data;
}

shared_ptr<mat> databasehandler::parseArmaRaceData(QList<QStringList> *raw_data, bool debug)
{
    int rows = 0;
    int cols = 0;

    if(raw_data == 0) return shared_ptr<mat>(new mat(rows,cols));

    arma_race_data_labels_indexes_.clear();

    for(int i = 0; i < raw_data->at(0).size(); ++i) {
        arma_race_data_labels_indexes_.insert(raw_data->at(0).at(i), i);
    }

    raw_data->removeAt(0);

    if(raw_data->size() > 0) cols = raw_data->at(0).size();
    if(raw_data->size() > 0) rows = raw_data->size();

    if(debug) qDebug() << "parseArmaRaceData:" << "Rows" << rows << "Columns" << cols;

    shared_ptr<mat> race_data = shared_ptr<mat>(new mat(rows,cols));

    uword weather_index = arma_race_data_labels_indexes_.value(QStringLiteral("SWeather"));
    uword downforce_index = arma_race_data_labels_indexes_.value(QStringLiteral("TDownforce"));
    uword overtaking_index = arma_race_data_labels_indexes_.value(QStringLiteral("TOvertaking"));
    uword suspension_index = arma_race_data_labels_indexes_.value(QStringLiteral("TSuspension"));
    uword fuel_consumption_index = arma_race_data_labels_indexes_.value(QStringLiteral("TFuelConsumption"));
    uword tyre_wear_index = arma_race_data_labels_indexes_.value(QStringLiteral("TTyreWear"));
    uword grip_index = arma_race_data_labels_indexes_.value(QStringLiteral("TGrip"));

    for(uword i = 0; i < rows; ++i) {

        for(uword j = 0; j < cols; ++j) {
            if(raw_data->at(i).at(j).size() == 0) {
                race_data->at(i,j) = -1;
            } else if(j == weather_index) {
                race_data->at(i,j) =  Weather::toDouble(raw_data->at(i).at(j));
            } else if(j == downforce_index) {
                race_data->at(i,j) =  Track::Downforce::toDouble(raw_data->at(i).at(j));
            } else if(j == overtaking_index) {
                race_data->at(i,j) =  Track::Overtaking::toDouble(raw_data->at(i).at(j));
            } else if(j == suspension_index) {
                race_data->at(i,j) =  Track::Suspension::toDouble(raw_data->at(i).at(j));
            } else if(j == fuel_consumption_index) {
                race_data->at(i,j) =  Track::FuelConsumption::toDouble(raw_data->at(i).at(j));
            } else if(j == tyre_wear_index) {
                race_data->at(i,j) =  Track::TyreWear::toDouble(raw_data->at(i).at(j));
            } else if(j == grip_index) {
                race_data->at(i,j) =  Track::Grip::toDouble(raw_data->at(i).at(j));
            } else {
                race_data->at(i,j) = raw_data->at(i).at(j).toDouble();
            }
        }

    }

    return race_data;
}

shared_ptr<mat> databasehandler::parseTyreDiffData(QList<QStringList> *raw_data, bool debug)
{
    int rows = 0;
    int cols = 0;

    if(raw_data == 0) return shared_ptr<mat>(new mat(rows,cols));

    tyre_diff_data_label_indexes_.clear();

    for(int i = 0; i < raw_data->at(0).size(); ++i) {
        tyre_diff_data_label_indexes_.insert(raw_data->at(0).at(i), i);
    }

    raw_data->removeAt(0);

    if(raw_data->size() > 0) cols = raw_data->at(0).size();
    if(raw_data->size() > 0) rows = raw_data->size();

    if(debug) qDebug() << "parseTyreDiffData:" << "Rows" << rows << "Columns" << cols;

    shared_ptr<mat> race_data = shared_ptr<mat>(new mat(rows,cols));

    uword soft_time_index = tyre_diff_data_label_indexes_.value(QStringLiteral("SoftTime"));
    uword es_time_index = tyre_diff_data_label_indexes_.value(QStringLiteral("ESTime"));

    for(uword i = 0; i < rows; ++i) {

        for(uword j = 0; j < cols; ++j) {
            if(j  == soft_time_index || j == es_time_index) {

                race_data->at(i,j) = Transformations::transformTimeToDouble(raw_data->at(i).at(j));

            } else race_data->at(i,j) = raw_data->at(i).at(j).toDouble();
        }

    }

    return race_data;
}

shared_ptr<mat> databasehandler::parsePracticeData(QList<QStringList> *raw_data, bool debug)
{
    int rows = 0;
    int cols = 0;

    if(raw_data == 0) return shared_ptr<mat>(new mat(rows,cols));

   practice_data_labels_indexes_.clear();

    for(int i = 0; i < raw_data->at(0).size(); ++i) {
        practice_data_labels_indexes_.insert(raw_data->at(0).at(i), i);
    }

    raw_data->removeAt(0);

    if(raw_data->size() > 0) cols = raw_data->at(0).size();
    if(raw_data->size() > 0) rows = raw_data->size();

    if(debug) qDebug() << "parsePracticeData:" << "Rows" << rows << "Columns" << cols;

    shared_ptr<mat> practice_data = shared_ptr<mat>(new mat(rows,cols));

    QRegularExpression time_regexp(QStringLiteral("([\\d]?)[:]?([\\d]{2})[.]([\\d]{3})"));

    uword net_time_index = practice_data_labels_indexes_.value(QStringLiteral("PNetTime"));
    uword tyre_type_index = practice_data_labels_indexes_.value(QStringLiteral("PTyreType"));
    uword weather_index = practice_data_labels_indexes_.value(QStringLiteral("PWeather"));
    uword downforce_index = practice_data_labels_indexes_.value(QStringLiteral("TDownforce"));
    uword overtaking_index = practice_data_labels_indexes_.value(QStringLiteral("TOvertaking"));
    uword suspension_index = practice_data_labels_indexes_.value(QStringLiteral("TSuspension"));
    uword fuel_consumption_index = practice_data_labels_indexes_.value(QStringLiteral("TFuelConsumption"));
    uword tyre_wear_index = practice_data_labels_indexes_.value(QStringLiteral("TTyreWear"));
    uword grip_index = practice_data_labels_indexes_.value(QStringLiteral("TGrip"));

    for(uword i = 0; i < rows; ++i) {

        for(uword j = 0; j < cols; ++j) {

            if( j == net_time_index ) {
                QString temp_time = raw_data->at(i).at(j);
                QRegularExpressionMatchIterator iter = time_regexp.globalMatch(temp_time);
                double time = 0;

                if( iter.hasNext() ) {
                    QRegularExpressionMatch match = iter.next();

                    if( match.capturedLength() >= 4 ) {
                        time += 60 * match.captured(1).toDouble();
                        time += match.captured(2).toDouble();
                        time += match.captured(3).toDouble() / 1000;
                    }
                }

                practice_data->at(i,j) = time;

            } else if ( j == tyre_type_index ) {
                practice_data->at(i,j) = TyreType(raw_data->at(i).at(j)).toDouble();
            } else if( j == weather_index ) {
                practice_data->at(i,j) = Weather::toDouble(raw_data->at(i).at(j));
            } else if( j == downforce_index ) {
                practice_data->at(i,j) = Track::Downforce::toDouble(raw_data->at(i).at(j));
            } else if( j == overtaking_index ) {
                practice_data->at(i,j) = Track::Overtaking::toDouble(raw_data->at(i).at(j));
            } else if( j == suspension_index ) {
                practice_data->at(i,j) = Track::Suspension::toDouble(raw_data->at(i).at(j));
            } else if ( j == fuel_consumption_index ) {
                practice_data->at(i,j) = Track::FuelConsumption::toDouble(raw_data->at(i).at(j));
            } else if ( j == tyre_wear_index ) {
                practice_data->at(i,j) = Track::TyreWear::toDouble(raw_data->at(i).at(j));
            } else if( j == grip_index ) {
                practice_data->at(i,j) = Track::Grip::toDouble(raw_data->at(i).at(j));
            } else {
                practice_data->at(i,j) = raw_data->at(i).at(j).toDouble();
            }

        }

    }



    return practice_data;

}

QList<array<double, 2> > *databasehandler::readMiscData(int track_id, int season)
{
    QMutexLocker locker(&ThreadResourceManager::getInstance().getDBMutex());

    if (!beginConnection()) {
        qDebug() << "Begin connection failed.";
        return 0;
    }

    QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());

    QSqlQuery query = db.exec(DatabaseStrings::getMiscDataQuery(track_id, season));

    bool has_first = query.first();

    qDebug() << "DBHandler misc data: " <<"Rows: " << query.size() << "Cols: " << query.record().count();
    QList< array<double,2> > *return_data = new QList< array<double,2> >();

    while ( has_first && query.isActive() ) {
        array<double,2> vals = {{ query.value(0).toDouble(), query.value(1).toDouble() }};

        return_data->append(vals);

        if(!query.next()) query.finish();
    }

    endConnection();

    return return_data;
}

shared_ptr<mat> databasehandler::readFuelTimeData()
{
    QMutexLocker locker(&ThreadResourceManager::getInstance().getDBMutex());

    if (!beginConnection()) {
        qDebug() << "Begin connection failed.";
        return 0;
    }

    QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());

    QSqlQuery query = db.exec(DatabaseStrings::getFuelTimeDataQuery());

    query.next();

    fuel_time_data_label_indexes_.clear();

    int col_num = query.size();
    int row_num = query.record().count();

    for(int i = 0; i < row_num; ++i) {
       fuel_time_data_label_indexes_.insert(query.record().fieldName(i), i);
    }

    qDebug() << "DBHandler fuel time data: " <<"Rows: " << col_num << "Cols: " << row_num;

    col_num = col_num > 0 ? col_num : 0;
    row_num = row_num > 0 ? row_num : 0;

    shared_ptr<mat> return_data = shared_ptr<mat> (new mat(col_num, row_num));

    int column = 0;

    while ( query.isActive() && col_num > 0) {
        vec temp_race = parseFuelTimeDataRow(query.record(), (col_num > 0 ? (unsigned int) col_num : 0));

        for(int i = 0; i < row_num; ++i) {
            return_data->at(column, i) = temp_race(i);
        }

        if(!query.next()) query.finish();

        column++;
    }

    endConnection();

    return return_data;
}

vec databasehandler::parseFuelTimeDataRow(const QSqlRecord &record, unsigned int columns)
{
    vec result_vec(columns);

    uword q1_status_index = fuel_time_data_label_indexes_.value(QString("Q1Status"));
    uword q2_status_index = fuel_time_data_label_indexes_.value(QString("Q2Status"));

    for(uword i = 0; i < columns; ++i) {
        if ( (i == q1_status_index) || (i == q2_status_index) ) {
            result_vec.at(i) = Weather::toDouble(record.value(i).toString());
        } else if (record.value(i).toDouble() == 0.0) {
            //qDebug() << stint_data_labels_.at(i) << record.value(i).toString();
            result_vec.at(i) = record.value(i).toDouble();
        } else {
            result_vec.at(i) = record.value(i).toDouble();
        }
    }

    return result_vec;
}

shared_ptr<mat> databasehandler::readTyreDiffData()
{
    QMutexLocker locker(&ThreadResourceManager::getInstance().getDBMutex());

    if (!beginConnection()) {
        qDebug() << "Begin connection failed.";
        return 0;
    }

    QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());

    QSqlQuery query = db.exec(DatabaseStrings::getTyreDiffDataQuery());

    query.next();

    tyre_diff_data_label_indexes_.clear();

    int col_num = query.size();
    int row_num = query.record().count();

    for(int i = 0; i < row_num; ++i) {
       tyre_diff_data_label_indexes_.insert(query.record().fieldName(i), i);
    }

    qDebug() << "DBHandler tyre diff data: " <<"Rows: " << col_num << "Cols: " << row_num;

    col_num = col_num > 0 ? col_num : 0;
    row_num = row_num > 0 ? row_num : 0;

    shared_ptr<mat> return_data = shared_ptr<mat> (new mat(col_num, row_num));

    int column = 0;

    while ( query.isActive() && col_num > 0) {
        vec temp_race = parseTyreDiffDataRow(query.record());

        for(int i = 0; i < row_num; ++i) {
            return_data->at(column, i) = temp_race(i);
        }

        if(!query.next()) query.finish();

        column++;
    }

    endConnection();

    return return_data;
}

vec databasehandler::parseTyreDiffDataRow(QSqlRecord record)
{
    vec result_vec(record.count());

    for(int i = 0; i < record.count(); ++i) {
        result_vec.at(i) = record.value(i).toDouble();
    }

    return result_vec;
}

databasehandler::databasehandler(QObject *parent) : QObject(parent),
    practice_data_(0),
    race_data_(0),
    stint_data_(0),
    arma_race_data_(0),
    practice_laps_(0),
    arma_practice_lap_data_(0),
    track_ids_(0),
    races_(0),
    weather_data_(0),
    cars_(0),
    drivers_(0),
    risks_(0),
    laps_(0),
    stints_(0),
    misc_data_(0),
    ct_time_data_(0),
    fuel_time_data_(0),
    tyre_diff_data_(0)
{
}

bool databasehandler::beginConnection()
{

    QSettings settings(General::ProgramName, General::CompanyName);
    QSqlDatabase db;

    QString type = settings.value(Settings::DatabaseTypeText, QVariant("QPSQL")).toString();
    QString name = settings.value(Settings::DatabaseNameText, QVariant("")).toString();
    QString username = settings.value(Settings::DatabaseUserNameText, QVariant("")).toString();
    QString password = settings.value(Settings::DatabasePasswordText, QVariant("")).toString();

    if ( type.size() == 0 || name.size() == 0 || username.size() == 0) return false;

    // previous !db.contains(QString("DBConnection") % type
    if (!db.contains(QString("DBConnection") + type)) {
        db = QSqlDatabase::addDatabase(type, QString("DBConnection") + type);
    } else {
        db = QSqlDatabase::database(QString("DBConnection") + type);
    }

    if (db.databaseName() != name) db.setDatabaseName(name);

    if (db.userName() != username) db.setUserName(username);

    if(db.password() != password)  db.setPassword(password);

    return db.open();
}

void databasehandler::endConnection()
{
    if(!QSqlDatabase::connectionNames().isEmpty()) {
        QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());
        db.close();
    }
}

shared_ptr<mat> databasehandler::getPracticeData(bool debug)
{
    if( practice_data_ == 0) {
        QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());
        QList<QStringList> *raw_data = DatabaseQueryHelper::queryPracticeData(db, debug);

        practice_data_ = parsePracticeData(raw_data, debug);
    }

    return practice_data_;

    // mlpack && shogun
}

QList<QStringList> *databasehandler::getRaceData(bool force, bool debug)
{
    if(force || race_data_ == 0) {
        QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());
        QList<QStringList> *raw_data = DatabaseQueryHelper::queryRaceData(db, debug);

        if(raw_data != 0 && raw_data->size() > 0) race_data_labels_ = raw_data->at(0);

        if(raw_data->size() > 0) raw_data->removeAt(0);

        race_data_ = raw_data;

    }

    return race_data_;
}

QList<QStringList> databasehandler::getTracks()
{
    if( tracks_.length() == 0 ) {
        QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());

        QList<QStringList> *raw_data = DatabaseQueryHelper::queryTracksTable(db, false);

        if(raw_data != 0 && raw_data->size() > 0) {
            track_labels_.clear();

            for(int i = 0; i < raw_data->at(0).size(); ++i) {
                track_labels_.append(raw_data->at(0).at(i));
            }

            raw_data->removeAt(0);

            tracks_ = *raw_data;
        }

        //tracks_ = readTracksData();
    }

    return tracks_;
}

shared_ptr<mat> databasehandler::getStintData(bool debug)
{
    if( stint_data_ == 0) {
        QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());
        QList<QStringList> *raw_data = DatabaseQueryHelper::queryStintData(db, debug);

        if(raw_data != 0 && raw_data->size() > 0) stint_data_labels_ = raw_data->at(0);

        stint_data_ = parseStintData(raw_data, debug);
    }

    return stint_data_;
}

shared_ptr<mat> databasehandler::getArmaRaceData(bool debug)
{
    if( arma_race_data_ == 0) {
        QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());
        QList<QStringList> *raw_data = DatabaseQueryHelper::queryFullRaceData(db, debug);

        arma_race_data_ = parseArmaRaceData(raw_data, debug);
    }

    return arma_race_data_;
}

shared_ptr<mat> databasehandler::getFullPracticeLapData(bool debug)
{
    if(arma_practice_lap_data_ == 0) {
        QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());
        QList<QStringList> *raw_data = DatabaseQueryHelper::queryFullPracticeData(db, debug);

        if(raw_data != 0 && raw_data->size() > 0) arma_practice_lap_labels_ = raw_data->at(0);

        arma_practice_lap_data_ = parseFullPracticeLapData(raw_data, debug);

    }

    return arma_practice_lap_data_;
}

const QStringList &databasehandler::getFullPracticeLapDataLabels()
{
    if(arma_practice_lap_labels_.size() == 0) {
        getFullPracticeLapData();
    }

    return arma_practice_lap_labels_;
}

QList<PracticeLap> databasehandler::getPracticeLaps(int track_id, int season, bool force)
{
    if(force || practice_laps_ == 0 || practice_laps_->size() == 0) {
        QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());
        QList<QStringList> *raw_data = DatabaseQueryHelper::queryPracticeTable(db, false);

        QList<PracticeLap> *plaps = new QList<PracticeLap>();

        foreach(QStringList raw_data_row, *raw_data) {
            plaps->append(*(PracticeLap::fromDatabaseStringList(raw_data_row)));
        }

        practice_laps_ = plaps;
    }

    if (track_id == 0 && season == 0) return *practice_laps_;

    QList<PracticeLap> return_laps;

    foreach(PracticeLap lap, *practice_laps_) {
        if((track_id > 0 && season > 0 && lap.getSeason() == season && lap.getTrackID() == track_id) ||
                (track_id == 0 && season > 0 && lap.getSeason() == season) ||
                (track_id > 0 && season == 0 && lap.getTrackID() == track_id)) {
            return_laps.append(lap);
        }
    }

    return return_laps;
}

const QList<Weather> &databasehandler::getWeatherData(bool force)
{
    if(force || weather_data_ == 0) {
        weather_data_ = readWeatherData();
    }

    return *weather_data_;
}

bool databasehandler::hasWeather(int track_id, int season)
{
    //QList<Weather> weather_data = getWeatherData();

    for(int i = 0; i < getWeatherData().size(); ++i) {
        if (getWeatherData().at(i).getTrackID() == track_id && getWeatherData().at(i).getSeason() == season)
            return true;
    }

    return false;
}

const QList<array<int, 2> >& databasehandler::getRaces(bool force)
{
    if(force || races_ == 0) {
        races_ = readRaces();
    }

    return *races_;
}

bool databasehandler::hasRace(int track_id, int season)
{
    QList< array<int,2> > race_data = getRaces();

    for(int i = 0; i < race_data.size(); ++i) {
        if (race_data.at(i).at(0) == track_id && race_data.at(i).at(1) == season)
            return true;
    }

    return false;
}

const QList<Car> &databasehandler::getCars(bool force)
{
    if(force || cars_ == 0) {
        cars_ = readCars();
    }

    return *cars_;
}

bool databasehandler::hasCar(int track_id, int season)
{
    QList<Car> cars = getCars();

    foreach(Car car, cars) {
        if(car.getTrackID() == track_id && car.getSeason() == season)
            return true;
    }

    return false;
}

const QList<Driver> &databasehandler::getDrivers(bool force)
{
    if(force || drivers_ == 0) {
        drivers_ = readDrivers();
    }

    return *drivers_;
}

bool databasehandler::hasDriver(int track_id, int season)
{
    QList<Driver> drivers = getDrivers();

    foreach(Driver driver, drivers) {
        if(driver.getTrackID() == track_id && driver.getSeason() == season)
            return true;
    }

    return false;
}

const QList<Risk> &databasehandler::getRisks(bool force)
{
    if(force || risks_ == 0) {
        risks_ = readRisks();
    }

    return *risks_;
}

bool databasehandler::hasRisk(int track_id, int season)
{
    QList<Risk> risks = getRisks();

    foreach(Risk risk, risks) {
        if(risk.getTrackID() == track_id && risk.getSeason() == season)
            return true;
    }

    return false;
}

const QList<Lap> &databasehandler::getLaps(int track_id, int season, bool force)
{
    if(force || laps_ == 0) {
        laps_ = readLapData(track_id, season);
    }

    return *laps_;
}

const QList<Stint> &databasehandler::getStints(int track_id, int season, bool force)
{
    if(force || stints_ == 0) {
        stints_ = readStintData(track_id, season);
    }

    return *stints_;
}

const QList<array<double, 2> > &databasehandler::getMiscData(int track_id, int season, bool force)
{
    if(force || misc_data_ == 0) {
        misc_data_ = readMiscData(track_id, season);
    }

    return *misc_data_;
}

int databasehandler::getTrackId(QString track_name)
{
    if(track_ids_ == 0) {
        QList< QStringList > track_data = getTracks();

        track_ids_ = new QMap<QString, int>();

        foreach(QStringList row, track_data) {
           track_ids_->insert( row.at(0), row.at(1).toInt() );
        }
    }

    if(track_ids_ != 0 && track_ids_->contains(track_name)) return track_ids_->value(track_name);

    return -1;
}

int databasehandler::getPracticeCommentValue(QString comment)
{
    if(practice_comments_.size() == 0) {
            int wcounter = 0;
            int ecounter = 0;
            int bcounter = 0;
            int gcounter = 0;
            int scounter = 0;

            QList<PracticeLap> practice_laps = getPracticeLaps();

            foreach (PracticeLap lap, practice_laps) {

                if(!practice_comments_.contains(lap.getWingComment())) {
                    practice_comments_.insert(lap.getWingComment(), wcounter);
                    ++wcounter;
                }

                if(!practice_comments_.contains(lap.getEngineComment())) {
                    practice_comments_.insert(lap.getEngineComment(), ecounter);
                    ++ecounter;
                }

                if(!practice_comments_.contains(lap.getBrakesComment())) {
                    practice_comments_.insert(lap.getBrakesComment(), bcounter);
                    ++bcounter;
                }

                if(!practice_comments_.contains(lap.getGearComment())) {
                    practice_comments_.insert(lap.getGearComment(), gcounter);
                    ++gcounter;
                }

                if(!practice_comments_.contains(lap.getSuspensionComment())) {
                    practice_comments_.insert(lap.getSuspensionComment(), scounter);
                    ++scounter;
                }
            }
    }

    return practice_comments_.value(comment);
}

shared_ptr<mat> databasehandler::getCTTimeData(bool force, bool debug)
{
    if( ct_time_data_ == 0 || force ) {
        QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());
        QList<QStringList> *raw_data = DatabaseQueryHelper::queryCTTimeData(db, debug);

        ct_time_data_ = parseCTTimeData(raw_data, debug);
    }

    return ct_time_data_;
}

shared_ptr<mat> databasehandler::getFuelTimeData(bool force, bool debug)
{
    if( fuel_time_data_ == 0 || force ) {
        QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());
        QList<QStringList> *raw_data = DatabaseQueryHelper::queryFuelTimeData(db, debug);

        fuel_time_data_ = parseFuelTimeData(raw_data, debug);

        //fuel_time_data_ = readFuelTimeData();
    }

    return fuel_time_data_;
}

shared_ptr<mat> databasehandler::getTyreDiffData(bool force, bool debug)
{
    if( tyre_diff_data_ == 0 || force) {

        QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());
        QList<QStringList> *raw_data = DatabaseQueryHelper::queryTyreDiffData(db, debug);

        tyre_diff_data_ = parseTyreDiffData(raw_data, debug);
        //tyre_diff_data_ = readTyreDiffData();
    }

    return tyre_diff_data_;
}
