#ifndef DATABASEHANDLER_H
#define DATABASEHANDLER_H

#include <array>
#include <memory>

#include <QObject>
#include <QString>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QList>
#include <QStringList>
#include <QMap>
#include <QMutex>
#include <QMutexLocker>

#include <mlpack/core.hpp>

#include "Objects/practicelap.h"
#include "Objects/weather.h"
#include "Objects/car.h"
#include "Objects/driver.h"
#include "Objects/risk.h"
#include "Objects/lap.h"
#include "Objects/stint.h"
#include "Misc/databasestrings.h"
#include "constantstrings.h"

class PracticeLap;

using std::array;
using std::shared_ptr;

using namespace arma;

class databasehandler : public QObject
{
    Q_OBJECT

private:

    QList< array<int,2> > *readRaces();
    array<int,2> parseRaceRow(QSqlRecord record);

    QList<Weather> *readWeatherData();

    Weather parseWeatherRow(QSqlRecord record);

    QList<Car> *readCars();
    Car parseCarRow(QSqlRecord record);

    QList<Driver> *readDrivers();
    Driver parseDriverRow(QSqlRecord record);

    QList<Risk> *readRisks();
    Risk parseRiskRow(QSqlRecord record);

    QList<Lap> *readLapData(int track_id = 0, int season = 0);
    Lap parseLapDataRow(QSqlRecord record);

    QList<Stint> *readStintData(int track_id, int season);
    Stint parseStintRow(QSqlRecord record);

    QList<PracticeLap> *readPracticeLapData();
    PracticeLap parsePracticeLapDataRow(QSqlRecord record);

    shared_ptr<mat> parseFullPracticeLapData(QList<QStringList> *raw_data, bool debug = false);

    shared_ptr<mat> parseStintData(QList<QStringList> *raw_data, bool debug = false);

    shared_ptr<mat> parseCTTimeData(QList<QStringList> *raw_data, bool debug = false);

    shared_ptr<mat> parseFuelTimeData(QList<QStringList> *raw_data, bool debug = false);

    shared_ptr<mat> parseArmaRaceData(QList<QStringList> *raw_data, bool debug = false);

    shared_ptr<mat> parseTyreDiffData(QList<QStringList> *raw_data, bool debug = false);

    shared_ptr<mat> parsePracticeData(QList<QStringList> *raw_data, bool debug = false);

    QList< array<double,2> > *readMiscData(int track_id, int season);

    shared_ptr<mat> readFuelTimeData();
    vec parseFuelTimeDataRow(const QSqlRecord &record, unsigned int columns);

    shared_ptr<mat> readTyreDiffData();
    vec parseTyreDiffDataRow(QSqlRecord record);

    shared_ptr<mat> practice_data_;
    QMap<QString,uword> practice_data_labels_indexes_;

    QList< QStringList > *race_data_;
    QStringList race_data_labels_;

    QList< QStringList > tracks_;
    QList< QString > track_labels_;

    QList<QString> stint_data_labels_;
    shared_ptr<mat> stint_data_;

    shared_ptr<mat> arma_race_data_;
    QMap<QString,uword> arma_race_data_labels_indexes_;

    QList<PracticeLap> *practice_laps_;
    QList<QString> practice_lap_labels_;

    shared_ptr<mat> arma_practice_lap_data_;
    QStringList arma_practice_lap_labels_;

    QMap<QString,int> *track_ids_;

    QList< array<int,2> > *races_;

    QList< Weather > *weather_data_;

    QList< Car > *cars_;

    QList< Driver > *drivers_;

    QList< Risk > *risks_;

    QList<Lap> *laps_;

    QList<Stint> *stints_;

    QList< array<double,2> > *misc_data_;

    QMap<QString,int> practice_comments_;

    shared_ptr<mat> ct_time_data_;
    QMap<QString,uword> ct_time_data_label_indexes_;

    shared_ptr<mat> fuel_time_data_;
    QMap<QString,uword> fuel_time_data_label_indexes_;

    shared_ptr<mat> tyre_diff_data_;
    QMap<QString,uword> tyre_diff_data_label_indexes_;


public:
    explicit databasehandler(QObject *parent = 0);

    bool beginConnection();
    void endConnection();

    shared_ptr<mat> getPracticeData(bool debug = false);
    QMap<QString, uword> getPracticeDataLabelsIndexes() {
        return practice_data_labels_indexes_;
    }

    QList<QStringList> *getRaceData( bool force = false, bool debug = false);
    const QList<QString> &getRaceDataLabels() { return race_data_labels_; }

    QList< QStringList > getTracks();

    shared_ptr<mat> getStintData(bool debug = false);
    QList<QString> getStintDataLabels() { return stint_data_labels_; }

    shared_ptr<mat> getArmaRaceData(bool debug = false);
    QMap<QString, uword> getArmaRaceDataLabelsIndexes() {
        return arma_race_data_labels_indexes_;
    }

    shared_ptr<mat> getFullPracticeLapData(bool debug = false);
    const QStringList &getFullPracticeLapDataLabels();

    QList<PracticeLap> getPracticeLaps(int track_id = 0, int season = 0, bool force = false);

    const QList< Weather >& getWeatherData(bool force = false);
    bool hasWeather(int track_id, int season);

    const QList< array<int,2> >& getRaces(bool force = false);
    bool hasRace(int track_id, int season);

    const QList<Car> &getCars(bool force = false);
    bool hasCar(int track_id, int season);

    const QList<Driver> &getDrivers(bool force = false);
    bool hasDriver(int track_id, int season);

    const QList<Risk> &getRisks(bool force = false);
    bool hasRisk(int track_id, int season);

    const QList<Lap> &getLaps(int track_id = 0, int season = 0, bool force = false);

    const QList<Stint> &getStints(int track_id = 0, int season = 0, bool force = false);

    const QList< array<double,2> > &getMiscData(int track_id = 0, int season = 0, bool force = false);

    int getTrackId(QString track_name);

    int getPracticeCommentValue(QString comment);

    shared_ptr<mat> getCTTimeData(bool force = false, bool debug = false);
    QMap<QString, uword> getCTTimeLabelsIndexes() {
        return ct_time_data_label_indexes_;
    }

    shared_ptr<mat> getFuelTimeData(bool force = false, bool debug = false);
    QMap<QString, uword> getFuelTimeLabelsIndexes() {
        return fuel_time_data_label_indexes_;
    }

    shared_ptr<mat> getTyreDiffData(bool force = false, bool debug = false);
    QMap<QString, uword> getTyreDiffDataLabelsIndexes() {
        return tyre_diff_data_label_indexes_;
    }


signals:

public slots:
};

#endif // DATABASEHANDLER_H
