#ifndef GPROCOOKIEMANAGER_H
#define GPROCOOKIEMANAGER_H

#include <QNetworkAccessManager>
#include <QUrl>
#include <QByteArray>
#include <QNetworkReply>
#include <QNetworkCookie>
#include <QNetworkCookieJar>

class gproCookieManager : public QObject
{
    Q_OBJECT

    QNetworkAccessManager *mManager;
    QUrl login_url_;
    QByteArray login_;
    QUrl other_url_;
    bool is_executing_;

public:
    explicit gproCookieManager(QObject *parent = 0);

    void sendLoginRequest(const QUrl &url, const QByteArray &data);

    QNetworkReply* sendGetRequest(const QUrl &url);

    virtual ~gproCookieManager(){}

signals:

public slots:

private slots:
};

#endif // GPROCOOKIEMANAGER_H
