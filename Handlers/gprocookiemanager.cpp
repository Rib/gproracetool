#include "gprocookiemanager.h"

#include <QEventLoop>
#include <QObject>

gproCookieManager::gproCookieManager(QObject *parent) : QObject(parent),
    is_executing_(false)
{
    mManager = new QNetworkAccessManager(this);
    mManager->setCookieJar(new QNetworkCookieJar(this));

}

void gproCookieManager::sendLoginRequest(const QUrl &url, const QByteArray &data)
{
    login_url_ = url;
    login_ = data;

    QNetworkRequest request(login_url_);
    request.setHeader(QNetworkRequest::ContentTypeHeader, false);
    request.setAttribute(QNetworkRequest::RedirectionTargetAttribute,true);

    QNetworkReply *reply = mManager->post(request, data);

    QEventLoop eventLoop;
    //QObject::connect(mManager, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(loginOnFetch(QNetworkReply*)));
    QObject::connect(mManager, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));
    eventLoop.exec();
}

QNetworkReply *gproCookieManager::sendGetRequest(const QUrl &url)
{
    other_url_ = url;

    QList<QNetworkCookie>  cookies = mManager->cookieJar()->cookiesForUrl(login_url_);

    QVariant var;
    var.setValue(cookies);

    QNetworkRequest request(other_url_);
    request.setHeader(QNetworkRequest::CookieHeader, var);
    request.setAttribute(QNetworkRequest::RedirectionTargetAttribute,false);

    QNetworkReply *reply = mManager->get(request);

    QEventLoop eventLoop;
    QObject::connect(mManager, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));
    eventLoop.exec();

    return reply;
}
