#include "constantstrings.h"

const QString ConstantStrings::DefaultDatabaseName = QStringLiteral("GproDatabase");
const QString ConstantStrings::SynchronizedText = QStringLiteral("Synced");
const QString ConstantStrings::NotSynchronizedText = QStringLiteral("Not Synced");
const QString ConstantStrings::DownloadingText = QStringLiteral("Downloading...");

ConstantStrings::ConstantStrings()
{

}
