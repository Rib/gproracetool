#ifndef DATABASEQUERYHELPER_H
#define DATABASEQUERYHELPER_H

#include <QString>
#include <QList>
#include <QStringList>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlError>
#include <QMutex>
#include <QMutexLocker>

#include <QRegularExpression>

#include <QSqlDriver>

#include <sqlite3.h>

class DatabaseQueryHelper
{
public:
    DatabaseQueryHelper();

    static QList<QStringList> *queryData(QSqlDatabase &db, const QString &query_str, const QString &debug_str = "", bool debug = false);

    static QList<QStringList> *querySimpleData(QSqlDatabase &db, const QString &table, int track_id = -1, int season = -1,
                                               const QString &debug_str = "", bool debug = false);

    static QList<QStringList> *queryFullPracticeData(QSqlDatabase &db, bool debug);

    static QList<QStringList> *queryRaceData(QSqlDatabase &db, bool debug);

    static QList<QStringList> *queryStintData(QSqlDatabase &db, bool debug);

    static QList<QStringList> *queryCTTimeData(QSqlDatabase &db, bool debug);

    static QList<QStringList> *queryFuelTimeData(QSqlDatabase &db, bool debug);

    static QList<QStringList> *queryFullRaceData(QSqlDatabase &db, bool debug);

    static QList<QStringList> *queryTyreDiffData(QSqlDatabase &db, bool debug);

    static QList<QStringList> *queryPracticeData(QSqlDatabase &db, bool debug);

    static QList<QStringList> *queryPracticeTable(QSqlDatabase &db, bool debug);

    static QList<QStringList> *queryTracksTable(QSqlDatabase &db, bool debug);

private:

    static void addRegexpFunction(QSqlDatabase &db, bool debug);

    static void sqlite_regexp(sqlite3_context* context, int argc, sqlite3_value** values) {
        QString ret;

        QString reg((const char*)sqlite3_value_text(values[0]));
        QString text((const char*)sqlite3_value_text(values[1]));

        QRegularExpression regexp(reg);

        QRegularExpressionMatchIterator iter = regexp.globalMatch(text);

        int has_success = 0;

        if(iter.hasNext()) {
            has_success = 1;
            QRegularExpressionMatch match = iter.next();
            ret = match.captured(1);
        }

        QByteArray ba = ret.toLatin1();
        const char *c_str2 = ba.data();

        sqlite3_result_text(context, c_str2, has_success,0);
    }
};

#endif // DATABASEQUERYHELPER_H
