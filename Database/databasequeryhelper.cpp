#include "databasequeryhelper.h"

#include "Misc/databasestrings.h"
#include "Misc/threadresourcemanager.h"
#include <QDebug>

DatabaseQueryHelper::DatabaseQueryHelper()
{

}

QList<QStringList> *DatabaseQueryHelper::queryData(QSqlDatabase &db, const QString &query_str, const QString &debug_str, bool debug)
{
    QMutexLocker locker(&ThreadResourceManager::getInstance().getDBMutex());

    QList<QStringList> *values = new QList<QStringList>();

    if(db.open()) {
        addRegexpFunction(db, debug);

        QSqlQuery query = db.exec(query_str);

        if(debug) qDebug() << "Query " << debug_str << query.isActive();

        if(debug && !query.isActive()) {
            qDebug() << "SQL Error" << query.lastError().text();
        }

        bool has_first = query.first();

        int cols = query.record().count();

        QStringList labels;

        // setting up labels
        for(int i = 0; i < cols; ++i) {
            labels.append(query.record().fieldName(i));
        }

        values->append(labels);

        // reading the values
        if (has_first) {

            while( query.isActive()) {

                if (query.isValid()) {
                    QStringList row;

                    for(int i = 0; i < cols; ++i) {
                        row.append(query.record().value(i).toString());
                    }

                    values->append(row);
                }

                if(!query.next()) query.finish();
            }

            if(debug) qDebug() << "DBHandler " << debug_str << "Rows: " << values->size() << "Cols: " << cols;

            query.finish();
            if(db.isOpen()) db.close();

            return values;

        } else if(debug) qDebug() << "query" << debug_str << "No rows";

        query.finish();
    } else if(debug) {
        qDebug() << "query" << debug_str << "Database not open";
        return 0;
    } else return 0;

    if(db.isOpen()) db.close();
    return values;
}

QList<QStringList> *DatabaseQueryHelper::querySimpleData(QSqlDatabase &db, const QString &table, int track_id, int season, const QString &debug_str, bool debug)
{
    QMutexLocker locker(&ThreadResourceManager::getInstance().getDBMutex());

    QList<QStringList> *values = new QList<QStringList>();

    if(db.open()) {
        QString query_str = QStringLiteral("SELECT Season, track_id FROM %1 WHERE Season = (:season) AND track_id = (:track_id);").arg(table);

        QSqlQuery query(db);
        query.prepare(query_str);
        query.bindValue(":season", season);
        query.bindValue(":track_id", track_id);

        query.exec();

        if(debug) qDebug() << "Query " << debug_str << query.isActive();

        if(debug && !query.isActive()) {
            qDebug() << "SQL Error" << query.lastError().text();
        }

        bool has_first = query.first();

        int cols = query.record().count();

        // reading the values
        if (has_first) {

            while( query.isActive()) {

                if (query.isValid()) {
                    QStringList row;

                    for(int i = 0; i < cols; ++i) {
                        row.append(query.record().value(i).toString());
                    }

                    values->append(row);
                }

                if(!query.next()) query.finish();
            }

            if(debug) qDebug() << "DBHandler " << debug_str << "Rows: " << values->size() << "Cols: " << cols;

            query.finish();
            if(db.isOpen()) db.close();

            return values;

        } else if(debug) qDebug() << "query" << debug_str << "No rows";

        query.finish();
    } else if(debug) {
        qDebug() << "query" << debug_str << "Database not open";
        return 0;
    } else return 0;

    if(db.isOpen()) db.close();
    return values;
}

QList<QStringList> *DatabaseQueryHelper::queryFullPracticeData(QSqlDatabase &db, bool debug = false)
{
    QMutexLocker locker(&ThreadResourceManager::getInstance().getDBMutex());

    QList<QStringList> *values = new QList<QStringList>();

    if(db.open()) {
        QSqlQuery query = db.exec(DatabaseStrings::getFullPracticeLapQuery());

        if(debug) qDebug() << "Query PracticeLap" << query.isActive();

        if(debug && !query.isActive()) {
            qDebug() << "SQL Error" << query.lastError().text();
        }

        bool has_first = query.first();

        int cols = query.record().count();

        QStringList labels;

        // setting up labels
        for(int i = 0; i < cols; ++i) {
            labels.append(query.record().fieldName(i));
        }

        values->append(labels);

        // reading the values
        if (has_first) {

            while( query.isActive()) {

                if (query.isValid()) {
                    QStringList row;

                    for(int i = 0; i < cols; ++i) {
                        row.append(query.record().value(i).toString());
                    }

                    values->append(row);
                }

                if(!query.next()) query.finish();
            }

            if(debug) qDebug() << "DBHandler FullPracticeData: " <<"Rows: " << values->size() << "Cols: " << cols;

            query.finish();
            if(db.isOpen()) db.close();

            return values;

        } else if(debug) qDebug() << "queryFullPracticeData:" << "No rows";

        query.finish();
    } else if(debug) {
        qDebug() << "queryFullPracticeData:" << "Database not open";
        return 0;
    } else return 0;

    if(db.isOpen()) db.close();
    return values;
}

QList<QStringList> *DatabaseQueryHelper::queryRaceData(QSqlDatabase &db, bool debug)
{
    QMutexLocker locker(&ThreadResourceManager::getInstance().getDBMutex());

    QList<QStringList> *values = new QList<QStringList>();

    if(db.open()) {
        QSqlQuery query = db.exec(DatabaseStrings::getRaceDataQuery());

        if(debug) qDebug() << "Query RaceData" << query.isActive();

        if(debug && !query.isActive()) {
            qDebug() << "SQL Error" << query.lastError().text();
        }

        bool has_first = query.first();

        int cols = query.record().count();

        QStringList labels;

        // setting up labels
        for(int i = 0; i < cols; ++i) {
            labels.append(query.record().fieldName(i));
        }

        values->append(labels);

        // reading the values
        if (has_first) {

            while( query.isActive()) {

                if (query.isValid()) {
                    QStringList row;

                    for(int i = 0; i < cols; ++i) {
                        row.append(query.record().value(i).toString());
                    }

                    values->append(row);
                }

                if(!query.next()) query.finish();
            }

            if(debug) qDebug() << "DBHandler RaceData: " <<"Rows: " << values->size() << "Cols: " << cols;

            query.finish();
            if(db.isOpen()) db.close();

            return values;

        } else if(debug) qDebug() << "queryRaceData:" << "No rows";

        query.finish();
    } else if(debug) {
        qDebug() << "queryRaceData:" << "Database not open";
        return 0;
    } else return 0;

    if(db.isOpen()) db.close();

    return values;
}

QList<QStringList> *DatabaseQueryHelper::queryStintData(QSqlDatabase &db, bool debug)
{
    QMutexLocker locker(&ThreadResourceManager::getInstance().getDBMutex());

    QList<QStringList> *values = new QList<QStringList>();

    if(db.open()) {
        QString query_str = DatabaseStrings::getStintDataQuery();
        QSqlQuery query = db.exec(query_str);

        if(debug) qDebug() << "Query StintData" << query.isActive();

        if(debug && !query.isActive()) {
            qDebug() << "SQL Error" << query.lastError().text();
        }

        bool has_first = query.first();

        int cols = query.record().count();

        QStringList labels;

        // setting up labels
        for(int i = 0; i < cols; ++i) {
            labels.append(query.record().fieldName(i));
        }

        values->append(labels);

        // reading the values
        if (has_first) {

            while( query.isActive()) {

                if (query.isValid()) {
                    QStringList row;

                    for(int i = 0; i < cols; ++i) {
                        row.append(query.record().value(i).toString());
                    }

                    values->append(row);
                }

                if(!query.next()) query.finish();
            }

            if(debug) qDebug() << "DBHandler StintData: " <<"Rows: " << values->size() << "Cols: " << cols;

            query.finish();
            if(db.isOpen()) db.close();

            return values;

        } else if(debug) qDebug() << "queryRaceData:" << "No rows";

        query.finish();
    } else if(debug) {
        qDebug() << "queryStintData:" << "Database not open";
        return 0;
    } else return 0;

    if(db.isOpen()) db.close();

    return values;
}

QList<QStringList> *DatabaseQueryHelper::queryCTTimeData(QSqlDatabase &db, bool debug)
{
    QString query_str = DatabaseStrings::getCTTimeDataQuery();
    return queryData(db,query_str,QStringLiteral("CTTimeData"),debug);
}

QList<QStringList> *DatabaseQueryHelper::queryFuelTimeData(QSqlDatabase &db, bool debug)
{
    QString query_str = DatabaseStrings::getFuelTimeDataQuery();
    return queryData(db, query_str, QStringLiteral("FuelTimeData"), debug);
}

QList<QStringList> *DatabaseQueryHelper::queryFullRaceData(QSqlDatabase &db, bool debug)
{
    QString query_str = DatabaseStrings::getFullRaceDataQuery();
    return queryData(db, query_str, QStringLiteral("FullRaceData"), debug);
}

QList<QStringList> *DatabaseQueryHelper::queryTyreDiffData(QSqlDatabase &db, bool debug)
{
    QString query_str = DatabaseStrings::getTyreDiffDataQuery();
    return queryData(db, query_str, QStringLiteral("TyreDiffData"), debug);
}

QList<QStringList> *DatabaseQueryHelper::queryPracticeData(QSqlDatabase &db, bool debug)
{
    QString query_str = DatabaseStrings::getPracticeDataQuery();
    return queryData(db, query_str, QStringLiteral("PracticeData"), debug);
}

QList<QStringList> *DatabaseQueryHelper::queryPracticeTable(QSqlDatabase &db, bool debug)
{
    QString query_str = DatabaseStrings::getPracticeTableQuery();
    return queryData(db, query_str, QStringLiteral("PracticeTableData"), debug);
}

QList<QStringList> *DatabaseQueryHelper::queryTracksTable(QSqlDatabase &db, bool debug)
{
    QString query_str = DatabaseStrings::getTrackTableQuery();
    return queryData(db, query_str, QStringLiteral("TracksTableData"), debug);
}

void DatabaseQueryHelper::addRegexpFunction(QSqlDatabase &db, bool debug)
{
    QVariant v = db.driver()->handle();

    sqlite3* handler = *static_cast<sqlite3**>(v.data());

    if (!handler) {
        if(debug) qDebug() << "Cannot get a sqlite3 handler.";
        return;
    }

    // Check validity of the state.
    if (!db.isValid()) {
        if(debug) qDebug() << "Cannot create SQLite custom functions: db object is not valid.";
        return;
    }

    if (!db.isOpen()) {
        if(debug) qDebug() << "Cannot create SQLite custom functions: db object is not open.";
        return;
    }

    sqlite3_initialize();

    if (sqlite3_create_function(handler, "regexp", 2, SQLITE_ANY, NULL, &DatabaseQueryHelper::sqlite_regexp, NULL, NULL))
        if(debug) qDebug() << "Cannot create SQLite functions: sqlite3_create_function failed.";

}
