#include "databasetablecreatehelper.h"

#include <QDebug>

#include "settingconstants.h"
#include "Utility/transformations.h"
#include "Misc/threadresourcemanager.h"

const QString DatabaseTableCreateHelper::TABLE_TRACK = QStringLiteral("Track");
const QString DatabaseTableCreateHelper::TABLE_RACE = QStringLiteral("Race");
const QString DatabaseTableCreateHelper::TABLE_CAR = QStringLiteral("Car");
const QString DatabaseTableCreateHelper::TABLE_CAR_WEAR = QStringLiteral("CarWear");
const QString DatabaseTableCreateHelper::TABLE_WEATHER = QStringLiteral("Weather");
const QString DatabaseTableCreateHelper::TABLE_PRACTICE_LAP = QStringLiteral("PracticeLap");
const QString DatabaseTableCreateHelper::TABLE_DRIVER = QStringLiteral("Driver");
const QString DatabaseTableCreateHelper::TABLE_RISK = QStringLiteral("Risk");
const QString DatabaseTableCreateHelper::TABLE_LAP = QStringLiteral("Lap");
const QString DatabaseTableCreateHelper::TABLE_MISC_DATA = QStringLiteral("MiscRaceData");
const QString DatabaseTableCreateHelper::TABLE_STINT_DATA = QStringLiteral("Stint");

DatabaseTableCreateHelper::DatabaseTableCreateHelper()
{

}

bool DatabaseTableCreateHelper::createDatabase()
{
    QSettings settings(General::ProgramName, General::CompanyName);

    QSqlDatabase db = QSqlDatabase::addDatabase(QStringLiteral("QSQLITE"), ConstantStrings::DefaultDatabaseName);
    settings.setValue(Settings::DatabaseTypeText, QStringLiteral("QSQLITE"));

    db.setDatabaseName(settings.value(Settings::DatabaseNameText, ConstantStrings::DefaultDatabaseName).toString());
    db.setUserName(settings.value(Settings::DatabaseUserNameText, QStringLiteral("")).toString());
    db.setPassword(settings.value(Settings::DatabasePasswordText, QStringLiteral("")).toString());

    qDebug() << "Create Track Table" << createTrackTable(db);

    qDebug() << "Create Race Table" << createRaceTable(db);

    qDebug() << "Create Car Table" << createCarTable(db);

    qDebug() << "Create CarWear Table" << createCarWearTable(db);

    qDebug() << "Create Weather Table" << createWeatherTable(db);

    qDebug() << "Create PracticeLap Table" << createPracticeTable(db);

    qDebug() << "Create Driver Table" << createDriverTable(db);

    qDebug() << "Create Risk Table" << createRiskTable(db);

    qDebug() << "Create Lap Table" << createLapTable(db);

    qDebug() << "Create Misc Data Table" << createMiscDataTable(db);

    qDebug() << "Create Stint Table" << createStintTable(db);

    qDebug() << "Database" << Settings::DatabaseNameText << "created.";

    if(!db.open()) qDebug() << "Database NOT open";

    return true;


}

bool DatabaseTableCreateHelper::createTrackTable(QSqlDatabase &db, bool debug)
{
    QString create = QStringLiteral("CREATE TABLE Track( ");
    QString columns = QStringLiteral("Laps INTEGER NOT NULL,"
                                     "Name TEXT NOT NULL,"
                                     "Distance REAL NOT NULL,"
                                     "Power INTEGER NOT NULL,"
                                     "Handling INTEGER NOT NULL,"
                                     "Acceleration INTEGER NOT NULL,"
                                     "Downforce TEXT NOT NULL,"
                                     "Overtaking TEXT NOT NULL,"
                                     "Suspension TEXT NOT NULL,"
                                     "FuelConsumption TEXT NOT NULL,"
                                     "TyreWear TEXT NOT NULL,"
                                     "AvgSpeed REAL,"
                                     "LapLength REAL NOT NULL,"
                                     "_id INTEGER PRIMARY KEY AUTOINCREMENT,"
                                     "Corners INTEGER NOT NULL,"
                                     "Grip TEXT NOT NULL,"
                                     "PitStop REAL NOT NULL );");

    if(db.open()) {
        //QSqlQuery drop_query = db.exec(QStringLiteral("DROP TABLE Track;"));
        //qDebug() << "Drop table Track" << drop_query.isActive();

        QSqlQuery query = db.exec(create + columns);
        if(debug) qDebug() << "Create Track" <<  query.isActive();

        if (query.isActive()) {
            if(db.isOpen()) db.close();
            return true;
        }
    } else return false;

    if(db.isOpen()) db.close();
    return false;

}

bool DatabaseTableCreateHelper::createRaceTable(QSqlDatabase &db, bool debug)
{
    QString create = QStringLiteral("CREATE TABLE Race( ");
    QString columns = QStringLiteral("_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                                     "Season INTEGER NOT NULL, "
                                     "track_id INTEGER NOT NULL, "
                                     "FOREIGN KEY(track_id) REFERENCES Track(_id), "
                                     "UNIQUE (Season, track_id) );");

    if(db.open()) {
        //QSqlQuery drop_query = db.exec(QStringLiteral("DROP TABLE Race;"));
        //qDebug() << "Drop table Race" << drop_query.isActive();

        QSqlQuery query = db.exec(create + columns);
        if(debug) qDebug() << "Create Race Table" <<  query.isActive();

        if (query.isActive()) {
            if(db.isOpen()) db.close();
            return true;
        }
    } else return false;

    if(db.isOpen()) db.close();
    return false;
}

bool DatabaseTableCreateHelper::createCarTable(QSqlDatabase &db, bool debug)
{

    QString create = QStringLiteral("CREATE TABLE Car( ");
    QString columns = QStringLiteral("Season INTEGER NOT NULL,"
                                     "Power INTEGER NOT NULL,"
                                     "Handling INTEGER NOT NULL,"
                                     "Acceleration INTEGER NOT NULL,"
                                     "Chassis INTEGER NOT NULL,"
                                     "Engine INTEGER NOT NULL,"
                                     "FrontWing INTEGER NOT NULL,"
                                     "RearWing INTEGER NOT NULL,"
                                     "Underbody INTEGER NOT NULL,"
                                     "Sidepods INTEGER NOT NULL,"
                                     "Cooling INTEGER NOT NULL,"
                                     "Gearbox INTEGER NOT NULL,"
                                     "Brakes INTEGER NOT NULL,"
                                     "Suspension INTEGER NOT NULL,"
                                     "Electronics INTEGER NOT NULL,"
                                     "_id INTEGER PRIMARY KEY AUTOINCREMENT,"
                                     "track_id INTEGER NOT NULL,"
                                     "FOREIGN KEY (Season, track_id) REFERENCES Race(Season, track_id),"
                                     "UNIQUE (Season, track_id) );" );

    if(db.open()) {
        //QSqlQuery drop_query = db.exec(QStringLiteral("DROP TABLE Car;"));
        //if(debug) qDebug() << "Drop table Car" << drop_query.isActive();

        QSqlQuery query = db.exec(create + columns);
        if(debug) qDebug() << "Create Car" << query.isActive();

        if (query.isActive()) {
            if(db.isOpen()) db.close();
            return true;
        }
    } else return false;

    if(db.isOpen()) db.close();
    return false;

}

bool DatabaseTableCreateHelper::createCarWearTable(QSqlDatabase &db, bool debug)
{

    QString create = QStringLiteral("CREATE TABLE CarWear( ");
    QString columns = QStringLiteral("Season INTEGER NOT NULL, "
                                     "ChassisWear INTEGER, "
                                     "EngineWear INTEGER, "
                                     "FrontWingWear INTEGER, "
                                     "RearWingWear INTEGER, "
                                     "UnderbodyWear INTEGER, "
                                     "SidepodsWear INTEGER, "
                                     "CoolingWear INTEGER, "
                                     "GearboxWear INTEGER, "
                                     "BrakesWear INTEGER, "
                                     "SuspensionWear INTEGER, "
                                     "ElectronicsWear INTEGER, "
                                     "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                                     "track_id INTEGER NOT NULL, "
                                     "FOREIGN KEY (Season, track_id) REFERENCES Race(Season, track_id), "
                                     "UNIQUE (Season, track_id) );");

    if(db.open()) {
        //QSqlQuery drop_query = db.exec(QStringLiteral("DROP TABLE CarWear;"));
        //if(debug) qDebug() << "Drop Table CarWear" << drop_query.isActive();

        QSqlQuery query = db.exec(create + columns);
        if(debug) qDebug() << "Create CarWear" << query.isActive();

        if(query.isActive()) {
            if(db.isOpen()) db.close();
            return true;
        }
    } else return false;

    if(db.isOpen()) db.close();
    return false;
}

bool DatabaseTableCreateHelper::createWeatherTable(QSqlDatabase &db, bool debug)
{

    QString create = QStringLiteral("CREATE TABLE Weather( ");
    QString columns = QStringLiteral("Q1Status TEXT NOT NULL, "
                                     "Q1Temperature INTEGER NOT NULL, "
                                     "Q1Humidity INTEGER NOT NULL, "
                                     "Q2Status TEXT NOT NULL, "
                                     "Q2Temperature INTEGER NOT NULL, "
                                     "Q2Humidity INTEGER NOT NULL,"
                                     "RaceQ1TemperatureLow INTEGER NOT NULL,"
                                     "RaceQ1TemperatureHigh INTEGER NOT NULL,"
                                     "RaceQ1HumidityLow INTEGER NOT NULL, "
                                     "RaceQ1HumidityHigh INTEGER NOT NULL,"
                                     "RaceQ1RainPLow INTEGER NOT NULL, "
                                     "RaceQ1RainPHigh INTEGER NOT NULL,"
                                     "RaceQ2TemperatureLow INTEGER NOT NULL,"
                                     "RaceQ2TemperatureHigh INTEGER NOT NULL,"
                                     "RaceQ2HumidityLow INTEGER NOT NULL, "
                                     "RaceQ2HumidityHigh INTEGER NOT NULL,"
                                     "RaceQ2RainPLow INTEGER NOT NULL, "
                                     "RaceQ2RainPHigh INTEGER NOT NULL,"
                                     "RaceQ3TemperatureLow INTEGER NOT NULL,"
                                     "RaceQ3TemperatureHigh INTEGER NOT NULL,"
                                     "RaceQ3HumidityLow INTEGER NOT NULL, "
                                     "RaceQ3HumidityHigh INTEGER NOT NULL,"
                                     "RaceQ3RainPLow INTEGER NOT NULL, "
                                     "RaceQ3RainPHigh INTEGER NOT NULL,"
                                     "RaceQ4TemperatureLow INTEGER NOT NULL,"
                                     "RaceQ4TemperatureHigh INTEGER NOT NULL,"
                                     "RaceQ4HumidityLow INTEGER NOT NULL, "
                                     "RaceQ4HumidityHigh INTEGER NOT NULL,"
                                     "RaceQ4RainPLow INTEGER NOT NULL, "
                                     "RaceQ4RainPHigh INTEGER NOT NULL,"
                                     "track_id INTEGER NOT NULL, "
                                     "Season INTEGER NOT NULL, "
                                     "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                                     "FOREIGN KEY (Season, track_id) REFERENCES Race(Season, track_id), "
                                     "UNIQUE (Season, track_id) );");

    if(db.open()) {
        //QSqlQuery drop_query = db.exec(QStringLiteral("DROP TABLE Weather;"));
        //if(debug) qDebug() << "Drop Table Weather" << drop_query.isActive();

        QSqlQuery query = db.exec(create + columns);
        if(debug) qDebug() << "Create Weather" << query.isActive();

        if(query.isActive()) {
            if(db.isOpen()) db.close();
            return true;
        }
    } else return false;

    if(db.isOpen()) db.close();
    return false;

}

bool DatabaseTableCreateHelper::createPracticeTable(QSqlDatabase &db, bool debug)
{

    QString create = QStringLiteral("CREATE TABLE PracticeLap ( ");
    QString columns = QStringLiteral("LapNumber INTEGER NOT NULL, "
                                     "LapTime TEXT NOT NULL, "
                                     "DriverMistake REAL NOT NULL, "
                                     "NetTime TEXT NOT NULL, "
                                     "FrontWing INTEGER NOT NULL, "
                                     "RearWing INTEGER NOT NULL, "
                                     "Engine INTEGER NOT NULL, "
                                     "Brakes INTEGER NOT NULL, "
                                     "Gear INTEGER NOT NULL, "
                                     "Suspension INTEGER NOT NULL, "
                                     "Tyres TEXT NOT NULL, "
                                     "WingComment TEXT,"
                                     "EngineComment TEXT, "
                                     "BrakesComment TEXT, "
                                     "GearComment TEXT, "
                                     "SuspensionComment TEXT, "
                                     "track_id INTEGER NOT NULL, "
                                     "Season INTEGER NOT NULL, "
                                     "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                                     "FOREIGN KEY (Season, track_id) REFERENCES Race(Season, track_id),"
                                     "UNIQUE (LapNumber, Season, track_id) );");


    if(db.open()) {
        //QSqlQuery drop_query = db.exec(QStringLiteral("DROP TABLE PracticeLap;"));
        //if(debug) qDebug() << "Drop Table PracticeLap" << drop_query.isActive();

        QSqlQuery query = db.exec(create + columns);
        if(debug) qDebug() << "Create PracticeLap" << query.isActive();

        if(query.isActive()) {
            if(db.isOpen()) db.close();
            return true;
        }
    } else return false;

    if(db.isOpen()) db.close();
    return false;
}

bool DatabaseTableCreateHelper::createDriverTable(QSqlDatabase &db, bool debug)
{

    QString create = QStringLiteral("CREATE TABLE Driver ( ");
    QString columns = QStringLiteral("Season INTEGER NOT NULL, "
                                     "Overall INTEGER NOT NULL, "
                                     "Concentration INTEGER NOT NULL, "
                                     "Talent INTEGER NOT NULL, "
                                     "Aggresiveness INTEGER NOT NULL, "
                                     "Experience INTEGER NOT NULL, "
                                     "TechInsight INTEGER NOT NULL, "
                                     "Stamina INTEGER NOT NULL, "
                                     "Charisma INTEGER NOT NULL, "
                                     "Motivation INTEGER NOT NULL, "
                                     "Weight INTEGER NOT NULL, "
                                     "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                                     "track_id INTEGER NOT NULL, "
                                     "Age INTEGER NOT NULL, "
                                     "Reputation INTEGER NOT NULL, "
                                     "FOREIGN KEY (Season, track_id) REFERENCES Race(Season, track_id),"
                                     "UNIQUE (Season, track_id) );");

    if(db.open()) {
        //QSqlQuery drop_query = db.exec(QStringLiteral("DROP TABLE Driver;"));
        //if(debug) qDebug() << "Drop Table Driver" << drop_query.isActive();

        QSqlQuery query = db.exec(create + columns);
        if(debug) qDebug() << "Create Driver" << query.isActive();

        if(query.isActive()) {
            if(db.isOpen()) db.close();
            return true;
        }
    } else return false;

    if(db.isOpen()) db.close();
    return false;
}

bool DatabaseTableCreateHelper::createRiskTable(QSqlDatabase &db, bool debug)
{

    QString create = QStringLiteral("CREATE TABLE Risk ( ");
    QString columns = QStringLiteral("Season INTEGER NOT NULL, "
                                     "Clear INTEGER NOT NULL, "
                                     "Wet INTEGER NOT NULL, "
                                     "Defend INTEGER NOT NULL, "
                                     "Malfunction INTEGER NOT NULL,"
                                     "Overtake INTEGER NOT NULL, "
                                     "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                                     "track_id INTEGER NOT NULL, "
                                     "StartRisk TEXT, "
                                     "FOREIGN KEY (Season, track_id) REFERENCES Race(Season, track_id),"
                                     "UNIQUE (Season, track_id) );");

    if(db.open()) {
        //QSqlQuery drop_query = db.exec(QStringLiteral("DROP TABLE Risk;"));
        //if(debug) qDebug() << "Drop Table Risk" << drop_query.isActive();

        QSqlQuery query = db.exec(create + columns);
        if(debug) qDebug() << "Create Risk" << query.isActive();

        if(query.isActive()) {
            if(db.isOpen()) db.close();
            return true;
        }
    } else return false;

    if(db.isOpen()) db.close();
    return false;
}

bool DatabaseTableCreateHelper::createLapTable(QSqlDatabase &db, bool debug)
{

    QString create = QStringLiteral("CREATE TABLE Lap ( ");
    QString columns = QStringLiteral("Season INTEGER NOT NULL, "
                                     "Lap INTEGER, "
                                     "LapTime TEXT, "
                                     "Pos INTEGER, "
                                     "Tyres TEXT NOT NULL, "
                                     "Weather TEXT NOT NULL, "
                                     "Temp INTEGER NOT NULL, "
                                     "Hum INTEGER NOT NULL, "
                                     "Events TEXT, "
                                     "track_id INTEGER NOT NULL, "
                                     "_id INTEGER PRIMARY KEY AUTOINCREMENT,"
                                     "FOREIGN KEY (Season, track_id) REFERENCES Race(Season, track_id), "
                                     "UNIQUE (Lap, Season, track_id) );");

    if(db.open()) {
        //QSqlQuery drop_query = db.exec(QStringLiteral("DROP TABLE Lap;"));
        //if(debug) qDebug() << "Drop Table Lap" << drop_query.isActive();

        QSqlQuery query = db.exec(create + columns);
        if(debug) qDebug() << "Create Lap" << query.isActive();

        if(query.isActive()) {
            if(db.isOpen()) db.close();
            return true;
        }
    } else return false;

    if(db.isOpen()) db.close();
    return false;
}

bool DatabaseTableCreateHelper::createMiscDataTable(QSqlDatabase &db, bool debug)
{

    QString create = QStringLiteral("CREATE TABLE MiscRaceData ( ");
    QString columns = QStringLiteral("Season INTEGER NOT NULL, "
                                     "track_id INTEGER NOT NULL, "
                                     "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                                     "Q1Time REAL,"
                                     "Q2Time REAL, "
                                     "FOREIGN KEY (Season, track_id) REFERENCES Race(Season, track_id), "
                                     "UNIQUE (Season, track_id) );");

    if(db.open()) {
        //QSqlQuery drop_query = db.exec(QStringLiteral("DROP TABLE MiscRaceData;"));
        //if(debug) qDebug() << "Drop Table MiscRaceData" << drop_query.isActive();

        QSqlQuery query = db.exec(create + columns);
        if(debug) qDebug() << "Create MiscRaceData" << query.isActive();

        if(query.isActive()) {
            if(db.isOpen()) db.close();
            return true;
        }
    } else return false;

    if(db.isOpen()) db.close();
    return false;
}

bool DatabaseTableCreateHelper::createStintTable(QSqlDatabase &db, bool debug)
{

    QString create = QStringLiteral("CREATE TABLE Stint ( ");
    QString columns = QStringLiteral("Season INTEGER NOT NULL, "
                                     "Laps INTEGER NOT NULL, "
                                     "Fuel REAL NOT NULL, "
                                     "Consumption REAL NOT NULL, "
                                     "FinalP INTEGER, "
                                     "AvgHumidity REAL NOT NULL,"
                                     "AvgTemperature REAL NOT NULL, "
                                     "TyreType TEXT,"
                                     "Weather TEXT NOT NULL,"
                                     "Km REAL NOT NULL, "
                                     "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                                     "StartLap INTEGER, "
                                     "PitStop REAL, "
                                     "StartFuel INTEGER, "
                                     "track_id INTEGER NOT NULL, "
                                     "FOREIGN KEY (Season, track_id) REFERENCES Race(Season, track_id), "
                                     "UNIQUE (Season, track_id, StartLap, Laps) );");

    if(db.open()) {
        //QSqlQuery drop_query = db.exec(QStringLiteral("DROP TABLE Stint;"));
        //if(debug) qDebug() << "Drop Table Stint" << drop_query.isActive();

        QSqlQuery query = db.exec(create + columns);
        if(debug) qDebug() << "Create Stint" << query.isActive();

        if(query.isActive()) {
            if(db.isOpen()) db.close();
            return true;
        }
    } else return false;

    if(db.isOpen()) db.close();
    return false;
}

bool DatabaseTableCreateHelper::insertRaceRow(QSqlDatabase &db, int season, int track_id, bool debug)
{
    QMutexLocker locker(&ThreadResourceManager::getInstance().getDBMutex());

    if(debug) qDebug() << "Inserting Race" << season << track_id;

    QString insert = QStringLiteral("INSERT INTO Race (");
    QString columns = QStringLiteral("Season, track_id ) ");
    QString values = QStringLiteral("VALUES (?, ?);");

    if(db.open()) {
        QSqlQuery query = db.exec();
        query.prepare(insert + columns + values);
        query.addBindValue(season);
        query.addBindValue(track_id);

        query.exec();

        if(debug) qDebug() << "Race Insert" << query.isActive();

        if(query.isActive()) {
            if(db.isOpen()) db.close();
            return true;
        } else qDebug() << query.lastError().text();
    } else return false;

    if(db.isOpen()) db.close();

    return false;
}

bool DatabaseTableCreateHelper::insertTrackRow (QSqlDatabase &db, Track track, bool debug)
{
    QMutexLocker locker(&ThreadResourceManager::getInstance().getDBMutex());

    if(debug) qDebug() << "Inserting Track" << track.getName() << track.getID();

    QString insert = QStringLiteral("INSERT INTO Track (");
    QString columns = QStringLiteral("Laps, Name, Distance, Power, Handling, Acceleration, Downforce, "
                                     "Overtaking, Suspension, FuelConsumption, TyreWear, AvgSpeed,"
                                     "LapLength, _id, Corners, Grip, PitStop ) ");
    QString values = QStringLiteral("VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");

    if(db.open()) {
        QSqlQuery query = db.exec();
        query.prepare(insert + columns + values);
        query.addBindValue(track.getLaps());
        query.addBindValue(track.getName());
        query.addBindValue(track.getDistance());
        query.addBindValue(track.getPower());
        query.addBindValue(track.getHandling());
        query.addBindValue(track.getAcceleration());
        query.addBindValue(Track::Downforce::toString(track.getDownforce()));
        query.addBindValue(Track::Overtaking::toString(track.getOvertaking()));
        query.addBindValue(Track::Suspension::toString(track.getSuspension()));
        query.addBindValue(Track::FuelConsumption::toString(track.getFuelConsumption()));
        query.addBindValue(Track::TyreWear::toString(track.getTyreWear()));
        query.addBindValue(track.getAvgSpeed());
        query.addBindValue(track.getLapLength());
        query.addBindValue(track.getID());
        query.addBindValue(track.getCorners());
        query.addBindValue(Track::Grip::toString(track.getGrip()));
        query.addBindValue(track.getPitStop());

        query.exec();

        if(debug) qDebug() << "Track Insert" << query.isActive();

        if(query.isActive()) {
            if(db.isOpen()) db.close();
            return true;
        } else qDebug() << query.lastError().text();
    } else return false;

    if(db.isOpen()) db.close();

    return false;
}

bool DatabaseTableCreateHelper::insertCarRow(QSqlDatabase &db, Car car, bool debug)
{
    QMutexLocker locker(&ThreadResourceManager::getInstance().getDBMutex());

    if(debug) qDebug() << "Inserting Car" << car.getSeason() << car.getTrackID();

    QString insert = QStringLiteral("INSERT INTO Car (");
    QString columns = QStringLiteral("Season, Power, Handling, Acceleration, Chassis, Engine, "
                                     "FrontWing, RearWing, Underbody, Sidepods, Cooling, Gearbox, "
                                     "Brakes, Suspension, Electronics, track_id ) ");
    QString values = QStringLiteral("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");

    if(db.open()) {
        QSqlQuery query = db.exec();
        query.prepare(insert + columns + values);
        query.addBindValue(car.getSeason());
        query.addBindValue(car.getPower());
        query.addBindValue(car.getHandling());
        query.addBindValue(car.getAcceleration());
        query.addBindValue(car.getChassisLvl());
        query.addBindValue(car.getEngineLvl());
        query.addBindValue(car.getFrontWingLvl());
        query.addBindValue(car.getRearWingLvl());
        query.addBindValue(car.getUnderbodyLvl());
        query.addBindValue(car.getSidepodsLvl());
        query.addBindValue(car.getCoolingLvl());
        query.addBindValue(car.getGearboxLvl());
        query.addBindValue(car.getBrakesLvl());
        query.addBindValue(car.getSuspensionLvl());
        query.addBindValue(car.getElectronicsLvl());
        query.addBindValue(car.getTrackID());

        query.exec();

        if(debug) qDebug() << "Car Insert" << query.isActive();

        if(query.isActive()) {
            if(db.isOpen()) db.close();
            return true;
        } else qDebug() << query.lastError().text();
    } else return false;

    if(db.isOpen()) db.close();

    return false;

}

bool DatabaseTableCreateHelper::insertCarWearRow(QSqlDatabase &db, Car car, bool debug)
{
    QMutexLocker locker(&ThreadResourceManager::getInstance().getDBMutex());

    if(debug) qDebug() << "Inserting CarWear" << car.getSeason() << car.getTrackID();

    QString insert = QStringLiteral("INSERT INTO CarWear ( ");
    QString columns = QStringLiteral("Season, ChassisWear, EngineWear, FrontWingWear, RearWingWear, UnderbodyWear,"
                                     "SidepodsWear, CoolingWear, GearboxWear, BrakesWear, SuspensionWear, "
                                     "ElectronicsWear, track_id ) ");
    QString values = QStringLiteral("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);");

    if(db.open()) {
        QSqlQuery query = db.exec();
        query.prepare(insert + columns + values);
        query.addBindValue(car.getSeason());
        query.addBindValue(car.getChassisWear());
        query.addBindValue(car.getEngineWear());
        query.addBindValue(car.getFrontWingWear());
        query.addBindValue(car.getRearWingWear());
        query.addBindValue(car.getUnderbodyWear());
        query.addBindValue(car.getSidepodsWear());
        query.addBindValue(car.getCoolingWear());
        query.addBindValue(car.getGearboxWear());
        query.addBindValue(car.getBrakesWear());
        query.addBindValue(car.getSuspensionWear());
        query.addBindValue(car.getElectronicsWear());
        query.addBindValue(car.getTrackID());

        query.exec();

        if(debug) qDebug() << "CarWear Insert" << query.isActive();

        if(query.isActive()) {
            if(db.isOpen()) db.close();
            return true;
        } else qDebug() << query.lastError().text();
    } else return false;

    if(db.isOpen()) db.close();

    return false;
}

bool DatabaseTableCreateHelper::insertWeatherRow(QSqlDatabase &db, Weather weather, bool debug)
{
    QMutexLocker locker(&ThreadResourceManager::getInstance().getDBMutex());

    if(debug) qDebug() << "Inserting Weather" << weather.getSeason() << weather.getTrackID();

    QString insert = QStringLiteral("INSERT INTO Weather ( ");
    QString columns = QStringLiteral("Q1Status, Q1Temperature, Q1Humidity, Q2Status, Q2Temperature, "
                                     "Q2Humidity, RaceQ1TemperatureLow, RaceQ1TemperatureHigh, "
                                     "RaceQ1HumidityLow, RaceQ1HumidityHigh, RaceQ1RainPLow, "
                                     "RaceQ1RainPHigh, RaceQ2TemperatureLow, RaceQ2TemperatureHigh, "
                                     "RaceQ2HumidityLow, RaceQ2HumidityHigh, RaceQ2RainPLow, "
                                     "RaceQ2RainPHigh, RaceQ3TemperatureLow, RaceQ3TemperatureHigh, "
                                     "RaceQ3HumidityLow, RaceQ3HumidityHigh, RaceQ3RainPLow, "
                                     "RaceQ3RainPHigh, RaceQ4TemperatureLow, RaceQ4TemperatureHigh, "
                                     "RaceQ4HumidityLow, RaceQ4HumidityHigh, RaceQ4RainPLow, RaceQ4RainPHigh, track_id, Season ) ");
    QString values = QStringLiteral("VALUES (?, ?, ?, ?, ?, ?, "
                                    "?, ?, ?, ?, ?, ?, "
                                    "?, ?, ?, ?, ?, ?, "
                                    "?, ?, ?, ?, ?, ?, "
                                    "?, ?, ?, ?, ?, ?, ?, ?);");

    if(db.open()) {
        QSqlQuery query = db.exec();
        query.prepare(insert + columns + values);
        query.addBindValue(Weather::toString(weather.getQ1Status()));
        query.addBindValue(weather.getQ1Temperature());
        query.addBindValue(weather.getQ1Humidity());
        query.addBindValue(Weather::toString(weather.getQ2Status()));
        query.addBindValue(weather.getQ2Temperature());
        query.addBindValue(weather.getQ2Humidity());

        query.addBindValue(weather.getRaceQ1TemperatureLow());
        query.addBindValue(weather.getRaceQ1TemperatureHigh());
        query.addBindValue(weather.getRaceQ1HumidityLow());
        query.addBindValue(weather.getRaceQ1HumidityHigh());
        query.addBindValue(weather.getRaceQ1RainProbabilityLow());
        query.addBindValue(weather.getRaceQ1RainProbabilityHigh());

        query.addBindValue(weather.getRaceQ2TemperatureLow());
        query.addBindValue(weather.getRaceQ2TemperatureHigh());
        query.addBindValue(weather.getRaceQ2HumidityLow());
        query.addBindValue(weather.getRaceQ2HumidityHigh());
        query.addBindValue(weather.getRaceQ2RainProbabilityLow());
        query.addBindValue(weather.getRaceQ2RainProbabilityHigh());

        query.addBindValue(weather.getRaceQ3TemperatureLow());
        query.addBindValue(weather.getRaceQ3TemperatureHigh());
        query.addBindValue(weather.getRaceQ3HumidityLow());
        query.addBindValue(weather.getRaceQ3HumidityHigh());
        query.addBindValue(weather.getRaceQ3RainProbabilityLow());
        query.addBindValue(weather.getRaceQ3RainProbabilityHigh());

        query.addBindValue(weather.getRaceQ4TemperatureLow());
        query.addBindValue(weather.getRaceQ4TemperatureHigh());
        query.addBindValue(weather.getRaceQ4HumidityLow());
        query.addBindValue(weather.getRaceQ4HumidityHigh());
        query.addBindValue(weather.getRaceQ4RainProbabilityLow());
        query.addBindValue(weather.getRaceQ4RainProbabilityHigh());

        query.addBindValue(weather.getTrackID());
        query.addBindValue(weather.getSeason());

        query.exec();

        if(debug) qDebug() << "Weather Insert" << query.isActive();

        if(query.isActive()) {
            if(db.isOpen()) db.close();
            return true;
        } else qDebug() << query.lastError().text();
    } else return false;

    if(db.isOpen()) db.close();

    return false;
}

bool DatabaseTableCreateHelper::insertPracticeRow(QSqlDatabase &db, PracticeLap practice_lap, int lap_nro, bool debug)
{
    QMutexLocker locker(&ThreadResourceManager::getInstance().getDBMutex());

    if(debug) qDebug() << "Inserting Practice Lap" << lap_nro << practice_lap.getSeason() << practice_lap.getTrackID();

    QString insert = QStringLiteral("INSERT INTO PracticeLap ( ");
    QString columns = QStringLiteral("LapNumber, LapTime, DriverMistake, NetTime, FrontWing, RearWing, Engine,"
                                     "Brakes, Gear, Suspension, Tyres, WingComment, EngineComment, BrakesComment, "
                                     "GearComment, SuspensionComment, track_id, Season ) ");
    QString values = QStringLiteral("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");

    QString lap_time = QString::number((int)(practice_lap.getLapTime()/60)) + QString(":") +
            (((int)practice_lap.getLapTime() % 60) < 10 ? "0" : "") + QString::number(((int)practice_lap.getLapTime()) % 60) + QString(".") +
            (std::round(((int)(practice_lap.getLapTime()*1000)) % 1000) < 100 ? "0" : "") + (std::round(((int)(practice_lap.getLapTime()*1000)) % 1000) < 10 ? "0" : "") +
            QString::number(std::round(((int)(practice_lap.getLapTime()*1000)) % 1000));
    QString net_time = QString::number((int)(practice_lap.getNetTime()/60)) + QString(":") +
            (((int)practice_lap.getNetTime() % 60) < 10 ? "0" : "") + QString::number(((int)practice_lap.getNetTime()) % 60) + QString(".") +
            (std::round(((int)(practice_lap.getNetTime()*1000)) % 1000) < 100 ? "0" : "") + (std::round(((int)(practice_lap.getNetTime()*1000)) % 1000) < 10 ? "0" : "") +
            QString::number(std::round(((int)(practice_lap.getNetTime()*1000)) % 1000));

    if(debug) qDebug() << "Lap Time" << lap_time << practice_lap.getLapTime();
    if(debug) qDebug() << "Net Time" << net_time << practice_lap.getNetTime();

    if(db.open()) {
        QSqlQuery query = db.exec();
        query.prepare(insert + columns + values);
        query.addBindValue(lap_nro);
        query.addBindValue(lap_time);
        query.addBindValue(practice_lap.getDriverMistake());
        query.addBindValue(net_time);
        query.addBindValue(practice_lap.getFrontWing());
        query.addBindValue(practice_lap.getRearWing());
        query.addBindValue(practice_lap.getEngine());
        query.addBindValue(practice_lap.getBrakes());
        query.addBindValue(practice_lap.getGear());
        query.addBindValue(practice_lap.getSuspension());
        query.addBindValue(practice_lap.getTyre().toString());
        query.addBindValue(practice_lap.getWingComment());
        query.addBindValue(practice_lap.getEngineComment());
        query.addBindValue(practice_lap.getBrakesComment());
        query.addBindValue(practice_lap.getGearComment());
        query.addBindValue(practice_lap.getSuspensionComment());
        query.addBindValue(practice_lap.getTrackID());
        query.addBindValue(practice_lap.getSeason());

        query.exec();

        if(debug) qDebug() << "PracticeLap Insert" << query.isActive();

        if(query.isActive()) {
            if(db.isOpen()) db.close();

            return true;
        } else qDebug() << query.lastError().text();
    } else return false;

    if(db.isOpen()) db.close();

    return false;
}

bool DatabaseTableCreateHelper::insertPracticeRows(QSqlDatabase &db, QList<PracticeLap> practice_laps, bool debug)
{
    QMutexLocker locker(&ThreadResourceManager::getInstance().getDBMutex());

    QString insert = QStringLiteral("INSERT INTO PracticeLap ( ");
    QString columns = QStringLiteral("LapNumber, LapTime, DriverMistake, NetTime, FrontWing, RearWing, Engine,"
                                     "Brakes, Gear, Suspension, Tyres, WingComment, EngineComment, BrakesComment, "
                                     "GearComment, SuspensionComment, track_id, Season ) ");
    QString values = QStringLiteral("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");

    if(db.open()) {
        int counter = 1;

        QVariantList lap_nros;
        QVariantList lap_times;
        QVariantList driver_mistakes;
        QVariantList net_times;
        QVariantList front_wings;
        QVariantList rear_wings;
        QVariantList engines;
        QVariantList brakes;
        QVariantList gears;
        QVariantList suspensions;
        QVariantList tyres;
        QVariantList wing_comments;
        QVariantList engine_comments;
        QVariantList brakes_comments;
        QVariantList gear_comments;
        QVariantList suspension_comments;
        QVariantList track_ids;
        QVariantList seasons;

        foreach(PracticeLap practice_lap, practice_laps) {
            QString lap_time = QString::number((int)(practice_lap.getLapTime()/60)) + QString(":") +
                    (((int)practice_lap.getLapTime() % 60) < 10 ? "0" : "") + QString::number(((int)practice_lap.getLapTime()) % 60) + QString(".") +
                    (std::round(((int)(practice_lap.getLapTime()*1000)) % 1000) < 100 ? "0" : "") + (std::round(((int)(practice_lap.getLapTime()*1000)) % 1000) < 10 ? "0" : "") +
                    QString::number(std::round(((int)(practice_lap.getLapTime()*1000)) % 1000));
            QString net_time = QString::number((int)(practice_lap.getNetTime()/60)) + QString(":") +
                    (((int)practice_lap.getNetTime() % 60) < 10 ? "0" : "") + QString::number(((int)practice_lap.getNetTime()) % 60) + QString(".") +
                    (std::round(((int)(practice_lap.getNetTime()*1000)) % 1000) < 100 ? "0" : "") + (std::round(((int)(practice_lap.getNetTime()*1000)) % 1000) < 10 ? "0" : "") +
                    QString::number(std::round(((int)(practice_lap.getNetTime()*1000)) % 1000));

            lap_nros.append(counter);
            lap_times.append(lap_time);
            driver_mistakes.append(practice_lap.getDriverMistake());
            net_times.append(net_time);
            front_wings.append(practice_lap.getFrontWing());
            rear_wings.append(practice_lap.getRearWing());
            engines.append(practice_lap.getEngine());
            brakes.append(practice_lap.getBrakes());
            gears.append(practice_lap.getGear());
            suspensions.append(practice_lap.getSuspension());
            tyres.append(practice_lap.getTyre().toString());
            wing_comments.append(practice_lap.getWingComment());
            engine_comments.append(practice_lap.getEngineComment());
            brakes_comments.append(practice_lap.getBrakesComment());
            gear_comments.append(practice_lap.getGearComment());
            suspension_comments.append(practice_lap.getSuspensionComment());
            track_ids.append(practice_lap.getTrackID());
            seasons.append(practice_lap.getSeason());

            counter++;
        }

        QSqlQuery query(db);
        query.prepare(insert + columns + values);

        query.addBindValue(lap_nros);
        query.addBindValue(lap_times);
        query.addBindValue(driver_mistakes);
        query.addBindValue(net_times);
        query.addBindValue(front_wings);
        query.addBindValue(rear_wings);
        query.addBindValue(engines);
        query.addBindValue(brakes);
        query.addBindValue(gears);
        query.addBindValue(suspensions);
        query.addBindValue(tyres);
        query.addBindValue(wing_comments);
        query.addBindValue(engine_comments);
        query.addBindValue(brakes_comments);
        query.addBindValue(gear_comments);
        query.addBindValue(suspension_comments);
        query.addBindValue(track_ids);
        query.addBindValue(seasons);

        if(debug) qDebug() << "PracticeLap Insert" << query.isActive();

        if (!query.execBatch()) {
            if(debug) qDebug() << query.lastError();
        } else {
            if(db.isOpen()) db.close();
            return true;
        }
    } else return false;

    if(db.isOpen()) db.close();

    return false;
}

bool DatabaseTableCreateHelper::insertDriverRow(QSqlDatabase &db, Driver driver, bool debug)
{
    QMutexLocker locker(&ThreadResourceManager::getInstance().getDBMutex());

    if(debug) qDebug() << "Inserting Driver" << driver.getSeason() << driver.getTrackID();

    QString insert = QStringLiteral("INSERT INTO Driver ( ");
    QString columns = QStringLiteral("Season, Overall, Concentration, Talent, Aggresiveness, "
                                     "Experience, TechInsight, Stamina, Charisma, Motivation, "
                                     "Weight, track_id, Age, Reputation ) ");
    QString values = QStringLiteral("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?);");

    if(db.open()) {
        QSqlQuery query = db.exec();
        query.prepare(insert + columns + values);
        query.addBindValue(driver.getSeason());
        query.addBindValue(driver.getOverall());
        query.addBindValue(driver.getConcentration());
        query.addBindValue(driver.getTalent());
        query.addBindValue(driver.getAggressiveness());
        query.addBindValue(driver.getExperience());
        query.addBindValue(driver.getTechnicalInsight());
        query.addBindValue(driver.getStamina());
        query.addBindValue(driver.getCharisma());
        query.addBindValue(driver.getMotivation());
        query.addBindValue(driver.getWeight());
        query.addBindValue(driver.getTrackID());
        query.addBindValue(driver.getAge());
        query.addBindValue(driver.getReputation());

        query.exec();

        if(debug) qDebug() << "Driver Insert" << query.isActive();

        if(query.isActive()) {
            if(db.isOpen()) db.close();
            return true;
        } else qDebug() << query.lastError().text();
    } else return false;

    if(db.isOpen()) db.close();

    return false;
}

bool DatabaseTableCreateHelper::insertRiskRow(QSqlDatabase &db, Risk risk, bool debug)
{
    QMutexLocker locker(&ThreadResourceManager::getInstance().getDBMutex());

    if(debug) qDebug() << "Inserting Risk" << risk.getSeason() << risk.getTrackID();

    QString insert = QStringLiteral("INSERT INTO Risk ( ");
    QString columns = QStringLiteral("Season, Clear, Wet, Defend, Malfunction, Overtake, "
                                     "track_id, StartRisk ) ");
    QString values = QStringLiteral("VALUES ( ?,?,?,?,?,?,?,? );");

    if(db.open()) {
        QSqlQuery query = db.exec();
        query.prepare(insert + columns + values);
        query.addBindValue(risk.getSeason());
        query.addBindValue(risk.getRisks().at(0));
        query.addBindValue(risk.getRisks().at(1));
        query.addBindValue(risk.getRisks().at(2));
        query.addBindValue(risk.getRisks().at(3));
        query.addBindValue(risk.getRisks().at(4));
        query.addBindValue(risk.getTrackID());
        query.addBindValue(risk.getStartingRisk());

        query.exec();

        if(debug) qDebug() << "Risk Insert" << query.isActive();

        if(query.isActive()) {
            if(db.isOpen()) db.close();
            return true;
        } else qDebug() << query.lastError().text();
    } else return false;

    if(db.isOpen()) db.close();

    return false;
}

bool DatabaseTableCreateHelper::insertLapRow(QSqlDatabase &db, Lap lap, bool debug)
{
    QMutexLocker locker(&ThreadResourceManager::getInstance().getDBMutex());

    if(debug) qDebug() << "Inserting Lap" << lap.getLapNum() << lap.getSeason() << lap.getTrackID();

    QString insert = QStringLiteral("INSERT INTO Lap ( ");
    QString columns = QStringLiteral("Season, Lap, LapTime, Pos, Tyres, Weather, Temp, Hum, Events, track_id ) ");
    QString values = QStringLiteral("VALUES ( ?,?,?,?,?,?,?,?,?,? );");

    if(db.open()) {
        QSqlQuery query = db.exec();
        query.prepare(insert + columns + values);
        query.addBindValue(lap.getSeason());
        query.addBindValue(lap.getLapNum());
        query.addBindValue(lap.getLapTime());
        query.addBindValue(lap.getPos());
        query.addBindValue(lap.getTyreType().toString());
        query.addBindValue(lap.getWeather());
        query.addBindValue(lap.getTemperature());
        query.addBindValue(lap.getHumidity());
        query.addBindValue(lap.getEvents());
        query.addBindValue(lap.getTrackID());

        query.exec();

        if(debug) qDebug() << "Lap Insert" << query.isActive();

        if(query.isActive()) {
            if(db.isOpen()) db.close();
            return true;
        } else qDebug() << query.lastError().text();
    } else return false;

    if(db.isOpen()) db.close();

    return false;
}

bool DatabaseTableCreateHelper::insertLapRows(QSqlDatabase &db, QList<Lap> laps, bool debug)
{
    QMutexLocker locker(&ThreadResourceManager::getInstance().getDBMutex());

    QString insert = QStringLiteral("INSERT INTO Lap ( ");
    QString columns = QStringLiteral("Season, Lap, LapTime, Pos, Tyres, Weather, Temp, Hum, Events, track_id ) ");
    QString values = QStringLiteral("VALUES ( ?,?,?,?,?,?,?,?,?,? );");

    if(db.open()) {
        QVariantList seasons;
        QVariantList lap_nums;
        QVariantList lap_times;
        QVariantList positions;
        QVariantList tyre_types;
        QVariantList weathers;
        QVariantList temperatures;
        QVariantList humidities;
        QVariantList events;
        QVariantList track_ids;

        foreach(Lap lap, laps) {
            seasons.append(lap.getSeason());
            lap_nums.append(lap.getLapNum());
            lap_times.append(lap.getLapTime());
            positions.append(lap.getPos());
            tyre_types.append(lap.getTyreType().toString());
            weathers.append(lap.getWeather());
            temperatures.append(lap.getTemperature());
            humidities.append(lap.getHumidity());
            events.append(lap.getEvents());
            track_ids.append(lap.getTrackID());
        }

        QSqlQuery query(db);
        query.prepare(insert + columns + values);

        query.addBindValue(seasons);
        query.addBindValue(lap_nums);
        query.addBindValue(lap_times);
        query.addBindValue(positions);
        query.addBindValue(tyre_types);
        query.addBindValue(weathers);
        query.addBindValue(temperatures);
        query.addBindValue(humidities);
        query.addBindValue(events);
        query.addBindValue(track_ids);

        if(debug) qDebug() << "Laps Insert" << query.isActive();

        if (!query.execBatch()) {
            if(debug) qDebug() << query.lastError();
        } else {
            if(db.isOpen()) db.close();
            return true;
        }

    } else return false;

    if(db.isOpen()) db.close();

    return false;
}

bool DatabaseTableCreateHelper::insertMiscDataRow(QSqlDatabase &db, double q1_time, double q2_time, int season, int track_id, bool debug)
{
    QMutexLocker locker(&ThreadResourceManager::getInstance().getDBMutex());

    if(debug) qDebug() << "Inserting Misc Data Row" << season << track_id;

    QString insert = QStringLiteral("INSERT INTO MiscRaceData ( ");
    QString columns = QStringLiteral("Season, track_id, Q1Time, Q2Time ) ");
    QString values = QStringLiteral("VALUES ( ?,?,?,? );");

    if(db.open()) {
        QSqlQuery query = db.exec();
        query.prepare(insert + columns + values);
        query.addBindValue(season);
        query.addBindValue(track_id);
        query.addBindValue(q1_time);
        query.addBindValue(q2_time);

        query.exec();

        if(debug) qDebug() << "MiscRaceData Insert" << query.isActive();

        if(query.isActive()) {
            if(db.isOpen()) db.close();
            return true;
        } else qDebug() << query.lastError().text();
    } else return false;

    if(db.isOpen()) db.close();

    return false;
}

bool DatabaseTableCreateHelper::insertStintRow(QSqlDatabase &db, Stint stint, bool debug)
{
    QMutexLocker locker(&ThreadResourceManager::getInstance().getDBMutex());

    if(debug) qDebug() << "Inserting Stint Row" << stint.getSeason() << stint.getTrackID() << stint.getStartLap() << stint.getLaps();

    QString insert = QStringLiteral("INSERT INTO Stint ( ");
    QString columns = QStringLiteral("Season, Laps, Fuel, Consumption, FinalP, AvgHumidity, AvgTemperature, "
                                     "TyreType, Weather, Km, StartLap, PitStop, StartFuel, track_id ) ");
    QString values = QStringLiteral("VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,? );");

    if(db.open()) {
        QSqlQuery query = db.exec();
        query.prepare(insert + columns + values);
        query.addBindValue(stint.getSeason());
        query.addBindValue(stint.getLaps());
        query.addBindValue(stint.getFuelUsed());
        query.addBindValue(stint.getFuelConsumption());
        query.addBindValue(stint.getFinalP());
        query.addBindValue(stint.getAvgHumidity());
        query.addBindValue(stint.getAvgTemperature());
        query.addBindValue(stint.getTyreType().toString());
        query.addBindValue(stint.getWeather());
        query.addBindValue(stint.getKM());
        query.addBindValue(stint.getStartLap());
        query.addBindValue(stint.getPitStop());
        query.addBindValue(stint.getStartFuel());
        query.addBindValue(stint.getTrackID());

        query.exec();

        if(debug) qDebug() << "Stint Insert" << query.isActive();

        if(query.isActive()) {
            if(db.isOpen()) db.close();
            return true;
        } else qDebug() << query.lastError().text();
    } else return false;

    if(db.isOpen()) db.close();

    return false;
}


