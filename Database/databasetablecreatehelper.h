#ifndef DATABASETABLECREATEHELPER_H
#define DATABASETABLECREATEHELPER_H

#include <QSettings>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QMutex>
#include <QMutexLocker>

#include "constantstrings.h"
#include "Objects/track.h"
#include "Objects/car.h"
#include "Objects/weather.h"
#include "Objects/practicelap.h"
#include "Objects/driver.h"
#include "Objects/risk.h"
#include "Objects/lap.h"
#include "Objects/stint.h"

class DatabaseTableCreateHelper
{
public:
    DatabaseTableCreateHelper();

    static const QString TABLE_TRACK;
    static const QString TABLE_RACE;
    static const QString TABLE_CAR;
    static const QString TABLE_CAR_WEAR;
    static const QString TABLE_WEATHER;
    static const QString TABLE_PRACTICE_LAP;
    static const QString TABLE_DRIVER;
    static const QString TABLE_RISK;
    static const QString TABLE_LAP;
    static const QString TABLE_MISC_DATA;
    static const QString TABLE_STINT_DATA;

    // create functions
    static bool createDatabase();

    static bool createTrackTable(QSqlDatabase &db, bool debug = false);

    static bool createRaceTable(QSqlDatabase &db, bool debug = false);

    static bool createCarTable(QSqlDatabase &db, bool debug = false);

    static bool createCarWearTable(QSqlDatabase &db, bool debug = false);

    static bool createWeatherTable(QSqlDatabase &db, bool debug = false);

    static bool createPracticeTable(QSqlDatabase &db, bool debug = false);

    static bool createDriverTable(QSqlDatabase &db, bool debug = false);

    static bool createRiskTable(QSqlDatabase &db, bool debug = false);

    static bool createLapTable(QSqlDatabase &db, bool debug = false);

    static bool createMiscDataTable(QSqlDatabase &db, bool debug = false);

    static bool createStintTable(QSqlDatabase &db, bool debug = false);

    static bool insertRaceRow(QSqlDatabase &db, int season, int track_id, bool debug = false);

    static bool insertTrackRow(QSqlDatabase &db, Track track, bool debug = false);

    static bool insertCarRow(QSqlDatabase &db, Car car, bool debug = false);

    static bool insertCarWearRow(QSqlDatabase &db, Car car, bool debug = false);

    static bool insertWeatherRow(QSqlDatabase &db, Weather weather, bool debug = false);

    static bool insertPracticeRow(QSqlDatabase &db, PracticeLap practice_lap, int lap_nro, bool debug = false);

    static bool insertPracticeRows(QSqlDatabase &db, QList<PracticeLap> practice_laps, bool debug = false);

    static bool insertDriverRow(QSqlDatabase &db, Driver driver, bool debug = false);

    static bool insertRiskRow(QSqlDatabase &db, Risk risk, bool debug = false);

    static bool insertLapRow(QSqlDatabase &db, Lap lap, bool debug = false);

    static bool insertLapRows(QSqlDatabase &db, QList<Lap> laps, bool debug = false);

    static bool insertMiscDataRow(QSqlDatabase &db, double q1_time, double q2_time, int season, int track_id, bool debug = false);

    static bool insertStintRow(QSqlDatabase &db, Stint stint, bool debug = false);

};

#endif // DATABASETABLECREATEHELPER_H
