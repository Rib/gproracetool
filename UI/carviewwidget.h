#ifndef CARVIEWWIDGET_H
#define CARVIEWWIDGET_H

#include <memory>

#include <QWidget>

#include "Objects/car.h"
#include "Objects/driver.h"
#include "Misc/gproresourcemanager.h"
#include "Algorithms/racealgorithms.h"
#include "Models/seasonlisttablemodel.h"
#include "Misc/defaultstyles.h"

using std::shared_ptr;

namespace Ui {
class CarViewWidget;
}

class CarViewWidget : public QWidget
{
    Q_OBJECT

public:
    explicit CarViewWidget(QWidget *parent = 0, QTableView *table = 0);
    ~CarViewWidget();

    void showSimple(bool simple);
    void showRaceCarView(bool rcv);

    void updateCarUI();

    void setCar(Car car) { *car_ = car; updateCarUI(); }
    void setPreviousCar(shared_ptr<Car> previous_car) { previous_car_ = previous_car; }
    void setTrackID(int track_id) { track_id_ = track_id; }
    void setTestTrackID(int track_id) { test_track_id_ = track_id; }
    void setClearTrack(int clear_track) {
        clear_track_ = clear_track;
        updateCarUI();
    }
    void setDriver(shared_ptr<Driver> driver) { driver_ = driver; updateCarUI(); }

    QList<double> getEstWears();

    shared_ptr<Car> getCar() { return car_; }

    double getTotalCost();
    int getClearTrack() { return clear_track_;}

    void calculateFinanceUI();

private slots:
    void on_fetch_current_car_button_clicked();

    void on_calc_est_wear_button_clicked();

    void on_chassis_lvl_text_textChanged(const QString &arg1);

    void on_engine_lvl_text_textChanged(const QString &arg1);

    void on_front_wing_lvl_text_textChanged(const QString &arg1);

    void on_rear_wing_lvl_text_textChanged(const QString &arg1);

    void on_underbody_lvl_text_textChanged(const QString &arg1);

    void on_sidepods_lvl_text_textChanged(const QString &arg1);

    void on_cooling_lvl_text_textChanged(const QString &arg1);

    void on_gearbox_lvl_text_textChanged(const QString &arg1);

    void on_brakes_lvl_text_textChanged(const QString &arg1);

    void on_suspension_lvl_text_textChanged(const QString &arg1);

    void on_electronics_lvl_text_textChanged(const QString &arg1);

    void on_cvw_clear_track_slider_valueChanged(int value);

    void on_chassis_wear_text_textChanged(const QString &arg1);

    void on_engine_wear_text_textChanged(const QString &arg1);

    void on_front_wing_wear_text_textChanged(const QString &arg1);

    void on_rear_wing_wear_text_textChanged(const QString &arg1);

    void on_underbody_wear_text_textChanged(const QString &arg1);

    void on_sidepods_wear_text_textChanged(const QString &arg1);

    void on_cooling_wear_text_textChanged(const QString &arg1);

    void on_gearbox_wear_text_textChanged(const QString &arg1);

    void on_brakes_wear_text_textChanged(const QString &arg1);

    void on_suspension_wear_text_textChanged(const QString &arg1);

    void on_electronics_wear_text_textChanged(const QString &arg1);

    void on_sync_previous_car_button_clicked();

    void chassis_est_wear_text_changed(const QString &text);

    void engine_est_wear_text_changed(const QString &text);

    void front_wing_est_wear_text_changed(const QString &text);

    void rear_wing_est_wear_text_changed(const QString &text);

    void underbody_est_wear_text_changed(const QString &text);

    void sidepods_est_wear_text_changed(const QString &text);

    void cooling_est_wear_text_changed(const QString &text);

    void gearbox_est_wear_text_changed(const QString &text);

    void brakes_est_wear_text_changed(const QString &text);

    void suspension_est_wear_text_changed(const QString &text);

    void electronics_est_wear_text_changed(const QString &text);

    void total_cost_text_changed(const QString &text) {
        emit totalCostChanged(text);
    }

    void on_test_checkbox_toggled(bool checked);

signals:
    void totalCostChanged(const QString &text);


private:
    Ui::CarViewWidget *ui;
    QTableView *table_;

    shared_ptr<Car> car_;
    shared_ptr<Car> previous_car_;
    shared_ptr<Driver> driver_;

    int track_id_;
    int test_track_id_;
    int clear_track_;

    int transformWear(const QString &str);

};

#endif // CARVIEWWIDGET_H
