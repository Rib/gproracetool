#include "racedatawidget.h"
#include "ui_racedatawidget.h"

#include <QDebug>
#include <QtAlgorithms>
#include <QStringList>

#include <array>

#include "Misc/constantcalculations.h"

using std::array;

RaceDataWidget::RaceDataWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RaceDataWidget)
{
    ui->setupUi(this);

    ui->temperature_min_search->setLimits(0,50);
    ui->temperature_max_search->setLimits(0,50);
    ui->temperature_min_search->setText(QString("0"));
    ui->temperature_max_search->setText(QString("50"));
    ui->humidity_min_search->setLimits(0,99);
    ui->humidity_max_search->setLimits(0,99);
    ui->humidity_min_search->setText(QString("0"));
    ui->humidity_max_search->setText(QString("99"));

    showRace();

    race_data_ = *(GproResourceManager::getInstance().getDatabaseHandler()->getRaceData());

    QStringList labels = GproResourceManager::getInstance().getDatabaseHandler()->getRaceDataLabels();

    int counter = 0;
    foreach(QString label, labels) {
        race_data_labels_indexes_.insert(label, counter);
        ++counter;
    }

    //for loading current seasons list
    QList<int> seasons = getSeasons();
    int seasons_size = seasons.size();

    ui->seasons_combo_box->insertItem(0, QString("All"));

    for(int i = 1; i < seasons_size+1; ++i) {
        ui->seasons_combo_box->insertItem(i, QString::number(seasons.at(i-1)));
    }

    QString current_season = ui->seasons_combo_box->currentText();

    QStringList races = getRaces(current_season);

    ui->races_combobox->insertItem(0, QString("All"));
    ui->races_combobox->insertItems(1,races);
}

RaceDataWidget::~RaceDataWidget()
{
    delete ui;
}

QList<int> RaceDataWidget::getSeasons()
{
    if(seasons_.size() > 0) return seasons_;

    QList<int> seasons;
    int season_index = getIndex("Season");

    for(int i = 0; i < race_data_.size(); ++i) {
        bool has_season = false;
        int current_season = race_data_.at(i).at(season_index).toInt();
        foreach(int s, seasons) {
            if(current_season == s) {
                has_season = true;
                break;
            }
        }

        if(!has_season) seasons.append(current_season);

    }

    qSort(seasons);

    seasons_ = seasons;

    return seasons;
}

QStringList RaceDataWidget::getRaces( const QString &season )
{
    QStringList races;

    unsigned int rd_size = race_data_.size();

    int race_index = getIndex(QStringLiteral("TrackName"));
    int season_index = getIndex(QStringLiteral("Season"));

    for(unsigned int i = 0; i < rd_size; ++i) {
        bool has_race = false;
        QString current_race = race_data_.at(i).at(race_index);
        foreach(QString s, races) {
            if(current_race == s) {
                has_race = true;
                break;
            }
        }

        if(!has_race && (race_data_.at(i).at(season_index) == season ||
                         season == QStringLiteral("All")))
            races.append(current_race);
    }

    return races;
}

void RaceDataWidget::setUIfromList(QList<QString> race)
{
    if(race.size() == 0) return;

    int race_distance_index;
    int race_power_index;
    int race_handling_index;
    int race_acceleration_index;
    int race_downforce_index;
    int race_overtaking_index;
    int race_suspension_index;
    int race_fuel_consumption_index;
    int race_tyre_wear_index;
    int race_lap_length_index;
    int race_corners_index;
    int race_grip_index;
    int race_pit_stop_index;

    int car_power_index;
    int car_handling_index;
    int car_acceleration_index;
    int car_chassis_index;
    int car_engine_index;
    int car_front_wing_index;
    int car_rear_wing_index;
    int car_underbody_index;
    int car_sidepods_index;
    int car_cooling_index;
    int car_gearbox_index;
    int car_brakes_index;
    int car_suspension_index;
    int car_electronics_index;

    int car_chassis_wear_index;
    int car_engine_wear_index;
    int car_front_wing_wear_index;
    int car_rear_wing_wear_index;
    int car_underbody_wear_index;
    int car_sidepods_wear_index;
    int car_cooling_wear_index;
    int car_gearbox_wear_index;
    int car_brakes_wear_index;
    int car_suspension_wear_index;
    int car_electronics_wear_index;

    int driver_overall_index;
    int driver_concentration_index;
    int driver_talent_index;
    int driver_aggressiveness_index;
    int driver_experience_index;
    int driver_technical_insight_index;
    int driver_stamina_index;
    int driver_charisma_index;
    int driver_motivation_index;
    int driver_weight_index;

    QList<QString> labels = GproResourceManager::getInstance().getDatabaseHandler()->getRaceDataLabels();

    for(int i = 0; i < labels.size(); ++i) {
        if(labels.at(i) == QString("TDistance")) {
            race_distance_index = i;
        } else if(labels.at(i) == QString("TPower")) {
            race_power_index = i;
        } else if(labels.at(i) == QString("THandling")) {
            race_handling_index = i;
        } else if(labels.at(i) == QString("TAcceleration")) {
            race_acceleration_index = i;
        } else if(labels.at(i) == QString("TDownforce")) {
            race_downforce_index = i;
        } else if(labels.at(i) == QString("TOvertaking")) {
            race_overtaking_index = i;
        } else if(labels.at(i) == QString("TSuspension")) {
            race_suspension_index = i;
        } else if(labels.at(i) == QString("TFuelConsumption")) {
            race_fuel_consumption_index = i;
        } else if(labels.at(i) == QString("TTyreWear")) {
            race_tyre_wear_index = i;
        } else if(labels.at(i) == QString("TLapLenght")) {
            race_lap_length_index = i;
        } else if(labels.at(i) == QString("TCorners")) {
            race_corners_index = i;
        } else if(labels.at(i) == QString("TGrip")) {
            race_grip_index = i;
        } else if(labels.at(i) == QString("TPitStop")) {
            race_pit_stop_index = i;
        } else if(labels.at(i) == QString("CPower")) {
            car_power_index = i;
        } else if(labels.at(i) == QString("CHandling")) {
            car_handling_index = i;
        } else if(labels.at(i) == QString("CAcceleration")) {
            car_acceleration_index = i;
        } else if(labels.at(i) == QString("CChassis")) {
            car_chassis_index = i;
        } else if(labels.at(i) == QString("CEngine")) {
            car_engine_index = i;
        } else if(labels.at(i) == QString("CFrontWing")) {
            car_front_wing_index = i;
        } else if(labels.at(i) == QString("CRearWing")) {
            car_rear_wing_index = i;
        } else if(labels.at(i) == QString("CUnderbody")) {
            car_underbody_index = i;
        } else if(labels.at(i) == QString("CSidepods")) {
            car_sidepods_index = i;
        } else if(labels.at(i) == QString("CCooling")) {
            car_cooling_index = i;
        } else if(labels.at(i) == QString("CGearbox")) {
            car_gearbox_index = i;
        } else if(labels.at(i) == QString("CBrakes")) {
            car_brakes_index = i;
        } else if(labels.at(i) == QString("CSuspension")) {
            car_suspension_index = i;
        } else if(labels.at(i) == QString("CElectronics")) {
            car_electronics_index = i;
        } else if(labels.at(i) == QString("CWChassis")) {
            car_chassis_wear_index = i;
        } else if(labels.at(i) == QString("CWEngine")) {
            car_engine_wear_index = i;
        } else if(labels.at(i) == QString("CWFrontWing")) {
            car_front_wing_wear_index = i;
        } else if(labels.at(i) == QString("CWRearWing")) {
            car_rear_wing_wear_index = i;
        } else if(labels.at(i) == QString("CWUnderbody")) {
            car_underbody_wear_index = i;
        } else if(labels.at(i) == QString("CWSidepods")) {
            car_sidepods_wear_index= i;
        } else if(labels.at(i) == QString("CWCooling")) {
            car_cooling_wear_index = i;
        } else if(labels.at(i) == QString("CWGearbox")) {
            car_gearbox_wear_index = i;
        } else if(labels.at(i) == QString("CWBrakes")) {
            car_brakes_wear_index = i;
        } else if(labels.at(i) == QString("CWSuspension")) {
            car_suspension_wear_index = i;
        } else if(labels.at(i) == QString("CWElectronics")) {
            car_electronics_wear_index = i;
        } else if(labels.at(i) == QString("DOverall")) {
            driver_overall_index = i;
        } else if(labels.at(i) == QString("DConcentration")) {
            driver_concentration_index = i;
        } else if(labels.at(i) == QString("DTalent")) {
            driver_talent_index = i;
        } else if(labels.at(i) == QString("DAggressiveness")) {
            driver_aggressiveness_index = i;
        } else if(labels.at(i) == QString("DExperience")) {
            driver_experience_index = i;
        } else if(labels.at(i) == QString("DTechInsight")) {
            driver_technical_insight_index = i;
        } else if(labels.at(i) == QString("DStamina")) {
            driver_stamina_index = i;
        } else if(labels.at(i) == QString("DCharisma")) {
            driver_charisma_index = i;
        } else if(labels.at(i) == QString("DMotivation")) {
            driver_motivation_index = i;
        } else if(labels.at(i) == QString("DWeight")) {
            driver_weight_index = i;
        }
    }

    double total_cost = 0;

    array<int,3> chassis = { {race.at(car_chassis_index).toInt(), race.at(car_chassis_wear_index).toInt(), 0 } };
    array<int,3> engine = { {race.at(car_engine_index).toInt(), race.at(car_engine_wear_index).toInt(), 0 } };
    array<int,3> front_wing = { {race.at(car_front_wing_index).toInt(), race.at(car_front_wing_wear_index).toInt(), 0 } };
    array<int,3> rear_wing = { {race.at(car_rear_wing_index).toInt(), race.at(car_rear_wing_wear_index).toInt(), 0 } };
    array<int,3> underbody = { {race.at(car_underbody_index).toInt(), race.at(car_underbody_wear_index).toInt(), 0 } };
    array<int,3> sidepods = { {race.at(car_sidepods_index).toInt(), race.at(car_sidepods_wear_index).toInt(), 0 } };
    array<int,3> cooling = { {race.at(car_cooling_index).toInt(), race.at(car_cooling_wear_index).toInt(), 0 } };
    array<int,3> gearbox = { {race.at(car_gearbox_index).toInt(), race.at(car_gearbox_wear_index).toInt(), 0 } };
    array<int,3> brakes = { {race.at(car_brakes_index).toInt(), race.at(car_brakes_wear_index).toInt(), 0 } };
    array<int,3> suspension = { {race.at(car_suspension_index).toInt(), race.at(car_suspension_wear_index).toInt(), 0 } };
    array<int,3> electronics = { {race.at(car_electronics_index).toInt(), race.at(car_electronics_wear_index).toInt(), 0 } };

    chassis.at(2) = ConstantCalculations::getPartPrice(QStringLiteral("Chassis"), chassis.at(0));
    engine.at(2) = ConstantCalculations::getPartPrice(QStringLiteral("Engine"), engine.at(0));
    front_wing.at(2) = ConstantCalculations::getPartPrice(QStringLiteral("FrontWing"), front_wing.at(0));
    rear_wing.at(2) = ConstantCalculations::getPartPrice(QStringLiteral("RearWing"), rear_wing.at(0));
    underbody.at(2) =ConstantCalculations::getPartPrice(QStringLiteral("Underbody"), underbody.at(0));
    sidepods.at(2) = ConstantCalculations::getPartPrice(QStringLiteral("Chassis"), sidepods.at(0));
    cooling.at(2) = ConstantCalculations::getPartPrice(QStringLiteral("Sidepods"), cooling.at(0));
    gearbox.at(2) = ConstantCalculations::getPartPrice(QStringLiteral("Gearbox"), gearbox.at(0));
    brakes.at(2) = ConstantCalculations::getPartPrice(QStringLiteral("Brakes"), brakes.at(0));
    suspension.at(2) = ConstantCalculations::getPartPrice(QStringLiteral("Suspension"), suspension.at(0));
    electronics.at(2) = ConstantCalculations::getPartPrice(QStringLiteral("Electronics"), electronics.at(0));

    total_cost += static_cast<double>(chassis.at(1))/100 * chassis.at(2);
    total_cost += static_cast<double>(engine.at(1))/100 * engine.at(2);
    total_cost += static_cast<double>(front_wing.at(1))/100 * front_wing.at(2);
    total_cost += static_cast<double>(rear_wing.at(1))/100 * rear_wing.at(2);
    total_cost += static_cast<double>(underbody.at(1))/100 * underbody.at(2);
    total_cost += static_cast<double>(sidepods.at(1))/100 * sidepods.at(2);
    total_cost += static_cast<double>(cooling.at(1))/100 * cooling.at(2);
    total_cost += static_cast<double>(gearbox.at(1))/100 * gearbox.at(2);
    total_cost += static_cast<double>(brakes.at(1))/100 * brakes.at(2);
    total_cost += static_cast<double>(suspension.at(1))/100 * suspension.at(2);
    total_cost += static_cast<double>(electronics.at(1))/100 * electronics.at(2);

    ui->car_cost_text->setText(QString::number(total_cost));

    ui->track_name_text->setText(race.at(getIndex("TrackName")));
    ui->race_distance_text->setText(race.at(race_distance_index));
    ui->track_power_text->setText(race.at(race_power_index));
    ui->track_handling_text->setText(race.at(race_handling_index));
    ui->track_acceleration_text->setText(race.at(race_acceleration_index));
    ui->track_downforce_text->setText(race.at(race_downforce_index));
    ui->track_overtaking_text->setText(race.at(race_overtaking_index));
    ui->track_suspension_text->setText(race.at(race_suspension_index));
    ui->track_fuel_consumption_text->setText(race.at(race_fuel_consumption_index));
    ui->track_tyre_wear_text->setText(race.at(race_tyre_wear_index));
    ui->track_lap_length_text->setText(race.at(race_lap_length_index));
    ui->track_corners_text->setText(race.at(race_corners_index));
    ui->track_grip_text->setText(race.at(race_grip_index));
    ui->track_pit_stop_text->setText(race.at(race_pit_stop_index));

    ui->car_power_text->setText(race.at(car_power_index));
    ui->car_handling_text->setText(race.at(car_handling_index));
    ui->car_acceleration_text->setText(race.at(car_acceleration_index));
    ui->car_chassis_text->setText(race.at(car_chassis_index));
    ui->car_engine_text->setText(race.at(car_engine_index));
    ui->car_front_wing_text->setText(race.at(car_front_wing_index));
    ui->car_rear_wing_text->setText(race.at(car_rear_wing_index));
    ui->car_underbody_text->setText(race.at(car_underbody_index));
    ui->car_sidepods_text->setText(race.at(car_sidepods_index));
    ui->car_cooling_text->setText(race.at(car_cooling_index));
    ui->car_gearbox_text->setText(race.at(car_gearbox_index));
    ui->car_brakes_text->setText(race.at(car_brakes_index));
    ui->car_suspension_text->setText(race.at(car_suspension_index));
    ui->car_electronics_text->setText(race.at(car_electronics_index));

    ui->car_chassis_wear_text->setText(race.at(car_chassis_wear_index));
    ui->car_engine_wear_text->setText(race.at(car_engine_wear_index));
    ui->car_front_wing_wear_text->setText(race.at(car_front_wing_wear_index));
    ui->car_rear_wing_wear_text->setText(race.at(car_rear_wing_wear_index));
    ui->car_underbody_wear_text->setText(race.at(car_underbody_wear_index));
    ui->car_sidepods_wear_text->setText(race.at(car_sidepods_wear_index));
    ui->car_cooling_wear_text->setText(race.at(car_cooling_wear_index));
    ui->car_gearbox_wear_text->setText(race.at(car_gearbox_wear_index));
    ui->car_brakes_wear_text->setText(race.at(car_brakes_wear_index));
    ui->car_suspension_wear_text->setText(race.at(car_suspension_wear_index));
    ui->car_electronics_wear_text->setText(race.at(car_electronics_wear_index));

    ui->driver_overall_text->setText(race.at(driver_overall_index));
    ui->driver_concentration_text->setText(race.at(driver_concentration_index));
    ui->driver_talent_text->setText(race.at(driver_talent_index));
    ui->driver_aggressiveness_text->setText(race.at(driver_aggressiveness_index));
    ui->driver_experience_text->setText(race.at(driver_experience_index));
    ui->driver_technical_insight_text->setText(race.at(driver_technical_insight_index));
    ui->driver_stamina_text->setText(race.at(driver_stamina_index));
    ui->driver_charisma_text->setText(race.at(driver_charisma_index));
    ui->driver_motivation_text->setText(race.at(driver_motivation_index));
    ui->driver_weight_text->setText(race.at(driver_weight_index));

    ui->avg_temperature_text->setText(race.at(getIndex(QString("STemperature"))));
    ui->avg_humidity_text->setText(race.at(getIndex(QString("SHumidity"))));
    ui->weather_type_text->setText(race.at(getIndex(QStringLiteral("SWeather"))));
}

void RaceDataWidget::doSearchOperations()
{
    QString season = ui->seasons_combo_box->currentText();
    QString tname = ui->races_combobox->currentText();

    QStringList races = getRaces(season);

    ui->races_combobox->clear();

    ui->races_combobox->insertItem(0, QStringLiteral("All"));
    ui->races_combobox->insertItems(1,races);
    ui->races_combobox->setCurrentText(tname);

    int race_index = getIndex(QStringLiteral("TrackName"));
    int season_index = getIndex(QStringLiteral("Season"));
    int temperature_index = getIndex(QStringLiteral("STemperature"));
    int humidity_index = getIndex(QStringLiteral("SHumidity"));
    int weather_index = getIndex(QStringLiteral("SWeather"));

    int race_cbox_index = ui->races_combobox->currentIndex();
    int season_cbox_index = ui->seasons_combo_box->currentIndex();

    int min_temperature = ui->temperature_min_search->text().toInt();
    int max_temperature = ui->temperature_max_search->text().toInt();
    int min_humidity = ui->humidity_min_search->text().toInt();
    int max_humidity = ui->humidity_max_search->text().toInt();
    QString weather = ui->weather_search->currentText();

    if(race_cbox_index == 0 || season_cbox_index == 0) {
        QList< QList<QString> > track_races;

        foreach(QList<QString> single_race, race_data_) {
            double temperature = single_race.at(temperature_index).toDouble();
            double humidity = single_race.at(humidity_index).toDouble();

            if(temperature < min_temperature || temperature > max_temperature ||
                    humidity < min_humidity || humidity > max_humidity ||
                    (weather != QStringLiteral("All") && weather != single_race.at(weather_index))) continue;

            if(race_cbox_index == 0 && season_cbox_index == 0)
                track_races.append(single_race);
            else if(race_cbox_index == 0 && season == single_race.at(season_index))
                track_races.append(single_race);
            else if(season_cbox_index == 0 && tname == single_race.at(race_index))
                track_races.append(single_race);
        }

        ui->races_list_widget->setRaceData(track_races, race_data_labels_indexes_);
        showRacesList();
    } else if(race_cbox_index > 0 && season_cbox_index > 0) {
        QList<QString> race;

        foreach(QList<QString> single_race, race_data_) {
            if(single_race.at(season_index) == season && single_race.at(race_index) == tname) {
                race = single_race;
                break;
            }
        }

        showRace();
        setUIfromList(race);
    }
}

void RaceDataWidget::on_seasons_combo_box_currentIndexChanged(const QString &arg1)
{
    ui->races_combobox->blockSignals(true);
    ui->seasons_combo_box->blockSignals(true);
    doSearchOperations();
    ui->races_combobox->blockSignals(false);
    ui->seasons_combo_box->blockSignals(false);
}

void RaceDataWidget::on_races_combobox_currentIndexChanged(const QString &arg1)
{
    ui->races_combobox->blockSignals(true);
    ui->seasons_combo_box->blockSignals(true);
    doSearchOperations();
    ui->races_combobox->blockSignals(false);
    ui->seasons_combo_box->blockSignals(false);
}

void RaceDataWidget::showRacesList()
{
    ui->races_list_widget->setVisible(true);
    ui->race_view_widget->setVisible(false);
}

void RaceDataWidget::showRace()
{
    ui->races_list_widget->setVisible(false);
    ui->race_view_widget->setVisible(true);
}

void RaceDataWidget::on_temperature_min_search_textChanged(const QString &arg1)
{
    ui->races_combobox->blockSignals(true);
    ui->seasons_combo_box->blockSignals(true);
    doSearchOperations();
    ui->races_combobox->blockSignals(false);
    ui->seasons_combo_box->blockSignals(false);
}

void RaceDataWidget::on_temperature_max_search_textChanged(const QString &arg1)
{
    ui->races_combobox->blockSignals(true);
    ui->seasons_combo_box->blockSignals(true);
    doSearchOperations();
    ui->races_combobox->blockSignals(false);
    ui->seasons_combo_box->blockSignals(false);
}

void RaceDataWidget::on_humidity_min_search_textChanged(const QString &arg1)
{
    ui->races_combobox->blockSignals(true);
    ui->seasons_combo_box->blockSignals(true);
    doSearchOperations();
    ui->races_combobox->blockSignals(false);
    ui->seasons_combo_box->blockSignals(false);
}

void RaceDataWidget::on_humidity_max_search_textChanged(const QString &arg1)
{
    ui->races_combobox->blockSignals(true);
    ui->seasons_combo_box->blockSignals(true);
    doSearchOperations();
    ui->races_combobox->blockSignals(false);
    ui->seasons_combo_box->blockSignals(false);
}

void RaceDataWidget::on_weather_search_currentIndexChanged(int index)
{
    ui->races_combobox->blockSignals(true);
    ui->seasons_combo_box->blockSignals(true);
    doSearchOperations();
    ui->races_combobox->blockSignals(false);
    ui->seasons_combo_box->blockSignals(false);
}
