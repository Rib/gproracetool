#include "practicecalculation.h"
#include "ui_practicecalculation.h"
#include "gproserverinterface.h"
#include "Handlers/databasehandler.h"
#include "Algorithms/practicealgorithms.h"
#include <QDebug>
#include <cmath>
#include <array>
#include <mlpack/core.hpp>
#include <QSettings>
#include <QTime>


#include "settingconstants.h"

array<double,2> calculateSettingRange(practiceTableModel* model, int setting) {
    vector< array<double,6> > data = model->getRawData();
    vector< array<int,6> > comments = model->getComments();

    array<double,2> setting_range;
    setting_range.at(0) = 0;
    setting_range.at(1) = 2000;

    for(unsigned int i = 0; i < data.size() && setting < 6 && setting >= 0; ++i) {
        int comment = comments.at(i).at(setting);

        if (std::abs(comment) < 3) {
            if (comment == 0) {
                setting_range.at(0) = std::max(setting_range.at(0), data.at(i).at(setting)-model->getDefaultRange());
                setting_range.at(1) = std::min(setting_range.at(1),data.at(i).at(setting)+model->getDefaultRange());
            } else if (std::signbit(comment)) {
                setting_range.at(0) = std::max(setting_range.at(0), data.at(i).at(setting)+(model->getDefaultRange()*comment)-model->getDefaultRange()*1.5);
                setting_range.at(1) = std::min(setting_range.at(1),data.at(i).at(setting));
            } else if (!std::signbit(comment)) {
                setting_range.at(0) = std::max(setting_range.at(0), data.at(i).at(setting));
                setting_range.at(1) = std::min(setting_range.at(1),data.at(i).at(setting)+(model->getDefaultRange()*comment)+model->getDefaultRange()*1.5);
            }
        } else if(std::abs(comment) == 3) {
            if (std::signbit(comment)) {
                setting_range.at(1) = std::min(setting_range.at(1),data.at(i).at(setting)+(model->getDefaultRange()*comment)+model->getDefaultRange()*1.5);
            } else if (!std::signbit(comment)) {
                setting_range.at(0) = std::max(setting_range.at(0), data.at(i).at(setting)+(model->getDefaultRange()*comment)-model->getDefaultRange()*1.5);
            }
        }

    }

    return setting_range;

}


PracticeCalculation::PracticeCalculation(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PracticeCalculation),
    practice_table_model_(new practiceTableModel(parent)),
    suggested_settings_(),
    weather_(0),
    test_weather_(0)
{
    ui->setupUi(this);

    ui->practice_table->setVisible(false);
    ui->calculate_settings_button->setVisible(false);

    setDefaultSettings();

    QSettings settings(General::ProgramName, General::CompanyName);

    // loadgin practice runs
    if(settings.value(Settings::PCTableModel).toStringList().size() > 0) {
        practice_table_model_ = new practiceTableModel(settings.value(Settings::PCTableModel).toStringList(), parent);

        array<int,6> last_comments = practice_table_model_->getComments().size() > 0 ?
                    practice_table_model_->getComments().at(practice_table_model_->getComments().size()-1) : array<int,6>();
        ui->wing_setting_cb->setCurrentIndex(3-last_comments.at(0));
        ui->engine_setting_cb->setCurrentIndex(3-last_comments.at(2));
        ui->brakes_setting_cb->setCurrentIndex(3-last_comments.at(3));
        ui->gear_setting_cb->setCurrentIndex(3-last_comments.at(4));
        ui->suspension_setting_cb->setCurrentIndex(3-last_comments.at(5));

    } else {
        reset_edit_fields();
    }

    ui->practice_table->setModel(practice_table_model_);

    //loading suggested settings
    int ssrows = settings.beginReadArray(Settings::PCSuggestedSettingsText);
    for(int i = 0; i < ssrows; ++i) {
        settings.setArrayIndex(i);
        bool valid;
        suggested_settings_.at(i) = settings.value(Settings::PCSuggestedSettingsText).toInt(&valid);
        if(!valid) suggested_settings_.at(i) = 500;
    }
    settings.endArray();

    ui->front_wing_setting_edit->setText(QString::number(suggested_settings_.at(0)));
    ui->rear_wing_setting_edit->setText(QString::number(suggested_settings_.at(1)));
    ui->engine_setting_edit->setText(QString::number(suggested_settings_.at(2)));
    ui->brakes_setting_edit->setText(QString::number(suggested_settings_.at(3)));
    ui->gear_setting_edit->setText(QString::number(suggested_settings_.at(4)));
    ui->suspension_setting_edit->setText(QString::number(suggested_settings_.at(5)));

    //setting test track to run concurrent
    test_weather_future_ = QtConcurrent::run([]() {
        gproServerInterface *gsi = gproServerInterface::createInstance();
        return shared_ptr<Weather>(new Weather(gsi->getTestWeather()));
    });

    shared_ptr<mat> practice_data = GproResourceManager::getInstance().getDatabaseHandler()->getPracticeData(false);

    PracticeAlgorithms *pa = GproResourceManager::getInstance().getPracticeAlgorithms();

    pa->setPracticeData(practice_data);
    pa->setPracticeDataLabelsIndexes(GproResourceManager::getInstance().getDatabaseHandler()->getPracticeDataLabelsIndexes());

    shared_ptr<mat> practice_lap_data = GproResourceManager::getInstance().getDatabaseHandler()->getFullPracticeLapData();

    ui->range_setting_edit->setText(QString::number(pa->getCommentRange()));

    weather_ = GproResourceManager::getInstance().getWeather();

    updateWeatherUI(*weather_);

    updatePracticeCalculationUI();
    on_calculate_settings_button_clicked();
}

PracticeCalculation::~PracticeCalculation()
{
    QSettings settings(General::ProgramName, General::CompanyName);

    suggested_settings_.at(0) = ui->front_wing_setting_edit->text().toInt();
    suggested_settings_.at(1) = ui->rear_wing_setting_edit->text().toInt();
    suggested_settings_.at(2) = ui->engine_setting_edit->text().toInt();
    suggested_settings_.at(3) = ui->brakes_setting_edit->text().toInt();
    suggested_settings_.at(4) = ui->gear_setting_edit->text().toInt();
    suggested_settings_.at(5) = ui->suspension_setting_edit->text().toInt();

    // saving suggested settings
    settings.beginWriteArray(Settings::PCSuggestedSettingsText);
    for(unsigned int i = 0; i < suggested_settings_.size(); ++i) {
        settings.setArrayIndex(i);
        settings.setValue(Settings::PCSuggestedSettingsText, suggested_settings_.at(i));
    }
    settings.endArray();

    // saving practice calculation model
    QStringList ptmodel = practice_table_model_->serialize();
    settings.setValue(Settings::PCTableModel, ptmodel);

    delete ui;
}

void PracticeCalculation::updatePracticeCalculationUI()
{
    practiceTableModel* model = static_cast<practiceTableModel*>(ui->practice_table->model());

    int row = model->rowCount(QModelIndex())-1;

    for(int column = 0; column < model->columnCount(QModelIndex()); ++column) {
        ui->practice_table->setIndexWidget(model->index(row,column), model->getCellWidget(column,row));
    }

    //temp_ranges = model->calculateRange(temp_row, temp_comments);

    array<double,2> temp_settings_range = calculateSettingRange(model, 0);

    array<double,5> min_settings;
    array<double,5> max_settings;
    array<double,5> suggested_settings_min;
    array<double,5> suggested_settings_max;

    for (unsigned int i = 1; i <= min_settings.size(); ++i) {
        temp_settings_range = calculateSettingRange(model, i);

        min_settings.at(i-1) = temp_settings_range.at(0);
        max_settings.at(i-1) = temp_settings_range.at(1);
        suggested_settings_min.at(i-1) = max_settings.at(i-1) -
                (ui->range_setting_edit->text().toDouble()/2);
        suggested_settings_max.at(i-1) = min_settings.at(i-1) +
                (ui->range_setting_edit->text().toDouble()/2);
    }

    unsigned int suggested_setting = 500;
    unsigned int full_range = 0;

    ui->wing_min_text->setText(QString::number(min_settings.at(0)));
    ui->wing_max_text->setText(QString::number(max_settings.at(0)));

    suggested_setting = (std::ceil(suggested_settings_max.at(0)) + std::floor(suggested_settings_min.at(0))) / 2;
    full_range = std::abs(std::floor(suggested_settings_min.at(0)) - std::ceil(suggested_settings_max.at(0)));
    ui->wing_suggested_setting_text->setText(QString::number(suggested_setting) + QString(": ") +
                                             QString::number(full_range));
    suggested_settings_.at(0) = suggested_setting;

    ui->engine_min_text->setText(QString::number(min_settings.at(1)));
    ui->engine_max_text->setText(QString::number(max_settings.at(1)));

    suggested_setting = (std::ceil(suggested_settings_max.at(1)) + std::floor(suggested_settings_min.at(1))) / 2;
    full_range = std::abs(std::floor(suggested_settings_min.at(1)) - std::ceil(suggested_settings_max.at(1)));
    ui->engine_suggest_text->setText(QString::number(suggested_setting) + QString(": ") +
                                     QString::number(full_range));
    suggested_settings_.at(1) = suggested_setting;

    ui->brakes_min_text->setText(QString::number(min_settings.at(2)));
    ui->brakes_max_text->setText(QString::number(max_settings.at(2)));

    suggested_setting = (std::ceil(suggested_settings_max.at(2)) + std::floor(suggested_settings_min.at(2))) / 2;
    full_range = std::abs(std::floor(suggested_settings_min.at(2)) - std::ceil(suggested_settings_max.at(2)));
    ui->brakes_suggest_text->setText(QString::number(suggested_setting) + QString(": ") +
                                     QString::number(full_range));
    suggested_settings_.at(2) = suggested_setting;

    ui->gear_min_text->setText(QString::number(min_settings.at(3)));
    ui->gear_max_text->setText(QString::number(max_settings.at(3)));

    suggested_setting = (std::ceil(suggested_settings_max.at(3)) + std::floor(suggested_settings_min.at(3))) / 2;
    full_range = std::abs(std::floor(suggested_settings_min.at(3)) - std::ceil(suggested_settings_max.at(3)));
    ui->gear_suggest_text->setText(QString::number(suggested_setting) + QString(": ") +
                                   QString::number(full_range));
    suggested_settings_.at(3) = suggested_setting;

    ui->suspension_min_text->setText(QString::number(min_settings.at(4)));
    ui->suspension_max_text->setText(QString::number(max_settings.at(4)));

    suggested_setting = (std::ceil(suggested_settings_max.at(4)) + std::floor(suggested_settings_min.at(4))) / 2;
    full_range = std::abs(std::floor(suggested_settings_min.at(4)) - std::ceil(suggested_settings_max.at(4)));
    ui->suspension_suggest_text->setText(QString::number(suggested_setting) + QString(": ") +
                                         QString::number(full_range));
    suggested_settings_.at(4) = suggested_setting;

    QString temp_string = QString::number(std::ceil((max_settings.at(0) - min_settings.at(0))/2 + min_settings.at(0) - model->getDefaultRange()/2));
    ui->front_wing_setting_edit->setText(temp_string);
    ui->rear_wing_setting_edit->setText(temp_string);

    temp_string = QString::number(std::ceil((max_settings.at(1) - min_settings.at(1))/2 + min_settings.at(1) - model->getDefaultRange()/2));
    ui->engine_setting_edit->setText(temp_string);

    temp_string = QString::number(std::ceil((max_settings.at(2) - min_settings.at(2))/2 + min_settings.at(2) - model->getDefaultRange()/2));
    ui->brakes_setting_edit->setText(temp_string);

    temp_string = QString::number(std::ceil((max_settings.at(3) - min_settings.at(3))/2 + min_settings.at(3) - model->getDefaultRange()/2));
    ui->gear_setting_edit->setText(temp_string);

    temp_string = QString::number(std::ceil((max_settings.at(4) - min_settings.at(4))/2 + min_settings.at(4) - model->getDefaultRange()/2));
    ui->suspension_setting_edit->setText(temp_string);

    on_calculate_settings_button_clicked();
}

void PracticeCalculation::updateRaceTestCombobox(int value)
{
    if (value == 0) {
        on_fetch_weather_button_clicked();
        ui->q2_stuff_layout->setVisible(true);
        ui->race_stuff_layout->setVisible(true);
    } else if (value  == 1) {

        ui->q2_stuff_layout->setVisible(false);
        ui->race_stuff_layout->setVisible(false);

        if(test_weather_future_.isRunning()) {
            test_weather_future_.waitForFinished();
        }

        if(test_weather_ == 0 && test_weather_future_.isFinished()) {
            test_weather_ = test_weather_future_.result();
        }

        ui->q1_weather_status->setCurrentIndex(test_weather_->getQ1Status());
        ui->q1_temperature->setText(QString::number(test_weather_->getQ1Temperature()));
        ui->q1_humidity->setText(QString::number(test_weather_->getQ1Humidity()));
    }
}

void PracticeCalculation::setDefaultSettings()
{
    ui->front_wing_setting_edit->setLimits(0,2000);
    ui->rear_wing_setting_edit->setLimits(0,2000);
    ui->engine_setting_edit->setLimits(0,2000);
    ui->brakes_setting_edit->setLimits(0,2000);
    ui->gear_setting_edit->setLimits(0,2000);
    ui->suspension_setting_edit->setLimits(0,2000);

    ui->q1_temperature->setLimits(0,50);
    ui->q1_humidity->setLimits(0,100);
    ui->q2_temperature->setLimits(0,50);
    ui->q2_humidity->setLimits(0,100);
    ui->race_q1_temperature_low->setLimits(0,50);
    ui->race_q1_temperature_high->setLimits(0,50);
    ui->race_q2_temperature_low->setLimits(0,50);
    ui->race_q2_temperature_high->setLimits(0,50);
    ui->race_q3_temperature_low->setLimits(0,50);
    ui->race_q3_temperature_high->setLimits(0,50);
    ui->race_q4_temperature_low->setLimits(0,50);
    ui->race_q4_temperature_high->setLimits(0,50);
    ui->race_q1_humidity_low->setLimits(0,100);
    ui->race_q1_humidity_high->setLimits(0,100);
    ui->race_q2_humidity_low->setLimits(0,100);
    ui->race_q2_humidity_high->setLimits(0,100);
    ui->race_q3_humidity_low->setLimits(0,100);
    ui->race_q3_humidity_high->setLimits(0,100);
    ui->race_q4_humidity_low->setLimits(0,100);
    ui->race_q4_humidity_high->setLimits(0,100);
    ui->race_q1_rain_probability_low->setLimits(0,99);
    ui->race_q1_rain_probability_high->setLimits(0,99);
    ui->race_q2_rain_probability_low->setLimits(0,99);
    ui->race_q2_rain_probability_high->setLimits(0,99);
    ui->race_q3_rain_probability_low->setLimits(0,99);
    ui->race_q3_rain_probability_high->setLimits(0,99);
    ui->race_q4_rain_probability_low->setLimits(0,99);
    ui->race_q4_rain_probability_high->setLimits(0,99);
}

void PracticeCalculation::on_add_practice_button_clicked()
{
    array<double,6> temp_row = {{ ui->front_wing_setting_edit->text().toDouble(),
                                  ui->rear_wing_setting_edit->text().toDouble(),
                                  ui->engine_setting_edit->text().toDouble(),
                                  ui->brakes_setting_edit->text().toDouble(),
                                  ui->gear_setting_edit->text().toDouble(),
                                  ui->suspension_setting_edit->text().toDouble()}};

    array<int,6> temp_comments = {{ 3 - ui->wing_setting_cb->currentIndex(),
                                    3 - ui->wing_setting_cb->currentIndex(),
                                    3 - ui->engine_setting_cb->currentIndex(),
                                    3 - ui->brakes_setting_cb->currentIndex(),
                                    3 - ui->gear_setting_cb->currentIndex(),
                                    3 - ui->suspension_setting_cb->currentIndex() }};

    practiceTableModel* model = static_cast<practiceTableModel*>(ui->practice_table->model());

    array<double,6> temp_ranges = model->calculateRange(temp_row, temp_comments);
    model->addRow(temp_row, temp_comments, temp_ranges);

    int row = model->rowCount(QModelIndex())-1;

    for(int column = 0; column < model->columnCount(QModelIndex()); ++column) {
        ui->practice_table->setIndexWidget(model->index(row,column), model->getCellWidget(column,row));
    }

    //temp_ranges = model->calculateRange(temp_row, temp_comments);

    array<double,2> temp_settings_range = calculateSettingRange(model, 0);

    array<double,5> min_settings;
    array<double,5> max_settings;
    array<double,5> suggested_settings_min;
    array<double,5> suggested_settings_max;

    for (unsigned int i = 1; i <= min_settings.size(); ++i) {
        temp_settings_range = calculateSettingRange(model, i);

        min_settings.at(i-1) = temp_settings_range.at(0);
        max_settings.at(i-1) = temp_settings_range.at(1);
        suggested_settings_min.at(i-1) = max_settings.at(i-1) -
                (ui->range_setting_edit->text().toDouble()/2);
        suggested_settings_max.at(i-1) = min_settings.at(i-1) +
                (ui->range_setting_edit->text().toDouble()/2);
    }

    unsigned int suggested_setting = 500;
    unsigned int full_range = 0;

    ui->wing_min_text->setText(QString::number(min_settings.at(0)));
    ui->wing_max_text->setText(QString::number(max_settings.at(0)));

    suggested_setting = (std::ceil(suggested_settings_max.at(0)) + std::floor(suggested_settings_min.at(0))) / 2;
    full_range = std::abs(std::floor(suggested_settings_min.at(0)) - std::ceil(suggested_settings_max.at(0)));
    ui->wing_suggested_setting_text->setText(QString::number(suggested_setting) + QString(": ") +
                                             QString::number(full_range));
    suggested_settings_.at(0) = suggested_setting;

    ui->engine_min_text->setText(QString::number(min_settings.at(1)));
    ui->engine_max_text->setText(QString::number(max_settings.at(1)));

    suggested_setting = (std::ceil(suggested_settings_max.at(1)) + std::floor(suggested_settings_min.at(1))) / 2;
    full_range = std::abs(std::floor(suggested_settings_min.at(1)) - std::ceil(suggested_settings_max.at(1)));
    ui->engine_suggest_text->setText(QString::number(suggested_setting) + QString(": ") +
                                     QString::number(full_range));
    suggested_settings_.at(1) = suggested_setting;

    ui->brakes_min_text->setText(QString::number(min_settings.at(2)));
    ui->brakes_max_text->setText(QString::number(max_settings.at(2)));

    suggested_setting = (std::ceil(suggested_settings_max.at(2)) + std::floor(suggested_settings_min.at(2))) / 2;
    full_range = std::abs(std::floor(suggested_settings_min.at(2)) - std::ceil(suggested_settings_max.at(2)));
    ui->brakes_suggest_text->setText(QString::number(suggested_setting) + QString(": ") +
                                     QString::number(full_range));
    suggested_settings_.at(2) = suggested_setting;

    ui->gear_min_text->setText(QString::number(min_settings.at(3)));
    ui->gear_max_text->setText(QString::number(max_settings.at(3)));

    suggested_setting = (std::ceil(suggested_settings_max.at(3)) + std::floor(suggested_settings_min.at(3))) / 2;
    full_range = std::abs(std::floor(suggested_settings_min.at(3)) - std::ceil(suggested_settings_max.at(3)));
    ui->gear_suggest_text->setText(QString::number(suggested_setting) + QString(": ") +
                                   QString::number(full_range));
    suggested_settings_.at(3) = suggested_setting;

    ui->suspension_min_text->setText(QString::number(min_settings.at(4)));
    ui->suspension_max_text->setText(QString::number(max_settings.at(4)));

    suggested_setting = (std::ceil(suggested_settings_max.at(4)) + std::floor(suggested_settings_min.at(4))) / 2;
    full_range = std::abs(std::floor(suggested_settings_min.at(4)) - std::ceil(suggested_settings_max.at(4)));
    ui->suspension_suggest_text->setText(QString::number(suggested_setting) + QString(": ") +
                                         QString::number(full_range));
    suggested_settings_.at(4) = suggested_setting;

    QString temp_string = QString::number(std::ceil((max_settings.at(0) - min_settings.at(0))/2 + min_settings.at(0) - model->getDefaultRange()/2));
    ui->front_wing_setting_edit->setText(temp_string);
    ui->rear_wing_setting_edit->setText(temp_string);

    temp_string = QString::number(std::ceil((max_settings.at(1) - min_settings.at(1))/2 + min_settings.at(1) - model->getDefaultRange()/2));
    ui->engine_setting_edit->setText(temp_string);

    temp_string = QString::number(std::ceil((max_settings.at(2) - min_settings.at(2))/2 + min_settings.at(2) - model->getDefaultRange()/2));
    ui->brakes_setting_edit->setText(temp_string);

    temp_string = QString::number(std::ceil((max_settings.at(3) - min_settings.at(3))/2 + min_settings.at(3) - model->getDefaultRange()/2));
    ui->gear_setting_edit->setText(temp_string);

    temp_string = QString::number(std::ceil((max_settings.at(4) - min_settings.at(4))/2 + min_settings.at(4) - model->getDefaultRange()/2));
    ui->suspension_setting_edit->setText(temp_string);

    on_calculate_settings_button_clicked();
}

void PracticeCalculation::on_reset_practice_button_clicked()
{
    practiceTableModel* model = static_cast<practiceTableModel*>(ui->practice_table->model());
    model->reset();
    reset_edit_fields();

    updatePracticeCalculationUI();
}

void PracticeCalculation::reset_edit_fields()
{
    ui->front_wing_setting_edit->setText("500");
    ui->rear_wing_setting_edit->setText("500");
    ui->engine_setting_edit->setText("500");
    ui->brakes_setting_edit->setText("500");
    ui->gear_setting_edit->setText("500");
    ui->suspension_setting_edit->setText("500");
    ui->wing_setting_cb->setCurrentIndex(3);
    ui->engine_setting_cb->setCurrentIndex(3);
    ui->brakes_setting_cb->setCurrentIndex(3);
    ui->gear_setting_cb->setCurrentIndex(3);
    ui->suspension_setting_cb->setCurrentIndex(3);
}

void PracticeCalculation::updateWeatherUI(Weather weather)
{
    ui->q1_weather_status->setCurrentIndex(weather.getQ1Status());
    ui->q2_weather_status->setCurrentIndex(weather.getQ2Status());
    ui->q1_temperature->setText(QString::number(weather.getQ1Temperature()));
    ui->q1_humidity->setText(QString::number(weather.getQ1Humidity()));
    ui->q2_temperature->setText(QString::number(weather.getQ2Temperature()));
    ui->q2_humidity->setText(QString::number(weather.getQ2Humidity()));
    ui->race_q1_temperature_low->setText(QString::number(weather.getRaceQ1TemperatureLow()));
    ui->race_q1_temperature_high->setText(QString::number(weather.getRaceQ1TemperatureHigh()));
    ui->race_q2_temperature_low->setText(QString::number(weather.getRaceQ2TemperatureLow()));
    ui->race_q2_temperature_high->setText(QString::number(weather.getRaceQ2TemperatureHigh()));
    ui->race_q3_temperature_low->setText(QString::number(weather.getRaceQ3TemperatureLow()));
    ui->race_q3_temperature_high->setText(QString::number(weather.getRaceQ3TemperatureHigh()));
    ui->race_q4_temperature_low->setText(QString::number(weather.getRaceQ4TemperatureLow()));
    ui->race_q4_temperature_high->setText(QString::number(weather.getRaceQ4TemperatureHigh()));
    ui->race_q1_humidity_low->setText(QString::number(weather.getRaceQ1HumidityLow()));
    ui->race_q1_humidity_high->setText(QString::number(weather.getRaceQ1HumidityHigh()));
    ui->race_q2_humidity_low->setText(QString::number(weather.getRaceQ2HumidityLow()));
    ui->race_q2_humidity_high->setText(QString::number(weather.getRaceQ2HumidityHigh()));
    ui->race_q3_humidity_low->setText(QString::number(weather.getRaceQ3HumidityLow()));
    ui->race_q3_humidity_high->setText(QString::number(weather.getRaceQ3HumidityHigh()));
    ui->race_q4_humidity_low->setText(QString::number(weather.getRaceQ4HumidityLow()));
    ui->race_q4_humidity_high->setText(QString::number(weather.getRaceQ4HumidityHigh()));
    ui->race_q1_rain_probability_low->setText(QString::number(weather.getRaceQ1RainProbabilityLow()));
    ui->race_q1_rain_probability_high->setText(QString::number(weather.getRaceQ1RainProbabilityHigh()));
    ui->race_q2_rain_probability_low->setText(QString::number(weather.getRaceQ2RainProbabilityLow()));
    ui->race_q2_rain_probability_high->setText(QString::number(weather.getRaceQ2RainProbabilityHigh()));
    ui->race_q3_rain_probability_low->setText(QString::number(weather.getRaceQ3RainProbabilityLow()));
    ui->race_q3_rain_probability_high->setText(QString::number(weather.getRaceQ3RainProbabilityHigh()));
    ui->race_q4_rain_probability_low->setText(QString::number(weather.getRaceQ4RainProbabilityLow()));
    ui->race_q4_rain_probability_high->setText(QString::number(weather.getRaceQ4RainProbabilityHigh()));
}

double PracticeCalculation::EditFieldDouble(const QString &str)
{
    bool ok = false;

    QString temp_str = str;

    double double_val = temp_str.toDouble(&ok);

    if(!ok) {
        temp_str = str.left(str.size()-1);

        double ret = temp_str.toDouble(&ok);
        if(ok) return ret;
    }

    if(!ok) return 0;

    if(str.size() > 1 && str.at(0) == '0') return str.mid(1).toDouble();

    return double_val;
}

void PracticeCalculation::on_range_setting_edit_textChanged(const QString &arg1)
{
    practiceTableModel* model = static_cast<practiceTableModel*>(ui->practice_table->model());

    double value = EditFieldDouble(arg1);

    if(arg1.at(arg1.size()-1) != '.')
        ui->range_setting_edit->setText(QString::number(value));

    model->setDefaultRange(value);
}

void PracticeCalculation::on_fetch_weather_button_clicked()
{
    if(weather_ == 0) {
        gproServerInterface *gsi = gproServerInterface::createInstance();
        weather_ = shared_ptr<Weather>(new Weather(gsi->getWeather()));
    }

    updateWeatherUI(*weather_);

    GproResourceManager::getInstance().saveWeather(*weather_);
    GproResourceManager::getInstance().loadWeather();
}

void PracticeCalculation::on_calculate_settings_button_clicked()
{
    PracticeAlgorithms *pa = GproResourceManager::getInstance().getPracticeAlgorithms();

    std::array<int,6> weather_diff_to_rain = pa->getWeatherDiff();
    std::array<int,6> weather_diff_from_rain = pa->getWeatherDiff(false);

    double temperature = ui->q2_temperature->text().toDouble() - ui->q1_temperature->text().toDouble();
    double humidity = ui->q2_humidity->text().toDouble()-ui->q1_humidity->text().toDouble();

    if (GproResourceManager::getInstance().getSSF().isRunning())
        GproResourceManager::getInstance().getSSF().waitForFinished();

    arma::vec setting_diff = pa->getSettingsDiff(temperature, humidity );

    std::array<int,6> q2_settings = {{ (int)std::round(suggested_settings_.at(0)+setting_diff(0)),
                                     (int)std::round(suggested_settings_.at(0)+setting_diff(0)),
                                     (int)std::round(suggested_settings_.at(1)+setting_diff(1)),
                                     (int)std::round(suggested_settings_.at(2)+setting_diff(2)),
                                     (int)std::round(suggested_settings_.at(3)+setting_diff(3)),
                                     (int)std::round(suggested_settings_.at(4)+setting_diff(4)) }};

    if(ui->q1_weather_status->currentText() == QString("Dry") && ui->q2_weather_status->currentText() == QString("Wet")) {
        for(unsigned int i = 0; i < q2_settings.size(); ++i) q2_settings.at(i) += weather_diff_to_rain.at(i);
    } else if(ui->q1_weather_status->currentText() == QString("Wet") && ui->q2_weather_status->currentText() == QString("Dry")) {
        for(unsigned int i = 0; i < q2_settings.size(); ++i) q2_settings.at(i) += weather_diff_from_rain.at(i);
    }

    ui->q2_wing_setting_text->setText(QString::number(q2_settings.at(0)));
    ui->q2_engine_setting_text->setText(QString::number(q2_settings.at(2)));
    ui->q2_brakes_setting_text->setText(QString::number(q2_settings.at(3)));
    ui->q2_gear_setting_text->setText(QString::number(q2_settings.at(4)));
    ui->q2_suspension_setting_text->setText(QString::number(q2_settings.at(5)));

    double race_temperature_final = weather_->getEstRaceTemperature();
    double race_humidity_final = weather_->getEstRaceHumidity();

    race_temperature_final -= ui->q1_temperature->text().toDouble();
    race_humidity_final -= ui->q1_humidity->text().toDouble();

    if (GproResourceManager::getInstance().getSSF().isRunning())
        GproResourceManager::getInstance().getSSF().waitForFinished();

    setting_diff = pa->getSettingsDiff(race_temperature_final, race_humidity_final );

    std::array<int,6> race_settings_dry = {{ (int)std::round(suggested_settings_.at(0)+setting_diff(0)),
                                             (int)std::round(suggested_settings_.at(0)+setting_diff(0)),
                                             (int)std::round(suggested_settings_.at(1)+setting_diff(1)),
                                             (int)std::round(suggested_settings_.at(2)+setting_diff(2)),
                                             (int)std::round(suggested_settings_.at(3)+setting_diff(3)),
                                             (int)std::round(suggested_settings_.at(4)+setting_diff(4)) }};
    std::array<int,6> race_settings_wet = race_settings_dry;

    if(ui->q1_weather_status->currentText() == QString("Wet")) {
        for(unsigned int i = 0; i < race_settings_dry.size(); ++i) race_settings_dry.at(i) += weather_diff_from_rain.at(i);
    } else {
        for(unsigned int i = 0; i < race_settings_wet.size(); ++i) race_settings_wet.at(i) += weather_diff_to_rain.at(i);
    }

    ui->race_wing_setting_text->setText(QString::number(race_settings_dry.at(0)));
    ui->race_engine_setting_text->setText(QString::number(race_settings_dry.at(2)));
    ui->race_brakes_setting_text->setText(QString::number(race_settings_dry.at(3)));
    ui->race_gear_setting_text->setText(QString::number(race_settings_dry.at(4)));
    ui->race_suspension_setting_text->setText(QString::number(race_settings_dry.at(5)));

    ui->race_wing_setting_text_wet->setText(QString::number(race_settings_wet.at(0)));
    ui->race_engine_setting_text_wet->setText(QString::number(race_settings_wet.at(2)));
    ui->race_brakes_setting_text_wet->setText(QString::number(race_settings_wet.at(3)));
    ui->race_gear_setting_text_wet->setText(QString::number(race_settings_wet.at(4)));
    ui->race_suspension_setting_text_wet->setText(QString::number(race_settings_wet.at(5)));

    return;
}

void PracticeCalculation::on_suggested_settings_button_clicked()
{
    PracticeAlgorithms *pa = GproResourceManager::getInstance().getPracticeAlgorithms();

    if (GproResourceManager::getInstance().getSSF().isRunning())
        GproResourceManager::getInstance().getSSF().waitForFinished();

    Weather::Status wstatus = Weather::toStatus(ui->q1_weather_status->currentText());
    unsigned int q1_temp = ui->q1_temperature->text().toUInt();
    unsigned int q1_hum = ui->q1_humidity->text().toUInt();
    QString ttname;

    if(ui->race_test_combobox->currentIndex() == 1) {
        gproServerInterface *gsi = new gproServerInterface();

        QSettings settings(General::ProgramName, General::CompanyName);

        QString username = settings.value(Settings::SafeLoginUsernameText, "").toString();
        QString password = settings.value(Settings::SafeLoginPasswordText, "").toString();
        password = GproResourceManager::getInstance().getSimpleCrypt()->decryptToString(password);
        gsi->gproLogin(username,password);

        ttname = gsi->getTestTrackName();
    }

    int track_id = ui->race_test_combobox->currentIndex() == 0 ? GproResourceManager::getInstance().getTrack()->getID() : GproResourceManager::getInstance().getDatabaseHandler()->getTrackId(ttname);

    array<int,6> settings = pa->getSuggestedSettings(GproResourceManager::getInstance().getCar()->getFrontWingLvl(), GproResourceManager::getInstance().getCar()->getRearWingLvl(),
                                                     GproResourceManager::getInstance().getCar()->getEngineLvl(), GproResourceManager::getInstance().getCar()->getBrakesLvl(),
                                                     GproResourceManager::getInstance().getCar()->getGearboxLvl(), GproResourceManager::getInstance().getCar()->getSuspensionLvl(),
                                                     wstatus, q1_temp, q1_hum,GproResourceManager::getInstance().getDriver()->getExperience(), GproResourceManager::getInstance().getDriver()->getTechnicalInsight(), track_id,
                                                     GproResourceManager::getInstance().getDatabaseHandler()->getPracticeCommentValue(""));

    suggested_settings_ = settings;

    ui->front_wing_setting_edit->setText(QString::number(settings.at(0)));
    ui->rear_wing_setting_edit->setText(QString::number(settings.at(1)));
    ui->engine_setting_edit->setText(QString::number(settings.at(2)));
    ui->brakes_setting_edit->setText(QString::number(settings.at(3)));
    ui->gear_setting_edit->setText(QString::number(settings.at(4)));
    ui->suspension_setting_edit->setText(QString::number(settings.at(5)));
}

void PracticeCalculation::on_q1_weather_status_currentIndexChanged(const QString &arg1)
{
    if(!arg1.isEmpty())
        on_calculate_settings_button_clicked();
}

void PracticeCalculation::on_q2_weather_status_currentIndexChanged(const QString &arg1)
{
    if(!arg1.isEmpty())
        on_calculate_settings_button_clicked();
}

void PracticeCalculation::on_race_test_combobox_currentIndexChanged(int index)
{
    updateRaceTestCombobox(index);
}
