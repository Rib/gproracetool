#ifndef DRIVERINFO_H
#define DRIVERINFO_H

#include <memory>

#include <QWidget>

#include "Objects/driver.h"
#include "Handlers/databasehandler.h"
#include "Misc/gproresourcemanager.h"
#include "Algorithms/racealgorithms.h"
#include "Algorithms/stintalgorithms.h"

using std::shared_ptr;

namespace Ui {
class DriverInfo;
}

class DriverInfo : public QWidget
{
    Q_OBJECT

public:
    explicit DriverInfo(QWidget *parent = 0);
    ~DriverInfo();

    void updateDriverUI();

private slots:
    void on_make_calculations_button_clicked();

    void on_driver_fetch_button_clicked();

    void on_concentration_changed_edit_textChanged(const QString &arg1);

    void on_talent_changed_edit_textChanged(const QString &arg1);

    void on_aggressiveness_changed_edit_textChanged(const QString &arg1);

    void on_experience_changed_edit_textChanged(const QString &arg1);

    void on_technical_insight_changed_edit_textChanged(const QString &arg1);

    void on_stamina_changed_edit_textChanged(const QString &arg1);

    void on_charisma_changed_edit_textChanged(const QString &arg1);

    void on_motivation_changed_edit_textChanged(const QString &arg1);

    void on_weight_changed_edit_textChanged(const QString &arg1);

    void on_age_changed_edit_textChanged(const QString &arg1);

    void on_reputation_changed_edit_textChanged(const QString &arg1);

private:
    Ui::DriverInfo *ui;

    shared_ptr<Driver> current_driver_;
};

#endif // DRIVERINFO_H
