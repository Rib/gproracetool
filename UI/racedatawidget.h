#ifndef RACEDATAWIDGET_H
#define RACEDATAWIDGET_H

#include <QWidget>
#include <QList>
#include <QMap>
#include <QString>

#include "Misc/gproresourcemanager.h"
#include "Handlers/databasehandler.h"
#include "Components/customintlineedit.h"

namespace Ui {
class RaceDataWidget;
}

class RaceDataWidget : public QWidget
{
    Q_OBJECT

public:
    explicit RaceDataWidget(QWidget *parent = 0);
    ~RaceDataWidget();

private slots:
    void on_seasons_combo_box_currentIndexChanged(const QString &arg1);

    void on_races_combobox_currentIndexChanged(const QString &arg1);

    void on_temperature_min_search_textChanged(const QString &arg1);

    void on_temperature_max_search_textChanged(const QString &arg1);

    void on_humidity_min_search_textChanged(const QString &arg1);

    void on_humidity_max_search_textChanged(const QString &arg1);

    void on_weather_search_currentIndexChanged(int index);

private:
    Ui::RaceDataWidget *ui;

    QList< QStringList > race_data_;
    QMap<QString,int> race_data_labels_indexes_;
    QList<int> seasons_;

    inline int getIndex(const QString &label) const { return race_data_labels_indexes_.value(label,0); }

    void showRacesList();
    void showRace();

    QList<int> getSeasons();
    QStringList getRaces(const QString &season = "0");

    void setUIfromList(QList<QString> race);

    void doSearchOperations();
};

#endif // RACEDATAWIDGET_H
