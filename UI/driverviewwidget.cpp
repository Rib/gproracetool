#include "driverviewwidget.h"
#include "ui_driverviewwidget.h"

DriverViewWidget::DriverViewWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DriverViewWidget),
    driver_(new Driver)
{
    ui->setupUi(this);

    showSimple(true);
}

DriverViewWidget::~DriverViewWidget()
{
    delete ui;
}

void DriverViewWidget::updateDriverUI()
{
    ui->overall_edit->setText(QString::number(driver_->getOverall()));
    ui->concentration_edit->setText(QString::number(driver_->getConcentration()));
    ui->talent_edit->setText(QString::number(driver_->getTalent()));
    ui->aggressiveness_edit->setText(QString::number(driver_->getAggressiveness()));
    ui->experience_edit->setText(QString::number(driver_->getExperience()));
    ui->technical_insight_edit->setText(QString::number(driver_->getTechnicalInsight()));
    ui->stamina_edit->setText(QString::number(driver_->getStamina()));
    ui->charisma_edit->setText(QString::number(driver_->getCharisma()));
    ui->motivation_edit->setText(QString::number(driver_->getMotivation()));
    ui->reputation_edit->setText(QString::number(driver_->getReputation()));
    ui->weight_edit->setText(QString::number(driver_->getWeight()));
    ui->age_edit->setText(QString::number(driver_->getAge()));

    ui->simple_driver_name_text->setText(driver_->getName());
}

void DriverViewWidget::showSimple(bool simple)
{
    if(simple) {
        this->setVisible(false);

        ui->complex_layout->setVisible(false);

        ui->simple_layout->setVisible(true);

        this->setVisible(true);

    } else {
        this->setVisible(false);

        ui->complex_layout->setVisible(true);

        ui->simple_layout->setVisible(false);

        this->setVisible(true);
    }
}

void DriverViewWidget::on_fetch_current_driver_button_clicked()
{
    *driver_ = *(GproResourceManager::getInstance().getDriver().get());

    updateDriverUI();
}

void DriverViewWidget::on_overall_edit_textChanged(const QString &arg1)
{
    int value = driverFieldValue(arg1);
    driver_->setOverall(value);

    ui->overall_edit->setText(QString::number(value));
}

int DriverViewWidget::driverFieldValue(const QString &str)
{
    QString temp_value = str;
    bool valid;
    int value = temp_value.toInt(&valid);

    if(str.size() == 0) return 0;

    if(valid && value >= 0 && value <= 250) return value;

    value = temp_value.right(1).toInt(&valid);
    if(valid) return value;

    if (temp_value.size()-1 < 4) {
        value = temp_value.left(temp_value.size()-1).toInt(&valid);
        if(valid && value >= 0 && value <= 250) return value;
    }

    return 0;
}

void DriverViewWidget::on_concentration_edit_textChanged(const QString &arg1)
{
    int value = driverFieldValue(arg1);
    driver_->setConcentration(value);

    ui->concentration_edit->setText(QString::number(value));
}

void DriverViewWidget::on_talent_edit_textChanged(const QString &arg1)
{
    int value = driverFieldValue(arg1);
    driver_->setTalent(value);

    ui->talent_edit->setText(QString::number(value));
}

void DriverViewWidget::on_aggressiveness_edit_textChanged(const QString &arg1)
{
    int value = driverFieldValue(arg1);
    driver_->setAggressiveness(value);

    ui->aggressiveness_edit->setText(QString::number(value));
}

void DriverViewWidget::on_experience_edit_textChanged(const QString &arg1)
{
    int value = driverFieldValue(arg1);
    driver_->setExperience(value);

    ui->experience_edit->setText(QString::number(value));
}

void DriverViewWidget::on_technical_insight_edit_textChanged(const QString &arg1)
{
    int value = driverFieldValue(arg1);
    driver_->setTechnicalInsight(value);

    ui->technical_insight_edit->setText(QString::number(value));
}

void DriverViewWidget::on_stamina_edit_textChanged(const QString &arg1)
{
    int value = driverFieldValue(arg1);
    driver_->setStamina(value);

    ui->stamina_edit->setText(QString::number(value));
}

void DriverViewWidget::on_charisma_edit_textChanged(const QString &arg1)
{
    int value = driverFieldValue(arg1);
    driver_->setCharisma(value);

    ui->charisma_edit->setText(QString::number(value));
}

void DriverViewWidget::on_motivation_edit_textChanged(const QString &arg1)
{
    int value = driverFieldValue(arg1);
    driver_->setMotivation(value);

    ui->motivation_edit->setText(QString::number(value));
}

void DriverViewWidget::on_reputation_edit_textChanged(const QString &arg1)
{
    int value = driverFieldValue(arg1);
    driver_->setReputation(value);
}

void DriverViewWidget::on_weight_edit_textChanged(const QString &arg1)
{
    int value = driverFieldValue(arg1);
    driver_->setWeight(value);

    ui->weight_edit->setText(QString::number(value));
}

void DriverViewWidget::on_age_edit_textChanged(const QString &arg1)
{
    int value = driverFieldValue(arg1);
    driver_->setAge(value);

    ui->age_edit->setText(QString::number(value));
}
