#ifndef PRACTICECALCULATION_H
#define PRACTICECALCULATION_H

#include <memory>

#include <QWidget>
#include <QResizeEvent>
#include <QtConcurrent/QtConcurrentRun>
#include <QFuture>

#include <array>

#include "practicetablemodel.h"
#include "Handlers/databasehandler.h"
#include "Objects/weather.h"

using std::shared_ptr;

namespace Ui {
class PracticeCalculation;
}

class PracticeCalculation : public QWidget
{
    Q_OBJECT

public:
    explicit PracticeCalculation(QWidget *parent = 0);
    ~PracticeCalculation();

    void updatePracticeCalculationUI();

    void updateRaceTestCombobox(int value);

    void setDefaultSettings();

private slots:
    void on_add_practice_button_clicked();

    void on_reset_practice_button_clicked();

    void on_range_setting_edit_textChanged(const QString &arg1);

    void on_fetch_weather_button_clicked();

    void on_calculate_settings_button_clicked();

    void on_suggested_settings_button_clicked();

    void on_q1_weather_status_currentIndexChanged(const QString &arg1);

    void on_q2_weather_status_currentIndexChanged(const QString &arg1);

    void on_race_test_combobox_currentIndexChanged(int index);

private:
    Ui::PracticeCalculation *ui;

    practiceTableModel *practice_table_model_;

    std::array<int,6> suggested_settings_;

    shared_ptr<Weather> weather_;

    QFuture< shared_ptr<Weather> > test_weather_future_;
    shared_ptr<Weather> test_weather_;

    void reset_edit_fields();

    void updateWeatherUI(Weather weather);

    int EditFieldInt(const QString &str);
    double EditFieldDouble(const QString &str);

};

#endif // PRACTICECALCULATION_H
