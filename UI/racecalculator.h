#ifndef RACECALCULATOR_H
#define RACECALCULATOR_H

#include <QWidget>

#include <array>
#include <memory>

#include "Objects/driver.h"
#include "Objects/car.h"
#include "Objects/weather.h"
#include "Objects/track.h"
#include "Objects/finance.h"
#include "Handlers/databasehandler.h"
#include "Algorithms/stintalgorithms.h"

#include <mlpack/core.hpp>

using std::array;
using std::shared_ptr;

using namespace arma;

namespace Ui {
class RaceCalculator;
}

class RaceCalculator : public QWidget
{
    Q_OBJECT

public:
    explicit RaceCalculator(QWidget *parent = 0);
    ~RaceCalculator();

private slots:
    void on_update_driver_button_clicked();

    void on_update_car_button_clicked();

    void on_update_weather_button_clicked();

    void on_update_track_button_clicked();

    void on_clear_track_slider_valueChanged(int value);

    void on_fuel_calculate_button_clicked();

    void on_es_stops_edit_field_textChanged(const QString &arg1);

    void on_s_stops_edit_field_textChanged(const QString &arg1);

    void on_m_stops_edit_field_textChanged(const QString &arg1);

    void on_h_stops_edit_field_textChanged(const QString &arg1);

    void on_fuel_calc_laps_textChanged(const QString &arg1);

    void on_update_finance_button_clicked();

    void on_clear_track_slider_sliderReleased();

private:
    Ui::RaceCalculator *ui;

    shared_ptr<mat> stint_data_;
    shared_ptr<mat> race_data_;

    Driver driver_;
    Car car_;
    Weather weather_;
    Track track_;
    Finance finance_;

    int EditFieldInt(const QString &str);

    void updateDriverUI(Driver driver);

    void updateCarUI(Car car);
    // assumes that every object is set
    void updateEstCarWearUI(int clear_track = 0);
    void saveCarSettings(Car car);

    void updateWeatherUI(Weather weather);
    void saveWeatherSettings(Weather weather);

    void updateTimeUI(int clear_track = 0);

    void loadTrack(QString track_name);

    void updateStintUI(int change_val = -1);

    void updateFuelUI();

    void updateFinanceUI();

};

#endif // RACECALCULATOR_H
