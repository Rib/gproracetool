#ifndef DRIVERVIEWWIDGET_H
#define DRIVERVIEWWIDGET_H

#include <memory>
#include <QWidget>

#include "Misc/gproresourcemanager.h"
#include "Objects/driver.h"

using std::shared_ptr;

namespace Ui {
class DriverViewWidget;
}

class DriverViewWidget : public QWidget
{
    Q_OBJECT

public:
    explicit DriverViewWidget(QWidget *parent = 0);
    ~DriverViewWidget();

    void updateDriverUI();

    void showSimple(bool simple);

    void setDriver(shared_ptr<Driver> driver) { driver_ = driver; updateDriverUI(); }
    shared_ptr<Driver> getDriver() { return driver_; }

private slots:
    void on_fetch_current_driver_button_clicked();

    void on_overall_edit_textChanged(const QString &arg1);

    void on_concentration_edit_textChanged(const QString &arg1);

    void on_talent_edit_textChanged(const QString &arg1);

    void on_aggressiveness_edit_textChanged(const QString &arg1);

    void on_experience_edit_textChanged(const QString &arg1);

    void on_technical_insight_edit_textChanged(const QString &arg1);

    void on_stamina_edit_textChanged(const QString &arg1);

    void on_charisma_edit_textChanged(const QString &arg1);

    void on_motivation_edit_textChanged(const QString &arg1);

    void on_reputation_edit_textChanged(const QString &arg1);

    void on_weight_edit_textChanged(const QString &arg1);

    void on_age_edit_textChanged(const QString &arg1);

private:
    Ui::DriverViewWidget *ui;

    shared_ptr<Driver> driver_;

    int driverFieldValue(const QString &str);
};

#endif // DRIVERVIEWWIDGET_H
