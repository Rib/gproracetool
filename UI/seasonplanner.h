#ifndef SEASONPLANNER_H
#define SEASONPLANNER_H

#include <QWidget>
#include <QString>
#include <QStringList>
#include <QList>
#include <QLayout>
#include <QItemDelegate>

#include "settingconstants.h"
#include "gproserverinterface.h"
#include "Objects/track.h"
#include "Models/seasonlisttablemodel.h"
#include "UI/trackviewwidget.h"
#include "UI/driverviewwidget.h"
#include "UI/carviewwidget.h"

namespace Ui {
class SeasonPlanner;
}

class SeasonPlanner : public QWidget
{
    Q_OBJECT

public:
    explicit SeasonPlanner(QWidget *parent = 0);
    ~SeasonPlanner();



private slots:
    void on_fetch_current_season_button_clicked();

    void on_seasons_table_pressed(const QModelIndex &index);

    void on_update_total_cost_button_clicked();

    void updated_total_cost(const QString &text);

    void recreatePreviousData(bool debug = false);

    void on_season_budget_edit_textChanged(const QString &arg1);

    void on_update_test_track_button_clicked();

private:
    Ui::SeasonPlanner *ui;

    shared_ptr< QList<Track> > season_tracks_;
    int test_track_id_;

    int intEdit(const QString &str);

    // QWidget interface
protected:
    void resizeEvent(QResizeEvent *event) override;
};

#endif // SEASONPLANNER_H
