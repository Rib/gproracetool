#include "racedownloader.h"
#include "ui_racedownloader.h"

#include <QSettings>

#include "settingconstants.h"
#include "Database/databasetablecreatehelper.h"
#include "Database/databasequeryhelper.h"

RaceDownloader::RaceDownloader(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RaceDownloader)
{
    ui->setupUi(this);

    QSettings settings(General::ProgramName, General::CompanyName);

    QStringList old_races_headers = settings.value(Settings::RDOldRacesData).toStringList();

    ui->races_headers_combobox->insertItem(0, QString("All"));
    ui->races_headers_combobox->insertItems(1,old_races_headers);
}

RaceDownloader::~RaceDownloader()
{
    delete ui;
}

void RaceDownloader::loadSettings()
{

}

void RaceDownloader::on_fetch_races_headers_button_clicked()
{
    gproServerInterface *gsi = gproServerInterface::createInstance();

    QStringList old_races_headers = gsi->getOldRacesHeaders();

    ui->races_headers_combobox->insertItem(0, QString("All"));
    ui->races_headers_combobox->insertItems(1,old_races_headers);

    QSettings settings(General::ProgramName, General::CompanyName);

    settings.setValue(Settings::RDOldRacesData, old_races_headers);
}

void RaceDownloader::on_races_headers_combobox_activated(const QString &arg1)
{
    if(arg1 == QString("All")) return;

    gproServerInterface *gsi = gproServerInterface::createInstance(true);

    current_race_ = gsi->getRace(arg1);

    ui->track_name_text_label->setText(current_race_.getTrack().getName());

    databasehandler *db_handler = GproResourceManager::getInstance().getDatabaseHandler();
    QList<PracticeLap> plaps = db_handler->getPracticeLaps(current_race_.getTrack().getID(), current_race_.getSeason());

    ui->practice_laps_synced_text->setText((plaps.size() > 0 && current_race_.getPracticeLaps().size() == plaps.size() ?
                ConstantStrings::SynchronizedText : ConstantStrings::NotSynchronizedText));

    ui->race_synced_text->setText((db_handler->hasRace(current_race_.getTrack().getID(), current_race_.getSeason()) ? "Synced" : "Not Synced"));

    ui->weather_synced_text->setText(db_handler->hasWeather(current_race_.getTrack().getID(), current_race_.getSeason()) ? "Synced" : "Not Synced");

    ui->car_synced_text->setText(db_handler->hasCar(current_race_.getTrack().getID(), current_race_.getSeason()) ? "Synced" : "Not Synced");

    ui->driver_synced_text->setText(db_handler->hasDriver(current_race_.getTrack().getID(), current_race_.getSeason()) ? "Synced" : "Not Synced");

    ui->risk_synced_text->setText(db_handler->hasRisk(current_race_.getTrack().getID(), current_race_.getSeason()) ? "Synced" : "Not Synced");


    QList<Lap> laps = db_handler->getLaps(current_race_.getTrack().getID(), current_race_.getSeason(), true);
    ui->laps_synced_text->setText(laps.size() > (current_race_.getTrack().getDistance() / current_race_.getTrack().getLapLength()) &&
                                  laps.size() == current_race_.getLaps().size() ?
                                      ConstantStrings::SynchronizedText : ConstantStrings::NotSynchronizedText);

    QList<Stint> stints = current_race_.getStints();

    QList<Stint> stints_2 = db_handler->getStints(current_race_.getTrack().getID(), current_race_.getSeason(), true);

    ui->stint_synced_text->setText(stints_2.size() > 0 && stints_2.size() == stints.size() ?
                                       ConstantStrings::SynchronizedText : ConstantStrings::NotSynchronizedText);

    ui->misc_data_synced_text->setText(db_handler->getMiscData(current_race_.getTrack().getID(), current_race_.getSeason(), true).size() > 0 ? "Synced" : "Not Synced");
}

void RaceDownloader::on_practice_sync_button_clicked()
{
    int track_id = current_race_.getTrack().getID();

    int season = current_race_.getSeason();

    QList<PracticeLap> plaps = current_race_.getPracticeLaps();

    connect(&practice_future_watcher_, SIGNAL(finished()), this, SLOT(practice_download_finished()));

    auto practice_lambda = bind([](QList<PracticeLap> plaps, int season, int track_id, QMutex &mutex){
            QMutexLocker locker(&mutex);

            //QWaitCondition cond;
            //cond.wait(locker.mutex());
            databasehandler *db_handler = GproResourceManager::getInstance().getDatabaseHandler();
            QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());

            for(PracticeLap &plap : plaps) {
                plap.setTrackID(track_id);
                plap.setSeason(season);
            }

            QList<QStringList> *plap_rows = DatabaseQueryHelper::querySimpleData(db, DatabaseTableCreateHelper::TABLE_PRACTICE_LAP,
                                                                                 track_id, season, QStringLiteral("TablePracticeQuery"), false);

            if(plap_rows->size() == 0 && plaps.size() > 0) {
                DatabaseTableCreateHelper::insertPracticeRows(db,plaps,false);
            } else {
                int counter = 1;
                foreach(PracticeLap plap, plaps) {
                    DatabaseTableCreateHelper::insertPracticeRow(db,plap,counter);
                    counter++;
                }
            }

            //forcing the update here
            db_handler->getPracticeLaps(0,0,true);

            QList<QStringList> *plap_rows2 = DatabaseQueryHelper::querySimpleData(db, DatabaseTableCreateHelper::TABLE_PRACTICE_LAP,
                                                                                 track_id, season, QStringLiteral("TablePracticeQuery"), false);

            return plap_rows2->size() > 0;

    }, plaps, season, track_id, std::ref(database_mutex_));

    if(race_future_.isRunning())
        race_future_.waitForFinished();

    ui->practice_laps_synced_text->setText(ConstantStrings::DownloadingText);

    practice_future_ = QtConcurrent::run(practice_lambda);

    practice_future_watcher_.setFuture(practice_future_);

}

void RaceDownloader::on_race_sync_button_clicked()
{
    int track_id = current_race_.getTrack().getID();

    int season = current_race_.getSeason();

    connect(&race_future_watcher_, SIGNAL(finished()), this, SLOT(race_download_finished()));

    auto race_lambda = bind([](int season, int track_id, QMutex &mutex){
        QMutexLocker locker(&mutex);

        //QWaitCondition cond;
        //cond.wait(locker.mutex());

        databasehandler *db_handler = GproResourceManager::getInstance().getDatabaseHandler();

        QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());

        if (DatabaseTableCreateHelper::insertRaceRow(db, season, track_id, false)) {
            db_handler->getRaces(true);
        }

        return db_handler->hasRace(track_id, season);

    }, season, track_id,  std::ref(database_mutex_));

    ui->race_synced_text->setText(ConstantStrings::DownloadingText);

    race_future_ = QtConcurrent::run(race_lambda);

    race_future_watcher_.setFuture(race_future_);
}

void RaceDownloader::on_weather_sync_button_clicked()
{
    int track_id = current_race_.getTrack().getID();

    int season  = current_race_.getSeason();

    Weather weather = current_race_.getWeather();

    weather.setTrackID(track_id);
    weather.setSeason(season);

    connect(&weather_future_watcher_, SIGNAL(finished()), this, SLOT(weather_download_finished()));

    auto weather_lambda = bind([](Weather weather, QMutex &mutex){
            QMutexLocker locker(&mutex);

            //QWaitCondition cond;
            //cond.wait(locker.mutex());

            databasehandler *db_handler = GproResourceManager::getInstance().getDatabaseHandler();

            QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());

            if(DatabaseTableCreateHelper::insertWeatherRow(db, weather, false) ) {
                db_handler->getWeatherData(true);
            }

            return db_handler->hasWeather(weather.getTrackID(), weather.getSeason());
    }, weather,  std::ref(database_mutex_));

    if(race_future_.isRunning())
        race_future_.waitForFinished();

    ui->weather_synced_text->setText(ConstantStrings::DownloadingText);

    weather_future_ = QtConcurrent::run(weather_lambda);

    weather_future_watcher_.setFuture(weather_future_);
}

void RaceDownloader::on_car_sync_button_clicked()
{
    int track_id = current_race_.getTrack().getID();

    int season  = current_race_.getSeason();

    Car car = current_race_.getCar();
    car.setSeason(season);
    car.setTrackID(track_id);

    connect(&car_future_watcher_, SIGNAL(finished()), this, SLOT(car_download_finished()));

    auto car_lambda = bind([](Car car, QMutex &mutex){
            QMutexLocker locker(&mutex);

            //QWaitCondition cond;
            //cond.wait(locker.mutex());

            databasehandler *db_handler = GproResourceManager::getInstance().getDatabaseHandler();

            QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());

            bool car_insert = false;
            bool car_wear_insert = false;

            if(DatabaseTableCreateHelper::insertCarRow(db, car, false)) {
                car_insert = true;
            }

            if(DatabaseTableCreateHelper::insertCarWearRow(db, car, false)) {
                car_wear_insert = true;
            }

            if(car_insert && car_wear_insert) {
                db_handler->getCars(true);
            }

            return db_handler->hasCar(car.getTrackID(), car.getSeason());
    }, car,  std::ref(database_mutex_));

    if(race_future_.isRunning())
        race_future_.waitForFinished();

    ui->car_synced_text->setText(ConstantStrings::DownloadingText);

    car_future_ = QtConcurrent::run(car_lambda);

    car_future_watcher_.setFuture(car_future_);

}

void RaceDownloader::on_driver_sync_button_clicked()
{
    int track_id = current_race_.getTrack().getID();

    int season  = current_race_.getSeason();

    Driver driver = current_race_.getDriver();
    driver.setTrackID(track_id);
    driver.setSeason(season);

    connect(&driver_future_watcher_, SIGNAL(finished()), this, SLOT(driver_download_finished()));

    auto driver_lambda = bind([](Driver driver, QMutex &mutex){

            QMutexLocker locker(&mutex);

            //QWaitCondition cond;
            //cond.wait(locker.mutex());

            databasehandler *db_handler = GproResourceManager::getInstance().getDatabaseHandler();

            QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());

            if(DatabaseTableCreateHelper::insertDriverRow(db, driver, false)) {
                db_handler->getDrivers(true);
            }

            return db_handler->hasDriver(driver.getTrackID(), driver.getSeason());
    }, driver,  std::ref(database_mutex_));

    if(race_future_.isRunning())
        race_future_.waitForFinished();

    ui->driver_synced_text->setText(ConstantStrings::DownloadingText);

    driver_future_ = QtConcurrent::run(driver_lambda);

    driver_future_watcher_.setFuture(driver_future_);

}

void RaceDownloader::on_risk_sync_button_clicked()
{
    int track_id = current_race_.getTrack().getID();

    int season  = current_race_.getSeason();

    Risk risk = current_race_.getRisk();
    risk.setTrackID(track_id);
    risk.setSeason(season);

    connect(&risk_future_watcher_, SIGNAL(finished()), this, SLOT(risk_download_finished()));

    auto risk_lambda = bind([](Risk risk, QMutex &mutex){
            QMutexLocker locker(&mutex);

            //QWaitCondition cond;
            //cond.wait(locker.mutex());

            databasehandler *db_handler = GproResourceManager::getInstance().getDatabaseHandler();

            QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());

            if(DatabaseTableCreateHelper::insertRiskRow(db, risk, false)) {
                db_handler->getRisks(true);

            }

            return db_handler->hasRisk(risk.getTrackID(), risk.getSeason());
    }, risk,  std::ref(database_mutex_));

    if(race_future_.isRunning())
        race_future_.waitForFinished();

    ui->risk_synced_text->setText(ConstantStrings::DownloadingText);

    risk_future_ = QtConcurrent::run(risk_lambda);

    risk_future_watcher_.setFuture(risk_future_);
}

void RaceDownloader::on_laps_sync_button_clicked()
{
    int track_id = current_race_.getTrack().getID();

    int season  = current_race_.getSeason();

    QList<Lap> laps = current_race_.getLaps();

    Track track = current_race_.getTrack();

    for(Lap &lap : laps) {
        lap.setSeason(season);
        lap.setTrackID(track_id);
    }

    connect(&laps_future_watcher_, SIGNAL(finished()), this, SLOT(laps_download_finished()));

    auto laps_lambda = bind([](QList<Lap> laps, Track track, int season, int track_id, QMutex &mutex){
            QMutexLocker locker(&mutex);

            //QWaitCondition cond;
            //cond.wait(locker.mutex());

            QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());

            QList<QStringList> *lap_rows = DatabaseQueryHelper::querySimpleData(db, DatabaseTableCreateHelper::TABLE_LAP,
                                                                                track_id, season, QStringLiteral("TableLapQuery"), false);

            if(lap_rows->size() == 0 && laps.size() > 0) {
                DatabaseTableCreateHelper::insertLapRows(db,laps,false);
            } else {
                foreach(Lap lap, laps) {
                    DatabaseTableCreateHelper::insertLapRow(db,lap);
                }
            }

            databasehandler *db_handler = GproResourceManager::getInstance().getDatabaseHandler();
            db_handler->getLaps(0,0,true);

            QList<QStringList> *lap_rows2 = DatabaseQueryHelper::querySimpleData(db, DatabaseTableCreateHelper::TABLE_LAP,
                                                                                track_id, season, QStringLiteral("TableLapQuery"), false);


            return lap_rows2->size() > track.getLaps();
    },  laps, track, season, track_id, std::ref(database_mutex_));

    if(race_future_.isRunning())
        race_future_.waitForFinished();

    ui->laps_synced_text->setText(ConstantStrings::DownloadingText);

    laps_future_ = QtConcurrent::run(laps_lambda);

    laps_future_watcher_.setFuture(laps_future_);

}

void RaceDownloader::on_stint_sync_button_clicked()
{
    int track_id = current_race_.getTrack().getID();

    int season  = current_race_.getSeason();

     QList<Stint> stints = current_race_.getStints();

     for(Stint &stint : stints) {
         stint.setTrackID(track_id);
         stint.setSeason(season);
     }

     connect(&stints_future_watcher_, SIGNAL(finished()), this, SLOT(stints_download_finished()));

     auto stints_lambda = bind([stints, season, track_id](QList<Stint> stints, int season, int track_id, QMutex &mutex){
         QMutexLocker locker(&mutex);

         //QWaitCondition cond;
         //cond.wait(locker.mutex());

         QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());

         foreach(Stint stint, stints) {
             DatabaseTableCreateHelper::insertStintRow(db,stint, false);
         }

         databasehandler *db_handler = GproResourceManager::getInstance().getDatabaseHandler();
         db_handler->getStints(0,0,true);

         QList<QStringList> *stint_rows = DatabaseQueryHelper::querySimpleData(db, DatabaseTableCreateHelper::TABLE_STINT_DATA,
                                                                             track_id, season, QStringLiteral("SimpleStintRows"), false);

         return stint_rows->size() > 0;
     },  stints, season, track_id, std::ref(database_mutex_));

     if(race_future_.isRunning())
         race_future_.waitForFinished();

     ui->stint_synced_text->setText(ConstantStrings::DownloadingText);

     stints_future_ = QtConcurrent::run(stints_lambda);

     stints_future_watcher_.setFuture(stints_future_);

}

void RaceDownloader::on_misc_data_sync_button_clicked()
{
    int track_id = current_race_.getTrack().getID();

    int season  = current_race_.getSeason();

    array<double, 2> misc_data = {{ current_race_.getQ1LapTime(), current_race_.getQ2LapTime() }};

    connect(&misc_future_watcher_, SIGNAL(finished()), this, SLOT(misc_download_finished()));

    auto misc_lambda = bind([](array<double,2> misc_data, int season, int track_id, QMutex &mutex){

            QMutexLocker locker(&mutex);

            //QWaitCondition cond;
            //cond.wait(locker.mutex());

            QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());

            DatabaseTableCreateHelper::insertMiscDataRow(db, misc_data.at(0), misc_data.at(1), season, track_id, false);

            databasehandler *db_handler = GproResourceManager::getInstance().getDatabaseHandler();
            db_handler->getMiscData(0,0,true);

            QList<QStringList> *misc_data_rows = DatabaseQueryHelper::querySimpleData(db, DatabaseTableCreateHelper::TABLE_MISC_DATA,
                                                                                      track_id, season, QStringLiteral("SimpleStintRows"), false);

            return misc_data_rows->size() > 0;
    },  misc_data, season, track_id, std::ref(database_mutex_));

    if(race_future_.isRunning())
        race_future_.waitForFinished();

    ui->misc_data_synced_text->setText(ConstantStrings::DownloadingText);

    misc_future_ = QtConcurrent::run(misc_lambda);

    misc_future_watcher_.setFuture(misc_future_);
}

void RaceDownloader::on_update_tracks_button_clicked()
{
    gproServerInterface *gsi = gproServerInterface::createInstance();

    QList<Track> tracks = gsi->getAllTracks();

    QSqlDatabase db = QSqlDatabase::database(QSqlDatabase::connectionNames().back());

    foreach(const Track &track, tracks) {
        DatabaseTableCreateHelper::insertTrackRow(db, track, false);
    }
}

void RaceDownloader::on_sync_whole_race_button_clicked()
{
    on_race_sync_button_clicked();
    on_weather_sync_button_clicked();
    on_practice_sync_button_clicked();
    on_car_sync_button_clicked();
    on_driver_sync_button_clicked();
    on_risk_sync_button_clicked();
    on_laps_sync_button_clicked();
    on_stint_sync_button_clicked();
    on_misc_data_sync_button_clicked();

}

void RaceDownloader::race_download_finished()
{
    ui->race_synced_text->setText(( race_future_.result() ?
                                        ConstantStrings::SynchronizedText :
                                        ConstantStrings::NotSynchronizedText));

}

void RaceDownloader::weather_download_finished()
{
    ui->weather_synced_text->setText( weather_future_.result() ?
                                          ConstantStrings::SynchronizedText :
                                          ConstantStrings::NotSynchronizedText);

}

void RaceDownloader::practice_download_finished()
{
    ui->practice_laps_synced_text->setText(practice_future_.result() ?
                                               ConstantStrings::SynchronizedText :
                                               ConstantStrings::NotSynchronizedText);

}

void RaceDownloader::car_download_finished()
{
    ui->car_synced_text->setText(car_future_.result() ?
                                     ConstantStrings::SynchronizedText :
                                     ConstantStrings::NotSynchronizedText);

}

void RaceDownloader::driver_download_finished()
{
    ui->driver_synced_text->setText( driver_future_.result() ?
                                         ConstantStrings::SynchronizedText :
                                         ConstantStrings::NotSynchronizedText);

}

void RaceDownloader::risk_download_finished()
{
    ui->risk_synced_text->setText(risk_future_.result() ?
                                      ConstantStrings::SynchronizedText :
                                      ConstantStrings::NotSynchronizedText);
}

void RaceDownloader::laps_download_finished()
{
    ui->laps_synced_text->setText( laps_future_.result() ?
                                       ConstantStrings::SynchronizedText :
                                       ConstantStrings::NotSynchronizedText);
}

void RaceDownloader::stints_download_finished()
{
    if(stints_future_.result()) {
        int track_id = current_race_.getTrack().getID();
        int season = current_race_.getSeason();
        databasehandler *db_handler = GproResourceManager::getInstance().getDatabaseHandler();

        QList<Stint> stints = db_handler->getStints(track_id, season, true);
        current_race_.clearStints();
        foreach (Stint stint, stints) {
            current_race_.addStint(stint);
        }

        ui->stint_synced_text->setText(ConstantStrings::SynchronizedText);
    } else ui->stint_synced_text->setText(ConstantStrings::NotSynchronizedText);
}

void RaceDownloader::misc_download_finished()
{
    ui->misc_data_synced_text->setText( misc_future_.result() ?
                                            ConstantStrings::SynchronizedText :
                                            ConstantStrings::NotSynchronizedText);
}
