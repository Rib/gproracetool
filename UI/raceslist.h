#ifndef RACESLIST_H
#define RACESLIST_H

#include <QWidget>
#include <QString>
#include <QList>
#include <QMap>

namespace Ui {
class RacesList;
}

class RacesList : public QWidget
{
    Q_OBJECT

public:
    explicit RacesList(QWidget *parent = 0);
    ~RacesList();

    void updateUI();

    void setRaceData(const QList< QList<QString> > &races_data, const QMap<QString,int> &label_indexes) {
        races_data_ = races_data;
        race_label_indexes_ = label_indexes;
        updateUI();
    }

signals:
    void setSeasonText(const QString &season);
    void setRaceText(const QString &race);

public slots:
    void racesTableClicked(const QModelIndex &index);

private slots:

private:
    Ui::RacesList *ui;

    QList< QList<QString> > races_data_;
    QMap<QString,int> race_label_indexes_;

    int getIndex(const QString &label) { return race_label_indexes_.value(label, 0); }

};

#endif // RACESLIST_H
