#ifndef TRACKVIEWWIDGET_H
#define TRACKVIEWWIDGET_H

#include <memory>

#include <QWidget>
#include <QString>

#include "Objects/track.h"

using std::shared_ptr;

namespace Ui {
class TrackViewWidget;
}

class TrackViewWidget : public QWidget
{
    Q_OBJECT

public:
    explicit TrackViewWidget(QWidget *parent = 0);
    ~TrackViewWidget();

    void setTrack(const Track &track);

    void showSimple(bool simple);

private:
    Ui::TrackViewWidget *ui;

    shared_ptr<Track> track_;
};

#endif // TRACKVIEWWIDGET_H
