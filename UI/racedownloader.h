#ifndef RACEDOWNLOADER_H
#define RACEDOWNLOADER_H

#include <functional>

#include <QDialog>

#include <QtConcurrent/QtConcurrentRun>
#include <QFuture>
#include <QFutureWatcher>
#include <QMutex>
#include <QMutexLocker>
#include <QWaitCondition>

#include "gproserverinterface.h"
#include "Objects/race.h"
#include "Handlers/databasehandler.h"

namespace Ui {
class RaceDownloader;
}

class RaceDownloader : public QDialog
{
    Q_OBJECT

public:
    explicit RaceDownloader(QWidget *parent = 0);
    ~RaceDownloader();

    void loadSettings();



private slots:
    void on_fetch_races_headers_button_clicked();

    void on_races_headers_combobox_activated(const QString &arg1);

    void on_practice_sync_button_clicked();

    void on_race_sync_button_clicked();

    void on_weather_sync_button_clicked();

    void on_car_sync_button_clicked();

    void on_driver_sync_button_clicked();

    void on_risk_sync_button_clicked();

    void on_laps_sync_button_clicked();

    void on_stint_sync_button_clicked();

    void on_misc_data_sync_button_clicked();

    void on_update_tracks_button_clicked();

    void on_sync_whole_race_button_clicked();

    void race_download_finished();

    void weather_download_finished();

    void practice_download_finished();

    void car_download_finished();

    void driver_download_finished();

    void risk_download_finished();

    void laps_download_finished();

    void stints_download_finished();

    void misc_download_finished();

private:
    Ui::RaceDownloader *ui;
    Race current_race_;

    QFuture<bool> race_future_;
    QFutureWatcher<bool> race_future_watcher_;

    QFuture<bool> weather_future_;
    QFutureWatcher<bool> weather_future_watcher_;

    QFuture<bool> practice_future_;
    QFutureWatcher<bool> practice_future_watcher_;

    QFuture<bool> car_future_;
    QFutureWatcher<bool> car_future_watcher_;

    QFuture<bool> driver_future_;
    QFutureWatcher<bool> driver_future_watcher_;

    QFuture<bool> risk_future_;
    QFutureWatcher<bool> risk_future_watcher_;

    QFuture<bool> laps_future_;
    QFutureWatcher<bool> laps_future_watcher_;

    QFuture<bool> stints_future_;
    QFutureWatcher<bool> stints_future_watcher_;

    QFuture<bool> misc_future_;
    QFutureWatcher<bool> misc_future_watcher_;

    QMutex database_mutex_;

};

#endif // RACEDOWNLOADER_H
