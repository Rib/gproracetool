#include "raceslist.h"
#include "ui_raceslist.h"

#include <QDebug>

RacesList::RacesList(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RacesList)
{
    ui->setupUi(this);
}

RacesList::~RacesList()
{
    delete ui;
}

void RacesList::updateUI()
{
    while(ui->races_table->rowCount() > 0) ui->races_table->removeRow(0);

    if(races_data_.size() > 0) {

        while(ui->races_table->rowCount() > 0) ui->races_table->removeRow(0);

        for(int i = 0; i < races_data_.size(); ++i) {
            ui->races_table->insertRow(i);

            QTableWidgetItem *twi_season  = new QTableWidgetItem();
            twi_season->setText(races_data_.at(i).at(getIndex(QStringLiteral("Season"))));
            ui->races_table->setItem(i,0,twi_season);

            QTableWidgetItem *twi_track  = new QTableWidgetItem();
            twi_track->setText(races_data_.at(i).at(getIndex(QStringLiteral("TrackName"))));
            ui->races_table->setItem(i,1,twi_track);

            QTableWidgetItem *twi_temperature  = new QTableWidgetItem();
            double temperature = std::floor(races_data_.at(i).at(getIndex(QStringLiteral("STemperature"))).toDouble() * 10) / 10;
            twi_temperature->setText(QString::number(temperature));
            ui->races_table->setItem(i,2,twi_temperature);

            QTableWidgetItem *twi_humidity  = new QTableWidgetItem();
            double humidity = std::floor(races_data_.at(i).at(getIndex(QStringLiteral("SHumidity"))).toDouble() * 10) / 10;
            twi_humidity->setText(QString::number(humidity));
            ui->races_table->setItem(i,3,twi_humidity);

            QTableWidgetItem *twi_weather  = new QTableWidgetItem();
            twi_weather->setText(races_data_.at(i).at(getIndex(QStringLiteral("SWeather"))));
            ui->races_table->setItem(i,4,twi_weather);
        }
    }
}

void RacesList::racesTableClicked(const QModelIndex &index)
{
    QString season_text = ui->races_table->item(index.row(), 0)->text();
    QString race_text = ui->races_table->item(index.row(), 1)->text();

    setSeasonText(season_text);
    setRaceText(race_text);
}
