#include "driverinfo.h"
#include "ui_driverinfo.h"

#include <QSettings>
#include <QDebug>

#include "gproserverinterface.h"
#include "settingconstants.h"

DriverInfo::DriverInfo(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DriverInfo),
    current_driver_(new Driver)
{
    ui->setupUi(this);

    *current_driver_ = *GproResourceManager::getInstance().getDriver();

    updateDriverUI();

    ui->concentration_changed_edit->setLimits(0,250);
    ui->talent_changed_edit->setLimits(0,250);
    ui->aggressiveness_changed_edit->setLimits(0,250);
    ui->experience_changed_edit->setLimits(0,250);
    ui->technical_insight_changed_edit->setLimits(0,250);
    ui->stamina_changed_edit->setLimits(0,250);
    ui->charisma_changed_edit->setLimits(0,250);
    ui->motivation_changed_edit->setLimits(0,250);
    ui->weight_changed_edit->setLimits(0,250);
    ui->age_changed_edit->setLimits(0,250);
    ui->reputation_changed_edit->setLimits(0,250);
}

DriverInfo::~DriverInfo()
{
    delete ui;
}

void DriverInfo::updateDriverUI()
{
    ui->driver_name_text->setText(current_driver_->getName());

    ui->overall_edit->setText(QString::number(current_driver_->getOverall()));
    ui->concentration_edit->setText(QString::number(current_driver_->getConcentration()));
    ui->talent_edit->setText(QString::number(current_driver_->getTalent()));
    ui->aggressiveness_edit->setText(QString::number(current_driver_->getAggressiveness()));
    ui->experience_edit->setText(QString::number(current_driver_->getExperience()));
    ui->technical_insight_edit->setText(QString::number(current_driver_->getTechnicalInsight()));
    ui->stamina_edit->setText(QString::number(current_driver_->getStamina()));
    ui->charisma_edit->setText(QString::number(current_driver_->getCharisma()));
    ui->motivation_edit->setText(QString::number(current_driver_->getMotivation()));
    ui->weight_edit->setText(QString::number(current_driver_->getWeight()));
    ui->age_edit->setText(QString::number(current_driver_->getAge()));
    ui->reputation_edit->setText(QString::number(current_driver_->getReputation()));
}

void DriverInfo::on_make_calculations_button_clicked()
{
    RaceAlgorithms *ralg = GproResourceManager::getInstance().getRaceAlgorithms();


    double overall_min = ralg->getEstimatedOverall(ui->concentration_changed_edit->text().toInt(), ui->talent_changed_edit->text().toInt(),
                              ui->aggressiveness_changed_edit->text().toInt(), ui->experience_changed_edit->text().toInt(),
                              ui->technical_insight_changed_edit->text().toInt(), ui->stamina_changed_edit->text().toInt(),
                              ui->charisma_changed_edit->text().toInt(), ui->motivation_changed_edit->text().toInt(),
                              ui->weight_changed_edit->text().toInt(), ui->age_changed_edit->text().toInt(),
                                               ui->reputation_changed_edit->text().toInt());

    double overall_max = ralg->getEstimatedOverall(ui->concentration_changed_edit->text().toInt() + 1, ui->talent_changed_edit->text().toInt() + 1,
                                                   ui->aggressiveness_changed_edit->text().toInt() + 1, ui->experience_changed_edit->text().toInt() + 1,
                                                   ui->technical_insight_changed_edit->text().toInt() + 1, ui->stamina_changed_edit->text().toInt() + 1,
                                                   ui->charisma_changed_edit->text().toInt() + 1, ui->motivation_changed_edit->text().toInt() + 1,
                                                   ui->weight_changed_edit->text().toInt(), ui->age_changed_edit->text().toInt(),
                                                                    ui->reputation_changed_edit->text().toInt());

    ui->overall_changed_edit->setText(QString::number(std::round(overall_min)) + " - " + QString::number(std::round(overall_max)) );

    StintAlgorithms *salg = GproResourceManager::getInstance().getStintAlgorithms();

    double overall_tdiff = salg->getDriverTime(ui->stamina_edit->text().toInt() - ui->stamina_changed_edit->text().toInt(),
                                               ui->concentration_edit->text().toInt() - ui->concentration_changed_edit->text().toInt(),
                                               ui->talent_edit->text().toInt() - ui->talent_changed_edit->text().toInt(),
                                               ui->aggressiveness_edit->text().toInt() - ui->aggressiveness_changed_edit->text().toInt(),
                                               ui->experience_edit->text().toInt() - ui->experience_changed_edit->text().toInt(),
                                               ui->motivation_edit->text().toInt() - ui->motivation_changed_edit->text().toInt(),
                                               ui->weight_edit->text().toInt() - ui->weight_changed_edit->text().toInt());
    double concentration_tdiff = salg->getDriverTime(0, ui->concentration_edit->text().toInt() - ui->concentration_changed_edit->text().toInt(),0,0,0,0,0);
    double talent_tdiff = salg->getDriverTime(0,0,ui->talent_edit->text().toInt() - ui->talent_changed_edit->text().toInt(),0,0,0,0);
    double aggressiveness_tdiff = salg->getDriverTime(0,0,0,ui->aggressiveness_edit->text().toInt() - ui->aggressiveness_changed_edit->text().toInt(),0,0,0);
    double experience_tdiff = salg->getDriverTime(0,0,0,0,ui->experience_edit->text().toInt() - ui->experience_changed_edit->text().toInt(),0,0);
    double stamina_tdiff = salg->getDriverTime(ui->stamina_edit->text().toInt() - ui->stamina_changed_edit->text().toInt(),0,0,0,0,0,0);
    double motivation_tdiff = salg->getDriverTime(0,0,0,0,0,ui->motivation_edit->text().toInt() - ui->motivation_changed_edit->text().toInt(),0);
    double weight_tdiff = salg->getDriverTime(0,0,0,0,0,0,ui->weight_edit->text().toInt() - ui->weight_changed_edit->text().toInt());

    ui->overall_time_text->setText(QString::number(overall_tdiff));
    ui->concentration_time_text->setText(QString::number(concentration_tdiff));
    ui->talent_time_text->setText(QString::number(talent_tdiff));
    ui->aggressiveness_time_text->setText(QString::number(aggressiveness_tdiff));
    ui->experience_time_text->setText(QString::number(experience_tdiff));
    ui->stamina_time_text->setText(QString::number(stamina_tdiff));
    ui->motivation_time_text->setText(QString::number(motivation_tdiff));
    ui->weight_time_text->setText(QString::number(weight_tdiff));

}

void DriverInfo::on_driver_fetch_button_clicked()
{
    gproServerInterface *gsi = new gproServerInterface();

    QSettings settings(General::ProgramName, General::CompanyName);

    QString username = settings.value(Settings::SafeLoginUsernameText, "").toString();
    QString password = settings.value(Settings::SafeLoginPasswordText, "").toString();
    password = GproResourceManager::getInstance().getSimpleCrypt()->decryptToString(password);
    gsi->gproLogin(username,password);

    Driver driver = gsi->getDriver();

    *current_driver_ = driver;

    updateDriverUI();

    GproResourceManager::getInstance().saveDriver(*current_driver_);

    ui->overall_changed_edit->setText(QString::number(driver.getOverall()));
    ui->concentration_changed_edit->setText(QString::number(driver.getConcentration()));
    ui->talent_changed_edit->setText(QString::number(driver.getTalent()));
    ui->aggressiveness_changed_edit->setText(QString::number(driver.getAggressiveness()));
    ui->experience_changed_edit->setText(QString::number(driver.getExperience()));
    ui->technical_insight_changed_edit->setText(QString::number(driver.getTechnicalInsight()));
    ui->stamina_changed_edit->setText(QString::number(driver.getStamina()));
    ui->charisma_changed_edit->setText(QString::number(driver.getCharisma()));
    ui->motivation_changed_edit->setText(QString::number(driver.getMotivation()));
    ui->weight_changed_edit->setText(QString::number(driver.getWeight()));
    ui->age_changed_edit->setText(QString::number(driver.getAge()));
    ui->reputation_changed_edit->setText(QString::number(driver.getReputation()));

    return;
}

void DriverInfo::on_concentration_changed_edit_textChanged(const QString &arg1)
{
    on_make_calculations_button_clicked();
}

void DriverInfo::on_talent_changed_edit_textChanged(const QString &arg1)
{
    on_make_calculations_button_clicked();
}

void DriverInfo::on_aggressiveness_changed_edit_textChanged(const QString &arg1)
{
    on_make_calculations_button_clicked();
}

void DriverInfo::on_experience_changed_edit_textChanged(const QString &arg1)
{
    on_make_calculations_button_clicked();
}

void DriverInfo::on_technical_insight_changed_edit_textChanged(const QString &arg1)
{
    on_make_calculations_button_clicked();
}

void DriverInfo::on_stamina_changed_edit_textChanged(const QString &arg1)
{
    on_make_calculations_button_clicked();
}

void DriverInfo::on_charisma_changed_edit_textChanged(const QString &arg1)
{
    on_make_calculations_button_clicked();
}

void DriverInfo::on_motivation_changed_edit_textChanged(const QString &arg1)
{
    on_make_calculations_button_clicked();
}

void DriverInfo::on_weight_changed_edit_textChanged(const QString &arg1)
{
    on_make_calculations_button_clicked();
}

void DriverInfo::on_age_changed_edit_textChanged(const QString &arg1)
{
    on_make_calculations_button_clicked();
}

void DriverInfo::on_reputation_changed_edit_textChanged(const QString &arg1)
{
    on_make_calculations_button_clicked();
}
