#include "trackviewwidget.h"
#include "ui_trackviewwidget.h"

#include <QObjectList>
#include <QRegularExpression>
#include <QDebug>

TrackViewWidget::TrackViewWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TrackViewWidget)
{
    ui->setupUi(this);

    showSimple(true);
}

TrackViewWidget::~TrackViewWidget()
{
    delete ui;
}

void TrackViewWidget::setTrack(const Track &track) {
    track_ = shared_ptr<Track>(new Track(track) );

    ui->track_name_text->setText(track_->getName());
    ui->track_id_text->setText(QString::number(track_->getID()));
    ui->distance_text->setText(QString::number(track_->getDistance()));
    ui->power_text->setText(QString::number(track_->getPower()));
    ui->handling_text->setText(QString::number(track_->getHandling()));
    ui->acceleration_text->setText(QString::number(track_->getAcceleration()));
    ui->downforce_text->setText(QString(Track::Downforce::toString(track_->getDownforce())));
    ui->overtaking_text->setText(QString(Track::Overtaking::toString(track_->getOvertaking())));
    ui->suspension_text->setText(QString(Track::Suspension::toString(track_->getSuspension())));
    ui->fuel_consumption_text->setText(QString(Track::FuelConsumption::toString(track_->getFuelConsumption())));
    ui->tyre_wear_text->setText(QString(Track::TyreWear::toString(track_->getTyreWear())));
    ui->avg_speed_text->setText(QString::number(track_->getAvgSpeed()));
    ui->lap_length_text->setText(QString::number(track_->getLapLength()));
    ui->corners_text->setText(QString::number(track_->getCorners()));
    ui->grip_text->setText(QString(Track::Grip::toString(track_->getGrip())));
    ui->pit_stop_text->setText(QString::number(track_->getPitStop()));

    ui->simple_track_name->setText(track_->getName());
}

void TrackViewWidget::showSimple(bool simple)
{
    if(simple) {
        this->setVisible(false);

        ui->complex_layout->setVisible(false);

        ui->simple_layout->setVisible(true);

        this->setVisible(true);

    } else {
        this->setVisible(false);

        ui->complex_layout->setVisible(true);

        ui->simple_layout->setVisible(false);

        this->setVisible(true);
    }
}
