#include "seasonplanner.h"
#include "ui_seasonplanner.h"

#include <QSettings>
#include <QDebug>

SeasonPlanner::SeasonPlanner(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SeasonPlanner),
    season_tracks_(0),
    test_track_id_(0)
{
    ui->setupUi(this);

    SeasonListTableModel *season_list_table = new SeasonListTableModel(ui->seasons_table);
    ui->seasons_table->setModel(season_list_table);

    recreatePreviousData();
}

SeasonPlanner::~SeasonPlanner()
{
    QSettings settings(General::ProgramName, General::CompanyName);
    SeasonListTableModel *sltm = (SeasonListTableModel*)ui->seasons_table->model();

    settings.setValue(Settings::SPBudget, ui->season_budget_edit->text().toInt());

    int rows = sltm->getTracks().size();
    settings.beginWriteArray(Settings::SPTracksText);
    for(int i = 0; i < rows; ++i) {
        settings.setArrayIndex(i);
        settings.setValue(Settings::SPTracksText, sltm->getTracks().at(i).getName());
    }
    settings.endArray();

    int drows = sltm->getDrivers().size();
    settings.beginWriteArray(Settings::SPDriversText);
    for(int i = 0; i < drows; ++i) {
        settings.setArrayIndex(i);
        settings.setValue(Settings::SPDriversText, (sltm->getDrivers().at(i) != 0 ? sltm->getDrivers().at(i)->serialize() : QStringList()));
    }
    settings.endArray();

    int crows = sltm->getCars().size();
    settings.beginWriteArray(Settings::SPCarText);
    for(int i = 0; i < crows; ++i) {
        settings.setArrayIndex(i);
        settings.setValue(Settings::SPCarText, (sltm->getCars().at(i) != 0 ? sltm->getCars().at(i)->serialize() : QStringList()));
    }
    settings.endArray();

    QList<int> clear_tracks;

    settings.beginWriteArray(Settings::SPClearTrackText);
    for(int i = 0; i < crows; ++i) {
        QModelIndex index = sltm->index(i,2);
        CarViewWidget *cvw = (CarViewWidget*)ui->seasons_table->indexWidget(index);
        settings.setArrayIndex(i);
        settings.setValue(Settings::SPClearTrackText, (cvw != 0) ? cvw->getClearTrack() : 0);
    }
    settings.endArray();

    delete ui;
}

void SeasonPlanner::on_fetch_current_season_button_clicked()
{
    gproServerInterface *gsi = gproServerInterface::createInstance();

    QStringList track_names = gsi->getSeasonTracks();
    QList<Track> tracks;

    qDebug() << track_names;

    QList<QStringList> all_tracks = GproResourceManager::getInstance().getDatabaseHandler()->getTracks();

    foreach (QString tname, track_names) {
        Track temp_track;

        foreach(QStringList string_track, all_tracks) {
            if(tname == string_track.at(0)) {
                temp_track = Track(string_track);
                //qDebug() << temp_track.toStringList();
            }
        }

        if(temp_track.getName() == "") {
            qDebug()  << tname << "track doesn't exist.";
            temp_track.setName(tname);
            tracks.append(temp_track);
        } else tracks.append(temp_track);
    }

    int counter = 0;

    if(ui->seasons_table->model()->rowCount() == 0)
        ui->seasons_table->model()->insertRows(0, tracks.size());
    else {
        ui->seasons_table->model()->removeRows(0,ui->seasons_table->model()->rowCount());
        ui->seasons_table->model()->insertRows(0, tracks.size());
    }

    SeasonListTableModel *sltm = ((SeasonListTableModel*)ui->seasons_table->model());

    foreach(Track track, tracks) {
        QModelIndex track_index = sltm->index(counter,0);
        QModelIndex driver_index = sltm->index(counter,1);
        QModelIndex car_index = sltm->index(counter,2);

        sltm->setTrackData(track_index, track, Qt::DisplayRole);

        TrackViewWidget *tvw = new TrackViewWidget(ui->seasons_table);
        DriverViewWidget *dvw = new DriverViewWidget(ui->seasons_table);
        sltm->setDriverData(driver_index, dvw->getDriver());
        CarViewWidget *cvw = new CarViewWidget(ui->seasons_table,ui->seasons_table);
        connect(cvw, &CarViewWidget::totalCostChanged, this, &SeasonPlanner::updated_total_cost);

        if(ui->seasons_table->indexWidget(track_index) == 0) {
            tvw->setTrack(sltm->getTrack(track_index));

            ui->seasons_table->setIndexWidget(track_index, tvw);
        }

        if (ui->seasons_table->indexWidget(driver_index) == 0) {
            ui->seasons_table->setIndexWidget(driver_index, dvw);
        }

        if ( ui->seasons_table->indexWidget(car_index) == 0) {
            cvw->setTrackID(sltm->getTrack(track_index).getID());
            cvw->setTestTrackID(test_track_id_);
            cvw->setDriver(dvw->getDriver());

            ui->seasons_table->setIndexWidget(car_index, cvw);
        }

        counter++;
    }

    // TODO get layout shit working :D
    ui->seasons_table->setVisible(false);
    ui->seasons_table->resizeColumnsToContents();
    ui->seasons_table->setVisible(true);

    ui->seasons_table->setVisible(false);
    ui->seasons_table->resizeRowsToContents();
    ui->seasons_table->setVisible(true);

    qDebug() << "Clicked";
}

void SeasonPlanner::on_seasons_table_pressed(const QModelIndex &index)
{
    SeasonListTableModel *sltm = ((SeasonListTableModel*)ui->seasons_table->model());

    for(int i = 0; i < sltm->columnCount(); ++i) {
        QString cheader = sltm->headerData(i,Qt::Orientation::Horizontal).toString();
        QModelIndex temp_index = sltm->index(index.row(), i);

        if(cheader == "Track" && ui->seasons_table->indexWidget(temp_index) == 0) {
            TrackViewWidget *tvw = new TrackViewWidget;
            tvw->setTrack(sltm->getTrack(temp_index));

            ui->seasons_table->setIndexWidget(temp_index, tvw);

        } else if (cheader == "Driver" && ui->seasons_table->indexWidget(temp_index) == 0) {
            DriverViewWidget *dvw = new DriverViewWidget;

            ui->seasons_table->setIndexWidget(temp_index, dvw);
        } else if (cheader == "Car" && ui->seasons_table->indexWidget(temp_index) == 0) {
            CarViewWidget *cvw = new CarViewWidget;

            ui->seasons_table->setIndexWidget(temp_index, cvw);
        }
    }

    QList<QModelIndex> active_widgets = sltm->getActiveWidgets();

    bool exists = false;
    bool diff_row = false;

    foreach(QModelIndex mindex, active_widgets) {
        if(mindex.row() != index.row()) {
            diff_row = true;
        }

        if(index == mindex) {
            //exists = true;
        }
    }

    if(exists || diff_row) {
        foreach(QModelIndex mindex, active_widgets) {
            QString mcheader = sltm->headerData(mindex.column(),Qt::Orientation::Horizontal).toString();

            if(mcheader == "Track") {
                ((TrackViewWidget*)ui->seasons_table->indexWidget(mindex))->showSimple(true);
            } else if (mcheader == "Driver") {
                ((DriverViewWidget*)ui->seasons_table->indexWidget(mindex))->showSimple(true);
            } else if(mcheader == "Car") {
                ((CarViewWidget*)ui->seasons_table->indexWidget(mindex))->showSimple(true);
            }

            sltm->clearActiveWidgets();
        }
    }

    for(int i = 0; i < sltm->columnCount(); ++i) {
        QString cheader = sltm->headerData(i,Qt::Orientation::Horizontal).toString();
        QModelIndex temp_index = sltm->index(index.row(), i);

        if (!exists && ui->seasons_table->indexWidget(temp_index) != 0) {
            if(cheader == "Track") {
                ((TrackViewWidget*)ui->seasons_table->indexWidget(temp_index))->showSimple(false);
            } else if (cheader == "Driver") {
                ((DriverViewWidget*)ui->seasons_table->indexWidget(temp_index))->showSimple(false);
            } else if(cheader == "Car") {
                ((CarViewWidget*)ui->seasons_table->indexWidget(temp_index))->showSimple(false);
            }

            sltm->addActiveWidget(temp_index);
        }
    }

    ui->seasons_table->setVisible(false);
    ui->seasons_table->resizeColumnsToContents();
    ui->seasons_table->setVisible(true);

    ui->seasons_table->setVisible(false);
    ui->seasons_table->resizeRowsToContents();
    ui->seasons_table->setVisible(true);

}

void SeasonPlanner::on_update_total_cost_button_clicked()
{
    double total_cost = 0;

    SeasonListTableModel *sltm = ((SeasonListTableModel*)ui->seasons_table->model());

    for(int i = 0; i < sltm->rowCount(); ++i) {
        QModelIndex index = sltm->index(i,2);
        if(ui->seasons_table->indexWidget(index) != 0) {
            CarViewWidget *cvw = (CarViewWidget*)ui->seasons_table->indexWidget(index);
            total_cost += cvw->getTotalCost();
        }
    }

    ui->season_total_cost_text->setText(QString::number(std::round(total_cost)));
}

void SeasonPlanner::updated_total_cost(const QString &text)
{
    double total_cost = 0;

    SeasonListTableModel *sltm = ((SeasonListTableModel*)ui->seasons_table->model());

    for(int i = 0; i < sltm->rowCount(); ++i) {
        QModelIndex index = sltm->index(i,2);
        if(ui->seasons_table->indexWidget(index) != 0) {
            CarViewWidget *cvw = (CarViewWidget*)ui->seasons_table->indexWidget(index);
            total_cost += cvw->getTotalCost();
        }
    }

    ui->season_total_cost_text->setText(QString::number(std::round(total_cost)));

    double budget = ui->season_budget_edit->text().toDouble();

    ui->season_net_income_text->setText(QString::number(budget-total_cost));
}

void SeasonPlanner::recreatePreviousData(bool debug)
{
    QSettings settings(General::ProgramName, General::CompanyName);

    ui->season_test_track_text->setText(settings.value(Settings::SPTestTrack, QStringLiteral("")).toString());
    //TODO
    test_track_id_ = GproResourceManager::getInstance().getDatabaseHandler()->getTrackId(ui->season_test_track_text->text());

    if(ui->season_test_track_text->text().length() == 0) {
        gproServerInterface *gsi = gproServerInterface::createInstance();
        QString tt_name = gsi->getTestTrackName();
        ui->season_test_track_text->setText(tt_name);
        settings.setValue(Settings::SPTestTrack, tt_name);
    }

    QStringList track_names;
    QList<Track> tracks;

    int rows = settings.beginReadArray(Settings::SPTracksText);

    for(int i = 0; i < rows; ++i) {
        settings.setArrayIndex(i);
        track_names.append(settings.value(Settings::SPTracksText).toString());
    }

    settings.endArray();

    QList<Driver> drivers;

    int drows = settings.beginReadArray(Settings::SPDriversText);

    for(int i = 0; i < drows; ++i) {
        settings.setArrayIndex(i);
        drivers.append(Driver(settings.value(Settings::SPDriversText).toStringList()));
    }

    settings.endArray();

    QList<Car> cars;

    int crows = settings.beginReadArray(Settings::SPCarText);

    for(int i = 0; i < crows; ++i) {
        settings.setArrayIndex(i);
        cars.append(Car(settings.value(Settings::SPCarText).toStringList()));
    }

    settings.endArray();

    QList<int> clear_tracks;

    int ctrows = settings.beginReadArray(Settings::SPClearTrackText);

    for(int i = 0; i < ctrows; ++i) {
        settings.setArrayIndex(i);
        clear_tracks.append(settings.value(Settings::SPClearTrackText).toInt());
    }

    settings.endArray();

    if(debug) qDebug() << "Season planner tracks" <<  track_names;

    QList<QStringList> all_tracks = GproResourceManager::getInstance().getDatabaseHandler()->getTracks();

    foreach (QString tname, track_names) {
        Track temp_track;

        foreach(QStringList string_track, all_tracks) {
            if(tname == string_track.at(0)) {
                temp_track = Track(string_track);
            }
        }

        if(temp_track.getName() == "") {
            if(debug) qDebug()  << tname << "track doesn't exist.";
            temp_track.setName(tname);
            tracks.append(temp_track);
        } else tracks.append(temp_track);
    }

    int counter = 0;

    if(ui->seasons_table->model()->rowCount() == 0)
        ui->seasons_table->model()->insertRows(0, tracks.size());

    SeasonListTableModel *sltm = ((SeasonListTableModel*)ui->seasons_table->model());

    foreach(Track track, tracks) {
        QModelIndex track_index = sltm->index(counter,0);
        QModelIndex driver_index = sltm->index(counter,1);
        QModelIndex car_index = sltm->index(counter,2);

        sltm->setTrackData(track_index, track, Qt::DisplayRole);

        TrackViewWidget *tvw = new TrackViewWidget(ui->seasons_table);
        DriverViewWidget *dvw = new DriverViewWidget(ui->seasons_table);

        if(counter < drivers.size()) dvw->setDriver(shared_ptr<Driver>(new Driver(drivers.at(counter))));
        sltm->setDriverData(driver_index, dvw->getDriver(), Qt::DisplayRole);

        CarViewWidget *cvw = new CarViewWidget(ui->seasons_table,ui->seasons_table);
        connect(cvw, &CarViewWidget::totalCostChanged, this, &SeasonPlanner::updated_total_cost);

        if(counter < cars.size()) cvw->setCar(cars.at(counter));
        sltm->setCarData(car_index, cvw->getCar(), Qt::DisplayRole);
        if(counter > 0 && counter < cars.size()) cvw->setPreviousCar(sltm->getCars().at(counter-1));

        if(counter >= 0 && counter < clear_tracks.size()) cvw->setClearTrack(clear_tracks.at(counter));

        if(ui->seasons_table->indexWidget(track_index) == 0) {
            tvw->setTrack(sltm->getTrack(track_index));

            ui->seasons_table->setIndexWidget(track_index, tvw);
        }

        if (ui->seasons_table->indexWidget(driver_index) == 0) {
            ui->seasons_table->setIndexWidget(driver_index, dvw);
        }

        if ( ui->seasons_table->indexWidget(car_index) == 0) {
            cvw->setTrackID(sltm->getTrack(track_index).getID());
            cvw->setTestTrackID(test_track_id_);
            cvw->setDriver(dvw->getDriver());

            ui->seasons_table->setIndexWidget(car_index, cvw);
        }

        counter++;
    }

    ui->seasons_table->setVisible(false);
    ui->seasons_table->resizeColumnsToContents();
    ui->seasons_table->setVisible(true);

    ui->seasons_table->setVisible(false);
    ui->seasons_table->resizeRowsToContents();
    ui->seasons_table->setVisible(true);

    ui->update_total_cost_button->clicked();
    ui->season_budget_edit->setText(settings.value(Settings::SPBudget, "0").toString());
}

void SeasonPlanner::on_season_budget_edit_textChanged(const QString &arg1)
{
    int value = intEdit(arg1);

    ui->season_budget_edit->setText(QString::number(value));

    ui->season_net_income_text->setText(QString::number(ui->season_budget_edit->text().toInt() - ui->season_total_cost_text->text().toDouble()));
}

int SeasonPlanner::intEdit(const QString &str)
{
    if(str.size() == 0) {
        return 0;
    }

    bool valid;
    int value = str.toInt(&valid);

    if(valid) return value > 0 ? value : 0;

    QString last_removed = str;
    last_removed.chop(1);

    value = last_removed.toInt(&valid);

    if(valid) return value > 0 ? value : 0;

    return 0;
}

void SeasonPlanner::resizeEvent(QResizeEvent *event)
{
    QWidget::resizeEvent(event);

    ui->seasons_table->resizeColumnsToContents();
    ui->seasons_table->resizeRowsToContents();
}

void SeasonPlanner::on_update_test_track_button_clicked()
{
    QSettings settings(General::ProgramName, General::CompanyName);

    gproServerInterface *gsi = gproServerInterface::createInstance();
    QString tt_name = gsi->getTestTrackName();
    ui->season_test_track_text->setText(tt_name);

    test_track_id_ = GproResourceManager::getInstance().getDatabaseHandler()->getTrackId(tt_name);

    settings.setValue(Settings::SPTestTrack, tt_name);
}
