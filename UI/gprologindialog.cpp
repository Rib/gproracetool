#include "gprologindialog.h"
#include "ui_gprologindialog.h"

#include <QSettings>
#include <QDebug>

#include "settingconstants.h"

GproLoginDialog::GproLoginDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GproLoginDialog)
{
    ui->setupUi(this);

    QSettings settings(General::ProgramName, General::CompanyName);

    QString safe_login_username = settings.value(Settings::SafeLoginUsernameText, "").toString();
    QString safe_login_password = settings.value(Settings::SafeLoginPasswordText, "").toString();
    QString login_user_name = settings.value(Settings::LoginUsernameText, "").toString();
    QString login_password = settings.value(Settings::LoginPasswordText, "").toString();

    safe_login_password = GproResourceManager::getInstance().getSimpleCrypt()->decryptToString(safe_login_password);
    login_password = GproResourceManager::getInstance().getSimpleCrypt()->decryptToString(login_password);

    ui->save_login_username->setText(safe_login_username);
    ui->safe_login_password->setText(safe_login_password);
    ui->login_username->setText(login_user_name);
    ui->login_password->setText(login_password);
}

GproLoginDialog::~GproLoginDialog()
{
    delete ui;
}

void GproLoginDialog::on_safe_login_save_button_clicked()
{
    QString safe_login_username = ui->save_login_username->text();
    QString safe_login_password = ui->safe_login_password->text();

    safe_login_password = GproResourceManager::getInstance().getSimpleCrypt()->encryptToString(safe_login_password);

    QSettings settings(General::ProgramName, General::CompanyName);

    settings.setValue(Settings::SafeLoginUsernameText, safe_login_username);
    settings.setValue(Settings::SafeLoginPasswordText, safe_login_password);
}

void GproLoginDialog::on_login_save_button_clicked()
{
    QString login_username = ui->login_username->text();
    QString login_password = ui->login_password->text();

    login_password = GproResourceManager::getInstance().getSimpleCrypt()->encryptToString(login_password);

    QSettings settings(General::ProgramName, General::CompanyName);

    settings.setValue(Settings::LoginUsernameText, login_username);
    settings.setValue(Settings::LoginPasswordText, login_password);
}
