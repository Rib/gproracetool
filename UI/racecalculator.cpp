#include "racecalculator.h"
#include "ui_racecalculator.h"

#include "settingconstants.h"
#include "gproserverinterface.h"

#include "Misc/gproresourcemanager.h"
#include "Misc/constantcalculations.h"
#include "Objects/tyretype.h"

#include <QTime>
#include <QTimer>

#include <QSettings>

RaceCalculator::RaceCalculator(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RaceCalculator),
    stint_data_(0)
{
    ui->setupUi(this);

    ui->fuel_calc_laps->setLimits(1,80);
    ui->es_stops_edit_field->setLimits(0,40);
    ui->s_stops_edit_field->setLimits(0,40);
    ui->m_stops_edit_field->setLimits(0,40);
    ui->h_stops_edit_field->setLimits(0,40);

    ui->fuel_calc_laps->setText("1");
    ui->h_stops_edit_field->setText("1");
    ui->m_stops_edit_field->setText("2");
    ui->s_stops_edit_field->setText("3");
    ui->es_stops_edit_field->setText("4");

    QSettings settings(General::ProgramName, General::CompanyName);

    driver_ = *GproResourceManager::getInstance().getDriver();

    updateDriverUI(driver_);

    car_ = *GproResourceManager::getInstance().getCar();

    updateCarUI(car_);

    weather_ = *GproResourceManager::getInstance().getWeather();

    updateWeatherUI(weather_);

    track_ = *GproResourceManager::getInstance().getTrack();

    ui->track_name_text->setText(track_.getName());

    stint_data_ = GproResourceManager::getInstance().getDatabaseHandler()->getStintData(false);

    StintAlgorithms *alg = GproResourceManager::getInstance().getStintAlgorithms();
    alg->setStintData(stint_data_);
    alg->setStintDataLabels(GproResourceManager::getInstance().getDatabaseHandler()->getStintDataLabels());

    alg->setCTTimeData(GproResourceManager::getInstance().getDatabaseHandler()->getCTTimeData(false,false));
    alg->setCTTimeDataLabelIndexes(GproResourceManager::getInstance().getDatabaseHandler()->getCTTimeLabelsIndexes());

    alg->setFuelTimeData(GproResourceManager::getInstance().getDatabaseHandler()->getFuelTimeData(false,false));
    alg->setFuelTimeDataLabelIndexes(GproResourceManager::getInstance().getDatabaseHandler()->getFuelTimeLabelsIndexes());

    race_data_ = GproResourceManager::getInstance().getDatabaseHandler()->getArmaRaceData(false);
    RaceAlgorithms *race_alg = GproResourceManager::getInstance().getRaceAlgorithms();
    race_alg->setRaceData(race_data_);
    race_alg->setRaceDataLabelsIndexes( GproResourceManager::getInstance().getDatabaseHandler()->getArmaRaceDataLabelsIndexes() );

    PracticeAlgorithms *palg = GproResourceManager::getInstance().getPracticeAlgorithms();
    palg->setTyreDiffData(GproResourceManager::getInstance().getDatabaseHandler()->getTyreDiffData(false, false));
    palg->setTyreDiffDataLabelIndexes(GproResourceManager::getInstance().getDatabaseHandler()->getTyreDiffDataLabelsIndexes());

    updateStintUI();
    updateEstCarWearUI(0);
    updateFuelUI();
    updateTimeUI(ui->clear_track_slider->value());

    ui->car_wear_widget->setCar(car_);
    ui->car_wear_widget->setTrackID(track_.getID());
    ui->car_wear_widget->setDriver(shared_ptr<Driver>(new Driver(driver_)));
    ui->car_wear_widget->showSimple(false);
    ui->car_wear_widget->showRaceCarView(true);
    ui->car_wear_widget->updateCarUI();
}

RaceCalculator::~RaceCalculator()
{
    delete ui;
}

void RaceCalculator::on_update_driver_button_clicked()
{
    gproServerInterface *gsi = new gproServerInterface();

    QSettings settings(General::ProgramName, General::CompanyName);

    QString username = settings.value(Settings::SafeLoginUsernameText, "").toString();
    QString password = settings.value(Settings::SafeLoginPasswordText, "").toString();
    password = GproResourceManager::getInstance().getSimpleCrypt()->decryptToString(password);
    gsi->gproLogin(username,password);

    Driver driver = gsi->getDriver();

    driver_ = driver;

    updateDriverUI(driver_);
}

void RaceCalculator::updateDriverUI(Driver driver)
{
    ui->driver_overall_text->setText(QString::number(driver.getOverall()));
    ui->driver_concentration_text->setText(QString::number(driver.getConcentration()));
    ui->driver_talent_text->setText(QString::number(driver.getTalent()));
    ui->driver_aggressiveness_text->setText(QString::number(driver.getAggressiveness()));
    ui->driver_experience_text->setText(QString::number(driver.getExperience()));
    ui->driver_technical_insight_text->setText(QString::number(driver.getTechnicalInsight()));
    ui->driver_stamina_text->setText(QString::number(driver.getStamina()));
    ui->driver_charisma_text->setText(QString::number(driver.getCharisma()));
    ui->driver_motivation_text->setText(QString::number(driver.getMotivation()));
    ui->driver_weight_text->setText(QString::number(driver.getWeight()));
    ui->driver_age_text->setText(QString::number(driver.getAge()));

    GproResourceManager::getInstance().saveDriver(driver);
}

void RaceCalculator::updateCarUI(Car car)
{
    saveCarSettings(car);
}

void RaceCalculator::updateEstCarWearUI(int clear_track)
{
    RaceAlgorithms *race_alg = GproResourceManager::getInstance().getRaceAlgorithms();

    QList<double> est_wears = race_alg->getEstimatedWears(track_.getID(), clear_track, driver_.getConcentration(),
                                            driver_.getTalent(), driver_.getExperience(), car_.getChassisLvl(), car_.getEngineLvl(),
                                            car_.getFrontWingLvl(), car_.getRearWingLvl(), car_.getUnderbodyLvl(), car_.getSidepodsLvl(),
                                            car_.getCoolingLvl(), car_.getGearboxLvl(), car_.getBrakesLvl(), car_.getSuspensionLvl(),
                                            car_.getElectronicsLvl());

    double total_cost = 0;

    array<double,4> chassis = { { (double)car_.getChassisLvl(), est_wears.at(0), 0,  car_.getChassisWear() + est_wears.at(0)} };
    array<double,4> engine = { { (double)car_.getEngineLvl(), est_wears.at(1), 0,  car_.getEngineWear() + est_wears.at(1) } };
    array<double,4> front_wing = { { (double)car_.getFrontWingLvl(), est_wears.at(2), 0, car_.getFrontWingWear() + est_wears.at(2) } };
    array<double,4> rear_wing = { { (double)car_.getRearWingLvl(), est_wears.at(3), 0, car_.getRearWingWear() + est_wears.at(3) } };
    array<double,4> underbody = { { (double)car_.getUnderbodyLvl(), est_wears.at(4), 0, car_.getUnderbodyWear() + est_wears.at(4) } };
    array<double,4> sidepods = { { (double)car_.getSidepodsLvl(), est_wears.at(5), 0, car_.getSidepodsWear() + est_wears.at(5) } };
    array<double,4> cooling = { { (double)car_.getCoolingLvl(), est_wears.at(6), 0, car_.getCoolingWear() + est_wears.at(6) } };
    array<double,4> gearbox = { { (double)car_.getGearboxLvl(), est_wears.at(7), 0, car_.getGearboxWear() + est_wears.at(7) } };
    array<double,4> brakes = { { (double)car_.getBrakesLvl(), est_wears.at(8), 0, car_.getBrakesWear() + est_wears.at(8) } };
    array<double,4> suspension = { { (double)car_.getSuspensionLvl(), est_wears.at(9), 0, car_.getSuspensionWear() + est_wears.at(9) } };
    array<double,4> electronics = { { (double)car_.getElectronicsLvl(), est_wears.at(10), 0, car_.getElectronicsWear() + est_wears.at(10) } };

    chassis.at(2) = ConstantCalculations::getPartPrice(QStringLiteral("Chassis"), chassis.at(0));
    engine.at(2) = ConstantCalculations::getPartPrice(QStringLiteral("Engine"), engine.at(0));
    front_wing.at(2) = ConstantCalculations::getPartPrice(QStringLiteral("FrontWing"), front_wing.at(0));
    rear_wing.at(2) = ConstantCalculations::getPartPrice(QStringLiteral("RearWing"), rear_wing.at(0));
    underbody.at(2) = ConstantCalculations::getPartPrice(QStringLiteral("Underbody"), underbody.at(0));
    sidepods.at(2) = ConstantCalculations::getPartPrice(QStringLiteral("Chassis"), sidepods.at(0));
    cooling.at(2) = ConstantCalculations::getPartPrice(QStringLiteral("Sidepods"), cooling.at(0));
    gearbox.at(2) = ConstantCalculations::getPartPrice(QStringLiteral("Gearbox"), gearbox.at(0));
    brakes.at(2) = ConstantCalculations::getPartPrice(QStringLiteral("Brakes"), brakes.at(0));
    suspension.at(2) = ConstantCalculations::getPartPrice(QStringLiteral("Suspension"), suspension.at(0));
    electronics.at(2) = ConstantCalculations::getPartPrice(QStringLiteral("Electronics"), electronics.at(0));

    total_cost += static_cast<double>((chassis.at(3) > 99 ? (99-(chassis.at(3)-chassis.at(1))) : chassis.at(1)))/100 * chassis.at(2);
    total_cost += static_cast<double>((engine.at(3) > 99 ? (99-(engine.at(3)-engine.at(1))) : engine.at(1)))/100 * engine.at(2);
    total_cost += static_cast<double>((front_wing.at(3) > 99 ? (99-(front_wing.at(3)-front_wing.at(1))) : front_wing.at(1)))/100 * front_wing.at(2);
    total_cost += static_cast<double>((rear_wing.at(3) > 99 ? (99-(rear_wing.at(3)-rear_wing.at(1))) : rear_wing.at(1)))/100 * rear_wing.at(2);
    total_cost += static_cast<double>((underbody.at(3) > 99 ? (99-(underbody.at(3)-underbody.at(1))) : underbody.at(1)))/100 * underbody.at(2);
    total_cost += static_cast<double>((sidepods.at(3) > 99 ? (99-(sidepods.at(3)-sidepods.at(1))) : sidepods.at(1)))/100 * sidepods.at(2);
    total_cost += static_cast<double>((cooling.at(3) > 99 ? (99-(cooling.at(3)-cooling.at(1))) : cooling.at(1)))/100 * cooling.at(2);
    total_cost += static_cast<double>((gearbox.at(3) > 99 ? (99-(gearbox.at(3)-gearbox.at(1))) : gearbox.at(1)))/100 * gearbox.at(2);
    total_cost += static_cast<double>((brakes.at(3) > 99 ? (99-(brakes.at(3)-brakes.at(1))) : brakes.at(1)))/100 * brakes.at(2);
    total_cost += static_cast<double>((suspension.at(3) > 99 ? (99-(suspension.at(3)-suspension.at(1))) : suspension.at(1)))/100 * suspension.at(2);
    total_cost += static_cast<double>((electronics.at(3) > 99 ? (99-(electronics.at(3)-electronics.at(1))) : electronics.at(1)))/100 * electronics.at(2);

    finance_.setEstCarCost(ui->car_wear_widget->getTotalCost());
    ui->car_cost_text->setText(QString::number(finance_.getEstCarCost()));

}

void RaceCalculator::saveCarSettings(Car car)
{
    QSettings settings(General::ProgramName, General::CompanyName);

    settings.setValue(Settings::CarPowerText,car.getPower());
    settings.setValue(Settings::CarHandlingText,car.getHandling());
    settings.setValue(Settings::CarAccelerationText,car.getAcceleration());

    settings.setValue(Settings::CarChassisLvlText,car.getChassisLvl());
    settings.setValue(Settings::CarChassisWearText,car.getChassisWear());
    settings.setValue(Settings::CarEngineLvlText,car.getEngineLvl());
    settings.setValue(Settings::CarEngineWearText,car.getEngineWear());
    settings.setValue(Settings::CarFrontWingLvlText,car.getFrontWingLvl());
    settings.setValue(Settings::CarFrontWingWearText,car.getFrontWingWear());
    settings.setValue(Settings::CarRearWingLvlText,car.getRearWingLvl());
    settings.setValue(Settings::CarRearWingWearText,car.getRearWingWear());
    settings.setValue(Settings::CarUnderbodyLvlText,car.getUnderbodyLvl());
    settings.setValue(Settings::CarUnderbodyWearText,car.getUnderbodyWear());
    settings.setValue(Settings::CarSidepodsLvlText,car.getSidepodsLvl());
    settings.setValue(Settings::CarSidepodsWearText,car.getSidepodsWear());
    settings.setValue(Settings::CarCoolingLvlText,car.getCoolingLvl());
    settings.setValue(Settings::CarCoolingWearText,car.getCoolingWear());
    settings.setValue(Settings::CarGearboxLvlText,car.getGearboxLvl());
    settings.setValue(Settings::CarGearboxWearText,car.getGearboxWear());
    settings.setValue(Settings::CarBrakesLvlText,car.getBrakesLvl());
    settings.setValue(Settings::CarBrakesWearText,car.getBrakesWear());
    settings.setValue(Settings::CarSuspensionLvlText,car.getSuspensionLvl());
    settings.setValue(Settings::CarSuspensionWearText,car.getSuspensionWear());
    settings.setValue(Settings::CarElectronicsLvlText,car.getElectronicsLvl());
    settings.setValue(Settings::CarElectronicsWearText,car.getElectronicsWear());
}

void RaceCalculator::updateWeatherUI(Weather weather)
{
    ui->race_q1_temperature->setText(QString::number(weather.getRaceQ1TemperatureLow()) + QString(" - ") +
                                     QString::number(weather.getRaceQ1TemperatureHigh()));
    ui->race_q1_humidity->setText(QString::number(weather.getRaceQ1HumidityLow()) + QString(" - ") +
                                     QString::number(weather.getRaceQ1HumidityHigh()));
    ui->race_q1_rain_probability->setText(QString::number(weather.getRaceQ1RainProbabilityLow()) + QString(" - ") +
                                     QString::number(weather.getRaceQ1RainProbabilityHigh()));

    ui->race_q2_temperature->setText(QString::number(weather.getRaceQ2TemperatureLow()) + QString(" - ") +
                                     QString::number(weather.getRaceQ2TemperatureHigh()));
    ui->race_q2_humidity->setText(QString::number(weather.getRaceQ2HumidityLow()) + QString(" - ") +
                                     QString::number(weather.getRaceQ2HumidityHigh()));
    ui->race_q2_rain_probability->setText(QString::number(weather.getRaceQ2RainProbabilityLow()) + QString(" - ") +
                                     QString::number(weather.getRaceQ2RainProbabilityHigh()));

    ui->race_q3_temperature->setText(QString::number(weather.getRaceQ3TemperatureLow()) + QString(" - ") +
                                     QString::number(weather.getRaceQ3TemperatureHigh()));
    ui->race_q3_humidity->setText(QString::number(weather.getRaceQ3HumidityLow()) + QString(" - ") +
                                     QString::number(weather.getRaceQ3HumidityHigh()));
    ui->race_q3_rain_probability->setText(QString::number(weather.getRaceQ3RainProbabilityLow()) + QString(" - ") +
                                     QString::number(weather.getRaceQ3RainProbabilityHigh()));

    ui->race_q4_temperature->setText(QString::number(weather.getRaceQ4TemperatureLow()) + QString(" - ") +
                                     QString::number(weather.getRaceQ4TemperatureHigh()));
    ui->race_q4_humidity->setText(QString::number(weather.getRaceQ4HumidityLow()) + QString(" - ") +
                                     QString::number(weather.getRaceQ4HumidityHigh()));
    ui->race_q4_rain_probability->setText(QString::number(weather.getRaceQ4RainProbabilityLow()) + QString(" - ") +
                                     QString::number(weather.getRaceQ4RainProbabilityHigh()));

    saveWeatherSettings(weather);
}

void RaceCalculator::saveWeatherSettings(Weather weather)
{
    QSettings settings(General::ProgramName, General::CompanyName);

    settings.setValue(Settings::WeatherQ2Temperature, weather.getQ2Temperature());
    settings.setValue(Settings::WeatherQ2Humidity, weather.getQ2Humidity());

    settings.setValue(Settings::WeatherRaceQ1TemperatureLowText, weather.getRaceQ1TemperatureLow());
    settings.setValue(Settings::WeatherRaceQ1TemperatureHighText, weather.getRaceQ1TemperatureHigh());
    settings.setValue(Settings::WeatherRaceQ1HumidityLowText, weather.getRaceQ1HumidityLow());
    settings.setValue(Settings::WeatherRaceQ1HumidityHighText, weather.getRaceQ1HumidityHigh());
    settings.setValue(Settings::WeatherRaceQ1RProbabilityLowText, weather.getRaceQ1RainProbabilityLow());
    settings.setValue(Settings::WeatherRaceQ1RProbabilityHighText, weather.getRaceQ1RainProbabilityHigh());

    settings.setValue(Settings::WeatherRaceQ2TemperatureLowText, weather.getRaceQ2TemperatureLow());
    settings.setValue(Settings::WeatherRaceQ2TemperatureHighText, weather.getRaceQ2TemperatureHigh());
    settings.setValue(Settings::WeatherRaceQ2HumidityLowText, weather.getRaceQ2HumidityLow());
    settings.setValue(Settings::WeatherRaceQ2HumidityHighText, weather.getRaceQ2HumidityHigh());
    settings.setValue(Settings::WeatherRaceQ2RProbabilityLowText, weather.getRaceQ2RainProbabilityLow());
    settings.setValue(Settings::WeatherRaceQ2RProbabilityHighText, weather.getRaceQ2RainProbabilityHigh());

    settings.setValue(Settings::WeatherRaceQ3TemperatureLowText, weather.getRaceQ3TemperatureLow());
    settings.setValue(Settings::WeatherRaceQ3TemperatureHighText, weather.getRaceQ3TemperatureHigh());
    settings.setValue(Settings::WeatherRaceQ3HumidityLowText, weather.getRaceQ3HumidityLow());
    settings.setValue(Settings::WeatherRaceQ3HumidityHighText, weather.getRaceQ3HumidityHigh());
    settings.setValue(Settings::WeatherRaceQ3RProbabilityLowText, weather.getRaceQ3RainProbabilityLow());
    settings.setValue(Settings::WeatherRaceQ3RProbabilityHighText, weather.getRaceQ3RainProbabilityHigh());

    settings.setValue(Settings::WeatherRaceQ4TemperatureLowText, weather.getRaceQ4TemperatureLow());
    settings.setValue(Settings::WeatherRaceQ4TemperatureHighText, weather.getRaceQ4TemperatureHigh());
    settings.setValue(Settings::WeatherRaceQ4HumidityLowText, weather.getRaceQ4HumidityLow());
    settings.setValue(Settings::WeatherRaceQ4HumidityHighText, weather.getRaceQ4HumidityHigh());
    settings.setValue(Settings::WeatherRaceQ4RProbabilityLowText, weather.getRaceQ4RainProbabilityLow());
    settings.setValue(Settings::WeatherRaceQ4RProbabilityHighText, weather.getRaceQ4RainProbabilityHigh());
}

void RaceCalculator::updateTimeUI(int clear_track)
{

    PracticeAlgorithms *pa = GproResourceManager::getInstance().getPracticeAlgorithms();
    StintAlgorithms *sta = GproResourceManager::getInstance().getStintAlgorithms();

    int es_stops = 0;
    int s_stops = 0;
    int m_stops = 0;
    int h_stops = 0;

    if(ui->es_stops_edit_field->text().size() > 0) {
        es_stops = ui->es_stops_edit_field->text().toInt();
    }
    if(ui->es_stops_edit_field->text().size() > 0) {
        s_stops = ui->s_stops_edit_field->text().toInt();
    }
    if(ui->es_stops_edit_field->text().size() > 0) {
        m_stops = ui->m_stops_edit_field->text().toInt();
    }
    if(ui->es_stops_edit_field->text().size() > 0) {
        h_stops = ui->h_stops_edit_field->text().toInt();
    }

    double total_ct_time_reduction = sta->getCTTime(clear_track, driver_.getStamina()) * track_.getDistance();
    ui->ct_time_reduction_text->setText(QString::number(total_ct_time_reduction));

    // TODO better prediction for pit stops
    double es_pit_stop_time = (track_.getPitStop() + 25) * es_stops;
    ui->es_pit_stop_time_text->setText(QString::number(es_pit_stop_time));

    double s_pit_stop_time = (track_.getPitStop() + 25) * s_stops;
    ui->s_pit_stop_time_text->setText(QString::number(s_pit_stop_time));

    double m_pit_stop_time = (track_.getPitStop() + 25) * m_stops;
    ui->m_pit_stop_time_text->setText(QString::number(m_pit_stop_time));

    double h_pit_stop_time = (track_.getPitStop() + 25) * h_stops;
    ui->h_pit_stop_time_text->setText(QString::number(h_pit_stop_time));

    double es_fuel_time = 0;
    double s_fuel_time = 0;
    double m_fuel_time = 0;
    double h_fuel_time = 0;

    double fuel_consumption = sta->getFuelConsumption(weather_.getQ2Status(), car_.getEngineLvl(), car_.getElectronicsLvl(),
                                                          track_.getFuelConsumption(), track_.getGrip(), track_.getDownforce());

    double es_stint_distance = track_.getDistance()/ (es_stops + 1);
    double s_stint_distance = track_.getDistance()/ (s_stops + 1);
    double m_stint_distance = track_.getDistance()/ (m_stops + 1);
    double h_stint_distance = track_.getDistance()/ (h_stops + 1);

    double es_fuel_amount = (std::ceil(es_stint_distance / track_.getLapLength()) + 0.5)   * (1 / fuel_consumption) * track_.getLapLength();
    double s_fuel_amount = (std::ceil(s_stint_distance / track_.getLapLength()) + 0.5)   * (1 / fuel_consumption) * track_.getLapLength();
    double m_fuel_amount = (std::ceil(m_stint_distance / track_.getLapLength()) + 0.5)   * (1 / fuel_consumption) * track_.getLapLength();
    double h_fuel_amount = (std::ceil(h_stint_distance / track_.getLapLength()) + 0.5)   * (1 / fuel_consumption) * track_.getLapLength();

    while(es_fuel_amount - fuel_consumption > 0 && es_stint_distance > 0) {
        es_fuel_time += sta->getFuelTime(es_fuel_amount, driver_.getStamina());

        es_fuel_amount -= 1/fuel_consumption;
        es_stint_distance -= 1;
    }

    es_fuel_time *= es_stops + 1;

    while(s_fuel_amount - fuel_consumption > 0 && s_stint_distance > 0) {
        s_fuel_time += sta->getFuelTime(s_fuel_amount, driver_.getStamina());

        s_fuel_amount -= 1/fuel_consumption;
        s_stint_distance -= 1;
    }

    s_fuel_time *= s_stops + 1;

    while(m_fuel_amount - fuel_consumption > 0 && m_stint_distance > 0) {
        m_fuel_time += sta->getFuelTime(m_fuel_amount, driver_.getStamina());

        m_fuel_amount -= 1/fuel_consumption;
        m_stint_distance -= 1;
    }

    m_fuel_time *= m_stops + 1;

    while(h_fuel_amount - fuel_consumption > 0 && h_stint_distance > 0) {
        h_fuel_time += sta->getFuelTime(h_fuel_amount, driver_.getStamina());

        h_fuel_amount -= 1/fuel_consumption;
        h_stint_distance -= 1;
    }

    h_fuel_time *= h_stops + 1;

    ui->es_fuel_time_text->setText(QString::number(es_fuel_time));
    ui->s_fuel_time_text->setText(QString::number(s_fuel_time));
    ui->m_fuel_time_text->setText(QString::number(m_fuel_time));
    ui->h_fuel_time_text->setText(QString::number(h_fuel_time));

    double tyre_diff = pa->getTyreDiff(weather_.getEstRaceTemperature(), weather_.getEstRaceHumidity(),track_.getCorners(), track_.getLapLength());
    tyre_diff *= track_.getDistance();

    ui->es_tyre_diff_text->setText(QString::number(tyre_diff * 0));
    ui->s_tyre_diff_text->setText(QString::number(tyre_diff * 1));
    ui->m_tyre_diff_text->setText(QString::number(tyre_diff * 2));
    ui->h_tyre_diff_text->setText(QString::number(tyre_diff * 3));

    double es_total_time = total_ct_time_reduction + es_pit_stop_time + es_fuel_time + tyre_diff * 0;
    double s_total_time = total_ct_time_reduction + s_pit_stop_time + s_fuel_time + tyre_diff * 1;
    double m_total_time = total_ct_time_reduction + m_pit_stop_time + m_fuel_time + tyre_diff * 2;
    double h_total_time = total_ct_time_reduction + h_pit_stop_time + h_fuel_time + tyre_diff * 3;

    ui->es_total_time_text->setText(QString::number(es_total_time));
    ui->s_total_time_text->setText(QString::number(s_total_time));
    ui->m_total_time_text->setText(QString::number(m_total_time));
    ui->h_total_time_text->setText(QString::number(h_total_time));


}

void RaceCalculator::loadTrack(QString track_name)
{
    Track track;

    track.setName(track_name);

    QList< QStringList > tracks = GproResourceManager::getInstance().getDatabaseHandler()->getTracks();

    foreach( QStringList str_list, tracks) {
        if(str_list.size() > 0 && str_list.at(0) == track.getName()) {
            track.setID( str_list.at(1).toInt() );
            track.setDistance( str_list.at(2).toDouble() );
            track.setPower( str_list.at(3).toInt() );
            track.setHandling( str_list.at(4).toInt() );
            track.setAcceleration( str_list.at(5).toInt() );
            track.setDownforce( Track::Downforce::toDownforce( str_list.at(6) ) );
            track.setOvertaking( Track::Overtaking::toOvertaking( str_list.at(7) ) );
            track.setSuspension( Track::Suspension::toSuspension( str_list.at(8) ) );
            track.setFuelConsumption( Track::FuelConsumption::toFuelConsumption( str_list.at(9) ) );
            track.setTyreWear( Track::TyreWear::toTyreWear( str_list.at(10) ) );
            track.setAvgSpeed( str_list.at(11).toDouble() );
            track.setLapLength( str_list.at(12).toDouble() );
            track.setCorners( str_list.at(13).toUInt() );
            track.setGrip( Track::Grip::toGrip( str_list.at(14) ) );
            track.setPitStop( str_list.at(15).toDouble() );
            break;
        }
    }

    track_ = track;
}

void RaceCalculator::updateStintUI(int change_val)
{
    int value = change_val >= 0 && change_val <= 100 ? change_val : 0;

    ui->clear_track_value_text->setText(QString::number(value));
    StintAlgorithms *alg = GproResourceManager::getInstance().getStintAlgorithms();

    double race_temperature_final = weather_.getEstRaceTemperature();
    double race_humidity_final = weather_.getEstRaceHumidity();

    double extra_soft_wear = alg->getTyreWear(track_.getTyreWear(), weather_.getQ2Status(), race_temperature_final, race_humidity_final, TyreType::TTEXTRA_SOFT,
                                              value, driver_.getExperience(), driver_.getAggressiveness(), driver_.getWeight(), car_.getSuspensionLvl(), track_.getSuspension());
    double soft_wear = alg->getTyreWear(track_.getTyreWear(), weather_.getQ2Status(), race_temperature_final, race_humidity_final, TyreType::TTSOFT,
                                        value, driver_.getExperience(), driver_.getAggressiveness(), driver_.getWeight(), car_.getSuspensionLvl(), track_.getSuspension());
    double medium_wear = alg->getTyreWear(track_.getTyreWear(), weather_.getQ2Status(), race_temperature_final, race_humidity_final, TyreType::TTMEDIUM,
                                          value, driver_.getExperience(), driver_.getAggressiveness(), driver_.getWeight(), car_.getSuspensionLvl(), track_.getSuspension());
    double hard_wear = alg->getTyreWear(track_.getTyreWear(), weather_.getQ2Status(), race_temperature_final, race_humidity_final, TyreType::TTHARD,
                                        value, driver_.getExperience(), driver_.getAggressiveness(), driver_.getWeight(), car_.getSuspensionLvl(), track_.getSuspension());
    double rain_wear = alg->getTyreWear(track_.getTyreWear(), weather_.getQ2Status(), race_temperature_final, race_humidity_final, TyreType::TTRAIN,
                                        value, driver_.getExperience(), driver_.getAggressiveness(), driver_.getWeight(), car_.getSuspensionLvl(), track_.getSuspension());

    extra_soft_wear = extra_soft_wear * track_.getLapLength();
    soft_wear = soft_wear * track_.getLapLength();
    medium_wear = medium_wear * track_.getLapLength();
    hard_wear = hard_wear * track_.getLapLength();
    rain_wear = rain_wear * track_.getLapLength();

    ui->extra_soft_max_laps_text->setText( QString::number( std::floor(85 / extra_soft_wear)) + QString("-") + QString::number( std::floor(100 / extra_soft_wear)) );
    ui->soft_max_laps_text->setText( QString::number( std::floor(85 / soft_wear)) + QString("-") + QString::number( std::floor( 100 / soft_wear) ));
    ui->medium_max_laps_text->setText( QString::number( std::floor(85 / medium_wear)) + QString("-") + QString::number( std::floor( 100 / medium_wear ) ));
    ui->hard_max_laps_text->setText( QString::number( std::floor(85 / hard_wear)) + QString("-") + QString::number( std::floor( 100 / hard_wear ) ));
    ui->rain_max_laps_text->setText( QString::number( std::floor(85 / rain_wear)) + QString("-") + QString::number( std::floor( 100 / rain_wear ) ));

    int laps = std::round(track_.getDistance() / track_.getLapLength());

    int es_stops_low = std::floor(laps / (std::floor(85 / extra_soft_wear)));
    int es_stops_high = std::floor(laps / (std::floor(100 / extra_soft_wear)));
    int s_stops_low = std::floor(laps / (std::floor(85 / soft_wear)));
    int s_stops_high = std::floor(laps / (std::floor(100 / soft_wear)));
    int m_stops_low = std::floor(laps / (std::floor(85 / medium_wear)));
    int m_stops_high = std::floor(laps / (std::floor(100 / medium_wear)));
    int h_stops_low = std::floor(laps / (std::floor(85 / hard_wear)));
    int h_stops_high = std::floor(laps / (std::floor(100 / hard_wear)));
    int r_stops_low = std::floor(laps / (std::floor(85 / rain_wear)));
    int r_stops_high = std::floor(laps / (std::floor(100 / rain_wear)));


    double es_optim_laps_low = std::floor((laps * 10.f) / (es_stops_low+1)) / 10.f;
    double s_optim_laps_low = std::floor((laps * 10.f) / (s_stops_low+1)) / 10.f;
    double m_optim_laps_low = std::floor((laps * 10.f) / (m_stops_low+1)) / 10.f;
    double h_optim_laps_low = std::floor((laps * 10.f) / (h_stops_low+1)) / 10.f;
    double r_optim_laps_low = std::floor((laps * 10.f) / (r_stops_low+1)) / 10.f;
    double es_optim_laps_high = std::floor((laps * 10.f) / (es_stops_high+1)) / 10.f;
    double s_optim_laps_high = std::floor((laps * 10.f) / (s_stops_high+1)) / 10.f;
    double m_optim_laps_high = std::floor((laps * 10.f) / (m_stops_high+1)) / 10.f;
    double h_optim_laps_high = std::floor((laps * 10.f) / (h_stops_high+1)) / 10.f;
    double r_optim_laps_high = std::floor((laps * 10.f) / (r_stops_high+1)) / 10.f;

    ui->extra_soft_optim_laps_text->setText(QString::number(es_optim_laps_low) + QString("-") + QString::number( es_optim_laps_high ) );
    ui->soft_optim_laps_text->setText(QString::number(s_optim_laps_low) + QString("-") + QString::number( s_optim_laps_high ) );
    ui->medium_optim_laps_text->setText(QString::number(m_optim_laps_low) + QString("-") + QString::number( m_optim_laps_high ) );
    ui->hard_optim_laps_text->setText(QString::number(h_optim_laps_low) + QString("-") + QString::number( h_optim_laps_high ) );
    ui->rain_optim_laps_text->setText(QString::number(r_optim_laps_low) + QString("-") + QString::number( r_optim_laps_high ) );

    ui->extra_soft_stops_text->setText( QString::number( es_stops_low )  + QString("-") + QString::number( es_stops_high ) );
    ui->soft_stops_text->setText( QString::number( s_stops_low )  + QString("-") + QString::number( s_stops_high ));
    ui->medium_stops_text->setText( QString::number( m_stops_low )  + QString("-") + QString::number( m_stops_high ));
    ui->hard_stops_text->setText( QString::number( h_stops_low )  + QString("-") + QString::number( h_stops_high ));
    ui->rain_stops_text->setText( QString::number( r_stops_low )  + QString("-") + QString::number( r_stops_high ));
}

void RaceCalculator::updateFuelUI()
{
    double laps = ui->fuel_calc_laps->text().size() > 0 ? ui->fuel_calc_laps->text().toDouble() : 1;

    StintAlgorithms *alg = GproResourceManager::getInstance().getStintAlgorithms();

    double fuel_consumption = alg->getFuelConsumption(weather_.getQ2Status(), car_.getEngineLvl(), car_.getElectronicsLvl(),
                                                      track_.getFuelConsumption(), track_.getGrip(), track_.getDownforce());

    double fuel_amount = laps * ( 1 / fuel_consumption ) * track_.getLapLength();

    ui->fuel_amount_text->setText(QString::number( (fuel_amount * 10.0f) / 10.0f));
}

void RaceCalculator::updateFinanceUI()
{
    ui->car_cost_text->setText(QString::number(finance_.getEstCarCost()));
    ui->sponsor_income_text->setText(QString::number(finance_.getSponsorMoney()));
    ui->driver_salary_text->setText(QString::number(finance_.getDriverSalary()));
    ui->staff_salary_text->setText(QString::number(finance_.getStaffSalary()));
    ui->facility_cost_text->setText(QString::number(finance_.getFacilityMaintenence()));
    ui->tyres_cost_text->setText(QString::number(finance_.getTyreCost()));

    ui->total_income_text->setText(QString::number(finance_.getTotalIncome()));
}

void RaceCalculator::on_update_car_button_clicked()
{
    gproServerInterface *gsi = new gproServerInterface();

    QSettings settings(General::ProgramName, General::CompanyName);

    QString username = settings.value(Settings::SafeLoginUsernameText, "").toString();
    QString password = settings.value(Settings::SafeLoginPasswordText, "").toString();
    password = GproResourceManager::getInstance().getSimpleCrypt()->decryptToString(password);
    gsi->gproLogin(username,password);

    Car car = gsi->getCar();

    car_ = car;

    updateCarUI(car_);

    ui->car_wear_widget->setCar(car_);
    ui->car_wear_widget->updateCarUI();
}

void RaceCalculator::on_update_weather_button_clicked()
{
    gproServerInterface *gsi = new gproServerInterface();

    QSettings settings(General::ProgramName, General::CompanyName);

    QString username = settings.value(Settings::SafeLoginUsernameText, "").toString();
    QString password = settings.value(Settings::SafeLoginPasswordText, "").toString();
    password = GproResourceManager::getInstance().getSimpleCrypt()->decryptToString(password);
    gsi->gproLogin(username,password);

    Weather weather = gsi->getWeather();

    weather_ = weather;

    updateWeatherUI(weather_);
}

void RaceCalculator::on_update_track_button_clicked()
{
    gproServerInterface *gsi = new gproServerInterface();

    QSettings settings(General::ProgramName, General::CompanyName);

    QString username = settings.value(Settings::SafeLoginUsernameText, "").toString();
    QString password = settings.value(Settings::SafeLoginPasswordText, "").toString();
    password = GproResourceManager::getInstance().getSimpleCrypt()->decryptToString(password);
    gsi->gproLogin(username,password);

    Track track = gsi->getTrackName();

    loadTrack(track.getName());

    ui->track_name_text->setText(track_.getName());

    settings.setValue(Settings::TrackNameText, track_.getName());
}

void RaceCalculator::on_clear_track_slider_valueChanged(int value)
{
    updateStintUI(value);
    updateEstCarWearUI(value);
    updateTimeUI(value);
    updateFinanceUI();
}

void RaceCalculator::on_fuel_calculate_button_clicked()
{
    updateFuelUI();
}

void RaceCalculator::on_es_stops_edit_field_textChanged(const QString &arg1)
{
    updateTimeUI(ui->clear_track_slider->value());
}

int RaceCalculator::EditFieldInt(const QString &str)
{
    bool ok = false;
    int int_val = str.toInt(&ok);

    if(!ok) {
        QString temp_str = str;
        temp_str = str.left(str.size()-1);
        int ret = temp_str.toInt(&ok);
        if(ok) return ret;
    }

    if(!ok) return 0;

    if(str.size() > 1 && str.at(0) == '0') return str.mid(1).toInt();

    return int_val;
}

void RaceCalculator::on_s_stops_edit_field_textChanged(const QString &arg1)
{
    updateTimeUI(ui->clear_track_slider->value());
}

void RaceCalculator::on_m_stops_edit_field_textChanged(const QString &arg1)
{
    updateTimeUI(ui->clear_track_slider->value());
}

void RaceCalculator::on_h_stops_edit_field_textChanged(const QString &arg1)
{

    updateTimeUI(ui->clear_track_slider->value());

}

void RaceCalculator::on_fuel_calc_laps_textChanged(const QString &arg1)
{
    updateFuelUI();
    updateTimeUI(ui->clear_track_slider->value());
}

void RaceCalculator::on_update_finance_button_clicked()
{
    gproServerInterface *gsi = gproServerInterface::createInstance();

    finance_ = gsi->getNonRaceFinance();

    int value = ui->clear_track_slider->value();

    updateStintUI(value);
    updateEstCarWearUI(value);
    updateTimeUI(value);
    updateFinanceUI();
}

void RaceCalculator::on_clear_track_slider_sliderReleased()
{
    int ct_value = ui->clear_track_slider->value();
    ui->car_wear_widget->setClearTrack(ct_value);
}
