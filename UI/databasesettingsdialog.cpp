#include "databasesettingsdialog.h"
#include "ui_databasesettingsdialog.h"
#include "settingconstants.h"

#include <QSettings>
#include <QDebug>

DatabaseSettingsDialog::DatabaseSettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DatabaseSettingsDialog)
{
    ui->setupUi(this);
}

DatabaseSettingsDialog::~DatabaseSettingsDialog()
{
    delete ui;
}

void DatabaseSettingsDialog::loadSettings()
{
    QSettings settings(General::ProgramName, General::CompanyName);

    qDebug() << "Joku teksti";
    qDebug() << settings.value(Settings::DatabaseTypeText, QVariant("QPSQL")).toString();
    qDebug() << settings.value(Settings::DatabaseNameText, QVariant("")).toString();
    qDebug() << settings.value(Settings::DatabaseUserNameText, QVariant("")).toString();
    qDebug() << settings.value(Settings::DatabasePasswordText, QVariant("")).toString();

    ui->db_type_text->setText(settings.value(Settings::DatabaseTypeText, QVariant("QPSQL")).toString());
    ui->db_name_text->setText(settings.value(Settings::DatabaseNameText, QVariant("")).toString());
    ui->user_name_text->setText(settings.value(Settings::DatabaseUserNameText, QVariant("")).toString());
    ui->password_text->setText(settings.value(Settings::DatabasePasswordText, QVariant("")).toString());
}

void DatabaseSettingsDialog::on_save_button_clicked()
{
    QSettings settings(General::ProgramName, General::CompanyName);

    settings.setValue(Settings::DatabaseTypeText, QVariant(ui->db_type_text->text()));
    settings.setValue(Settings::DatabaseNameText, QVariant(ui->db_name_text->text()));
    settings.setValue(Settings::DatabaseUserNameText, QVariant(ui->user_name_text->text()));
    settings.setValue(Settings::DatabasePasswordText, QVariant(ui->password_text->text()));

    this->close();
}
