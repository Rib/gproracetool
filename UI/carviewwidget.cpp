#include "carviewwidget.h"
#include "ui_carviewwidget.h"

#include <QTableView>
#include <QDebug>

#include "Misc/constantcalculations.h"

CarViewWidget::CarViewWidget(QWidget *parent, QTableView *table) :
    QWidget(parent),
    ui(new Ui::CarViewWidget),
    table_(table),
    car_(new Car),
    previous_car_(0),
    driver_(new Driver),
    track_id_(0),
    test_track_id_(0),
    clear_track_(0)
{
    ui->setupUi(this);

    showSimple(true);

    connect(ui->chassis_est_wear_text, &CustomQLabel::textChanged, this, &CarViewWidget::chassis_est_wear_text_changed);
    connect(ui->engine_est_wear_text, &CustomQLabel::textChanged, this, &CarViewWidget::engine_est_wear_text_changed);
    connect(ui->front_wing_est_wear_text, &CustomQLabel::textChanged, this, &CarViewWidget::front_wing_est_wear_text_changed);
    connect(ui->rear_wing_est_wear_text, &CustomQLabel::textChanged, this, &CarViewWidget::rear_wing_est_wear_text_changed);
    connect(ui->underbody_est_wear_text, &CustomQLabel::textChanged, this, &CarViewWidget::underbody_est_wear_text_changed);
    connect(ui->sidepods_est_wear_text, &CustomQLabel::textChanged, this, &CarViewWidget::sidepods_est_wear_text_changed);
    connect(ui->cooling_est_wear_text, &CustomQLabel::textChanged, this, &CarViewWidget::cooling_est_wear_text_changed);
    connect(ui->gearbox_est_wear_text, &CustomQLabel::textChanged, this, &CarViewWidget::gearbox_est_wear_text_changed);
    connect(ui->brakes_est_wear_text, &CustomQLabel::textChanged, this, &CarViewWidget::brakes_est_wear_text_changed);
    connect(ui->suspension_est_wear_text, &CustomQLabel::textChanged, this, &CarViewWidget::suspension_est_wear_text_changed);
    connect(ui->electronics_est_wear_text, &CustomQLabel::textChanged, this, &CarViewWidget::electronics_est_wear_text_changed);

    connect(ui->total_cost_text, &CustomQLabel::textChanged, this, &CarViewWidget::total_cost_text_changed);

}

CarViewWidget::~CarViewWidget()
{
    delete ui;
}

void CarViewWidget::showSimple(bool simple)
{
    if(simple) {
        this->setVisible(false);

        ui->complex_layout->setVisible(false);

        ui->simple_layout->setVisible(true);

        this->setVisible(true);

    } else {
        this->setVisible(false);

        ui->complex_layout->setVisible(true);

        ui->simple_layout->setVisible(false);

        this->setVisible(true);
    }
}

void CarViewWidget::showRaceCarView(bool rcv)
{
    if(rcv) {
        ui->fetch_current_car_button->setVisible(false);
        ui->sync_previous_car_button->setVisible(false);
        ui->clear_track_v_layout_2->setVisible(false);
        ui->button_v_layout_2->setVisible(false);
    }
}

void CarViewWidget::updateCarUI()
{
    ui->car_power_text->setText(QString::number(car_->getPower()));
    ui->car_handling_text->setText(QString::number(car_->getHandling()));
    ui->car_acceleration_text->setText(QString::number(car_->getAcceleration()));

    ui->chassis_lvl_text->setText(QString::number(car_->getChassisLvl()));
    ui->engine_lvl_text->setText(QString::number(car_->getEngineLvl()));
    ui->front_wing_lvl_text->setText(QString::number(car_->getFrontWingLvl()));
    ui->rear_wing_lvl_text->setText(QString::number(car_->getRearWingLvl()));
    ui->underbody_lvl_text->setText(QString::number(car_->getUnderbodyLvl()));
    ui->sidepods_lvl_text->setText(QString::number(car_->getSidepodsLvl()));
    ui->cooling_lvl_text->setText(QString::number(car_->getCoolingLvl()));
    ui->gearbox_lvl_text->setText(QString::number(car_->getGearboxLvl()));
    ui->brakes_lvl_text->setText(QString::number(car_->getBrakesLvl()));
    ui->suspension_lvl_text->setText(QString::number(car_->getSuspensionLvl()));
    ui->electronics_lvl_text->setText(QString::number(car_->getElectronicsLvl()));

    ui->chassis_wear_text->setText(QString::number(car_->getChassisWear()));
    ui->engine_wear_text->setText(QString::number(car_->getEngineWear()));
    ui->front_wing_wear_text->setText(QString::number(car_->getFrontWingWear()));
    ui->rear_wing_wear_text->setText(QString::number(car_->getRearWingWear()));
    ui->underbody_wear_text->setText(QString::number(car_->getUnderbodyWear()));
    ui->sidepods_wear_text->setText(QString::number(car_->getSidepodsWear()));
    ui->cooling_wear_text->setText(QString::number(car_->getCoolingWear()));
    ui->gearbox_wear_text->setText(QString::number(car_->getGearboxWear()));
    ui->brakes_wear_text->setText(QString::number(car_->getBrakesWear()));
    ui->suspension_wear_text->setText(QString::number(car_->getSuspensionWear()));
    ui->electronics_wear_text->setText(QString::number(car_->getElectronicsWear()));

    ui->cvw_clear_track_slider->setValue(clear_track_);

    on_calc_est_wear_button_clicked();

}

QList<double> CarViewWidget::getEstWears()
{
    if(track_id_ == 0 || driver_ == 0) return QList<double>();

    RaceAlgorithms *ralg = GproResourceManager::getInstance().getRaceAlgorithms();

    QList<double> est_wears = ralg->getEstimatedWears(track_id_, clear_track_, driver_->getConcentration(), driver_->getTalent(), driver_->getExperience(),
                            car_->getChassisLvl(), car_->getEngineLvl(), car_->getFrontWingLvl(), car_->getRearWingLvl(), car_->getUnderbodyLvl(),
                            car_->getSidepodsLvl(), car_->getCoolingLvl(), car_->getGearboxLvl(), car_->getBrakesLvl(), car_->getSuspensionLvl(),
                            car_->getElectronicsLvl());

    return est_wears;
}

double CarViewWidget::getTotalCost()
{
    return ui->total_cost_text->text().toDouble();
}

void CarViewWidget::calculateFinanceUI()
{
    QList<double> est_fixed_wears;

    double chassis_wear = (ui->chassis_est_wear_text->text().toDouble() <= 99 ? ui->chassis_est_wear_text->text().toDouble() : 99) -
            ui->chassis_wear_text->text().toDouble();
    double engine_wear = (ui->engine_est_wear_text->text().toDouble() <= 99 ? ui->engine_est_wear_text->text().toDouble() : 99) -
            ui->engine_wear_text->text().toDouble();
    double front_wing_wear = (ui->front_wing_est_wear_text->text().toDouble() <= 99 ? ui->front_wing_est_wear_text->text().toDouble() : 99) -
            ui->front_wing_wear_text->text().toDouble();
    double rear_wing_wear = (ui->rear_wing_est_wear_text->text().toDouble() <= 99 ? ui->rear_wing_est_wear_text->text().toDouble() : 99) -
            ui->rear_wing_wear_text->text().toDouble();
    double underbody_wear = (ui->underbody_est_wear_text->text().toDouble() <= 99 ? ui->underbody_est_wear_text->text().toDouble() : 99) -
            ui->underbody_wear_text->text().toDouble();
    double sidepods_wear = (ui->sidepods_est_wear_text->text().toDouble() <= 99 ? ui->sidepods_est_wear_text->text().toDouble() : 99) -
            ui->sidepods_wear_text->text().toDouble();
    double cooling_wear = (ui->cooling_est_wear_text->text().toDouble() <= 99 ? ui->cooling_est_wear_text->text().toDouble() : 99) -
            ui->cooling_wear_text->text().toDouble();
    double gearbox_wear = (ui->gearbox_est_wear_text->text().toDouble() <= 99 ? ui->gearbox_est_wear_text->text().toDouble() : 99) -
            ui->gearbox_wear_text->text().toDouble();
    double brakes_wear = (ui->brakes_est_wear_text->text().toDouble() <= 99 ? ui->brakes_est_wear_text->text().toDouble() : 99) -
            ui->brakes_wear_text->text().toDouble();
    double suspension_wear = (ui->suspension_est_wear_text->text().toDouble() <= 99 ? ui->suspension_est_wear_text->text().toDouble() : 99) -
            ui->suspension_wear_text->text().toDouble();
    double electronics_wear = (ui->electronics_est_wear_text->text().toDouble() <= 99 ? ui->electronics_est_wear_text->text().toDouble() : 99) -
            ui->electronics_wear_text->text().toDouble();

    est_fixed_wears.append(chassis_wear);
    est_fixed_wears.append(engine_wear);
    est_fixed_wears.append(front_wing_wear);
    est_fixed_wears.append(rear_wing_wear);
    est_fixed_wears.append(underbody_wear);
    est_fixed_wears.append(sidepods_wear);
    est_fixed_wears.append(cooling_wear);
    est_fixed_wears.append(gearbox_wear);
    est_fixed_wears.append(brakes_wear);
    est_fixed_wears.append(suspension_wear);
    est_fixed_wears.append(electronics_wear);

    QList<double> costs = est_fixed_wears;

    double total_cost = 0;

    costs[0] = ConstantCalculations::getPartPrice(QStringLiteral("Chassis"), car_->getChassisLvl()) * est_fixed_wears.at(0) / 100;
    costs[1] = ConstantCalculations::getPartPrice(QStringLiteral("Engine"), car_->getEngineLvl()) * est_fixed_wears.at(1) / 100;
    costs[2] = ConstantCalculations::getPartPrice(QStringLiteral("FrontWing"), car_->getFrontWingLvl()) * est_fixed_wears.at(2) / 100;
    costs[3] = ConstantCalculations::getPartPrice(QStringLiteral("RearWing"), car_->getRearWingLvl()) * est_fixed_wears.at(3) / 100;
    costs[4] = ConstantCalculations::getPartPrice(QStringLiteral("Underbody"), car_->getUnderbodyLvl()) * est_fixed_wears.at(4) / 100;
    costs[5] = ConstantCalculations::getPartPrice(QStringLiteral("Chassis"), car_->getSidepodsLvl()) * est_fixed_wears.at(5) / 100;
    costs[6] = ConstantCalculations::getPartPrice(QStringLiteral("Sidepods"), car_->getCoolingLvl()) * est_fixed_wears.at(6) / 100;
    costs[7] = ConstantCalculations::getPartPrice(QStringLiteral("Gearbox"), car_->getGearboxLvl()) * est_fixed_wears.at(7) / 100;
    costs[8] = ConstantCalculations::getPartPrice(QStringLiteral("Brakes"), car_->getBrakesLvl()) * est_fixed_wears.at(8) / 100;
    costs[9] = ConstantCalculations::getPartPrice(QStringLiteral("Suspension"), car_->getSuspensionLvl()) * est_fixed_wears.at(9) / 100;
    costs[10] = ConstantCalculations::getPartPrice(QStringLiteral("Electronics"), car_->getElectronicsLvl()) * est_fixed_wears.at(10) / 100;

    foreach(double cost, costs) total_cost += cost;

    ui->chassis_cost_text->setText(QString::number(costs.at(0)));
    ui->engine_cost_text->setText(QString::number(costs.at(1)));
    ui->front_wing_cost_text->setText(QString::number(costs.at(2)));
    ui->rear_wing_cost_text->setText(QString::number(costs.at(3)));
    ui->underbody_cost_text->setText(QString::number(costs.at(4)));
    ui->sidepods_cost_text->setText(QString::number(costs.at(5)));
    ui->cooling_cost_text->setText(QString::number(costs.at(6)));
    ui->gearbox_cost_text->setText(QString::number(costs.at(7)));
    ui->brakes_cost_text->setText(QString::number(costs.at(8)));
    ui->suspension_cost_text->setText(QString::number(costs.at(9)));
    ui->electronics_cost_text->setText(QString::number(costs.at(10)));

    ui->total_cost_text->setText(QString::number(total_cost));
}

void CarViewWidget::on_fetch_current_car_button_clicked()
{
    *car_ = *(GproResourceManager::getInstance().getCar());

    updateCarUI();
}

void CarViewWidget::on_calc_est_wear_button_clicked()
{
    if(track_id_ == 0 || driver_ == 0) return;

    QList<double> est_wears = getEstWears();

    if(est_wears.size() == 11) {

        ui->chassis_est_wear_text->setText(QString::number(std::round(car_->getChassisWear() + est_wears.at(0))));
        ui->engine_est_wear_text->setText(QString::number(std::round(car_->getEngineWear() + est_wears.at(1))));
        ui->front_wing_est_wear_text->setText(QString::number(std::round(car_->getFrontWingWear() + est_wears.at(2))));
        ui->rear_wing_est_wear_text->setText(QString::number(std::round(car_->getRearWingWear() + est_wears.at(3))));
        ui->underbody_est_wear_text->setText(QString::number(std::round(car_->getUnderbodyWear() + est_wears.at(4))));
        ui->sidepods_est_wear_text->setText(QString::number(std::round(car_->getSidepodsWear() + est_wears.at(5))));
        ui->cooling_est_wear_text->setText(QString::number(std::round(car_->getCoolingWear() + est_wears.at(6))));
        ui->gearbox_est_wear_text->setText(QString::number(std::round(car_->getGearboxWear() + est_wears.at(7))));
        ui->brakes_est_wear_text->setText(QString::number(std::round(car_->getBrakesWear() + est_wears.at(8))));
        ui->suspension_est_wear_text->setText(QString::number(std::round(car_->getSuspensionWear() + est_wears.at(9))));
        ui->electronics_est_wear_text->setText(QString::number(std::round(car_->getElectronicsWear() + est_wears.at(10))));

        QList<double> est_fixed_wears;

        est_fixed_wears.append(car_->getChassisWear() + est_wears.at(0) <= 99 ? est_wears.at(0) : 99 - car_->getChassisWear());
        est_fixed_wears.append(car_->getEngineWear() + est_wears.at(1) <= 99 ? est_wears.at(1) : 99 - car_->getEngineWear());
        est_fixed_wears.append(car_->getFrontWingWear() + est_wears.at(2) <= 99 ? est_wears.at(2) : 99 - car_->getFrontWingWear());
        est_fixed_wears.append(car_->getRearWingWear() + est_wears.at(3) <= 99 ? est_wears.at(3) : 99 - car_->getRearWingWear());
        est_fixed_wears.append(car_->getUnderbodyWear() + est_wears.at(4) <= 99 ? est_wears.at(4) : 99 - car_->getUnderbodyWear());
        est_fixed_wears.append(car_->getSidepodsWear() + est_wears.at(5) <= 99 ? est_wears.at(5) : 99 - car_->getSidepodsWear());
        est_fixed_wears.append(car_->getCoolingWear() + est_wears.at(6) <= 99 ? est_wears.at(6) : 99 - car_->getCoolingWear());
        est_fixed_wears.append(car_->getGearboxWear() + est_wears.at(7) <= 99 ? est_wears.at(7) : 99 - car_->getGearboxWear());
        est_fixed_wears.append(car_->getBrakesWear() + est_wears.at(8) <= 99 ? est_wears.at(8) : 99 - car_->getBrakesWear());
        est_fixed_wears.append(car_->getSuspensionWear() + est_wears.at(9) <= 99 ? est_wears.at(9) : 99 - car_->getSuspensionWear());
        est_fixed_wears.append(car_->getElectronicsWear() + est_wears.at(10) <= 99 ? est_wears.at(10) : 99 - car_->getElectronicsWear());

        QList<double> costs = est_fixed_wears;

        double total_cost = 0;

        costs[0] = ConstantCalculations::getPartPrice(QStringLiteral("Chassis"), car_->getChassisLvl()) * est_fixed_wears.at(0) / 100;
        costs[1] = ConstantCalculations::getPartPrice(QStringLiteral("Engine"), car_->getEngineLvl()) * est_fixed_wears.at(1) / 100;
        costs[2] = ConstantCalculations::getPartPrice(QStringLiteral("FrontWing"), car_->getFrontWingLvl()) * est_fixed_wears.at(2) / 100;
        costs[3] = ConstantCalculations::getPartPrice(QStringLiteral("RearWing"), car_->getRearWingLvl()) * est_fixed_wears.at(3) / 100;
        costs[4] = ConstantCalculations::getPartPrice(QStringLiteral("Underbody"), car_->getUnderbodyLvl()) * est_fixed_wears.at(4) / 100;
        costs[5] = ConstantCalculations::getPartPrice(QStringLiteral("Chassis"), car_->getSidepodsLvl()) * est_fixed_wears.at(5) / 100;
        costs[6] = ConstantCalculations::getPartPrice(QStringLiteral("Sidepods"), car_->getCoolingLvl()) * est_fixed_wears.at(6) / 100;
        costs[7] = ConstantCalculations::getPartPrice(QStringLiteral("Gearbox"), car_->getGearboxLvl()) * est_fixed_wears.at(7) / 100;
        costs[8] = ConstantCalculations::getPartPrice(QStringLiteral("Brakes"), car_->getBrakesLvl()) * est_fixed_wears.at(8) / 100;
        costs[9] = ConstantCalculations::getPartPrice(QStringLiteral("Suspension"), car_->getSuspensionLvl()) * est_fixed_wears.at(9) / 100;
        costs[10] = ConstantCalculations::getPartPrice(QStringLiteral("Electronics"), car_->getElectronicsLvl()) * est_fixed_wears.at(10) / 100;

        foreach(double cost, costs) total_cost += cost;

        ui->chassis_cost_text->setText(QString::number(costs.at(0)));
        ui->engine_cost_text->setText(QString::number(costs.at(1)));
        ui->front_wing_cost_text->setText(QString::number(costs.at(2)));
        ui->rear_wing_cost_text->setText(QString::number(costs.at(3)));
        ui->underbody_cost_text->setText(QString::number(costs.at(4)));
        ui->sidepods_cost_text->setText(QString::number(costs.at(5)));
        ui->cooling_cost_text->setText(QString::number(costs.at(6)));
        ui->gearbox_cost_text->setText(QString::number(costs.at(7)));
        ui->brakes_cost_text->setText(QString::number(costs.at(8)));
        ui->suspension_cost_text->setText(QString::number(costs.at(9)));
        ui->electronics_cost_text->setText(QString::number(costs.at(10)));

        ui->total_cost_text->setText(QString::number(total_cost));
    }

}

void CarViewWidget::on_chassis_lvl_text_textChanged(const QString &arg1)
{
    if(arg1.size() == 0) {
        ui->chassis_lvl_text->setText("1");
        car_->setChassis(1,car_->getChassisWear());
        return;
    }

    bool valid;
    int value = QString(arg1.at(arg1.size()-1)).toInt(&valid);

    if(valid) ui->chassis_lvl_text->setText(QString::number(value));
    else {
        int value2 = QString(arg1.at(0)).toInt(&valid);
        if(valid) ui->chassis_lvl_text->setText(QString::number(value2));
        else ui->chassis_lvl_text->setText("1");
    }

    car_->setChassis(ui->chassis_lvl_text->text().toInt(),car_->getChassisWear());

    on_calc_est_wear_button_clicked();
}

void CarViewWidget::on_engine_lvl_text_textChanged(const QString &arg1)
{
    if(arg1.size() == 0) {
        ui->engine_lvl_text->setText("1");
        car_->setEngine(1,car_->getEngineWear());
        return;
    }

    bool valid;
    int value = QString(arg1.at(arg1.size()-1)).toInt(&valid);

    if(valid) ui->engine_lvl_text->setText(QString::number(value));
    else {
        int value2 = QString(arg1.at(0)).toInt(&valid);
        if(valid) ui->engine_lvl_text->setText(QString::number(value2));
        else ui->engine_lvl_text->setText("1");
    }

    car_->setEngine(ui->engine_lvl_text->text().toInt(),car_->getEngineWear());

    on_calc_est_wear_button_clicked();
}

void CarViewWidget::on_front_wing_lvl_text_textChanged(const QString &arg1)
{
    if(arg1.size() == 0) {
        ui->front_wing_lvl_text->setText("1");
        car_->setFrontWing(1,car_->getFrontWingWear());
        return;
    }

    bool valid;
    int value = QString(arg1.at(arg1.size()-1)).toInt(&valid);

    if(valid) ui->front_wing_lvl_text->setText(QString::number(value));
    else {
        int value2 = QString(arg1.at(0)).toInt(&valid);
        if(valid) ui->front_wing_lvl_text->setText(QString::number(value2));
        else ui->front_wing_lvl_text->setText("1");
    }

    car_->setFrontWing(ui->front_wing_lvl_text->text().toInt(),car_->getFrontWingWear());

    on_calc_est_wear_button_clicked();
}

void CarViewWidget::on_rear_wing_lvl_text_textChanged(const QString &arg1)
{
    if(arg1.size() == 0) {
        ui->rear_wing_lvl_text->setText("1");
        car_->setRearWing(1,car_->getRearWingWear());
        return;
    }

    bool valid;
    int value = QString(arg1.at(arg1.size()-1)).toInt(&valid);

    if(valid) ui->rear_wing_lvl_text->setText(QString::number(value));
    else {
        int value2 = QString(arg1.at(0)).toInt(&valid);
        if(valid) ui->rear_wing_lvl_text->setText(QString::number(value2));
        else ui->rear_wing_lvl_text->setText("1");
    }

    car_->setRearWing(ui->rear_wing_lvl_text->text().toInt(),car_->getRearWingWear());

    on_calc_est_wear_button_clicked();
}

void CarViewWidget::on_underbody_lvl_text_textChanged(const QString &arg1)
{
    if(arg1.size() == 0) {
        ui->underbody_lvl_text->setText("1");
        car_->setUnderbody(1,car_->getUnderbodyWear());
        return;
    }

    bool valid;
    int value = QString(arg1.at(arg1.size()-1)).toInt(&valid);

    if(valid) ui->underbody_lvl_text->setText(QString::number(value));
    else {
        int value2 = QString(arg1.at(0)).toInt(&valid);
        if(valid) ui->underbody_lvl_text->setText(QString::number(value2));
        else ui->underbody_lvl_text->setText("1");
    }

    car_->setUnderbody(ui->underbody_lvl_text->text().toInt(),car_->getUnderbodyWear());

    on_calc_est_wear_button_clicked();
}

void CarViewWidget::on_sidepods_lvl_text_textChanged(const QString &arg1)
{
    if(arg1.size() == 0) {
        ui->sidepods_lvl_text->setText("1");
        car_->setSidepods(1,car_->getSidepodsWear());
        return;
    }

    bool valid;
    int value = QString(arg1.at(arg1.size()-1)).toInt(&valid);

    if(valid) ui->sidepods_lvl_text->setText(QString::number(value));
    else {
        int value2 = QString(arg1.at(0)).toInt(&valid);
        if(valid) ui->sidepods_lvl_text->setText(QString::number(value2));
        else ui->sidepods_lvl_text->setText("1");
    }

    car_->setSidepods(ui->sidepods_lvl_text->text().toInt(),car_->getSidepodsWear());

    on_calc_est_wear_button_clicked();
}

void CarViewWidget::on_cooling_lvl_text_textChanged(const QString &arg1)
{
    if(arg1.size() == 0) {
        ui->cooling_lvl_text->setText("1");
        car_->setCooling(1,car_->getCoolingWear());
        return;
    }

    bool valid;
    int value = QString(arg1.at(arg1.size()-1)).toInt(&valid);

    if(valid) ui->cooling_lvl_text->setText(QString::number(value));
    else {
        int value2 = QString(arg1.at(0)).toInt(&valid);
        if(valid) ui->cooling_lvl_text->setText(QString::number(value2));
        else ui->cooling_lvl_text->setText("1");
    }

    car_->setCooling(ui->cooling_lvl_text->text().toInt(),car_->getCoolingWear());

    on_calc_est_wear_button_clicked();
}

void CarViewWidget::on_gearbox_lvl_text_textChanged(const QString &arg1)
{
    if(arg1.size() == 0) {
        ui->gearbox_lvl_text->setText("1");
        car_->setGearbox(1,car_->getGearboxWear());
        return;
    }

    bool valid;
    int value = QString(arg1.at(arg1.size()-1)).toInt(&valid);

    if(valid) ui->gearbox_lvl_text->setText(QString::number(value));
    else {
        int value2 = QString(arg1.at(0)).toInt(&valid);
        if(valid) ui->gearbox_lvl_text->setText(QString::number(value2));
        else ui->gearbox_lvl_text->setText("1");
    }

    car_->setGearbox(ui->gearbox_lvl_text->text().toInt(),car_->getGearboxWear());

    on_calc_est_wear_button_clicked();
}

void CarViewWidget::on_brakes_lvl_text_textChanged(const QString &arg1)
{
    if(arg1.size() == 0) {
        ui->brakes_lvl_text->setText("1");
        car_->setBrakes(1,car_->getBrakesWear());
        return;
    }

    bool valid;
    int value = QString(arg1.at(arg1.size()-1)).toInt(&valid);

    if(valid) ui->brakes_lvl_text->setText(QString::number(value));
    else {
        int value2 = QString(arg1.at(0)).toInt(&valid);
        if(valid) ui->brakes_lvl_text->setText(QString::number(value2));
        else ui->brakes_lvl_text->setText("1");
    }

    car_->setBrakes(ui->brakes_lvl_text->text().toInt(),car_->getBrakesWear());

    on_calc_est_wear_button_clicked();
}

void CarViewWidget::on_suspension_lvl_text_textChanged(const QString &arg1)
{
    if(arg1.size() == 0) {
        ui->suspension_lvl_text->setText("1");
        car_->setSuspension(1,car_->getSuspensionWear());
        return;
    }

    bool valid;
    int value = QString(arg1.at(arg1.size()-1)).toInt(&valid);

    if(valid) ui->suspension_lvl_text->setText(QString::number(value));
    else {
        int value2 = QString(arg1.at(0)).toInt(&valid);
        if(valid) ui->suspension_lvl_text->setText(QString::number(value2));
        else ui->suspension_lvl_text->setText("1");
    }

    car_->setSuspension(ui->suspension_lvl_text->text().toInt(),car_->getSuspensionWear());

    on_calc_est_wear_button_clicked();
}

void CarViewWidget::on_electronics_lvl_text_textChanged(const QString &arg1)
{
    if(arg1.size() == 0) {
        ui->electronics_lvl_text->setText("1");
        car_->setElectronics(1,car_->getElectronicsWear());
        return;
    }

    bool valid;
    int value = QString(arg1.at(arg1.size()-1)).toInt(&valid);

    if(valid) ui->electronics_lvl_text->setText(QString::number(value));
    else {
        int value2 = QString(arg1.at(0)).toInt(&valid);
        if(valid) ui->electronics_lvl_text->setText(QString::number(value2));
        else ui->electronics_lvl_text->setText("1");
    }

    car_->setElectronics(ui->electronics_lvl_text->text().toInt(),car_->getElectronicsWear());

    on_calc_est_wear_button_clicked();
}

void CarViewWidget::on_cvw_clear_track_slider_valueChanged(int value)
{
    clear_track_ = value;
    ui->cvw_clear_track_value_text->setText(QString::number(value));
    on_calc_est_wear_button_clicked();
}

void CarViewWidget::on_chassis_wear_text_textChanged(const QString &arg1)
{
    int value = transformWear(arg1);

    if(value >= 80 && value < 95) {
        ui->chassis_wear_text->setStyleSheet(DefaultStyles::getCarWearEdit80p());
    } else if ( value >= 95 ) {
        ui->chassis_wear_text->setStyleSheet(DefaultStyles::getCarWearEdit95p());
    } else ui->chassis_wear_text->setStyleSheet("");

    ui->chassis_wear_text->setText(QString::number(value));
    car_->setChassis(car_->getChassisLvl(),value);

    on_calc_est_wear_button_clicked();
}

int CarViewWidget::transformWear(const QString &str)
{
    if(str.size() == 0) {
        return 0;
    }

    if(str.size() > 2) {
        bool valid;
        int value = QString(str.at(str.size()-1)).toInt(&valid);

        if(valid) return value;

        QString two_first = str;
        two_first.chop(str.size()-2);

        value = two_first.toInt(&valid);

        if(valid) return value;
        else return 0;
    }

    bool valid;
    int value = str.toInt(&valid);

    if(valid) return value;
    else return 0;


}

void CarViewWidget::on_engine_wear_text_textChanged(const QString &arg1)
{
    int value = transformWear(arg1);

    if(value >= 80 && value < 95) {
        ui->engine_wear_text->setStyleSheet(DefaultStyles::getCarWearEdit80p());
    } else if ( value >= 95 ) {
        ui->engine_wear_text->setStyleSheet(DefaultStyles::getCarWearEdit95p());
    } else ui->engine_wear_text->setStyleSheet("");

    ui->engine_wear_text->setText(QString::number(value));
    car_->setEngine(car_->getEngineLvl(),value);

    on_calc_est_wear_button_clicked();
}

void CarViewWidget::on_front_wing_wear_text_textChanged(const QString &arg1)
{
    int value = transformWear(arg1);

    if(value >= 80 && value < 95) {
        ui->front_wing_wear_text->setStyleSheet(DefaultStyles::getCarWearEdit80p());
    } else if ( value >= 95 ) {
        ui->front_wing_wear_text->setStyleSheet(DefaultStyles::getCarWearEdit95p());
    } else ui->front_wing_wear_text->setStyleSheet("");

    ui->front_wing_wear_text->setText(QString::number(value));
    car_->setFrontWing(car_->getFrontWingLvl(),value);

    on_calc_est_wear_button_clicked();
}

void CarViewWidget::on_rear_wing_wear_text_textChanged(const QString &arg1)
{
    int value = transformWear(arg1);

    if(value >= 80 && value < 95) {
        ui->rear_wing_wear_text->setStyleSheet(DefaultStyles::getCarWearEdit80p());
    } else if ( value >= 95 ) {
        ui->rear_wing_wear_text->setStyleSheet(DefaultStyles::getCarWearEdit95p());
    } else ui->rear_wing_wear_text->setStyleSheet("");

    ui->rear_wing_wear_text->setText(QString::number(value));
    car_->setRearWing(car_->getRearWingLvl(),value);

    on_calc_est_wear_button_clicked();
}

void CarViewWidget::on_underbody_wear_text_textChanged(const QString &arg1)
{
    int value = transformWear(arg1);

    if(value >= 80 && value < 95) {
        ui->underbody_wear_text->setStyleSheet(DefaultStyles::getCarWearEdit80p());
    } else if ( value >= 95 ) {
        ui->underbody_wear_text->setStyleSheet(DefaultStyles::getCarWearEdit95p());
    } else ui->underbody_wear_text->setStyleSheet("");

    ui->underbody_wear_text->setText(QString::number(value));
    car_->setUnderbody(car_->getUnderbodyLvl(),value);

    on_calc_est_wear_button_clicked();
}

void CarViewWidget::on_sidepods_wear_text_textChanged(const QString &arg1)
{
    int value = transformWear(arg1);

    if(value >= 80 && value < 95) {
        ui->sidepods_wear_text->setStyleSheet(DefaultStyles::getCarWearEdit80p());
    } else if ( value >= 95 ) {
        ui->sidepods_wear_text->setStyleSheet(DefaultStyles::getCarWearEdit95p());
    } else ui->sidepods_wear_text->setStyleSheet("");

    ui->sidepods_wear_text->setText(QString::number(value));
    car_->setSidepods(car_->getSidepodsLvl(),value);

    on_calc_est_wear_button_clicked();
}

void CarViewWidget::on_cooling_wear_text_textChanged(const QString &arg1)
{
    int value = transformWear(arg1);

    if(value >= 80 && value < 95) {
        ui->cooling_wear_text->setStyleSheet(DefaultStyles::getCarWearEdit80p());
    } else if ( value >= 95 ) {
        ui->cooling_wear_text->setStyleSheet(DefaultStyles::getCarWearEdit95p());
    } else ui->cooling_wear_text->setStyleSheet("");

    ui->cooling_wear_text->setText(QString::number(value));
    car_->setCooling(car_->getCoolingLvl(),value);

    on_calc_est_wear_button_clicked();
}

void CarViewWidget::on_gearbox_wear_text_textChanged(const QString &arg1)
{
    int value = transformWear(arg1);

    if(value >= 80 && value < 95) {
        ui->gearbox_wear_text->setStyleSheet(DefaultStyles::getCarWearEdit80p());
    } else if ( value >= 95 ) {
        ui->gearbox_wear_text->setStyleSheet(DefaultStyles::getCarWearEdit95p());
    } else ui->gearbox_wear_text->setStyleSheet("");

    ui->gearbox_wear_text->setText(QString::number(value));
    car_->setGearbox(car_->getGearboxLvl(),value);

    on_calc_est_wear_button_clicked();
}

void CarViewWidget::on_brakes_wear_text_textChanged(const QString &arg1)
{
    int value = transformWear(arg1);

    if(value >= 80 && value < 95) {
        ui->brakes_wear_text->setStyleSheet(DefaultStyles::getCarWearEdit80p());
    } else if ( value >= 95 ) {
        ui->brakes_wear_text->setStyleSheet(DefaultStyles::getCarWearEdit95p());
    } else ui->brakes_wear_text->setStyleSheet("");

    ui->brakes_wear_text->setText(QString::number(value));
    car_->setBrakes(car_->getBrakesLvl(),value);

    on_calc_est_wear_button_clicked();
}

void CarViewWidget::on_suspension_wear_text_textChanged(const QString &arg1)
{
    int value = transformWear(arg1);

    if(value >= 80 && value < 95) {
        ui->suspension_wear_text->setStyleSheet(DefaultStyles::getCarWearEdit80p());
    } else if ( value >= 95 ) {
        ui->suspension_wear_text->setStyleSheet(DefaultStyles::getCarWearEdit95p());
    } else ui->suspension_wear_text->setStyleSheet("");

    ui->suspension_wear_text->setText(QString::number(value));
    car_->setSuspension(car_->getSuspensionLvl(),value);

    on_calc_est_wear_button_clicked();
}

void CarViewWidget::on_electronics_wear_text_textChanged(const QString &arg1)
{
    int value = transformWear(arg1);

    if(value >= 80 && value < 95) {
        ui->electronics_wear_text->setStyleSheet(DefaultStyles::getCarWearEdit80p());
    } else if ( value >= 95 ) {
        ui->electronics_wear_text->setStyleSheet(DefaultStyles::getCarWearEdit95p());
    } else ui->electronics_wear_text->setStyleSheet("");

    ui->electronics_wear_text->setText(QString::number(value));
    car_->setElectronics(car_->getElectronicsLvl(),value);

    on_calc_est_wear_button_clicked();
}

void CarViewWidget::on_sync_previous_car_button_clicked()
{
    if(table_ == 0) return;

    SeasonListTableModel *sltm = (SeasonListTableModel*)table_->model();

    int rows = sltm->rowCount();

    int column = 2;
    int row = 0;

    for(int i = 0; i < rows; ++i) {
        QModelIndex index = sltm->index(i, column);

        if(table_->indexWidget(index) == this) {
            row = i;
            break;
        }
    }

    if(row > 0) {
        row--;

        QModelIndex index = sltm->index(row, column);

        if(previous_car_ == 0)
            previous_car_ = ((CarViewWidget*)table_->indexWidget(index))->getCar();

    } else return;

    QList<double> est_wears = ((CarViewWidget*)table_->indexWidget(sltm->index(row, column)))->getEstWears();

    if(est_wears.size() == 11) {
        car_->setChassis(previous_car_->getChassisLvl(), previous_car_->getChassisWear() + est_wears.at(0));
        car_->setEngine(previous_car_->getEngineLvl(), previous_car_->getEngineWear() + est_wears.at(1));
        car_->setFrontWing(previous_car_->getFrontWingLvl(), previous_car_->getFrontWingWear() + est_wears.at(2));
        car_->setRearWing(previous_car_->getRearWingLvl(), previous_car_->getRearWingWear() + est_wears.at(3));
        car_->setUnderbody(previous_car_->getUnderbodyLvl(), previous_car_->getUnderbodyWear() + est_wears.at(4));
        car_->setSidepods(previous_car_->getSidepodsLvl(), previous_car_->getSidepodsWear() + est_wears.at(5));
        car_->setCooling(previous_car_->getCoolingLvl(), previous_car_->getCoolingWear() + est_wears.at(6));
        car_->setGearbox(previous_car_->getGearboxLvl(), previous_car_->getGearboxWear() + est_wears.at(7));
        car_->setBrakes(previous_car_->getBrakesLvl(), previous_car_->getBrakesWear() + est_wears.at(8));
        car_->setSuspension(previous_car_->getSuspensionLvl(), previous_car_->getSuspensionWear() + est_wears.at(9));
        car_->setElectronics(previous_car_->getElectronicsLvl(), previous_car_->getElectronicsWear() + est_wears.at(10));

        updateCarUI();
    }

}

void CarViewWidget::chassis_est_wear_text_changed(const QString &text)
{
    int est_wear_value = text.toInt();

    if(est_wear_value >= 80 && est_wear_value < 95) {
        ui->chassis_est_wear_text->setStyleSheet(DefaultStyles::getCarWearText80p());
    } else if ( est_wear_value >= 95 ) {
        ui->chassis_est_wear_text->setStyleSheet(DefaultStyles::getCarWearText95p());
    } else ui->chassis_est_wear_text->setStyleSheet("");
}

void CarViewWidget::engine_est_wear_text_changed(const QString &text)
{
    int est_wear_value = text.toInt();

    if(est_wear_value >= 80 && est_wear_value < 95) {
        ui->engine_est_wear_text->setStyleSheet(DefaultStyles::getCarWearText80p());
    } else if ( est_wear_value >= 95 ) {
        ui->engine_est_wear_text->setStyleSheet(DefaultStyles::getCarWearText95p());
    } else ui->engine_est_wear_text->setStyleSheet("");
}

void CarViewWidget::front_wing_est_wear_text_changed(const QString &text)
{
    int est_wear_value = text.toInt();

    if(est_wear_value >= 80 && est_wear_value < 95) {
        ui->front_wing_est_wear_text->setStyleSheet(DefaultStyles::getCarWearText80p());
    } else if ( est_wear_value >= 95 ) {
        ui->front_wing_est_wear_text->setStyleSheet(DefaultStyles::getCarWearText95p());
    } else ui->front_wing_est_wear_text->setStyleSheet("");
}

void CarViewWidget::rear_wing_est_wear_text_changed(const QString &text)
{
    int est_wear_value = text.toInt();

    if(est_wear_value >= 80 && est_wear_value < 95) {
        ui->rear_wing_est_wear_text->setStyleSheet(DefaultStyles::getCarWearText80p());
    } else if ( est_wear_value >= 95 ) {
        ui->rear_wing_est_wear_text->setStyleSheet(DefaultStyles::getCarWearText95p());
    } else ui->rear_wing_est_wear_text->setStyleSheet("");
}

void CarViewWidget::underbody_est_wear_text_changed(const QString &text)
{
    int est_wear_value = text.toInt();

    if(est_wear_value >= 80 && est_wear_value < 95) {
        ui->underbody_est_wear_text->setStyleSheet(DefaultStyles::getCarWearText80p());
    } else if ( est_wear_value >= 95 ) {
        ui->underbody_est_wear_text->setStyleSheet(DefaultStyles::getCarWearText95p());
    } else ui->underbody_est_wear_text->setStyleSheet("");
}

void CarViewWidget::sidepods_est_wear_text_changed(const QString &text)
{
    int est_wear_value = text.toInt();

    if(est_wear_value >= 80 && est_wear_value < 95) {
        ui->sidepods_est_wear_text->setStyleSheet(DefaultStyles::getCarWearText80p());
    } else if ( est_wear_value >= 95 ) {
        ui->sidepods_est_wear_text->setStyleSheet(DefaultStyles::getCarWearText95p());
    } else ui->sidepods_est_wear_text->setStyleSheet("");
}

void CarViewWidget::cooling_est_wear_text_changed(const QString &text)
{
    int est_wear_value = text.toInt();

    if(est_wear_value >= 80 && est_wear_value < 95) {
        ui->cooling_est_wear_text->setStyleSheet(DefaultStyles::getCarWearText80p());
    } else if ( est_wear_value >= 95 ) {
        ui->cooling_est_wear_text->setStyleSheet(DefaultStyles::getCarWearText95p());
    } else ui->cooling_est_wear_text->setStyleSheet("");
}

void CarViewWidget::gearbox_est_wear_text_changed(const QString &text)
{
    int est_wear_value = text.toInt();

    if(est_wear_value >= 80 && est_wear_value < 95) {
        ui->gearbox_est_wear_text->setStyleSheet(DefaultStyles::getCarWearText80p());
    } else if ( est_wear_value >= 95 ) {
        ui->gearbox_est_wear_text->setStyleSheet(DefaultStyles::getCarWearText95p());
    } else ui->gearbox_est_wear_text->setStyleSheet("");
}

void CarViewWidget::brakes_est_wear_text_changed(const QString &text)
{
    int est_wear_value = text.toInt();

    if(est_wear_value >= 80 && est_wear_value < 95) {
        ui->brakes_est_wear_text->setStyleSheet(DefaultStyles::getCarWearText80p());
    } else if ( est_wear_value >= 95 ) {
        ui->brakes_est_wear_text->setStyleSheet(DefaultStyles::getCarWearText95p());
    } else ui->brakes_est_wear_text->setStyleSheet("");
}

void CarViewWidget::suspension_est_wear_text_changed(const QString &text)
{
    int est_wear_value = text.toInt();

    if(est_wear_value >= 80 && est_wear_value < 95) {
        ui->suspension_est_wear_text->setStyleSheet(DefaultStyles::getCarWearText80p());
    } else if ( est_wear_value >= 95 ) {
        ui->suspension_est_wear_text->setStyleSheet(DefaultStyles::getCarWearText95p());
    } else ui->suspension_est_wear_text->setStyleSheet("");
}

void CarViewWidget::electronics_est_wear_text_changed(const QString &text)
{
    int est_wear_value = text.toInt();

    if(est_wear_value >= 80 && est_wear_value < 95) {
        ui->electronics_est_wear_text->setStyleSheet(DefaultStyles::getCarWearText80p());
    } else if ( est_wear_value >= 95 ) {
        ui->electronics_est_wear_text->setStyleSheet(DefaultStyles::getCarWearText95p());
    } else ui->electronics_est_wear_text->setStyleSheet("");
}

void CarViewWidget::on_test_checkbox_toggled(bool checked)
{

    if(test_track_id_ == 0 || driver_ == 0) return;

    RaceAlgorithms *ralg = GproResourceManager::getInstance().getRaceAlgorithms();

    QList<double> est_wears = ralg->getEstimatedWears(test_track_id_, 0, driver_->getConcentration(), driver_->getTalent(), driver_->getExperience(),
                            car_->getChassisLvl(), car_->getEngineLvl(), car_->getFrontWingLvl(), car_->getRearWingLvl(), car_->getUnderbodyLvl(),
                            car_->getSidepodsLvl(), car_->getCoolingLvl(), car_->getGearboxLvl(), car_->getBrakesLvl(), car_->getSuspensionLvl(),
                            car_->getElectronicsLvl());

    for(double &wear : est_wears) {
        wear *= 0.8;
    }

    if(checked) {
        QList<double> real_est_wears = getEstWears();

        for(int i = 0; i < est_wears.size() && i < real_est_wears.size(); ++i) {
            est_wears[i] += real_est_wears.at(i);
        }

        if(est_wears.size() == 11) {
            ui->chassis_est_wear_text->setText(QString::number( est_wears.at(0) ));
            ui->engine_est_wear_text->setText(QString::number( est_wears.at(1) ));
            ui->front_wing_est_wear_text->setText(QString::number( est_wears.at(2) ));
            ui->rear_wing_est_wear_text->setText(QString::number( est_wears.at(3) ));
            ui->underbody_est_wear_text->setText(QString::number( est_wears.at(4) ));
            ui->sidepods_est_wear_text->setText(QString::number( est_wears.at(5) ));
            ui->cooling_est_wear_text->setText(QString::number( est_wears.at(6) ));
            ui->gearbox_est_wear_text->setText(QString::number( est_wears.at(7) ));
            ui->brakes_est_wear_text->setText(QString::number( est_wears.at(8) ));
            ui->suspension_est_wear_text->setText(QString::number( est_wears.at(9) ));
            ui->electronics_est_wear_text->setText(QString::number( est_wears.at(10) ));
        }
    } else {
        QList<double> real_est_wears = getEstWears();

        if(real_est_wears.size() == 11) {
            ui->chassis_est_wear_text->setText(QString::number( real_est_wears.at(0) ));
            ui->engine_est_wear_text->setText(QString::number( real_est_wears.at(1) ));
            ui->front_wing_est_wear_text->setText(QString::number( real_est_wears.at(2) ));
            ui->rear_wing_est_wear_text->setText(QString::number( real_est_wears.at(3) ));
            ui->underbody_est_wear_text->setText(QString::number( real_est_wears.at(4) ));
            ui->sidepods_est_wear_text->setText(QString::number( real_est_wears.at(5) ));
            ui->cooling_est_wear_text->setText(QString::number( real_est_wears.at(6) ));
            ui->gearbox_est_wear_text->setText(QString::number( real_est_wears.at(7) ));
            ui->brakes_est_wear_text->setText(QString::number( real_est_wears.at(8) ));
            ui->suspension_est_wear_text->setText(QString::number( real_est_wears.at(9) ));
            ui->electronics_est_wear_text->setText(QString::number( real_est_wears.at(10) ));
        }
    }

    calculateFinanceUI();

}
