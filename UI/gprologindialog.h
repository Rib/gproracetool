#ifndef GPROLOGINDIALOG_H
#define GPROLOGINDIALOG_H

#include <memory>

#include <QDialog>

#include "Algorithms/simplecrypt.h"
#include "Misc/gproresourcemanager.h"

using std::shared_ptr;

namespace Ui {
class GproLoginDialog;
}

class GproLoginDialog : public QDialog
{
    Q_OBJECT

public:
    explicit GproLoginDialog(QWidget *parent = 0);
    ~GproLoginDialog();

private slots:
    void on_safe_login_save_button_clicked();

    void on_login_save_button_clicked();

private:
    Ui::GproLoginDialog *ui;

};

#endif // GPROLOGINDIALOG_H
