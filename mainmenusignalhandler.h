#ifndef MAINMENUSIGNALHANDLER_H
#define MAINMENUSIGNALHANDLER_H

#include <QObject>

class MainMenuSignalHandler : public QObject
{
    Q_OBJECT
public:
    explicit MainMenuSignalHandler(QObject *parent = 0);

signals:

public slots:

};

#endif // MAINMENUSIGNALHANDLER_H
