#ifndef GPROSERVERINTERFACE_H
#define GPROSERVERINTERFACE_H

#include <QtNetwork/QNetworkReply>
#include <QNetworkRequest>
#include <QNetworkCookieJar>
#include <QStringList>
#include <QRegularExpressionMatch>
#include <QSettings>

#include "Objects/weather.h"
#include "Objects/driver.h"
#include "Objects/car.h"
#include "Objects/track.h"
#include "Objects/race.h"
#include "Objects/practicelap.h"
#include "Objects/lap.h"
#include "Objects/finance.h"
#include "Handlers/gprocookiemanager.h"
#include "Misc/gproresourcemanager.h"
#include "settingconstants.h"


class gproServerInterface : public QObject {
    Q_OBJECT

    private:
        QStringList current_list_;
        QList<QRegularExpressionMatch*> matches_;
        gproCookieManager *cookie_manager_;

        QStringList parseReply(QNetworkReply *rep);

    public:
        gproServerInterface(QObject *parent = 0);

        bool gproLogin(QString login_name, QString password);

        Weather getWeather();
        Weather getTestWeather();
        QString getTestTrackName();
        Driver getDriver();
        Car getCar();
        Track getTrackName();
        Race getRace(QString header);

        QStringList getCurrentList();

        QStringList getOldRacesHeaders();

        QStringList getSeasonTracks();

        Finance getNonRaceFinance();

        Track getTrack(int gpro_track_id, bool debug = false);
        QList<Track> getAllTracks();

        // creates login instance of gproServerInterface
        static gproServerInterface *createInstance(bool non_safe_login = false) {
            gproServerInterface *gsi = new gproServerInterface();

            QSettings settings(General::ProgramName, General::CompanyName);

            if (!non_safe_login) {
                QString username = settings.value(Settings::SafeLoginUsernameText, "").toString();
                QString password = settings.value(Settings::SafeLoginPasswordText, "").toString();
                password = GproResourceManager::getInstance().getSimpleCrypt()->decryptToString(password);
                gsi->gproLogin(username,password);
            } else {
                QString username = settings.value(Settings::LoginUsernameText, "").toString();
                QString password = settings.value(Settings::LoginPasswordText, "").toString();
                password = GproResourceManager::getInstance().getSimpleCrypt()->decryptToString(password);
                gsi->gproLogin(username,password);
            }

            return gsi;
        }



    private slots:
        void replyFinished(QNetworkReply *rep);
        void weatherOnFetch(QNetworkReply *rep);
};

#endif // GPROSERVERINTERFACE_H
