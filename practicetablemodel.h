#ifndef PRACTICETABLEMODEL_H
#define PRACTICETABLEMODEL_H

#include <QAbstractTableModel>
#include <QString>
#include <QStringList>

#include <array>
#include <vector>

#include <practicetablecell.h>

using std::array;
using std::vector;

class practiceTableModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit practiceTableModel(QObject *parent = 0);
    practiceTableModel(QStringList data, QObject *parent = 0);

    inline int columnCount(const QModelIndex &parent) const { if (parent.isValid()) return 6; return 6;}

    inline int rowCount(const QModelIndex &parent) const { if (parent.isValid())  return data_.size(); return data_.size(); }

    inline QVariant data(const QModelIndex &index, int role) const {
        if (role == Qt::DisplayRole && false) {
            //return cells_.at(index.row()).at(index.column());
            return data_.at(index.row()).at(index.column());
        }
        else return QVariant();
    }

    inline QVariant headerData(int section, Qt::Orientation orientation, int role) const {
        if (section >= 0 && section < 6 && Qt::Horizontal == orientation && role == Qt::DisplayRole)
            return headers_.at(section);
        else return QVariant();
    }

    inline bool addRow(array<double,6> row, array<int,6> comments, array<double,6> ranges, const QModelIndex &parent = QModelIndex()) {
        try {
            QAbstractTableModel::beginInsertRows(parent,data_.size(),data_.size());
            array<practiceTableCell*,6> cell_row;
            for(int i = 0; i < 6; ++i) {
                cell_row.at(i) = new practiceTableCell();
                cell_row.at(i)->setValues(row.at(i),comments.at(i), ranges.at(i));
            }

            cells_.push_back(cell_row);
            data_.push_back(row);
            comments_.push_back(comments);
            QAbstractTableModel::endInsertRows();
        } catch(...) {
            return false;
        }

        return true;
    }

    bool insertRows(int row, int count, const QModelIndex &parent);

    inline bool reset() {
        try {
            QAbstractTableModel::beginResetModel();
            cells_.clear();
            data_.clear();
            comments_.clear();
            QAbstractTableModel::endResetModel();
        } catch(...) {
            return false;
        }\

        return true;
    }

    inline practiceTableCell* getCellWidget(int column, int row) {
        try {
            return cells_.at(row).at(column);
        } catch(...) {
            return new practiceTableCell;
        }
    }

    inline void setDefaultRange(double range) { default_range_ = range; }

    array<double,6> calculateRange(const array<double, 6> &data, const array<int, 6> &comment) const;

    inline vector< array<double,6> > getRawData() { return data_; }
    inline vector< array<int,6> > getComments() { return comments_; }
    inline double getDefaultRange() { return default_range_; }

    QStringList serialize();

signals:

public slots:

private:
    vector< array<practiceTableCell*,6> > cells_;
    vector< array<double,6> > data_;
    vector< array<int,6> > comments_;
    double default_range_;
    array<QString,6> headers_;

    inline void init_headers() { headers_ = {{QString("Front Wing"), QString("Rear Wing"), QString("Engine"),
                                             QString("Brakes"), QString("Gear"), QString("Suspension")}}; }

    vector< array<double,6> > calculateRanges() const;
};

#endif // PRACTICETABLEMODEL_H
