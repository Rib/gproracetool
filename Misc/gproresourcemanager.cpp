#include "gproresourcemanager.h"

#include <QSettings>

#include "settingconstants.h"
#include "Database/databasequeryhelper.h"

GproResourceManager::GproResourceManager(QObject *parent) : QObject(parent),
    db_handler_(new databasehandler),
    stint_algorithms_(new StintAlgorithms),
    race_algorithms_(new RaceAlgorithms),
    practice_algorithms_(new PracticeAlgorithms),
    car_(new Car),
    driver_(new Driver),
    track_(new Track),
    weather_(new Weather),
    crypto_(new SimpleCrypt(Q_UINT64_C(0x9f4c5fba35e56c3b)))
{
    QSettings settings(General::ProgramName, General::CompanyName);

    car_->setPower(settings.value(Settings::CarPowerText,"0").toInt());
    car_->setHandling(settings.value(Settings::CarHandlingText,"0").toInt());
    car_->setAcceleration(settings.value(Settings::CarAccelerationText,"0").toInt());
    car_->setChassis(settings.value(Settings::CarChassisLvlText,"0").toInt(),
                    settings.value(Settings::CarChassisWearText,"0").toInt());
    car_->setEngine(settings.value(Settings::CarEngineLvlText,"0").toInt(),
                    settings.value(Settings::CarEngineWearText,"0").toInt());
    car_->setFrontWing(settings.value(Settings::CarFrontWingLvlText,"0").toInt(),
                    settings.value(Settings::CarFrontWingWearText,"0").toInt());
    car_->setRearWing(settings.value(Settings::CarRearWingLvlText,"0").toInt(),
                    settings.value(Settings::CarRearWingWearText,"0").toInt());
    car_->setUnderbody(settings.value(Settings::CarUnderbodyLvlText,"0").toInt(),
                    settings.value(Settings::CarUnderbodyWearText,"0").toInt());
    car_->setSidepods(settings.value(Settings::CarSidepodsLvlText,"0").toInt(),
                    settings.value(Settings::CarSidepodsWearText,"0").toInt());
    car_->setCooling(settings.value(Settings::CarCoolingLvlText,"0").toInt(),
                    settings.value(Settings::CarCoolingWearText,"0").toInt());
    car_->setGearbox(settings.value(Settings::CarGearboxLvlText,"0").toInt(),
                    settings.value(Settings::CarGearboxWearText,"0").toInt());
    car_->setBrakes(settings.value(Settings::CarBrakesLvlText,"0").toInt(),
                    settings.value(Settings::CarBrakesWearText,"0").toInt());
    car_->setSuspension(settings.value(Settings::CarSuspensionLvlText,"0").toInt(),
                    settings.value(Settings::CarSuspensionWearText,"0").toInt());
    car_->setElectronics(settings.value(Settings::CarElectronicsLvlText,"0").toInt(),
                    settings.value(Settings::CarElectronicsWearText,"0").toInt());

    loadDriver();

    shared_ptr<Track> track = shared_ptr<Track>(new Track);

    track->setName(settings.value(Settings::TrackNameText, "").toString());

    QList< QStringList > tracks = getDatabaseHandler()->getTracks();

    foreach( QStringList str_list, tracks) {
        if(str_list.size() > 0 && str_list.at(0) == track->getName()) {
            track->setID( str_list.at(1).toInt() );
            track->setDistance( str_list.at(2).toDouble() );
            track->setPower( str_list.at(3).toInt() );
            track->setHandling( str_list.at(4).toInt() );
            track->setAcceleration( str_list.at(5).toInt() );
            track->setDownforce( Track::Downforce::toDownforce( str_list.at(6) ) );
            track->setOvertaking( Track::Overtaking::toOvertaking( str_list.at(7) ) );
            track->setSuspension( Track::Suspension::toSuspension( str_list.at(8) ) );
            track->setFuelConsumption( Track::FuelConsumption::toFuelConsumption( str_list.at(9) ) );
            track->setTyreWear( Track::TyreWear::toTyreWear( str_list.at(10) ) );
            track->setAvgSpeed( str_list.at(11).toDouble() );
            track->setLapLength( str_list.at(12).toDouble() );
            track->setCorners( str_list.at(13).toUInt() );
            track->setGrip( Track::Grip::toGrip( str_list.at(14) ) );
            track->setPitStop( str_list.at(15).toDouble() );
            break;
        }
    }

    track_ = track;

    loadWeather();

    shared_ptr<mat> practice_lap_data = db_handler_->getFullPracticeLapData();
    QStringList practice_lap_data_labels = db_handler_->getFullPracticeLapDataLabels();

    practice_algorithms_->setPracticeLapData(practice_lap_data, practice_lap_data_labels);

    auto xd_pointer = bind(&PracticeAlgorithms::getSuggestedSettings, practice_algorithms_,0,0,0,0,0,0,0,0,0,0,0,0,0);

    suggested_settings_future_ = QtConcurrent::run(xd_pointer);

    stint_algorithms_->setStintData(db_handler_->getStintData(false));
    stint_algorithms_->setStintDataLabels(db_handler_->getStintDataLabels());
}

void GproResourceManager::loadWeather()
{
    QSettings settings(General::ProgramName, General::CompanyName);

    weather_->setQ1Temperature(settings.value(Settings::WeatherQ1Temperature,"0").toInt());
    weather_->setQ1Humidity(settings.value(Settings::WeatherQ1Humidity, "0").toInt());

    weather_->setQ2Temperature(settings.value(Settings::WeatherQ2Temperature,"0").toInt());
    weather_->setQ2Humidity(settings.value(Settings::WeatherQ2Humidity, "0").toInt());

    weather_->setRaceQ1Temperature(settings.value(Settings::WeatherRaceQ1TemperatureLowText,"0").toInt(),
                                  settings.value(Settings::WeatherRaceQ1TemperatureHighText,"0").toInt());
    weather_->setRaceQ1Humidity(settings.value(Settings::WeatherRaceQ1HumidityLowText,"0").toInt(),
                                  settings.value(Settings::WeatherRaceQ1HumidityHighText,"0").toInt());
    weather_->setRaceQ1RainProbability(settings.value(Settings::WeatherRaceQ1RProbabilityLowText,"0").toInt(),
                                  settings.value(Settings::WeatherRaceQ1RProbabilityHighText,"0").toInt());

    weather_->setRaceQ2Temperature(settings.value(Settings::WeatherRaceQ2TemperatureLowText,"0").toInt(),
                                  settings.value(Settings::WeatherRaceQ2TemperatureHighText,"0").toInt());
    weather_->setRaceQ2Humidity(settings.value(Settings::WeatherRaceQ2HumidityLowText,"0").toInt(),
                                  settings.value(Settings::WeatherRaceQ2HumidityHighText,"0").toInt());
    weather_->setRaceQ2RainProbability(settings.value(Settings::WeatherRaceQ2RProbabilityLowText,"0").toInt(),
                                  settings.value(Settings::WeatherRaceQ2RProbabilityHighText,"0").toInt());

    weather_->setRaceQ3Temperature(settings.value(Settings::WeatherRaceQ3TemperatureLowText,"0").toInt(),
                                  settings.value(Settings::WeatherRaceQ3TemperatureHighText,"0").toInt());
    weather_->setRaceQ3Humidity(settings.value(Settings::WeatherRaceQ3HumidityLowText,"0").toInt(),
                                  settings.value(Settings::WeatherRaceQ3HumidityHighText,"0").toInt());
    weather_->setRaceQ3RainProbability(settings.value(Settings::WeatherRaceQ3RProbabilityLowText,"0").toInt(),
                                  settings.value(Settings::WeatherRaceQ3RProbabilityHighText,"0").toInt());

    weather_->setRaceQ4Temperature(settings.value(Settings::WeatherRaceQ4TemperatureLowText,"0").toInt(),
                                  settings.value(Settings::WeatherRaceQ4TemperatureHighText,"0").toInt());
    weather_->setRaceQ4Humidity(settings.value(Settings::WeatherRaceQ4HumidityLowText,"0").toInt(),
                                  settings.value(Settings::WeatherRaceQ4HumidityHighText,"0").toInt());
    weather_->setRaceQ4RainProbability(settings.value(Settings::WeatherRaceQ4RProbabilityLowText,"0").toInt(),
                                       settings.value(Settings::WeatherRaceQ4RProbabilityHighText,"0").toInt());
}

void GproResourceManager::saveWeather(const Weather &weather)
{
    QSettings settings(General::ProgramName, General::CompanyName);

    settings.setValue(Settings::WeatherQ1Temperature, weather.getQ1Temperature());
    settings.setValue(Settings::WeatherQ1Humidity, weather.getQ1Humidity());
    settings.setValue(Settings::WeatherQ2Temperature, weather.getQ2Temperature());
    settings.setValue(Settings::WeatherQ2Humidity, weather.getQ2Humidity());

    settings.setValue(Settings::WeatherRaceQ1TemperatureLowText, weather.getRaceQ1TemperatureLow());
    settings.setValue(Settings::WeatherRaceQ1TemperatureHighText, weather.getRaceQ1TemperatureHigh());
    settings.setValue(Settings::WeatherRaceQ1HumidityLowText, weather.getRaceQ1HumidityLow());
    settings.setValue(Settings::WeatherRaceQ1HumidityHighText, weather.getRaceQ1HumidityHigh());
    settings.setValue(Settings::WeatherRaceQ1RProbabilityLowText, weather.getRaceQ1RainProbabilityLow());
    settings.setValue(Settings::WeatherRaceQ1RProbabilityHighText, weather.getRaceQ1RainProbabilityHigh());

    settings.setValue(Settings::WeatherRaceQ2TemperatureLowText, weather.getRaceQ2TemperatureLow());
    settings.setValue(Settings::WeatherRaceQ2TemperatureHighText, weather.getRaceQ2TemperatureHigh());
    settings.setValue(Settings::WeatherRaceQ2HumidityLowText, weather.getRaceQ2HumidityLow());
    settings.setValue(Settings::WeatherRaceQ2HumidityHighText, weather.getRaceQ2HumidityHigh());
    settings.setValue(Settings::WeatherRaceQ2RProbabilityLowText, weather.getRaceQ2RainProbabilityLow());
    settings.setValue(Settings::WeatherRaceQ2RProbabilityHighText, weather.getRaceQ2RainProbabilityHigh());

    settings.setValue(Settings::WeatherRaceQ3TemperatureLowText, weather.getRaceQ3TemperatureLow());
    settings.setValue(Settings::WeatherRaceQ3TemperatureHighText, weather.getRaceQ3TemperatureHigh());
    settings.setValue(Settings::WeatherRaceQ3HumidityLowText, weather.getRaceQ3HumidityLow());
    settings.setValue(Settings::WeatherRaceQ3HumidityHighText, weather.getRaceQ3HumidityHigh());
    settings.setValue(Settings::WeatherRaceQ3RProbabilityLowText, weather.getRaceQ3RainProbabilityLow());
    settings.setValue(Settings::WeatherRaceQ3RProbabilityHighText, weather.getRaceQ3RainProbabilityHigh());

    settings.setValue(Settings::WeatherRaceQ4TemperatureLowText, weather.getRaceQ4TemperatureLow());
    settings.setValue(Settings::WeatherRaceQ4TemperatureHighText, weather.getRaceQ4TemperatureHigh());
    settings.setValue(Settings::WeatherRaceQ4HumidityLowText, weather.getRaceQ4HumidityLow());
    settings.setValue(Settings::WeatherRaceQ4HumidityHighText, weather.getRaceQ4HumidityHigh());
    settings.setValue(Settings::WeatherRaceQ4RProbabilityLowText, weather.getRaceQ4RainProbabilityLow());
    settings.setValue(Settings::WeatherRaceQ4RProbabilityHighText, weather.getRaceQ4RainProbabilityHigh());
}

void GproResourceManager::loadDriver()
{
    if(driver_ == 0) driver_ = shared_ptr<Driver>(new Driver);

    QSettings settings(General::ProgramName, General::CompanyName);

    driver_->setName(settings.value(Settings::DriverNameText, "").toString());
    driver_->setOverall(settings.value(Settings::DriverOverallText,"0").toInt());
    driver_->setConcentration(settings.value(Settings::DriverConcentrationText,"0").toInt());
    driver_->setTalent(settings.value(Settings::DriverTalentText,"0").toInt());
    driver_->setAggressiveness(settings.value(Settings::DriverAggressivenessText,"0").toInt());
    driver_->setExperience(settings.value(Settings::DriverExperienceText,"0").toInt());
    driver_->setTechnicalInsight(settings.value(Settings::DriverTechnicalInsightText,"0").toInt());
    driver_->setStamina(settings.value(Settings::DriverStaminaText,"0").toInt());
    driver_->setCharisma(settings.value(Settings::DriverCharismaText,"0").toInt());
    driver_->setMotivation(settings.value(Settings::DriverMotivationText,"0").toInt());
    driver_->setWeight(settings.value(Settings::DriverWeightText,"0").toInt());
    driver_->setAge(settings.value(Settings::DriverAgeText,"0").toInt());
}

void GproResourceManager::saveDriver(const Driver &driver)
{
    QSettings settings(General::ProgramName, General::CompanyName);

    settings.setValue(Settings::DriverNameText, driver.getName());
    settings.setValue(Settings::DriverOverallText,driver.getOverall());
    settings.setValue(Settings::DriverConcentrationText,driver.getConcentration());
    settings.setValue(Settings::DriverTalentText,driver.getTalent());
    settings.setValue(Settings::DriverAggressivenessText,driver.getAggressiveness());
    settings.setValue(Settings::DriverExperienceText,driver.getExperience());
    settings.setValue(Settings::DriverTechnicalInsightText,driver.getTechnicalInsight());
    settings.setValue(Settings::DriverStaminaText,driver.getStamina());
    settings.setValue(Settings::DriverCharismaText,driver.getCharisma());
    settings.setValue(Settings::DriverMotivationText,driver.getMotivation());
    settings.setValue(Settings::DriverWeightText,driver.getWeight());
    settings.setValue(Settings::DriverAgeText,driver.getAge());
}
