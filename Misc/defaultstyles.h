#ifndef DEFAULTSTYLES_H
#define DEFAULTSTYLES_H

#include <QString>

class DefaultStyles
{
private:
    static const QString car_wear_edit_80p;
    static const QString car_wear_edit_95p;
    static const QString car_wear_text_80p;
    static const QString car_wear_text_95p;
public:
    DefaultStyles();

    static const QString &getCarWearEdit80p () { return car_wear_edit_80p; }
    static const QString &getCarWearEdit95p () { return car_wear_edit_95p; }
    static const QString &getCarWearText80p () { return car_wear_text_80p; }
    static const QString &getCarWearText95p () { return car_wear_text_95p; }
};

#endif // DEFAULTSTYLES_H
