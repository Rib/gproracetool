#include "constantcalculations.h"

ConstantCalculations::ConstantCalculations()
{

}

int ConstantCalculations::getPartPrice(const QString &part, int lvl)
{
    int price = 0;
    int power_val = lvl-1;

    if(power_val < 0) return 0;

    if(part.contains(QStringLiteral("Chassis"))) price = 1292539;
    else if(part.contains(QStringLiteral("Engine"))) price = 3311737;
    else if(part.contains(QStringLiteral("FrontWing"))) price = 1551354;
    else if(part.contains(QStringLiteral("RearWing"))) price = 1504126;
    else if(part.contains(QStringLiteral("Underbody"))) price = 510128;
    else if(part.contains(QStringLiteral("Sidepods"))) price = 459831;
    else if(part.contains(QStringLiteral("Cooling"))) price = 454545;
    else if(part.contains(QStringLiteral("Gearbox"))) price = 3098104;
    else if(part.contains(QStringLiteral("Brakes"))) price = 697674;
    else if(part.contains(QStringLiteral("Suspension"))) price = 1181545;
    else if(part.contains(QStringLiteral("Electronics"))) price = 938416;

    return std::round(std::pow(1.2385,power_val)*price);
}
