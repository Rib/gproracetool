#include "defaultstyles.h"

const QString DefaultStyles::car_wear_edit_80p = QStringLiteral("QLineEdit { background-color: rgb(209, 209, 0); }"
                                                                "QLineEdit:hover { background-color: rgb(0, 255, 127); }");
const QString DefaultStyles::car_wear_edit_95p = QStringLiteral("QLineEdit { background-color: red; }"
                                                                "QLineEdit:hover { background-color: rgb(0, 255, 127); }");
const QString DefaultStyles::car_wear_text_80p = QStringLiteral(" QLabel { color: rgb(209, 209, 0); } ");
const QString DefaultStyles::car_wear_text_95p = QStringLiteral(" QLabel { color: red; } ");

DefaultStyles::DefaultStyles()
{
}
