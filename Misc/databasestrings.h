#ifndef DATABASESTRINGS_H
#define DATABASESTRINGS_H

#include <QString>

class DatabaseStrings
{
public:
    DatabaseStrings();

    static QString getTrackFields();

    static QString getCarFields();

    static QString getCarWearFields();

    static QString getDriverFields();

    static QString getStintFields();

    static QString getMiscDataFields();

    static QString getPracticeLapFields();

    static QString getRiskFields();

    static QString getStintQuery(int track_id, int season);

    static QString getMiscDataQuery(int track_id = 0, int season = 0);

    static QString getFullRaceDataQuery();

    static QString getFullPracticeLapQuery();

    static QString getCTTimeDataQuery();

    static QString getFuelTimeDataQuery();

    static const QString &getTyreDiffDataQuery();

    static QString getPracticeDataQuery();

    static QString getRaceDataQuery();

    static QString getStintDataQuery();

    static QString getPracticeTableQuery();

    static QString getTrackTableQuery();
};

#endif // DATABASESTRINGS_H
