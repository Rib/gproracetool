#include "databasestrings.h"

#include <QDebug>

#include "Database/databasetablecreatehelper.h"

DatabaseStrings::DatabaseStrings()
{

}

QString DatabaseStrings::getTrackFields()
{
    QString fields = QStringLiteral(" Track.Name AS TrackName, ") +
            QStringLiteral("Track._id AS TrackID, ") +
            QStringLiteral(" Track.Distance AS TDistance, ") +
            QStringLiteral(" Track.Power AS TPower, ") +
            QStringLiteral(" Track.Handling AS THandling, ") +
            QStringLiteral(" Track.Acceleration AS TAcceleration, ") +
            QStringLiteral(" Track.Downforce AS TDownforce, ") +
            QStringLiteral(" Track.Overtaking AS TOvertaking, ") +
            QStringLiteral(" Track.Suspension AS TSuspension, ") +
            QStringLiteral(" Track.FuelConsumption AS TFuelConsumption, ") +
            QStringLiteral(" Track.TyreWear AS TTyreWear, ") +
            QStringLiteral(" Track.AvgSpeed AS TAvgSpeed, ") +
            QStringLiteral(" Track.LapLength AS TLapLenght, ") +
            QStringLiteral(" Track.Corners AS TCorners, ") +
            QStringLiteral(" Track.Grip AS TGrip, ") +
            QStringLiteral(" Track.PitStop AS TPitStop ");

    return fields;
}

QString DatabaseStrings::getCarFields()
{
    return QStringLiteral("Car.Power AS CPower, "
                             "Car.Handling AS CHandling, "
                             "Car.Acceleration AS CAcceleration, "
                             "Car.Chassis AS CChassis, "
                             "Car.Engine AS CEngine, "
                             "Car.FrontWing AS CFrontWing, "
                             "Car.RearWing AS CRearWing, "
                             "Car.Underbody AS CUnderbody,"
                             "Car.Sidepods AS CSidepods, "
                             "Car.Cooling AS CCooling, "
                             "Car.Gearbox AS CGearbox, "
                             "Car.Brakes AS CBrakes, "
                             "Car.Suspension AS CSuspension,"
                             "Car.Electronics AS CElectronics ");
}

QString DatabaseStrings::getCarWearFields()
{
    return QStringLiteral("CarWear.ChassisWear AS CWChassis,"
                             "CarWear.EngineWear AS CWEngine,"
                             "CarWear.FrontWingWear AS CWFrontWing,"
                             "CarWear.RearWingWear AS CWRearWing,"
                             "CarWear.UnderbodyWear AS CWUnderbody,"
                             "CarWear.SidepodsWear AS CWSidepods,"
                             "CarWear.CoolingWear AS CWCooling,"
                             "CarWear.GearboxWear AS CWGearbox,"
                             "CarWear.BrakesWear AS CWBrakes,"
                             "CarWear.SuspensionWear AS CWSuspension,"
                             "CarWear.ElectronicsWear AS CWElectronics ");
}

QString DatabaseStrings::getDriverFields()
{
    return QStringLiteral("Driver.Overall AS DOverall,"
                             "Driver.Concentration AS DConcentration,"
                             "Driver.Talent AS DTalent,"
                             "Driver.Aggresiveness AS DAggressiveness,"
                             "Driver.Experience AS DExperience,"
                             "Driver.TechInsight AS DTechInsight,"
                             "Driver.Stamina AS DStamina,"
                             "Driver.Charisma AS DCharisma,"
                             "Driver.Motivation AS DMotivation,"
                             "Driver.Weight AS DWeight,"
                             "Driver.Age AS DAge,"
                             "Driver.Reputation AS DReputation ");
}

QString DatabaseStrings::getStintFields()
{
    return QStringLiteral("Stint.Laps AS SLaps, Stint.Fuel AS SFuel, "
                             "Stint.Consumption AS SConsumption, Stint.FinalP AS SFinalP, "
                             "Stint.AvgHumidity AS SHumidity, Stint.AvgTemperature AS STemperature, "
                             "Stint.TyreType AS STyreType, Stint.Weather AS SWeather,"
                             "Stint.Km AS SKm, Stint.StartLap AS SStartLap, "
                             "Stint.PitStop AS SPitStop, Stint.StartFuel AS SStartFuel ");
}

QString DatabaseStrings::getMiscDataFields()
{
    return QStringLiteral("MiscRaceData.Q1Time,"
                             "MiscRaceData.Q2Time,"
                             "MiscRaceData.Season, "
                             "MiscRaceData.track_id ");
}

QString DatabaseStrings::getPracticeLapFields()
{
    QString practice_lap_string = QStringLiteral("PracticeLap.LapNumber, ") +
            QStringLiteral( "PracticeLap.LapTime, ") +
            QStringLiteral("PracticeLap.DriverMistake, ") +
            QStringLiteral("PracticeLap.NetTime, ") +
            QStringLiteral("PracticeLap.FrontWing,") +
            QStringLiteral("PracticeLap.RearWing, ") +
            QStringLiteral("PracticeLap.Engine, ") +
            QStringLiteral("PracticeLap.Brakes, ") +
            QStringLiteral("PracticeLap.Gear,") +
            QStringLiteral("PracticeLap.Suspension,") +
            QStringLiteral("PracticeLap.Tyres,") +
            QStringLiteral("PracticeLap.WingComment, ") +
            QStringLiteral("PracticeLap.EngineComment,") +
            QStringLiteral("PracticeLap.BrakesComment, ") +
            QStringLiteral("PracticeLap.GearComment, ") +
            QStringLiteral("PracticeLap.SuspensionComment, ") +
            QStringLiteral("PracticeLap.track_id, PracticeLap.Season, ") +
            QStringLiteral("PracticeLap._id ");

    return practice_lap_string;
}

QString DatabaseStrings::getRiskFields()
{
    return QStringLiteral("Risk.Clear AS RClear, "
                          "Risk.Wet AS RWet, "
                          "Risk.Defend AS RDefend,"
                          "Risk.Malfunction AS RMalfunction, "
                          "Risk.Overtake AS ROvertake, "
                          "Risk.StartRisk AS RStartRisk ");
}

QString DatabaseStrings::getStintQuery(int track_id, int season)
{
    QString select = "SELECT ";
    QString stint_fields = getStintFields() + QStringLiteral(", ");
    QString race_fields = QStringLiteral("Stint.Season, Stint.track_id ");
    QString from = QStringLiteral("FROM ") +
            QStringLiteral("Stint ");

    if (track_id != 0 || season != 0) {
        QString where = "WHERE ";
        where += track_id == 0 ? QStringLiteral("") : QStringLiteral("Stint.track_id = ") + QString::number(track_id);
        if(track_id != 0) where += QStringLiteral(" AND ");
        where += season == 0 ? QStringLiteral("") : QStringLiteral(" Stint.Season = ") + QString::number(season);

        from += where;
    }

    return select + stint_fields + race_fields + from;
}

QString DatabaseStrings::getMiscDataQuery(int track_id, int season)
{
    QString select = "SELECT ";
    QString misc_fields = getMiscDataFields();
    QString from = QStringLiteral("FROM ") +
            QStringLiteral("MiscRaceData ");

    if (track_id != 0 || season != 0) {
        QString where = "WHERE ";
        where += track_id == 0 ? QStringLiteral("") : QStringLiteral("MiscRaceData.track_id = ") + QString::number(track_id);
        if(track_id != 0) where += QStringLiteral(" AND ");
        where += season == 0 ? QStringLiteral("") : QStringLiteral(" MiscRaceData.Season = ") + QString::number(season);

        from += where;
    }

    return select + misc_fields + from;
}

QString DatabaseStrings::getFullRaceDataQuery()
{
    QString select = "SELECT ";
    QString race_fields = QStringLiteral("Race.Season") + QStringLiteral(", ");
    QString car_fields = getCarFields() + QStringLiteral(", ");
    QString car_wear_fields = getCarWearFields() + QStringLiteral(", ");
    QString driver_fields = getDriverFields() + QStringLiteral(", ");
    QString risk_fields = QStringLiteral("Risk.Clear AS RClear,"
                                  "Risk.Wet AS RWet ") + QStringLiteral(", ");
    QString track_fields = getTrackFields() + QStringLiteral(", ");
    QString misc_fields = QStringLiteral("MiscRaceData.Q1Time,"
                                  "MiscRaceData.Q2Time, ");
    QString stint_fields = QStringLiteral("Stint.Laps AS SLaps, "
                                   "Stint.AvgHumidity AS SHumidity, "
                                   "Stint.AvgTemperature AS STemperature,"
                                   "Stint.Weather AS SWeather ");

    QString from = QStringLiteral("FROM Race JOIN Track ON Race.track_id = track._id "
                                  "JOIN Car USING(Season, track_id) "
                                  "JOIN CarWear USING(Season, track_id) "
                                  "JOIN Driver USING(Season, track_id) "
                                  "JOIN MiscRaceData USING(Season, track_id) "
                                  "JOIN Risk USING(Season, track_id) "
                                  "JOIN Stint USING(Season, track_id) ");

    QString where = QStringLiteral("WHERE Stint.StartFuel = -1;");

    return select + race_fields + car_fields + car_wear_fields + driver_fields +
            risk_fields + track_fields + misc_fields + stint_fields + from + where;
}

QString DatabaseStrings::getFullPracticeLapQuery()
{
    QString select = QStringLiteral("SELECT ");
    QString practice_lap_fields = getPracticeLapFields() + QStringLiteral(", ");
    QString track_fields = getTrackFields() + QStringLiteral(", ");
    QString car_lvl_fields = getCarFields() + QStringLiteral(", ");
    QString driver_fields = getDriverFields() + QStringLiteral(", ");
    QString weather_fields = QStringLiteral("Weather.Q1Status, "
                                     "Weather.Q1Temperature, "
                                     "Weather.Q1Humidity ");
    QString from = QStringLiteral("FROM Race "
                                  "JOIN Track ON Race.track_id = Track._id "
                                  "JOIN PracticeLap USING(Season, track_id)"
                                  "JOIN Car USING(Season, track_id)"
                                  "JOIN Driver USING(Season, track_id)"
                                  "JOIN Weather USING(Season, track_id);");

    return select +
            practice_lap_fields + track_fields + car_lvl_fields + driver_fields +
            weather_fields + from;
}

QString DatabaseStrings::getCTTimeDataQuery()
{
    QString select = QStringLiteral("SELECT ") +
            QStringLiteral("  Race.Season, "
                    "Race.track_id, Track.LapLength AS TLapLength,"
                    "Stint.AvgTemperature AS STemperature, Stint.AvgHumidity AS SHumidity, "
                    "Stint.Weather AS SWeather, Driver.Stamina AS DStamina, "
                    "Stint.StartFuel AS SStartFuel, Stint.Fuel AS SFuel,"
                    "Car.Power AS CPower, Car.Handling AS CHandling,"
                    "Car.Acceleration AS CAcceleration, Track.Power AS TPower,"
                    "Track.Handling AS THandling,"
                    "Track.Acceleration AS TAcceleration, Driver.Concentration AS DConcentration,"
                    "Driver.Talent AS DTalent, Driver.Aggresiveness AS DAggressiveness, "
                    "Driver.Experience AS DExperience, Driver.Motivation AS DMotivation, Driver.Weight AS DWeight,"
                    "Lap.LapTime, Lap.Events, "
                    "case when Stint.Weather = 'Dry' then Risk.Clear else Risk.Wet end AS ClearTrack, Stint._id AS STID ");
    QString from = QStringLiteral("FROM Race JOIN Track ON Race.track_id = Track._id "
                                  "JOIN Lap USING(Season, track_id) "
                                  "JOIN Stint USING(Season, track_id) "
                                  "JOIN Driver USING(Season, track_id) "
                                  "JOIN Car USING(Season, track_id) "
                                  "JOIN Risk USING(Season, track_id) ");

    QString where = QStringLiteral("WHERE Stint.FinalP >= 0 AND "
                                   "Lap.Lap >= Stint.StartLap  AND "
                                   "Lap.Lap < (Stint.StartLap + Stint.Laps) AND "
                                   "(Lap.Events is null OR Lap.Events = '' OR Lap.Events = '-') AND "
                                   "Stint.Weather NOT LIKE 'Mixed'");

    return select + from + where;
}

QString DatabaseStrings::getFuelTimeDataQuery()
{
    QString select = QStringLiteral("SELECT ") +
            QStringLiteral("Track.LapLength AS TLapLength, MiscRaceData.Q1Time, MiscRaceData.Q2Time, "
                    "Weather.Q1Status, Weather.Q1Temperature, Weather.Q1Humidity, "
                    "Weather.Q2Status, Weather.Q2Temperature, Weather.Q2Humidity, "
                    "Race.Season, Race.track_id, Driver.Stamina AS DStamina, Stint.StartFuel AS SStartFuel ");

    QString from = QStringLiteral("FROM Race JOIN Track ON Race.track_id = Track._id "
                                  "JOIN MiscRaceData USING(Season, track_id) "
                                  "JOIN Weather USING(Season, track_id) "
                                  "JOIN Driver USING(Season, track_id) "
                                  "JOIN Stint USING(Season, track_id) ");

    QString where = QStringLiteral("WHERE Stint.StartLap = 1 AND Stint.StartFuel > 0;");

    return select + from + where;
}

const QString &DatabaseStrings::getTyreDiffDataQuery()
{
    static QString select = QStringLiteral("SELECT ") +
            QStringLiteral("SoftData.Season AS Season, SoftData.track_id AS track_id, SoftData.Q1Temperature as Q1Temperature, "
                           "SoftData.Q1Humidity AS Q1Humidity, SoftData.TCorners AS TCorners, SoftData.TLapLength AS TLapLength, "
                           "SoftData.NetTime AS SoftTime,  ESData.NetTime AS ESTime ");
    static QString from = QStringLiteral("FROM ");
    static QString s_select = QStringLiteral("(SELECT Race.Season, Race.track_id, Weather.Q1Temperature, Weather.Q1Humidity, "
                                "Track.Corners AS TCorners, Track.LapLength AS TLapLength, PracticeLap.FrontWing, "
                                "PracticeLap.RearWing, PracticeLap.Engine, PracticeLap.Brakes, PracticeLap.Gear, "
                                "PracticeLap.Suspension, PracticeLap.Tyres, PracticeLap.NetTime ");
    static QString s_from = QStringLiteral("FROM Race JOIN Track ON Race.track_id = Track._id "
                                    "JOIN PracticeLap USING(Season, track_id) "
                                    "JOIN Weather USING(Season, track_id) ");
    static QString s_where = QStringLiteral("WHERE (PracticeLap.Tyres LIKE 'Soft') AND Weather.Q1Status LIKE 'Dry') AS SoftData, ");
    static QString es_select = QStringLiteral("(SELECT Race.Season, Race.track_id, Weather.Q1Temperature, Weather.Q1Humidity, "
                                "Track.Corners AS TCorners, Track.LapLength AS TLapLength, PracticeLap.FrontWing, "
                                "PracticeLap.RearWing, PracticeLap.Engine, PracticeLap.Brakes, PracticeLap.Gear, PracticeLap.Suspension, "
                                "PracticeLap.Tyres, PracticeLap.NetTime ");
    static QString es_from = QStringLiteral("FROM Race JOIN Track ON Race.track_id = Track._id "
                                     "JOIN PracticeLap USING(Season, track_id) "
                                     "JOIN Weather USING(Season, track_id) ");
    static QString es_where = QStringLiteral("WHERE (PracticeLap.Tyres LIKE 'Extra Soft') AND Weather.Q1Status LIKE 'Dry') AS ESData ");
    static QString where = QStringLiteral("WHERE SoftData.NetTime > 0 AND ESData.NetTime > 0 AND SoftData.Season = ESData.Season AND "
                            "SoftData.track_id = ESData.track_id AND SoftData.FrontWing = ESData.FrontWing AND "
                            "SoftData.RearWing = ESData.RearWing AND SoftData.Engine = ESData.Engine AND "
                            "SoftData.Brakes = ESData.Brakes AND SoftData.Gear = ESData.Gear AND SoftData.Suspension = ESData.Suspension;");

    static QString query_str = select + from + s_select + s_from + s_where + es_select + es_from + es_where + where;
    return query_str;
}

QString DatabaseStrings::getPracticeDataQuery()
{
    QString select = QStringLiteral("SELECT ") +
            QStringLiteral("PracticeLap.NetTime AS PNetTime, PracticeLap.FrontWing AS PFrontWing, PracticeLap.RearWing AS PRearWing, "
                    "PracticeLap.Engine AS PEngine, PracticeLap.Brakes AS PBrakes, PracticeLap.Gear AS PGear, "
                    "PracticeLap.Suspension AS PSuspension, PracticeLap.Tyres AS PTyreType, Weather.Q1Status AS PWeather, "
                    "Weather.Q1Temperature AS PTemperature, Weather.Q1Humidity AS PHumidity, Track.Name AS TrackName, "
                    "Race.track_id AS TrackID, Track.Distance AS TDistance, Track.Power AS TPower, Track.Handling AS THandling, "
                    "Track.Acceleration AS TAcceleration, Track.Downforce AS TDownforce, Track.Overtaking AS TOvertaking, "
                    "Track.Suspension AS TSuspension, Track.FuelConsumption AS TFuelConsumption, Track.TyreWear AS TTyreWear, "
                    "Track.AvgSpeed AS TAvgSpeed, Track.LapLength AS TLapLength, Track.Corners AS TCorners, "
                    "Track.Grip AS TGrip, Track.PitStop AS TPitStop, Car.Power AS CPower, Car.Handling AS CHandling, "
                    "Car.Acceleration AS CAcceleration, Car.Chassis AS CChassis, Car.Engine AS CEngine, "
                    "Car.FrontWing AS CFrontWing, Car.RearWing AS CRearWing, Car.Underbody AS CUnderbody, "
                    "Car.Sidepods AS CSidepods, Car.Cooling AS CCooling, Car.Gearbox AS CGearbox, Car.Brakes AS CBrakes, "
                    "Car.Suspension AS CSuspension, Car.Electronics AS CElectronics, Driver.Overall AS DOverall, "
                    "Driver.Concentration AS DConcentration, Driver.Talent AS DTalent, Driver.Aggresiveness AS DAggressiveness, "
                    "Driver.Experience AS DExperience, Driver.TechInsight AS DTechInsight, Driver.Stamina AS DStamina, "
                    "Driver.Charisma AS DCharisma, Driver.Motivation AS DMotivation, Driver.Weight AS DWeight, "
                    "Driver.Reputation AS DReputation, Driver.Age AS DAge, PracticeLap.WingComment, PracticeLap.EngineComment, "
                    "PracticeLap.BrakesComment, PracticeLap.GearComment, PracticeLap.SuspensionComment ");

    QString from = QStringLiteral("FROM Race JOIN Track ON Race.track_id = Track._id "
                                  "JOIN PracticeLap USING(Season, track_id) "
                                  "JOIN Weather USING(Season, track_id) "
                                  "JOIN Car USING(Season, track_id) "
                                  "JOIN Driver USING(Season, track_id); ");

    return select + from;
}

QString DatabaseStrings::getRaceDataQuery()
{
    QString select = QStringLiteral("SELECT ");
    QString track_fields = getTrackFields() + QStringLiteral(", ");
    QString car_lvl_fields = getCarFields() + QStringLiteral(", ");
    QString car_wear_fields = getCarWearFields() + QStringLiteral(", ");
    QString driver_fields = getDriverFields() + QStringLiteral(", ");
    QString misc_fields = QStringLiteral("Race.Season, "
                                  "Stint.AvgHumidity AS SHumidity, "
                                  "Stint.AvgTemperature AS STemperature, "
                                  "Stint.Weather AS SWeather ");

    QString from = QStringLiteral("FROM Race "
                                  "JOIN Track ON Race.track_id = Track._id "
                                  "JOIN Stint USING(Season, track_id)"
                                  "JOIN Car USING(Season, track_id)"
                                  "JOIN CarWear USING(Season, track_id)"
                                  "JOIN Driver USING(Season, track_id)"
                                  "JOIN Weather USING(Season, track_id) ");

    QString where = QStringLiteral("WHERE Stint.FinalP = -1;");

    return select +
            track_fields + car_lvl_fields + car_wear_fields + driver_fields + misc_fields +
            from + where;
}

QString DatabaseStrings::getStintDataQuery()
{
    static QString select = QStringLiteral("SELECT ");
    static QString track_fields = getTrackFields() + QStringLiteral(", ");
    static QString car_lvl_fields = getCarFields() + QStringLiteral(", ");
    static QString car_wear_fields = getCarWearFields() + QStringLiteral(", ");
    static QString driver_fields = getDriverFields() + QStringLiteral(", ");
    static QString risk_fields = getRiskFields() + QStringLiteral(", ");
    static QString stint_fields = getStintFields();
    static QString from = QStringLiteral("FROM Race JOIN Track ON Race.track_id = Track._id "
                                  "JOIN Stint USING(Season, track_id) "
                                  "JOIN Car USING(Season, track_id) "
                                  "JOIN CarWear USING(Season, track_id) "
                                  "JOIN Risk USING(Season, track_id) "
                                  "JOIN Driver USING(Season, track_id);");

    return select + track_fields + car_lvl_fields + car_wear_fields + driver_fields + risk_fields + stint_fields + from;
}

QString DatabaseStrings::getPracticeTableQuery()
{
    static QString select = QStringLiteral("SELECT ");
    static QString practice_fields = getPracticeLapFields();
    static QString from = QStringLiteral(" FROM PracticeLap;");

    return select + practice_fields + from;
}

QString DatabaseStrings::getTrackTableQuery()
{
    static QString select = QStringLiteral("SELECT ");
    static QString track_fields = getTrackFields();
    static QString from = QStringLiteral("FROM ") +
            DatabaseTableCreateHelper::TABLE_TRACK + QStringLiteral(";");

    return select + track_fields + from;

}
