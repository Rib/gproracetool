#ifndef THREADRESOURCEMANAGER_H
#define THREADRESOURCEMANAGER_H


#include <QMutex>

class ThreadResourceManager
{
public:
    ThreadResourceManager();

    static ThreadResourceManager& getInstance() {
        static ThreadResourceManager instance;

        return instance;
    }

    QMutex &getDBMutex() { return db_mutex_; }

private:
    QMutex db_mutex_;
};

#endif // THREADRESOURCEMANAGER_H
