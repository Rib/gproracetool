#ifndef CONSTANTCALCULATIONS_H
#define CONSTANTCALCULATIONS_H

#include <QString>

class ConstantCalculations
{
public:
    ConstantCalculations();

    static int getPartPrice(const QString &part, int lvl);
};

#endif // CONSTANTCALCULATIONS_H
