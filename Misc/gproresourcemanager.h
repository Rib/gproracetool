#ifndef GPRORESOURCEMANAGER_H
#define GPRORESOURCEMANAGER_H

#include <memory>
#include <functional>

#include <QtConcurrent/QtConcurrentRun>
#include <QFuture>
#include <QMutex>

#include "Handlers/databasehandler.h"
#include "Algorithms/stintalgorithms.h"
#include "Algorithms/racealgorithms.h"
#include "Algorithms/practicealgorithms.h"
#include "Algorithms/simplecrypt.h"

#include "Objects/car.h"
#include "Objects/driver.h"
#include "Objects/weather.h"
#include "Objects/practicelap.h"

using std::shared_ptr;
using std::bind;

class GproResourceManager : public QObject
{
    Q_OBJECT

private:
    databasehandler *db_handler_;

    StintAlgorithms *stint_algorithms_;

    RaceAlgorithms *race_algorithms_;

    PracticeAlgorithms *practice_algorithms_;

    shared_ptr<Car> car_;

    shared_ptr<Driver> driver_;

    shared_ptr<Track> track_;

    shared_ptr<Weather> weather_;

    shared_ptr<SimpleCrypt> crypto_;

    QFuture< array<int,6> > suggested_settings_future_;

    ~GproResourceManager() {
        delete(stint_algorithms_);
        stint_algorithms_ = 0;
        delete(race_algorithms_);
        race_algorithms_ = 0;
        delete(practice_algorithms_);
        practice_algorithms_ = 0;
    }

    GproResourceManager(GproResourceManager const&) = delete;
    void operator=(GproResourceManager const&)  = delete;

public:
    explicit GproResourceManager(QObject *parent = 0);

    static GproResourceManager& getInstance() {
        static GproResourceManager instance;

        return instance;
    }

    databasehandler *getDatabaseHandler() {
        return db_handler_;
    }
    StintAlgorithms *getStintAlgorithms() { return stint_algorithms_; }
    RaceAlgorithms *getRaceAlgorithms() { return race_algorithms_; }
    PracticeAlgorithms *getPracticeAlgorithms() { return practice_algorithms_; }

    shared_ptr<Car> getCar() { return car_; }
    shared_ptr<Driver> getDriver() { return driver_; }
    shared_ptr<Track> getTrack() { return track_; }
    shared_ptr<Weather> getWeather() { return weather_; }
    shared_ptr<SimpleCrypt> getSimpleCrypt() { return crypto_; }

    QFuture< array<int,6> > getSSF() { return suggested_settings_future_; }

    void loadWeather();
    void saveWeather(const Weather &weather);

    void loadDriver();
    void saveDriver(const Driver &driver);



signals:

public slots:
};

#endif // GPRORESOURCEMANAGER_H
