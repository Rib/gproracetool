#include "mainwindow.h"

#include <QApplication>

#include "Database/databasetablecreatehelper.h"

int main(int argc, char *argv[]){
    // creates the database if it doesn't already exist
    DatabaseTableCreateHelper::createDatabase();

    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
